import flash.display.Sprite;
import flash.Lib;

class Test extends Sprite {
    static function main() {
        trace("Haxe is great!");
        Lib.current.addChild(new Test());
    }
    
    public function new() {
        super();
        
        var g = graphics;
        var color = 0x008020;
        for (j in 0 ... 10) {
            for (i in 0 ... 100) {
                var x = i + Math.random() * 10;
                var y = j * 10 + Math.random() * 10;
                if (Math.random() < .2) {
                    var colorIndex = Std.int(Math.random() * 3);
                    if (colorIndex == 0) {
                        color = 0x10ee00;
                    } else if (colorIndex == 1) {
                        color = 0x20ff00;
                    } else if (colorIndex == 2) {
                        color = 0x00cc00;
                    }
                }
                g.beginFill(color);
                g.moveTo(x - 5 * Math.random(), y);
                g.lineTo(x + 5 * Math.random(), y);
                g.lineTo(x + 10 * Math.random(), y + 10 - 30 * Math.random());
                g.lineTo(x + 5 * Math.random(), y);
                g.lineTo(x + 10 * Math.random(), y + 10 - 30 * Math.random());
                g.lineTo(x - 5 * Math.random(), y);
                g.lineTo(x - 10 * Math.random(), y + 10 - 30 * Math.random());
                g.endFill();
            }
        }
    }
}
