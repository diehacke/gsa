﻿var gsa = "file:///Z|/home/user/gsa/";
var exportPNG = function(path) {
	fl.openDocument(path);
	var doc = fl.getDocumentDOM();
	var timeline = doc.timelines[doc.currentTimeline];
	timeline.currentLayer = 0;
	for (i = 0; i < timeline.frameCount; i++) {
		timeline.currentFrame = i;
		var layer = timeline.layers[timeline.currentLayer];
		var frame = layer.frames[timeline.currentFrame];
		if (frame.name)
			doc.exportPNG(gsa + "assets/surface_" + frame.name + ".png", true, true);
	}
}
exportPNG(gsa + "assets-raw/surfaces_512x512.fla");
exportPNG(gsa + "assets-raw/surfaces_1024x1024.fla");
