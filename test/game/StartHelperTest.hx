package game;

import massive.munit.Assert;
import flash.display.DisplayObject;

class StartHelperTest {
	@Test
	public function normal() {
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("http://www.emanueleferonato.com/downloads/sitelock.swf"));
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("https://www.emanueleferonato.com/downloads/sitelock.swf"));
	}

	@Test
	public function incorrect() {
		Assert.areEqual(
			null,
			StartHelper.domainOfUrl("ftp://www.emanueleferonato.com/downloads/sitelock.swf"));
	}

	@Test
	public function custom() {
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("http://www.emanueleferonato.com/downloads"));
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("http://www.emanueleferonato.com"));
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("http://emanueleferonato.com"));
	}

	@Test
	public function custom2() {
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("http://subdomain.emanueleferonato.com/downloads"));
		Assert.areEqual(
			"emanueleferonato.com",
			StartHelper.domainOfUrl("http://gtw-02.office4.emanueleferonato.com"));
		Assert.areEqual(
			"narod.ru",
			StartHelper.domainOfUrl("http://nt021.narod.ru/test0.swf"));
	}

	private var _text:String;
	private var loaderInfo:{url:String};

	private function addChild(child:DisplayObject):Void {
	}

	private function check0() {
		StartHelper.check(["narod.ru", "site.com"], {
			_text += "a";
			_text += "b";
			_text += "c";
		});
	}

	@Test
	public function checkTest0() {
		loaderInfo = {url:"http://nt021.narod.ru/test0.swf"};
		_text = "";
		check0();
		Assert.areEqual("abc", _text);
	}

	@Test
	public function checkTest1() {
		loaderInfo = {url:"https://www.site.com/test1.swf"};
		_text = "";
		check0();
		Assert.areEqual("abc", _text);
	}

	@Test
	public function checkTest2() {
		loaderInfo.url = "http://www.site2.com/test1.swf";
		_text = "";
		check0();
		Assert.areEqual("", _text);
	}

	private function check1() {
		StartHelper.check(["a.ru", "b.com"], {
			_text += "1";
			_text += "2";
			_text += "3";
			_text += "4";
		});
	}

	@Test
	public function checkTest3() {
		loaderInfo = {url:"http://a.ru"};
		_text = "";
		check1();
		Assert.areEqual("1234", _text);

		loaderInfo = {url:"http://b.ru"};
		_text = "";
		check1();
		Assert.areEqual("", _text);
	}

	private function check2() {
		StartHelper.check(["a.ru", "b.com"], {
			_text += "1";
			_text += "2";
			_text += "3";
			_text += "4";
			_text += "5";
		});
	}

	@Test
	public function checkTest4() {
		loaderInfo = {url:"http://a.ru"};
		_text = "";
		check2();
		Assert.areEqual("12345", _text);

		loaderInfo = {url:"http://b.ru"};
		_text = "";
		check2();
		Assert.areEqual("", _text);
	}

	private function check3() {
		StartHelper.check(["a.ru"], {
			_text += "1";
			_text += "2";
			_text += "3";
		});
	}

	@Test
	public function checkTest5() {
		loaderInfo = {url:"http://a.ru"};
		_text = "";
		check3();
		Assert.areEqual("123", _text);

		loaderInfo = {url:"http://b.ru"};
		_text = "";
		check3();
		Assert.areEqual("", _text);
	}
}
