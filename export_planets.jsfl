var gsa = "file:///Z|/home/user/gsa/";

fl.openDocument(gsa + "assets-raw/planets.fla");
var doc = fl.getDocumentDOM();
var timeline = doc.timelines[doc.currentTimeline];
timeline.currentLayer = 0;
timeline.currentFrame = 0;
var layer = timeline.layers[timeline.currentLayer];
var frame = layer.frames[timeline.currentFrame];
doc.exportPNG(gsa + "assets/planets.png", true, true);

var text = "";
text += "// <<Generated>>\n"
text += "package game;\n"
text += "\n";
text += "class PlanetInfos {\n";
text += "\tprivate static var _all:Array<PlanetInfo> = new Array<PlanetInfo>();\n";
text += "\tpublic static var all(get, never):Iterable<PlanetInfo>;\n";
text += "\tprivate static function get_all():Iterable<PlanetInfo> {\n";
text += "\t\treturn _all;\n";
text += "\t}\n";
text += "\n";
text += "\tprivate static function add(name:String):PlanetInfo {\n";
text += "\t\tvar info = new PlanetInfo(name);\n";
text += "\t\t_all.push(info);\n";
text += "\t\treturn info;\n";
text += "\t}\n";
for (var i in frame.elements) {
	var element = frame.elements[i];
	if (element.elementType == "instance" &&
		element.libraryItem &&
		element.libraryItem.itemType == "component") {

		var params = {};
		for (var p in element.parameters) {
			var param = element.parameters[p];
			params[param.name] = param.value;
		}

		text += "\n";
		text += "\tpublic static var " + element.name.toLowerCase() + ":PlanetInfo = " +
			"add(\"" + element.name + "\")\n";
		text += "\t\t.setSprite(" + Math.round(element.matrix.tx) + ", " + Math.round(element.matrix.ty) + ", " +
			Math.round(params["size"]) + ")\n";
		text += "\t\t.setRadius(" + Math.round(params["radius"]) + ")\n";
		text += "\t\t.setAtmH(" + Math.round(params["atmH"]) + ")\n";
		text += "\t\t.setHasDark(" + params["hasDark"] + ");\n"
	}
	if (element.libraryItem && element.libraryItem.itemType == "movie clip") {
		if (element.name == "flame") {
			text += "\n";
			text += "	public static var flameTextureX:Float = " + Math.round(element.matrix.tx) + ";\n";
			text += "	public static var flameTextureY:Float = " + Math.round(element.matrix.ty) + ";\n";
			text += "	public static var flameTextureWidth:Float = " + 25 + ";\n";
			text += "	public static var flameTextureHeight:Float = " + 39 + ";\n";
			text += "	public static var flameFrames:Int = " + 9 + ";\n";
		}
		else if (element.name == "ion_flame") {
			text += "\n";
			text += "	public static var ionFlameTextureX:Float = " + Math.round(element.matrix.tx) + ";\n";
			text += "	public static var ionFlameTextureY:Float = " + Math.round(element.matrix.ty) + ";\n";
			text += "	public static var ionFlameTextureWidth:Float = " + 25 + ";\n";
			text += "	public static var ionFlameTextureHeight:Float = " + 39 + ";\n";
			text += "	public static var ionFlameFrames:Int = " + 9 + ";\n";
		}
	}
}
text += "}";
FLfile.write(gsa + "src/game/PlanetInfos.hx", text);
