package elm.utils;

class Rand {
	public static var instance = new Rand();

	public var seed:UInt;

	public function new() {
		seed = 0;
	}

	public function getInt(min:Int, max:Int):Int {
		seed = 214013 * seed + 2531011;
		return min + (seed ^ (seed >> 15)) % (max - min + 1);
	}

	public function getFloat(min:Float, max:Float):Float {
		seed = 214013 * seed + 2531011;
		return min + (seed >>> 16) * (1.0 / 65535.0) * (max - min);
	}
}
