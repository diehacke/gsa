package ;

import flash.events.Event;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

typedef PlayerData = {
	soundMuted:Bool,
	musicMuted:Bool
}

class Player {
	private var _main:Main;

	public function new(main:Main) {
		_main = main;
		_musicState = Music.SILENT;
	}

	public function /**/playSound(sound:Sound, ?volume:Float) {
		if (!_soundMuted) {
			if (volume != null)
				sound.play(0, 1, new SoundTransform(volume));
			else
				sound.play();
		}
	}

	private var _solelySound:PlayerSound;

	public function /**/playSoundSolely(sound:Sound, volume:Float, ?loops:Int):Void {
		stopSoundSolely();
		_solelySound = new PlayerSound(sound, 0, loops, null);
		_solelySound.volume = volume;
		_solelySound.muted = _soundMuted;
	}

	public function /**/stopSoundSolely():Void {
		if (_solelySound != null) {
			_solelySound.channel.stop();
			_solelySound = null;
		}
	}

	private var _musicSound:PlayerSound;

	public function /**/stopMusic():Void {
		if (_musicSound != null) {
			_musicSound.channel.stop();
			_musicSound = null;
		}
	}

	public function /**/playMusic(sound:Sound, volume:Float, startTime:Float = 0):Void {
		stopMusic();
		_musicSound = new PlayerSound(sound, startTime, 1, onMusicComplete);
		_musicSound.volume = volume;
		_musicSound.muted = _musicMuted;
	}

	private function onMusicComplete(sound:PlayerSound):Void {
		sound.play(0, 1);
	}

	private var _soundMuted:Bool;
	public var soundMuted(get, set):Bool;
	private function get_soundMuted():Bool {
		return _soundMuted;
	}
	private function set_soundMuted(value:Bool):Bool {
		if (_soundMuted != value) {
			_soundMuted = value;
			if (_enginesSound != null)
				_enginesSound.muted = _soundMuted;
			if (_solelySound != null)
				_solelySound.muted = _soundMuted;
			_main.save();
		}
		return _soundMuted;
	}

	private var _musicMuted:Bool;
	public var musicMuted(get, set):Bool;
	private function get_musicMuted():Bool {
		return _musicMuted;
	}
	private function set_musicMuted(value:Bool):Bool {
		if (_musicMuted != value) {
			_musicMuted = value;
			if (_musicSound != null)
				_musicSound.muted = _musicMuted;
			_main.save();
		}
		return _musicMuted;
	}

	private var _enginesSound:PlayerSound;

	public function engines(isOn:Bool):Void {
		if (isOn) {
			if (_enginesSound == null) {
				_enginesSound = new PlayerSound(Assets.Blast_Of_Kevin_Ph_7939_hifi_wav, 0, 100500, null);
				_enginesSound.muted = _soundMuted;
			}
		} else {
			if (_enginesSound != null) {
				_enginesSound.channel.stop();
				_enginesSound = null;
			}
		}
	}

	public function save(data:PlayerData):Void {
		data.soundMuted = _soundMuted;
		data.musicMuted = _musicMuted;
	}

	public function load(data:PlayerData):Void {
		_soundMuted = data.soundMuted;
		_musicMuted = data.musicMuted;
	}

	private var _musicState:Music;
	private var _musicPositionUi:Float;
	private var _musicPositionOrbit:Float;

	public function setMusic(musicState:Music):Void {
		if (_musicState != musicState) {
			if (_musicSound != null) {
				switch(_musicState) {
					case Music.SILENT:
					case Music.MENU:
					case Music.UI:
						_musicPositionUi = _musicSound.channel.position;
					case Music.ORBIT:
						_musicPositionOrbit = _musicSound.channel.position;
				}
			}
			_musicState = musicState;
			switch(_musicState) {
				case Music.SILENT:
					stopMusic();
				case Music.MENU:
					playMusic(Assets.music_fail_mp3, .5);
				case Music.UI:
					var startTime = !Math.isNaN(_musicPositionUi) ? _musicPositionUi : .0;
					playMusic(Assets.music_major_short_mp3, .5, startTime);
				case Music.ORBIT:
					var startTime = !Math.isNaN(_musicPositionOrbit) ? _musicPositionOrbit : .0;
					playMusic(Assets.music_orbit_mp3, .5, startTime);
			}
		}
	}
}
