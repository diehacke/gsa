﻿package ;

import ui.Widget;
import model.PlanetStatus;
import model.StageModels;
import ui.SoundPanel;
import flash.net.SharedObject;
import model.Home;
import utils.KeyUtil;
import flash.Lib;
import flash.display.Stage;
import flash.events.KeyboardEvent;
import flash.display.StageScaleMode;
import flash.display.StageAlign;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import screens.Screen;
import screens.MenuScreen;
import screens.MissionsScreen;
import debug.DebugMonitor;
import ui.SoundButton;

class Main extends Widget {
	public static var /**/SHARED_OBJECT_FILE:String = "in_orbit";

	public static function main() {
		Lib.current.addChild(new Main());
	}

	private var _player:Player;
	private var _soundPanel:SoundPanel;

	private var _home:Home;
	public var home(get, never):Home;
	private function get_home():Home {
		return _home;
	}

	public function new() {
		super();
		addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
	}

	private function /**/onAddedToStage(event:Event):Void {
		removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);

		StartHelper.check([
			"fgl.com",
			"kongregate.com"
		], {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			haxe.Log.setColor(0x00ff00);
			KeyUtil.init();
			StageModels.init();

			_player = new Player(this);
			_home = new Home();
			load();
			updateSize();

			setScreen(new MenuScreen());
			stage.addEventListener(Event.RESIZE, onStageResize);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);

			_soundPanel = new SoundPanel(_player);
			_soundPanel.redraw();
			addChild(_soundPanel);
		});
	}

	private var _screen:Screen;

	private var _anyScreenShowed:Bool;
	public var anyScreenShowed(get, never):Bool;
	private function get_anyScreenShowed():Bool {
		return _anyScreenShowed;
	}
	
	public function /**/setScreen(screen:Screen):Void {
		if (_screen != null) {
			_screen.doOff();
			if (_screen.parent != null)
				_screen.parent.removeChild(_screen);
		}
		_screen = screen;
		if (_screen != null)
		{
			addChildAt(_screen, 0);
			_screen.w = _w;
			_screen.h = _h;
			_screen.init(this, _player);
			_screen.doOn();
			_screen.doOnResize();
			_anyScreenShowed = true;
		}
	}

	private function /**/updateSize():Void {
		_w = stage.stageWidth;
		_h = stage.stageHeight;
	}

	private function /**/onStageResize(event:Event):Void {
		updateSize();
		_w = stage.stageWidth;
		_h = stage.stageHeight;
		if (_screen != null) {
			_screen.w = _w;
			_screen.h = _h;
			_screen.doOnResize();
		}
	}

	private function /**/onKeyDown(event:KeyboardEvent):Void {
		if (_screen != null)
			_screen.doOnKeyDown(event.keyCode);
	}

	private function /**/onMouseDown(event:MouseEvent):Void {
		if (_screen != null)
			_screen.doOnMouseDown();
	}

	private function /**/onMouseUp(event:MouseEvent):Void {
		if (_screen != null)
			_screen.doOnMouseUp();
	}

	private var _debugMonitor:DebugMonitor;
	private var _switchDebugFlag:Bool;
	private var _switchDebugMonitor:Bool;

	private function /**/onEnterFrame(event:Event):Void {
		if (KeyUtil.isDown(KeyUtil.E) &&
			KeyUtil.isDown(KeyUtil.N6) &&
			KeyUtil.isDown(KeyUtil.U)) {
			if (!_switchDebugFlag) {
				_switchDebugFlag = true;
				if (_screen != null)
					_screen.doOnDebugPress();
			}
		} else {
			_switchDebugFlag = false;
		}
		if (KeyUtil.isDown(KeyUtil.Q) &&
			KeyUtil.isDown(KeyUtil.E) &&
			KeyUtil.isDown(KeyUtil.T)) {
			if (!_switchDebugMonitor) {
				_switchDebugMonitor = true;
				if (_debugMonitor == null) {
					_debugMonitor = new DebugMonitor(100, 100);
					stage.addChild(_debugMonitor);
				} else {
					stage.removeChild(_debugMonitor);
					_debugMonitor = null;
				}
			}
		} else {
			_switchDebugMonitor = false;
		}
		if (_screen != null)
			_screen.doOnEnterFrame();
	}

	public function debugClearSO():Void {
		var so = SharedObject.getLocal(SHARED_OBJECT_FILE);
		so.clear();
		so.flush();
	}

	private function load():Void {
		var so = SharedObject.getLocal(SHARED_OBJECT_FILE);
		_player.load(so.data);
		_home.load(so.data);
		so.close();
	}

	public function save():Void {
		var so = SharedObject.getLocal(SHARED_OBJECT_FILE);
		_home.save(so.data);
		_player.save(so.data);
		so.flush();
	}
}
