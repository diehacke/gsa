package windows;

import flash.text.TextField;
import utils.MathUtil;
import flash.events.MouseEvent;
import model.Mission;
import model.StageModelStack;
import ui.MissionTile;
import ui.SmartFormat;
import ui.Widget;
import ui.WindowButton;
import ui.UICommon;
import ui.UIRects;
import ui.StageModelStackBlock;

class MissionWindow extends Window {
	private var _mission:Mission;
	private var _titleContainer:Widget;
	private var _titleTf:TextField;
	private var _tile:MissionTile;
	private var _ok:WindowButton;
	private var _openedStagesBlock:StageModelStackBlock;

	public function new(mission:Mission, title:String, openedStages:StageModelStack) {
		super();

		_mission = mission;

		_titleContainer = new Widget();
		_titleContainer.w = UIRects.window_title_normal.logicW;
		_titleContainer.h = 50;
		UIRects.window_title_normal.draw(_titleContainer.graphics, Assets.ui_png, 0, 0);
		addChild(_titleContainer);

		_titleTf = new SmartFormat("Arial", 30).setBold(true).setColor(UICommon.BLUE).newAutoSized();
		_titleTf.text = title;
		_titleContainer.addChild(_titleTf);

		_tile = new MissionTile(_mission, false);
		addChild(_tile);

		_ok = new WindowButton();
		_ok.text = "OK";
		_ok.redraw();
		_ok.addEventListener(MouseEvent.CLICK, onOKClick);
		addChild(_ok);

		if (openedStages != null) {
			_openedStagesBlock = new StageModelStackBlock(openedStages, "New accessible stages");
			_openedStagesBlock.redraw();
			addChild(_openedStagesBlock);
		}

		_fullW = _titleContainer.w;
		_fullH = MissionTile.SELECTED_HEIGHT + 50 + _ok.h;
		if (_openedStagesBlock != null) {
			_fullH += _openedStagesBlock.h;
		}
	}

	override public function redraw():Void {
		var k = _h / _fullH;
		var k2 = k > .5 ? (k - .5) * 2 : .0;

		_titleContainer.y = _titleContainer.h;
		_titleContainer.scaleY = k2;
		var offsetY = 10;
		_titleTf.x = Std.int(offsetY + (_titleContainer.w - offsetY) * .5 - _titleTf.width * .5);
		_titleTf.y = Std.int(-30 - _titleTf.height * .5);

		_tile.w = MathUtil.lerp(MissionTile.WIDTH, MissionTile.SELECTED_WIDTH, k);
		_tile.h = MathUtil.lerp(MissionTile.HEIGHT, MissionTile.SELECTED_HEIGHT, k);
		_tile.x = Std.int(_w * .5 - _tile.w * .5);
		_tile.y = 50;
		_tile.redraw();

		var offsetY = _tile.y + _tile.h;
		if (_openedStagesBlock != null) {
			_openedStagesBlock.x = Std.int(_w * .5 - _openedStagesBlock.w * .5);
			_openedStagesBlock.y = Math.round(offsetY);
			_openedStagesBlock.w = _tile.w;
			_openedStagesBlock.redraw();
			offsetY += _openedStagesBlock.h;
		}

		_ok.x = Std.int(_w * .5 - _ok.w * .5);
		_ok.y = Math.round(offsetY);
		_ok.scaleY = k2;
	}

	private function onOKClick(event:MouseEvent):Void {
		closeClick.dispatch(this);
	}
}
