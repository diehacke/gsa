package windows;

import ui.Widget;
import utils.KeyUtil;
import utils.Signal;
import flash.display.BitmapData;
import flash.ui.Keyboard;

class Window extends Widget {
	public var closeClick:Signal<Window> = new Signal<Window>();

	public function new() {
		super();
		_fullW = 0;
		_fullH = 0;
	}

	private var _fullW:Float;
	public var fullW(get, never):Float;
	private function get_fullW():Float {
		return _fullW;
	}

	private var _fullH:Float;
	public var fullH(get, never):Float;
	private function get_fullH():Float {
		return _fullH;
	}

	public var keyBd(get, never):BitmapData;
	private function get_keyBd():BitmapData {
		return Assets.key_space_png;
	}

	public function doOnKeyDown(keyCode:UInt):Void {
		if (keyCode == Keyboard.ESCAPE ||
			keyCode == Keyboard.SPACE ||
			keyCode == Keyboard.ENTER ||
			keyCode == KeyUtil.LEFT_BRACKET && KeyUtil.isDown(Keyboard.CONTROL))
			closeClick.dispatch(this);
	}
}
