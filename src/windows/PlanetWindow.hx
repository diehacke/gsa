package windows;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import game.Camera;
import game.Planet;
import model.StageModelStack;
import ui.PlanetTile;
import ui.SmartFormat;
import ui.Widget;
import ui.WindowButton;
import ui.UICommon;
import ui.UIRects;
import ui.StageModelStackBlock;
import utils.MathUtil;

class PlanetWindow extends Window {
	private var _planet:Planet;
	private var _titleContainer:Widget;
	private var _titleTf:TextField;
	private var _tile:PlanetTile;
	private var _ok:WindowButton;
	private var _openedStagesBlock:StageModelStackBlock;

	public function new(planet:Planet, title:String, openedStages:StageModelStack) {
		super();

		_planet = planet;

		_titleContainer = new Widget();
		_titleContainer.w = UIRects.window_title_normal.logicW;
		_titleContainer.h = 50;
		UIRects.window_title_normal.draw(_titleContainer.graphics, Assets.ui_png, 0, 0);
		addChild(_titleContainer);

		_titleTf = new SmartFormat("Arial", 30).setBold(true).setColor(UICommon.BLUE).newAutoSized();
		_titleTf.text = title;
		_titleContainer.addChild(_titleTf);

		_tile = new PlanetTile(_planet, getBd());
		addChild(_tile);

		_ok = new WindowButton();
		_ok.text = "OK";
		_ok.redraw();
		_ok.addEventListener(MouseEvent.CLICK, onOKClick);
		addChild(_ok);

		if (openedStages != null) {
			_openedStagesBlock = new StageModelStackBlock(openedStages, "New accessible stages");
			_openedStagesBlock.redraw();
			addChild(_openedStagesBlock);
		}

		_fullW = _titleContainer.w;
		_fullH = PlanetTile.SELECTED_HEIGHT + 50 + _ok.h;

		if (_openedStagesBlock != null) {
			_fullH += _openedStagesBlock.h;
		}
	}

	override public function redraw():Void {
		var k = _h / _fullH;
		var k2 = k > .5 ? (k - .5) * 2 : .0;

		_titleContainer.y = _titleContainer.h;
		_titleContainer.scaleY = k2;
		var offsetY = 10;
		_titleTf.x = Std.int(offsetY + (_titleContainer.w - offsetY) * .5 - _titleTf.width * .5);
		_titleTf.y = Std.int(-30 - _titleTf.height * .5);

		_tile.w = MathUtil.lerp(50, PLANET_BD_WIDTH, k);
		_tile.h = MathUtil.lerp(30, PLANET_BD_HEIGHT, k);
		_tile.x = Std.int(_w * .5 - _tile.w * .5);
		_tile.y = 50;
		_tile.redraw();

		var offsetY = _tile.y + _tile.h;
		if (_openedStagesBlock != null) {
			_openedStagesBlock.x = Std.int(_w * .5 - _openedStagesBlock.w * .5);
			_openedStagesBlock.y = Math.round(offsetY);
			_openedStagesBlock.w = _tile.w;
			_openedStagesBlock.redraw();
			offsetY += _openedStagesBlock.h;
		}

		_ok.x = Std.int(_w * .5 - _ok.w * .5);
		_ok.y = Math.round(offsetY);
		_ok.scaleY = k2;
	}

	private function onOKClick(event:MouseEvent):Void {
		closeClick.dispatch(this);
	}

	private static var PLANET_BD_WIDTH:Int = 300;
	private static var PLANET_BD_HEIGHT:Int = 200;

	private function getBd():BitmapData {
		var camera = new Camera();
		camera.bottomContainer = new Sprite();
		camera.container = new Sprite();
		camera.topContainer = new Sprite();
		camera.updateGraphics();
		camera.halfSW = PLANET_BD_WIDTH >> 1;
		camera.halfSH = PLANET_BD_HEIGHT >> 1;
		camera.accelerationIndex = 4;
		camera.updateHalfSize();

		camera.bottomGraphics.clear();
		camera.graphics.clear();
		camera.topGraphics.clear();
		camera.x = _planet.r.x;
		camera.y = _planet.r.y;
		camera.sunX = _planet.r.x - 100;
		camera.sunY = _planet.r.y;
		_planet.render(camera);

		var bd = new BitmapData(PLANET_BD_WIDTH, PLANET_BD_HEIGHT, true, 0x000000);
		bd.draw(camera.bottomContainer);
		bd.draw(camera.container);
		bd.draw(camera.topContainer);
		return bd;
	}
}
