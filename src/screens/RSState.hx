package screens;

import ui.StageTile;
import ui.StageView;

class RSState {
	public function new() {
	}

	private var _screen:RocketScreen;

	public function init(screen:RocketScreen):Void {
		_screen = screen;
	}

	public function doOn():Void {
	}

	public function doOff():Void {
	}

	public function doStageTileMouseDown(tile:StageTile):Void {
	}

	public function doMouseUp():Void {
	}

	public function doOnEnterFrame():Void {
	}

	public function doOnStageOver(view:StageView):Void {
	}

	public function doOnStageOut(view:StageView):Void {
	}

	public function doOnStageMouseDown(view:StageView):Void {
	}
}
