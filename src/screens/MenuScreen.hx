package screens;

import flash.events.MouseEvent;
import flash.display.Shape;
import flash.geom.ColorTransform;
import flash.display.Bitmap;
import flash.text.TextField;
import flash.ui.Keyboard;
import game.Game;
import game.RoundIntersectionTest;
import model.Mission;
import screens.Screen;
import ui.Button;
import ui.MissionTile;
import ui.MenuButton;
import ui.Menu;
import ui.SmartFormat;
import ui.SymbolPanel;
import ui.TileSelector;
import ui.UIRects;
import ui.Widget;
import utils.KeyUtil;

class MenuScreen extends Screen {
	public function new() {
		super();
	}

	private var _title:Shape;
	private var _stars:Bitmap;
	private var _menu:Menu;
	private var _credits:MenuButton;

	override public function doOn():Void {
		addChild(new Bitmap(Assets.stars_jpg));
		addChild(new Bitmap(Assets.ui_planet_png));

		_title = new Shape();
		addChild(_title);

		_menu = new Menu(_player, false);
		_menu.playOpenClose = false;
		_menu.addButton("Missions").addEventListener(MouseEvent.CLICK, onStartClick);
		_menu.addButton("Known planets").addEventListener(MouseEvent.CLICK, onPlanetsClick);
		_menu.addButton("Credits").addEventListener(MouseEvent.CLICK, onCreditsClick);
		_menu.redraw();
		_menu.open(false);
		addChild(_menu);

		if (_main.anyScreenShowed)
			_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);

		_player.setMusic(Music.MENU);
	}

	private function onStartClick(event:MouseEvent):Void {
		_main.save();
		_main.setScreen(new MissionsScreen());
	}

	private function onCreditsClick(event:MouseEvent):Void {
		_main.setScreen(new CreditsScreen());
	}

	private function onPlanetsClick(event:MouseEvent):Void {
		_main.setScreen(new PlanetsScreen(null, null));
	}

	override public function doOnResize():Void {
		_title.graphics.clear();
		UIRects.new_orbit_normal.draw(_title.graphics, Assets.ui_png, 0, 0);
		_title.x = Std.int(_w / 2 - _title.width / 2);
		_title.y = 10;

		_menu.x = _w / 2;
		_menu.y = _h;
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_menu.doOnKeyDown(keyCode);
	}
}
