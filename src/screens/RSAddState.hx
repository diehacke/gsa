package screens;

import model.LinkType;
import ui.RocketPartView;
import utils.MathUtil;
import model.LinkModel;
import flash.geom.Point;
import flash.display.Sprite;
import model.StageModelIterator;
import model.StageModel;
import ui.StageTile;
import utils.Selector;

class RSAddState extends RSState {
	private var _model:StageModel;

	public function new(model:StageModel) {
		super();
		_model = model;
	}

	private var _view:RocketPartView;
	private var _markersView:Sprite;

	override public function doOn():Void {
		_view = new RocketPartView(_model, null);
		_view.redraw();
		_screen.addChild(_view);

		_markersView = new Sprite();
		_screen.addChild(_markersView);
	}

	override public function doOff():Void {
		clearHighlighting();
		_screen.removeChild(_view);
		_markersView.parent.removeChild(_markersView);

		if (_linkToAdd != null) {
			_linkToAdd.child = _model.clone();
			_linkToAdd.stage.calculateTree();
			_screen.rocketView.redraw();
			_screen.updateOnRocketChange();
		}
	}

	override public function doStageTileMouseDown(tile:StageTile):Void {
	}

	override public function doMouseUp():Void {
		_screen.setState(new RSDefaultState());
	}

	private var _linkToAdd:LinkModel;

	override public function doOnEnterFrame():Void {
		var g = _markersView.graphics;
		g.clear();

		_view.x = _screen.mouseX;
		_view.y = _screen.mouseY;
		var rootPoint = _screen.globalToLocal(_screen.rocketView.partView.localToGlobal(new Point()));
		var point = _screen.globalToLocal(_view.localToGlobal(new Point()));
		clearHighlighting();
		var iterator = new StageModelIterator(_screen.rocketView.model);
		var selector = new Selector<LinkModel>();
		while (iterator.next()) {
			for (link in iterator.current.links) {
				if (link.child != null)
					continue;
				if (link.type == LinkType.NONE)
					continue;
				if (link.type != _view.model.linkType)
					continue;
				var distance = MathUtil.distance(
					rootPoint.x + link.calculatedX,
					rootPoint.y + link.calculatedY,
					point.x,
					point.y);
				if (distance < 50 && !hasIntersection(_screen.rocketView.model, link, _view.model)) {
					selector.add(link, -distance);
				}
			}
		}
		_linkToAdd = selector.selected;
		var scale = 1.;
		if (_linkToAdd != null) {
			if (_linkToAdd.calculatedMirror)
				scale = -1.;

			g.lineStyle(2, 0x00cc50);

			g.beginFill(0x70ff00);
			g.drawCircle(rootPoint.x + _linkToAdd.calculatedX, rootPoint.y + _linkToAdd.calculatedY, 6);
			g.endFill();

			g.beginFill(0x70ff00);
			g.drawRect(point.x - 6, point.y - 6, 12, 12);
			g.endFill();
		}
		_view.scaleX = scale;
	}

	private function clearHighlighting():Void {
		_markersView.graphics.clear();
	}

	private function hasIntersection(model0:StageModel, link0:LinkModel, model1:StageModel):Bool {
		var iterator0 = new StageModelIterator(model0);
		var iterator1 = new StageModelIterator(model1);
		while (iterator0.next()) {
			var model0I = iterator0.current;

			var x0 = model0I.calculatedX;
			var y0 = model0I.calculatedY;

			var left0 = x0 - model0I.info.sizeX * .5;
			var right0 = x0 + model0I.info.sizeX * .5;
			var top0 = y0 - model0I.info.sizeY * .5;
			var bottom0 = y0 + model0I.info.sizeY * .5;

			iterator1.reset();
			while (iterator1.next()) {
				var model1I = iterator1.current;
				if (model1I.isCowl)
					continue;

				var x1 = link0.calculatedX + (link0.calculatedMirror ? -1 : 1) * model1I.calculatedX;
				var y1 = link0.calculatedY + model1I.calculatedY;

				var left1 = x1 - model1I.info.sizeX * .5;
				var right1 = x1 + model1I.info.sizeX * .5;
				var top1 = y1 - model1I.info.sizeY * .5;
				var bottom1 = y1 + model1I.info.sizeY * .5;

				if (right0 > left1 && left0 < right1 &&
					bottom0 > top1 && top0 < bottom1) {
					return true;
				}
			}
		}
		return false;
	}
}
