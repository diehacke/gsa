package screens;

import flash.geom.ColorTransform;
import ui.StageTile;
import ui.StageView;

class RSDefaultState extends RSState {
	public function new() {
		super();
	}

	override public function doStageTileMouseDown(tile:StageTile):Void {
		_screen.setState(new RSAddState(tile.model.clone().calculateTree()));
	}

	override public function doOnStageOver(view:StageView):Void {
		view.transform.colorTransform = new ColorTransform(0, 0, 1);
	}

	override public function doOnStageOut(view:StageView):Void {
		view.transform.colorTransform = new ColorTransform();
	}

	override public function doOnStageMouseDown(view:StageView):Void {
		if (_screen.rocketView.removeStage(view.model)) {
			_screen.setState(new RSAddState(view.model.clone().calculateTree()));
			_screen.updateOnRocketChange();
		}
	}
}
