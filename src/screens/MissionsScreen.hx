package screens;

import flash.events.MouseEvent;
import utils.Tween;
import ui.NextButton;
import screens.Screen;
import flash.geom.ColorTransform;
import flash.display.Bitmap;
import flash.text.TextField;
import ui.SmartFormat;
import flash.ui.Keyboard;
import ui.Widget;
import ui.Button;
import ui.BackButton;
import ui.TileSelector;
import ui.SymbolPanel;
import ui.ScreenTitle;
import game.Game;
import game.RoundIntersectionTest;
import ui.MissionTile;
import utils.KeyUtil;
import model.Mission;

class MissionsScreen extends Screen {
	public function new() {
		super();
	}

	private var _title:ScreenTitle;
	private var _tileSelector:TileSelector<MissionTile>;
	private var _back:BackButton;
	private var _start:NextButton;
	private var _lbtr:Bitmap;

	override public function doOn():Void {
		_title = new ScreenTitle();
		_title.text = "Missions";
		_title.redraw();
		addChild(_title);
		_title.show();

		var numOpened = 0;
		_tileSelector = new TileSelector<MissionTile>(_player);
		_tileSelector.numCols = 4;
		_tileSelector.tileW = MissionTile.WIDTH;
		_tileSelector.tileH = MissionTile.HEIGHT;
		_tileSelector.selectedTileW = MissionTile.SELECTED_WIDTH;
		_tileSelector.selectedTileH = MissionTile.SELECTED_HEIGHT;
		for (mission in _main.home.missions) {
			var tile = new MissionTile(mission, true);
			_tileSelector.add(tile);
			tile.mouseDown.add(onMissionTileMouseDown);
			tile.doubleClick.add(onMissionTileDoubleClick);
			tile.redraw();
			if (mission.opened)
				numOpened++;
		}
		_tileSelector.redraw();
		addChild(_tileSelector);

		_back = new BackButton();
		_back.redraw();
		_back.addEventListener(MouseEvent.CLICK, onBackClick);
		addChild(_back);

		_start = new NextButton();
		_start.text = "Next";
		_start.redraw();
		_start.addEventListener(MouseEvent.CLICK, onStartClick);
		addChild(_start);

		_lbtr = new Bitmap(Assets.key_lbtr_png);
		_lbtr.visible = numOpened > 1;
		addChild(_lbtr);

		updateSelectedMission();

		if (_main.anyScreenShowed)
			_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);

		if (!_main.anyScreenShowed)
			start();

		_player.setMusic(Music.UI);
	}

	private function onMissionTileMouseDown(tile:MissionTile):Void {
		setSelectedMission(tile.mission);
	}

	private function onMissionTileDoubleClick(tile:MissionTile):Void {
		setSelectedMission(tile.mission);
		start();
	}

	private function setSelectedMission(mission:Mission):Void {
		if (mission.opened) {
			_main.home.selectedMission = mission;
			updateSelectedMission();
		}
	}

	private function onStartClick(event:MouseEvent):Void {
		start();
	}

	private function onBackClick(event:MouseEvent):Void {
		_main.setScreen(new MenuScreen());
	}

	override public function doOnResize():Void {
		var g = graphics;
		g.clear();
		g.beginFill(0xeeeeee);
		g.drawRect(0, 0, _w, _h);

		_title.x = Std.int(_w / 2);

		_tileSelector.x = Std.int(_w / 2 - _tileSelector.w / 2);
		_tileSelector.y = Std.int(_h / 2 - _tileSelector.h / 2);

		_start.x = _w;
		_start.y = _h - _start.h;

		_back.y = _h - _back.h;

		_lbtr.x = Std.int(_w / 2 - _lbtr.width / 2);
		_lbtr.y = Std.int(_h - _lbtr.height - 2);
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_back.doOnKeyDown(keyCode);
		_start.doOnKeyDown(keyCode);
		if (keyCode == Keyboard.LEFT || keyCode == KeyUtil.H || keyCode == KeyUtil.A)
			moveSelected(-1, 0);
		if (keyCode == Keyboard.RIGHT || keyCode == KeyUtil.L || keyCode == KeyUtil.D)
			moveSelected(1, 0);
		if (keyCode == Keyboard.UP || keyCode == KeyUtil.K || keyCode == KeyUtil.W)
			moveSelected(0, -1);
		if (keyCode == Keyboard.DOWN || keyCode == KeyUtil.J || keyCode == KeyUtil.S)
			moveSelected(0, 1);
	}

	private function moveSelected(offsetX:Int, offsetY:Int):Void {
		var tile = null;
		for (i in 1 ... Math.round(_tileSelector.realNumCols * _tileSelector.realNumRows))
		{
			tile = _tileSelector.getTile(
				_tileSelector.getCol(_tileSelector.selected) + offsetX * i,
				_tileSelector.getRow(_tileSelector.selected) + offsetY * i);
			if (tile != null && tile.mission.opened)
				break;
		}
		if (tile != null) {
			setSelectedMission(tile.mission);
		}
	}

	private function start():Void {
		_main.save();
		if (_main.home.selectedMission.allowConstructor) {
			_main.setScreen(new RocketScreen());
		} else {
			var mission = _main.home.selectedMission;
			_main.setScreen(new Game(mission, mission.newDefaultRocketModel(), stage.frameRate,
				mission.tutorial));
		}
	}

	private function updateSelectedMission():Void {
		var tile = null;
		for (tileI in _tileSelector.tiles) {
			var missionI = tileI.mission;
			if (missionI == _main.home.selectedMission && missionI.opened) {
				tile = tileI;
				break;
			}
		}
		if (tile == null) {
			for (tileI in _tileSelector.tiles) {
				var missionI = tileI.mission;
				if (missionI.opened) {
					tile = tileI;
					break;
				}
			}
			_main.home.selectedMission = tile.mission;
		}
		_tileSelector.selected = tile;
		_start.visible = tile != null;
		if (tile != null) {
			_start.redraw();
		}
	}
}
