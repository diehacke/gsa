package screens;

import ui.Widget;

class Screen extends Widget {
	public function new() {
		super();
	}

	private var _main:Main;
	private var _player:Player;

	public function init(main:Main, player:Player):Void {
		_main = main;
		_player = player;
	}

	public function /**/doOn():Void {
	}

	public function /**/doOff():Void {
	}

	public function /**/doOnResize():Void {
	}

	public function /**/doOnKeyDown(keyCode:UInt):Void {
	}

	public function /**/doOnEnterFrame():Void {
	}

	public function /**/doOnDebugPress():Void {
		_main.setScreen(new CheatsScreen());
	}

	public function /**/doOnMouseDown():Void {
	}

	public function /**/doOnMouseUp():Void {
	}
}
