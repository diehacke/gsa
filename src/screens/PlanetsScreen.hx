package screens;

import flash.events.Event;
import game.WorldSystem;
import ui.NextButton;
import flash.display.BitmapData;
import utils.Selector;
import game.Game;
import game.Camera;
import game.Asteroid;
import game.Unit;
import game.Planet;
import model.ArialSystem;
import model.PlanetStatus;
import ui.UIRects;
import flash.net.URLRequest;
import flash.Lib;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.text.TextField;
import ui.Widget;
import flash.display.Sprite;
import flash.display.Bitmap;
import flash.errors.Error;
import ui.SmartFormat;
import ui.BackButton;
import ui.ScreenTitle;
import flash.ui.Keyboard;
import utils.KeyUtil;
import ui.ScrollBar;
import ui.UICommon;
import ui.TileSelector;
import ui.PlanetTile;
import model.CreditsData;

class PlanetsScreen extends Screen {
	private var PLANET_BD_WIDTH:Int = 300;
	private var PLANET_BD_HEIGHT:Int = 180;

	private var _game:Game;
	private var _selectedPlanet:PlanetStatus;
	private var _system:WorldSystem;

	public function new(selectedPlanet:PlanetStatus, game:Game) {
		super();
		_game = game;
		_selectedPlanet = selectedPlanet;
	}

	private var _title:ScreenTitle;
	private var _back:BackButton;
	private var _show:NextButton;
	private var _tileSelector:TileSelector<PlanetTile>;
	private var _arial:Planet;
	private var _arialTf:TextField;
	private var _lbtr:Bitmap;

	override public function doOn():Void {
		_title = new ScreenTitle();
		_title.text = "Planets";
		_title.redraw();
		addChild(_title);
		_title.show();

		_back = new BackButton();
		_back.redraw();
		_back.addEventListener(MouseEvent.CLICK, onBackClick);
		addChild(_back);

		_show = new NextButton();
		_show.text = "Show";
		_show.redraw();
		_show.addEventListener(MouseEvent.CLICK, onShowClick);
		addChild(_show);

		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);

		_system = new ArialSystem();
		_arial = _system.star;

		_arialTf = new SmartFormat("Tahoma", 16, 0xffffff).setBold(true)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		_arialTf.text = _system.star.name;
		addChild(_arialTf);

		var camera = new Camera();
		camera.bottomContainer = new Sprite();
		camera.container = new Sprite();
		camera.topContainer = new Sprite();
		camera.updateGraphics();
		camera.halfSW = PLANET_BD_WIDTH >> 1;
		camera.halfSH = PLANET_BD_HEIGHT >> 1;
		camera.accelerationIndex = 4;
		camera.updateHalfSize();

		_tileSelector = new TileSelector<PlanetTile>(_player);
		_tileSelector.numCols = 4;
		_tileSelector.tileW = PlanetTile.WIDTH;
		_tileSelector.tileH = PlanetTile.HEIGHT;
		_tileSelector.selectedTileW = PlanetTile.SELECTED_WIDTH;
		_tileSelector.selectedTileH = PlanetTile.SELECTED_HEIGHT;

		var selectedPlanet = _selectedPlanet != null && _selectedPlanet.discovered ?
			_selectedPlanet : _system.glory.status;
		var selectedTile = null;
		var numItems = 0;
		for (unit in _system.units) {
			var planet = unit.asPlanet;
			if (planet != null && planet != _system.star) {
				camera.bottomGraphics.clear();
				camera.graphics.clear();
				camera.topGraphics.clear();
				camera.x = planet.r.x;
				camera.y = planet.r.y;
				camera.sunX = planet.r.x - 100;
				camera.sunY = planet.r.y;
				planet.render(camera);

				var bd = new BitmapData(PLANET_BD_WIDTH, PLANET_BD_HEIGHT, true, 0x000000);
				bd.draw(camera.bottomContainer);
				bd.draw(camera.container);
				bd.draw(camera.topContainer);

				var tile = new PlanetTile(planet, bd);
				_tileSelector.add(tile);
				tile.mouseDown.add(onPlanetTileMouseDown);
				tile.click.add(onPlanetTileClick);
				tile.redraw();
				numItems++;

				if (planet.status == selectedPlanet)
					selectedTile = tile;
			}
		}
		_tileSelector.redraw();
		if (selectedTile == null) {
			for (tile in _tileSelector.tiles) {
				if (tile.planet.status.discovered) {
					selectedTile = tile;
					break;
				}
			}
		}
		_tileSelector.selected = selectedTile;
		addChild(_tileSelector);

		_lbtr = new Bitmap(Assets.key_lbtr_png);
		_lbtr.visible = numItems > 1;
		addChild(_lbtr);

		_player.setMusic(Music.UI);
	}

	override public function doOnResize():Void {
		_title.x = Std.int(_w / 2);

		_back.y = _h - _back.h;

		_show.x = _w;
		_show.y = _h - _show.h;

		_lbtr.x = Std.int(_w / 2 - _lbtr.width / 2);
		_lbtr.y = Std.int(_h - _lbtr.height - 2);

		_tileSelector.x = Std.int(_w - _tileSelector.w - 20);
		_tileSelector.y = Std.int(80 + (_h - 106) * .5 - _tileSelector.h * .5);

		var g = graphics;
		g.clear();
		g.beginFill(0xeeeeee);
		g.drawRect(0, 0, _w, 80);
		g.endFill();
		g.beginBitmapFill(Assets.stars_jpg);
		g.drawRect(0, 80, _w, _h - 80);
		g.endFill();

		var arialY = 80 + (_h - 80) * .5;
		_arial.drawSimple(g, 0, arialY);
		_arialTf.x = 10;
		_arialTf.y = arialY;
	}

	private function onBackClick(event:MouseEvent):Void {
		if (_game != null) {
			_main.setScreen(_game);
		} else {
			_main.setScreen(new MenuScreen());
		}
	}

	private function onShowClick(event:MouseEvent):Void {
		if (_tileSelector.selected != null) {
			var planet = _tileSelector.selected.planet;
			_main.setScreen(new PlanetScreen(_system, planet, getBd, _game));
		}
	}

	private function getBd(planet:Planet):BitmapData {
		for (tile in _tileSelector.tiles) {
			if (tile.planet == planet)
				return tile.bd;
		}
		return null;
	}

	private function onPlanetTileMouseDown(tile:PlanetTile):Void {
		if (tile.planet.status.discovered) {
			_tileSelector.selected = tile;
		}
	}

	private function onPlanetTileClick(tile:PlanetTile):Void {
		if (tile.planet.status.discovered) {
			_tileSelector.selected = tile;
			_show.processClick();
		}
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_back.doOnKeyDown(keyCode);
		_show.doOnKeyDown(keyCode);
		if (keyCode == Keyboard.LEFT || keyCode == KeyUtil.H || keyCode == KeyUtil.A)
			moveSelected(-1, 0);
		if (keyCode == Keyboard.RIGHT || keyCode == KeyUtil.L || keyCode == KeyUtil.D)
			moveSelected(1, 0);
		if (keyCode == Keyboard.UP || keyCode == KeyUtil.K || keyCode == KeyUtil.W)
			moveSelected(0, -1);
		if (keyCode == Keyboard.DOWN || keyCode == KeyUtil.J || keyCode == KeyUtil.S)
			moveSelected(0, 1);
	}

	private function moveSelected(offsetX:Int, offsetY:Int):Void {
		var tile = null;
		for (i in 1 ... Math.round(_tileSelector.realNumCols * _tileSelector.realNumRows))
		{
			var tileI = _tileSelector.getTile(
				_tileSelector.getCol(_tileSelector.selected) + offsetX * i,
				_tileSelector.getRow(_tileSelector.selected) + offsetY * i);
			if (tileI != null && tileI.planet.status.discovered) {
				tile = tileI;
				break;
			}
		}
		if (tile != null) {
			_tileSelector.selected = tile;
		}
	}
}
