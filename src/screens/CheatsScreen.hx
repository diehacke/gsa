package screens;

import flash.events.MouseEvent;
import game.PlanetInfos;
import ui.ScreenTitle;
import flash.geom.ColorTransform;
import flash.display.Bitmap;
import flash.net.SharedObject;
import flash.text.TextField;
import flash.ui.Keyboard;
import game.Game;
import game.RoundIntersectionTest;
import model.Mission;
import screens.Screen;
import ui.Button;
import ui.MissionTile;
import ui.MenuButton;
import ui.Menu;
import ui.SmartFormat;
import ui.SymbolPanel;
import ui.TileSelector;
import ui.UIRects;
import ui.Widget;
import utils.KeyUtil;

class CheatsScreen extends Screen {
	public function new() {
		super();
	}

	private var _title:ScreenTitle;
	private var _stars:Bitmap;
	private var _menu:Menu;
	private var _credits:MenuButton;

	override public function doOn():Void {
		_title = new ScreenTitle();
		_title.text = "Cheats";
		_title.redraw();
		addChild(_title);
		_title.show();

		_menu = new Menu(_player, false);
		_menu.addButton("Missions").addEventListener(MouseEvent.CLICK, onStartClick);
		_menu.addButton("Reset").addEventListener(MouseEvent.CLICK, onResetClick);
		_menu.addButton("Clear SO").addEventListener(MouseEvent.CLICK, onClearSOClick);
		_menu.addButton("RoundIntersectionTest").addEventListener(MouseEvent.CLICK, onRoundItersectionTest);
		_menu.addButton("Photo all").addEventListener(MouseEvent.CLICK, onPhotoAllClick);
		_menu.addButton("Open all").addEventListener(MouseEvent.CLICK, onOpenAll);
		_menu.addButton("Save").addEventListener(MouseEvent.CLICK, onSaveClick);
		_menu.addButton("Load").addEventListener(MouseEvent.CLICK, onLoadClick);
		_menu.redraw();
		_menu.open(false);
		addChild(_menu);

		if (_main.anyScreenShowed)
			_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
	}

	private function onStartClick(event:MouseEvent):Void {
		_main.save();
		_main.setScreen(new MissionsScreen());
	}

	private function onResetClick(event:MouseEvent):Void {
		_main.home.reset();
		_main.save();
		_main.setScreen(new MissionsScreen());
	}

	private function onClearSOClick(event:MouseEvent):Void {
		_main.debugClearSO();
	}

	private function onRoundItersectionTest(event:MouseEvent):Void {
		_main.setScreen(new RoundIntersectionTest());
	}

	private function onPhotoAllClick(event:MouseEvent):Void {
		for (info in PlanetInfos.all) {
			info.status.discovered = true;
			info.status.hasPhoto = true;
			info.status.hasClosePhoto = true;
		}
		_main.setScreen(new PlanetsScreen(null, null));
	}

	private function onOpenAll(event:MouseEvent):Void {
		for (mission in _main.home.missions) {
			mission.opened = true;
		}
		_main.setScreen(new MissionsScreen());
	}

	override public function doOnResize():Void {
		_title.x = Std.int(_w / 2);

		_menu.x = _w / 2;
		_menu.y = _h;
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_menu.doOnKeyDown(keyCode);
	}

	private static var CHEAT_SAVE:String = "in_orbit_cheat_save";

	private function onSaveClick(event:MouseEvent):Void {
		var so = SharedObject.getLocal(Main.SHARED_OBJECT_FILE);
		var cheatSo = SharedObject.getLocal(CHEAT_SAVE);
		cheatSo.data.so = so.data;
		cheatSo.flush();
	}

	private function onLoadClick(event:MouseEvent):Void {
		var so = SharedObject.getLocal(Main.SHARED_OBJECT_FILE);
		var cheatSo = SharedObject.getLocal(CHEAT_SAVE);
		so.clear();
		var data = cheatSo.data.so;
		if (data != null) {
			for (p in Reflect.fields(data)) {
				Reflect.setField(so.data, p, Reflect.getProperty(data, p));
			}
		}
		so.flush();
	}
}
