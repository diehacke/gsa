package screens;

import flash.display.Sprite;
import utils.Tween;
import ui.WindowButton;
import flash.events.MouseEvent;
import game.Game;
import game.RocketStage;
import model.LinkModel;
import model.StageModel;
import model.StageModelIterator;
import model.StageModels;
import ui.UIRects;
import ui.ScreenTitle;
import ui.StageTile;
import ui.NextButton;
import ui.BackButton;
import ui.StageView;
import ui.TileList;
import ui.ShipView;
import ui.RocketView;

class RocketScreen extends Screen {
	public function new() {
		super();
	}

	private var _title:ScreenTitle;
	private var _back:BackButton;
	private var _start:NextButton;
	private var _reset:WindowButton;

	override public function doOn():Void {
		_title = new ScreenTitle();
		_title.text = "Rocket";
		_title.redraw();
		addChild(_title);
		_title.show();

		_back = new BackButton();
		_back.redraw();
		_back.addEventListener(MouseEvent.CLICK, onBackClick);
		addChild(_back);

		initStages();

		_start = new NextButton();
		_start.text = "Start";
		_start.redraw();
		_start.addEventListener(MouseEvent.CLICK, onStartClick);
		addChild(_start);

		_reset = new WindowButton();
		_reset.text = "Reset";
		_reset.redraw();
		_reset.addEventListener(MouseEvent.CLICK, onResetClick);
		addChild(_reset);

		if (_main.anyScreenShowed)
			_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
		_player.setMusic(Music.UI);

		updateResetButton(true);
	}

	override public function doOff():Void {
		setState(null);
		destroyStages();
	}

	private function onBackClick(event:MouseEvent):Void {
		_main.setScreen(new MissionsScreen());
	}

	private function onStartClick(event:MouseEvent):Void {
		_main.save();
		var mission = _main.home.selectedMission;
		_main.setScreen(new Game(mission, _rocketView.model, stage.frameRate, mission.tutorial));
	}

	private function onResetClick(event:MouseEvent):Void {
		_main.home.selectedMission.rocketModel = null;
		_rocketView.model = getRocket();
		_rocketView.redraw();
		updateOnRocketChange();
	}

	override public function doOnResize():Void {
		var g = graphics;
		g.clear();
		g.beginFill(0xeeeeee);
		g.drawRect(0, 0, _w, 80);
		g.endFill();
		g.beginFill(0xeeeeee);
		g.drawRect(0, _h - 80, _w, 80);
		g.endFill();

		_title.x = Std.int(_w / 2);

		_back.y = _h - _back.h;

		_start.x = _w;
		_start.y = _h - _start.h;

		_reset.x = _w - _reset.w - 75;
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_back.doOnKeyDown(keyCode);
		_start.doOnKeyDown(keyCode);
		_stagesList.doOnKeyDown(keyCode);
	}

	private var _stagesList:TileList<StageTile>;

	private var _rocketView:RocketView;
	public var rocketView(get, never):RocketView;
	private function get_rocketView():RocketView {
		return _rocketView;
	}

	private function getRocket():StageModel {
		var model = _main.home.selectedMission.rocketModel;
		if (model == null)
			model = _main.home.selectedMission.newDefaultRocketModel();
		model.calculateTree();
		fixRocketModel(model);
		return model;
	}

	private function initStages():Void {
		_stagesList = new TileList<StageTile>(_player);
		_stagesList.tileW = StageTile.WIDTH;
		_stagesList.tileH = StageTile.HEIGHT;
		_stagesList.gapY = 10;
		for (model in getModels()) {
			if (model.isShip)
				continue;
			var tile = new StageTile(model, getUsedCount);
			tile.mouseDown.add(onStageTileMouseDown);
			tile.redraw();
			_stagesList.add(tile);
		}
		_stagesList.h = _h - 80 - 80;
		_stagesList.redraw();
		_stagesList.x = _w - _stagesList.w;
		_stagesList.y = 80;
		addChild(_stagesList);

		var rocketModel = getRocket();

		var shipView = new ShipView(rocketModel);
		shipView.x = 0;
		shipView.y = 90;
		shipView.redraw();
		addChild(shipView);

		_rocketView = new RocketView(rocketModel, onStageViewCreate);
		_rocketView.x = _w / 2 - 40;
		_rocketView.y = 80;
		_rocketView.h = _h - 80;
		_rocketView.redraw();
		addChild(_rocketView);

		setState(new RSDefaultState());
		stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);

		_stagesList.redrawTiles();
	}

	private function getModels():Array<StageModel> {
		var stages = new Array<StageModel>();
		for (model in StageModels.models) {
			if (model.count > 0)
				stages.push(model);
		}
		for (model in StageModels.models) {
			if (model.count <= 0 && model.visible)
				stages.push(model);
		}
		return stages;
	}

	private function onStageViewCreate(view:StageView):Void {
		view.rollOver.add(onStageViewRollOver);
		view.rollOut.add(onStageViewRollOut);
		view.mouseDown.add(onStageViewMouseDown);
	}

	private function onStageViewRollOver(view:StageView):Void {
		_state.doOnStageOver(view);
	}

	private function onStageViewRollOut(view:StageView):Void {
		_state.doOnStageOut(view);
	}

	private function onStageViewMouseDown(view:StageView):Void {
		_state.doOnStageMouseDown(view);
	}

	private function destroyStages():Void {
		stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
	}

	private function onStageTileMouseDown(tile:StageTile):Void {
		if (!tile.model.getLocked() && tile.model.count - _rocketView.model.getUsedCount(tile.model) > 0)
			_state.doStageTileMouseDown(tile);
	}

	private function onMouseUp(event:MouseEvent):Void {
		_state.doMouseUp();
	}

	override public function doOnEnterFrame():Void {
		_state.doOnEnterFrame();
	}

	private var _state:RSState;

	public function setState(state:RSState):Void {
		if (_state != state) {
			if (_state != null) {
				_state.doOff();
			}
			_state = state;
			if (_state != null) {
				_state.init(this);
				_state.doOn();
				_state.doOnEnterFrame();
			}
		}
	}

	private function getUsedCount(model:StageModel):Int {
		return _rocketView != null ? _rocketView.model.getUsedCount(model) : 0;
	}

	public function updateOnRocketChange():Void {
		_main.home.selectedMission.rocketModel = _rocketView.model;
		_main.save();
		_stagesList.redrawTiles();
		updateResetButton(false);
		_player.playSound(Assets.unlink_wav, .3);
	}

	private function updateResetButton(immediate:Bool):Void {
		var active = !_main.home.selectedMission.newDefaultRocketModel().equalTo(_rocketView.model);
		if (immediate) {
			_reset.y = active ? .0 : -_reset.height;
		} else {
			Tween.to(_reset, 200, {y:active ? .0 : -_reset.height}).setEase(Tween.easeOut);
		}
	}

	private function fixRocketModel(rocketModel:StageModel):Void {
		var countOf = new Map<String, Int>();
		for (model in StageModels.models) {
			countOf[model.name] = model.count;
		}
		for (link in rocketModel.links) {
			fixRocketModelRecursive(link, countOf);
		}
	}

	private function fixRocketModelRecursive(link:LinkModel, countOf:Map<String, Int>):Void
	{
		if (link.child == null)
			return;
		if (countOf[link.child.name] > 0) {
			countOf[link.child.name] = countOf[link.child.name] - 1;
			for (linkI in link.child.links) {
				fixRocketModelRecursive(linkI, countOf);
			}
		} else {
			link.child = null;
		}
	}
}
