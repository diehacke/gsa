package screens;

import flash.events.Event;
import ui.UIRects;
import flash.net.URLRequest;
import flash.Lib;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.text.TextField;
import ui.Widget;
import flash.display.Sprite;
import flash.display.Bitmap;
import flash.errors.Error;
import ui.SmartFormat;
import ui.BackButton;
import ui.ScreenTitle;
import haxe.Json;
import flash.ui.Keyboard;
import utils.KeyUtil;
import ui.ScrollBar;
import ui.UICommon;
import model.CreditsData;

class CreditsScreen extends Screen {
	public function new() {
		super();
	}

	private var _title:ScreenTitle;
	private var _content:Widget;
	private var _sb:ScrollBar;
	private var _back:BackButton;
	private var _bt:Bitmap;
	private var _btOwner:Sprite;

	override public function doOn():Void {
		_title = new ScreenTitle();
		_title.text = "Credits";
		_title.redraw();
		addChild(_title);
		_title.show();

		_content = new Widget();
		addChild(_content);

		var offsetY = .0;

		var tf = new SmartFormat("Tahoma", 16).setColor(0x000000).setBold(true).newAutoSized();
		tf.text = "Built " + StartHelper.getBuildDate();
		tf.x = 10;
		tf.y = offsetY + 5;
		_content.addChild(tf);
		offsetY += 20;

		var tf = new SmartFormat("Tahoma", 16).setColor(0x000000).setBold(true).newAutoSized();
		tf.text = "Using assets";
		tf.x = 10;
		tf.y = offsetY + 5;
		_content.addChild(tf);
		offsetY += 40;
		for (item in CreditsData.data) {
			var icon = new Bitmap();
			if (item.type == "sound") {
				icon.bitmapData = Assets.credit_type_0001_png;
			} else if (item.type == "code") {
				icon.bitmapData = Assets.credit_type_0002_png;
			}
			icon.transform.colorTransform = UICommon.BLUE_TRANSFORM;
			icon.x = 10;
			icon.y = offsetY;
			_content.addChild(icon);

			var tf = new SmartFormat("Tahoma", 14).setColor(UICommon.BLUE).setBold(true).newAutoSized(true);
			tf.text = item.asset;
			tf.x = 45;
			tf.y = offsetY + 5;
			_content.addChild(tf);

			var offsetX = tf.x + tf.width + 10;
			if (item.licenceName != null && item.licenceName != "") {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).newAutoSized(true);
				tf.text = "[ " + item.licenceName + " ]";
				tf.x = offsetX;
				tf.y = offsetY + 5;
				_content.addChild(tf);
				offsetX += tf.width;
			}
			if (item.changes != null && item.changes != "" && item.changes != "origin") {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).newAutoSized(true);
				tf.text = "[ " + item.changes + " ]";
				tf.x = offsetX;
				tf.y = offsetY + 5;
				_content.addChild(tf);
			}
			offsetY += 30;

			if (item.name != null && item.name != "") {
				var tf = new SmartFormat("Tahoma", 14).setColor(UICommon.BLUE).newAutoSized(true);
				tf.text = item.name;
				tf.x = 10;
				tf.y = offsetY;
				_content.addChild(tf);
				offsetY += 20;
			}

			var offsetX = 10.;
			if (item.authorName != null && item.authorName != "") {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).newAutoSized(true);
				tf.text = "author: ";
				tf.x = offsetX;
				tf.y = offsetY;
				_content.addChild(tf);
				offsetX += tf.width;
			}
			if (item.authorName != null && item.authorName != "") {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).setBold(true).newAutoSized(true);
				tf.text = item.authorName;
				tf.x = offsetX;
				tf.y = offsetY;
				_content.addChild(tf);
				offsetY += 20;
			}
			for (authorSite in item.authorSites) {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).newAutoSized(true);
				tf.text = authorSite;
				tf.x = 30;
				tf.y = offsetY;
				tf.addEventListener(MouseEvent.CLICK, onLinkClick);
				_content.addChild(tf);
				offsetY += 20;
			}

			if (item.sources.length > 0) {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).newAutoSized();
				tf.text = "source:";
				tf.x = 10;
				tf.y = offsetY;
				_content.addChild(tf);
				offsetY += 20;
			}
			for (source in item.sources) {
				var tf = new SmartFormat("Tahoma", 14).setColor(0x000000).newAutoSized(true);
				tf.text = source;
				tf.x = 30;
				tf.y = offsetY;
				tf.addEventListener(MouseEvent.CLICK, onLinkClick);
				_content.addChild(tf);
				offsetY += 20;
			}

			offsetY += 10;
		}
		offsetY += 80;
		_content.h = offsetY;
		_content.addEventListener(MouseEvent.MOUSE_WHEEL, onContentMouseWheel);

		_sb = new ScrollBar();
		_sb.addEventListener(Event.CHANGE, onScrollBarChange);
		addChild(_sb);

		_back = new BackButton();
		_back.redraw();
		_back.addEventListener(MouseEvent.CLICK, onBackClick);
		addChild(_back);

		_btOwner = new Sprite();
		UIRects.scroll_bar_v_key_owner.draw(_btOwner.graphics, Assets.ui_png, 0, 0);
		addChild(_btOwner);

		_bt = new Bitmap(Assets.key_bt_png);
		_bt.x = -_bt.width - 2;
		_bt.y = -_bt.height - 2;
		_btOwner.addChild(_bt);

		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
		_player.setMusic(Music.UI);
	}

	private function onScrollBarChange(event:Event):Void {
		updateScroll();
	}

	override public function doOnResize():Void {
		_title.x = Std.int(_w / 2);

		_content.y = 80;
		_sb.h = _h - 80;
		_sb.contentSize = _content.h;
		_sb.redraw();
		_sb.x = _w - _sb.w;
		_sb.y = 80;
		updateScroll();

		_back.y = _h - _back.h;

		_btOwner.x = Std.int(_w);
		_btOwner.y = 80;

		var g = graphics;
		g.clear();
		g.beginFill(0xeeeeee);
		g.drawRect(0, 0, _w, 80);
		g.endFill();
		g.beginFill(0x808080);
		g.drawRect(_bt.x - 2, _bt.y - 2, _bt.width + 4, _bt.height + 4);
		g.endFill();

		var g = _content.graphics;
		g.clear();
		g.beginFill(0xffffff);
		g.drawRect(0, 0, _w, _content.h);
		g.endFill();
	}

	private function updateScroll():Void {
		_content.scrollRect = new Rectangle(0, _sb.value, _w, _h - 80);
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_back.doOnKeyDown(keyCode);
		if (keyCode == Keyboard.UP || keyCode == KeyUtil.K || keyCode == KeyUtil.W)
			scroll(-3);
		if (keyCode == Keyboard.DOWN || keyCode == KeyUtil.J || keyCode == KeyUtil.S)
			scroll(3);
	}

	private function scroll(delta:Int):Void {
		_sb.value += delta * 30;
		_sb.redraw();
		updateScroll();
	}

	private function onBackClick(event:MouseEvent):Void {
		_main.setScreen(new MenuScreen());
	}

	private function onLinkClick(event:MouseEvent):Void {
		var tf:TextField = cast(event.target, TextField);
		Lib.getURL(new URLRequest(tf.text));
	}

	private function onContentMouseWheel(event:MouseEvent):Void {
		scroll(-event.delta);
	}
}
