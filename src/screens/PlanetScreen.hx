package screens;

import flash.Vector;
import ui.NextButton;
import flash.text.TextField;
import flash.events.MouseEvent;
import flash.display.Bitmap;
import flash.display.BitmapData;
import game.World;
import game.WorldSystem;
import game.Game;
import game.Planet;
import game.PlanetInfos;
import ui.BackButton;
import ui.PlanetParameter;
import ui.ScreenTitle;
import ui.SmartFormat;
import ui.UICommon;
import utils.StringUtil;

class PlanetScreen extends Screen {
	private var _system:WorldSystem;
	private var _planet:Planet;
	private var _game:Game;
	private var _getBd:Planet->BitmapData;

	public function new(system:WorldSystem, planet:Planet, getBd:Planet->BitmapData, game:Game) {
		super();
		_system = system;
		_planet = planet;
		_game = game;
		_getBd = getBd;
	}

	private var _title:ScreenTitle;
	private var _back:BackButton;
	private var _next:NextButton;
	private var _bitmap:Bitmap;
	private var _surface:Bitmap;
	private var _leftParameters:Vector<PlanetParameter>;
	private var _rightParameters:Vector<PlanetParameter>;
	private var _notLandedYet:TextField;

	override public function doOn():Void {
		_title = new ScreenTitle();
		_title.text = _planet.name;
		_title.redraw();
		addChild(_title);
		_title.show();

		_bitmap = new Bitmap(_getBd(_planet));
		addChild(_bitmap);

		_surface = new Bitmap(_planet.surface);
		_surface.visible = _planet.status.hasLandingPhoto;
		addChild(_surface);

		_back = new BackButton();
		_back.redraw();
		_back.addEventListener(MouseEvent.CLICK, onBackClick);
		addChild(_back);

		_next = new NextButton();
		_next.text = "Next";
		_next.redraw();
		_next.addEventListener(MouseEvent.CLICK, onNextClick);
		addChild(_next);

		_leftParameters = new Vector<PlanetParameter>();
		_rightParameters = new Vector<PlanetParameter>();

		_leftParameters[0] = new PlanetParameter(true);
		_leftParameters[0].title = "Mass";
		_leftParameters[0].value = _planet.m + "";
		_leftParameters[0].redraw();
		_leftParameters[0].show(100);
		addChild(_leftParameters[0]);

		_leftParameters[1] = new PlanetParameter(true);
		_leftParameters[1].title = "Surface acceleration";
		_leftParameters[1].value =
			StringUtil.fixed2(World.getAcceleration(_planet.m, _planet.radius) / World.GLORY_G) + " g";
		_leftParameters[1].redraw();
		_leftParameters[1].show(120);
		addChild(_leftParameters[1]);

		_leftParameters[2] = new PlanetParameter(true);
		_leftParameters[2].title = "Second velocity";
		_leftParameters[2].value = Math.round(World.getSecondVelocity(_planet.m, _planet.radius) * .1) + "";
		_leftParameters[2].redraw();
		_leftParameters[2].show(140);
		addChild(_leftParameters[2]);

		_rightParameters[0] = new PlanetParameter(false);
		_rightParameters[0].title = PlanetInfos.arial.name + " remoteness";
		_rightParameters[0].value = StringUtil.fixed2(_planet.r.length() / _system.glory.r.length()) + " a.u.";
		_rightParameters[0].redraw();
		_rightParameters[0].show(100);
		addChild(_rightParameters[0]);

		if (_planet.status.hasClosePhoto) {
			_rightParameters[1] = new PlanetParameter(false);
			_rightParameters[1].title = "Diameter";
			_rightParameters[1].value = StringUtil.fixed2(_planet.radius / _system.glory.radius) + " r.u.";
			_rightParameters[1].redraw();
			_rightParameters[1].show(120);
			addChild(_rightParameters[1]);

			_rightParameters[2] = new PlanetParameter(false);
			_rightParameters[2].title = "Atmosphere height";
			_rightParameters[2].value = Math.round(Math.max(0, _planet.atmRadius - _planet.radius)) + "";
			_rightParameters[2].redraw();
			_rightParameters[2].show(140);
			addChild(_rightParameters[2]);
		}

		_notLandedYet = new SmartFormat("Tahoma", 16, 0xffffff).setBold(true).setFilters(UICommon.BLACK_FILTERS)
			.newAutoSized();
		_notLandedYet.text = "No one landing was yet";
		_notLandedYet.visible = !_planet.status.hasLandingPhoto;
		addChild(_notLandedYet);

		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
		_player.setMusic(Music.UI);
	}

	override public function doOnResize():Void {
		_title.x = Std.int(_w * .5);

		_back.y = _h - _back.h;

		_next.x = _w;
		_next.y = _h - _next.h;

		_bitmap.x = Std.int(_w * .5 - _bitmap.width * .5);
		_bitmap.y = 90;

		_surface.width = _w;
		_surface.height = _h * .8;
		_surface.y = _h - _surface.height * .85;

		var offsetY = 100.;
		for (parameter in _leftParameters) {
			parameter.y = offsetY;
			offsetY += parameter.h + 10;
		}

		var offsetY = 100.;
		for (parameter in _rightParameters) {
			parameter.x = _w;
			parameter.y = offsetY;
			offsetY += parameter.h + 10;
		}

		_notLandedYet.x = Std.int(_w * .5 - _notLandedYet.width * .5);
		_notLandedYet.y = Std.int(_h - _notLandedYet.height - 10);

		var g = graphics;
		g.clear();
		g.beginFill(0xeeeeee);
		g.drawRect(0, 0, _w, 80);
		g.endFill();
		g.beginBitmapFill(Assets.stars_jpg);
		g.drawRect(0, 80, _w, _h - 80);
		g.endFill();
	}

	private function onBackClick(event:MouseEvent):Void {
		_main.setScreen(new PlanetsScreen(_planet.status, _game));
	}

	private function onNextClick(event:MouseEvent):Void {
		var planets = new Array<Planet>();
		for (unit in _system.units) {
			if (unit != _system.star && unit.asPlanet != null && unit.asPlanet.status.discovered) {
				planets.push(unit.asPlanet);
			}
		}
		var planet = planets[(planets.indexOf(_planet) + 1) % planets.length];
		_main.setScreen(new PlanetScreen(_system, planet, _getBd, _game));
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		_back.doOnKeyDown(keyCode);
		_next.doOnKeyDown(keyCode);
	}
}
