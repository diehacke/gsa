package ui;

import flash.geom.ColorTransform;
import flash.filters.BitmapFilter;
import flash.filters.ColorMatrixFilter;
import flash.filters.GlowFilter;

class UICommon {
	public function new() {
	}

	public static var BLUE:Int = 0x5070ee;
	public static var BLUE_TRANSFORM:ColorTransform =new ColorTransform(
		0x50 / 0x100, 0x70 / 0x100, 0xee / 0x100);

	public static var GOLD:Int = 0xffee30;
	public static var GOLD_FILTERS:Array<BitmapFilter> = [new GlowFilter(0x303000, .5)];

	public static var BLACK_FILTERS:Array<BitmapFilter> = [new GlowFilter(0x000000, .5)];

	public static var GRAY_FILTERS:Array<BitmapFilter> = [new ColorMatrixFilter([
		.3, .4, .3, 0, 0,
		.3, .4, .3, 0, 0,
		.3, .4, .3, 0, 0,
		0, 0, 0, 1, 0
	])];
}
