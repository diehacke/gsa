package ui;

import flash.display.Shape;
import flash.geom.Matrix;
import flash.display.BitmapData;
import game.Planet;
import game.RocketStage;
import flash.events.Event;
import ui.UIRects;
import flash.display.MovieClip;
import flash.text.TextField;
import model.StageModel;
import flash.events.MouseEvent;
import utils.StringUtil;
import utils.Signal;

class PlanetTile extends Tile {
	public static var WIDTH:Int = 150;
	public static var HEIGHT:Int = 145;
	public static var SELECTED_WIDTH:Int = 170;
	public static var SELECTED_HEIGHT:Int = 165;

	public var mouseDown:Signal<PlanetTile> = new Signal<PlanetTile>();
	public var click:Signal<PlanetTile> = new Signal<PlanetTile>();

	private var _title:TextField;
	private var _state:TextField;

	public function new(planet:Planet, bd:BitmapData) {
		super();
		_planet = planet;
		_bd = bd;
		_w = WIDTH;
		_h = HEIGHT;

		mouseChildren = false;

		_title = new SmartFormat("Tahoma", 16, _planet.status.discovered ? 0xffffff : 0x808080)
			.setCenter()
			.setBold(true)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		addChild(_title);

		var format;
		if (_planet.status.discovered) {
			format = new SmartFormat("Tahoma", 14, 0xffffff).setBold(true).setCenter()
				.setFilters(UICommon.BLACK_FILTERS);
		} else {
			format = new SmartFormat("Tahoma", 16, 0x808080).setBold(true).setCenter()
				.setFilters(UICommon.BLACK_FILTERS);
		}
		_state = format.newAutoSized();
		addChild(_state);

		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.CLICK, onClick);
	}
	
	private var _planet:Planet;
	public var planet(get, never):Planet;
	private function get_planet():Planet {
		return _planet;
	}

	private var _bd:BitmapData;
	public var bd(get, never):BitmapData;
	private function get_bd():BitmapData {
		return _bd;
	}

	override public function redraw():Void {
		_title.text = _planet.status.discovered ? _planet.name : "";
		_title.x = Std.int(_w * .5 - _title.width * .5);

		if (_planet.status.discovered) {
			_state.text = _planet.status.hasPhoto ? "" : "Not yet explored";
			_state.y = _h - _state.height;
		} else {
			_state.text = "Undiscovered";
			_state.y = _h * .5 - _state.height * .5;
		}
		_state.x = Std.int(_w * .5 - _state.width * .5);

		updateSelected();
	}

	private function onMouseDown(event:MouseEvent):Void {
		mouseDown.dispatch(this);
	}

	private function onClick(event:MouseEvent):Void {
		click.dispatch(this);
	}

	private static var _matrix:Matrix = new Matrix();

	override private function updateSelected():Void {
		var g = graphics;
		g.clear();
		var headerH = 22;
		if (_planet.status.discovered) {
			g.beginFill(0x888888);
			g.drawRect(0, 0, _w, headerH);
			g.endFill();
			g.beginFill(0xffffff, .2);
			g.drawRect(0, headerH, _w, _h - headerH);
			g.endFill();
		} else {
			g.beginFill(0xffffff, .2);
			g.drawRect(0, 0, _w, _h);
			g.endFill();
		}

		_matrix.tx = (SELECTED_WIDTH-_bd.width) * .5 + (_w - SELECTED_WIDTH) * .5 -
			Math.min(0, _w * .5 - _planet.radius - 20);
		_matrix.ty = (SELECTED_HEIGHT-_bd.height) * .5 + _h - SELECTED_HEIGHT + headerH;
		g.beginBitmapFill(_bd, _matrix);
		g.drawRect(0, headerH, _w, _h - headerH);
		g.endFill();

		if (_planet.status.discovered && !_planet.status.hasPhoto) {
			g.beginFill(0x808080);
			g.drawRect(0, _h - 20, _w, 20);
			g.endFill();
		}

		if (_selected) {
			var offset = 5;
			var bevel = 6;
			var x0 = -offset;
			var x1 = _w + offset;
			var y0 = -offset;
			var y1 = _h + offset;
			g.lineStyle(4, 0x888888);
			g.moveTo(x0 + bevel, y0);
			g.lineTo(x1 - bevel, y0);
			g.lineTo(x1, y0 + bevel);
			g.lineTo(x1, y1 - bevel);
			g.lineTo(x1 - bevel, y1);
			g.lineTo(x0 + bevel, y1);
			g.lineTo(x0, y1 - bevel);
			g.lineTo(x0, y0 + bevel);
			g.lineTo(x0 + bevel, y0);
		}
	}
}
