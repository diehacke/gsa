package ui;

import flash.display.Sprite;
import model.StageModelIterator;
import model.StageModel;

class RocketView extends Widget {
	private var _view:RocketPartView;
	public var partView(get, never):RocketPartView;
	private function get_partView():RocketPartView {
		return _view;
	}

	public var model(get, set):StageModel;
	private function get_model():StageModel {
		return _view.model;
	}
	private function set_model(value:StageModel):StageModel {
		if (_view.model != value) {
			removeChild(_view);
			_view = new RocketPartView(value, _onViewCreate);
			addChild(_view);
		}
		return _view.model;
	}

	private var _onViewCreate:StageView->Void;

	public function new(model:StageModel, onViewCreate:StageView->Void) {
		super();
		_onViewCreate = onViewCreate;
		_view = new RocketPartView(model, _onViewCreate);
		addChild(_view);
	}

	public function getView(model:StageModel):StageView {
		return _view.getView(model);
	}

	override public function redraw():Void {
		_view.redraw();
		_view.y = _h / 2 - _view.nozzlesH / 2;
		var maxPropH = UIRects.prop_normal.h - 4;
		if (_view.y < _h - maxPropH - _view.nozzlesH)
			_view.y = _h - maxPropH - _view.nozzlesH;
		_view.x = Math.round(_view.x);
		_view.y = Math.round(_view.y);
		var g = _view.graphics;
		g.clear();
		UIRects.prop_normal.draw(g, Assets.ui_png, 0, _view.nozzlesH);
	}

	public function removeStage(model:StageModel):Bool {
		if (model.parent == null)
			return false;
		model.parent.child = null;
		redraw();
		return true;
	}
}
