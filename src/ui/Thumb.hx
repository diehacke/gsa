package ui;

import flash.text.TextField;
import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import utils.KeyUtil;

class Thumb extends AButton {
	private var _color:Int;

	public function new() {
		super();
	}

	override private function init():Void {
		_w = UIRects.thumb_v_up.w;
		_h = UIRects.thumb_v_up.h;
	}

	override public function redraw():Void {
		var g = graphics;
		g.clear();
		var indent = 5;
		if (_isOver && !_isDown)
			UIRects.thumb_v_over.drawUnoffsettedVertical(g, Assets.ui_png, -16, indent, 20, 20, _h - indent * 2);
		else
			UIRects.thumb_v_up.drawUnoffsettedVertical(g, Assets.ui_png, -16, indent, 20, 20, _h - indent * 2);
	}

	override public function updateState():Void {
		redraw();
	}
}
