package ui;

import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.geom.ColorTransform;

class SoundButton extends AButton {
	private var _isMusic:Bool;

	public function new(isMusic:Bool) {
		_isMusic = isMusic;
		super();
		_w = 36;
		_h = 36;
	}

	override public function updateState():Void {
		var g = graphics;
		g.clear();
		var normal = _isMusic ? UIRects.sound_button_2_normal : UIRects.sound_button_1_normal;
		var over = _isMusic ? UIRects.sound_button_2_over : UIRects.sound_button_1_over;
		var selected = _isMusic ? UIRects.sound_button_2_selected : UIRects.sound_button_1_selected;
		var selectedOver = _isMusic ? UIRects.sound_button_2_selected_over : UIRects.sound_button_1_selected_over;
		if (_selected) {
			if (_isOver)
				selectedOver.drawWithoutTolerance(g, Assets.ui_png, 0, 0);
			else
				selected.drawWithoutTolerance(g, Assets.ui_png, 0, 0);
		} else {
			if (_isOver)
				over.drawWithoutTolerance(g, Assets.ui_png, 0, 0);
			else
				normal.drawWithoutTolerance(g, Assets.ui_png, 0, 0);
		}
	}
}
