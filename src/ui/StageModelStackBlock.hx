package ui;

import model.StageModelStack;
import flash.text.TextField;

class StageModelStackBlock extends Widget {
	private var _title:TextField;
	private var _tiles:Array<StageCountedTile>;

	public function new(stack:StageModelStack, title:String) {
		super();

		_title = new SmartFormat("Tahoma", 16, 0xffffff).setBold(true)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		_title.text = title;
		addChild(_title);

		_tiles = new Array<StageCountedTile>();
		while (stack != null) {
			var tile = new StageCountedTile(stack.model, stack.count);
			tile.redraw();
			_tiles.push(tile);
			addChild(tile);
			stack = stack.next;
		}
	}

	override public function redraw():Void {
		var g = graphics;
		g.clear();
		g.beginFill(0x808080);
		g.drawRect(0, 0, _w, _h);
		g.endFill();

		_title.x = Std.int(_w * .5 - _title.width * .5);

		_h = 0;
		var w = .0;
		for (tile in _tiles) {
			w += tile.w;
		}
		var offsetX = _w * .5 - w * .5;
		var offsetY = 20;
		for (tile in _tiles) {
			tile.y = offsetY;
			tile.x = Std.int(offsetX);
			offsetX += tile.w;
			if (_h < tile.h + offsetY)
				_h = tile.h + offsetY;
		}
	}
}
