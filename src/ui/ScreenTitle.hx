package ui;

import flash.text.TextField;
import utils.Tween;
import flash.display.Sprite;
import ui.UIRects;
import flash.display.Shape;

class ScreenTitle extends Widget {
	private var _container:Sprite;
	private var _tf:TextField;

	public function new() {
		super();

		_container = new Sprite();
		addChild(_container);

		_tf = new SmartFormat("Arial", 30).setBold(true).setColor(UICommon.BLUE).newAutoSized();
		_container.addChild(_tf);

		_container.y = -UIRects.screen_title_normal.h;
	}

	public var text(get, set):String;
	private function get_text():String {
		return _tf.text;
	}
	private function set_text(value:String):String {
		_tf.text = value;
		return value;
	}

	override public function redraw():Void {
		var offset = 42.;
		_w = UIRects.screen_title_normal.w;
		_h = UIRects.screen_title_normal.h;
		_tf.x = Std.int(-_tf.width / 2);
		_tf.y = Std.int(_h * .5 - _tf.height * .5 - 3);

		var g = _container.graphics;
		g.clear();
		UIRects.screen_title_normal.draw(g, Assets.ui_png, Std.int(-offset - (_w - offset) / 2), 0);
	}

	public function show():Void {
		Tween.to(_container, 150, {y:0}).setEase(Tween.easeInOut);
	}
}
