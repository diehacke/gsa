package ui;

import ui.Widget;
import utils.MathUtil;
import utils.Tween;

class TileSelector<T:Tile> extends Widget {
	private var _player:Player;

	public function new(player:Player) {
		super();
		_player = player;
		_tiles = new Array<T>();
		numCols = 10;
		tileW = 100;
		tileH = 100;
		selectedTileW = 200;
		selectedTileH = 200;
		gapX = 10;
		gapY = 10;
	}

	public var numCols:Int;
	public var tileW:Int;
	public var tileH:Int;
	public var selectedTileW:Int;
	public var selectedTileH:Int;
	public var gapX:Int;
	public var gapY:Int;
	public var swap:Bool;

	private var _tiles:Array<T>;
	public var tiles(get, never):Array<T>;
	private function get_tiles():Array<T> {
		return _tiles;
	}

	public function getTile(col:Int, row:Int):T {
		return _tiles[row * numCols + col];
	}

	public function getCol(tile:T):Int {
		return colOfIndex(_tiles.indexOf(tile));
	}

	public function getRow(tile:T):Int {
		return rowOfIndex(_tiles.indexOf(tile));
	}

	public function colOfIndex(index:Int):Int {
		return index >= 0 && index < _tiles.length ? index % numCols : -1;
	}

	public function rowOfIndex(index:Int):Int {
		return index >= 0 && index < _tiles.length ? Math.floor(index / numCols) : -1;
	}

	public function clear():Void {
		for (tile in _tiles) {
			removeChild(tile);
		}
		_tiles = new Array<T>();
	}

	public function add(tile:T):Void {
		_tiles.push(tile);
		addChild(tile);
	}

	override public function redraw():Void {
		_w = realNumCols * (tileW + gapX) - (realNumCols > 0 ? gapX : 0);
		_h = realNumRows * (tileH + gapY) - (realNumRows > 0 ? gapY : 0);
		updatePositions(true);
	}

	private function updatePositions(immediate:Bool):Void {
		var selectedCol = 0;
		var selectedRow = 0;
		if (_selected != null) {
			var index = _tiles.indexOf(_selected);
			selectedCol = colOfIndex(index);
			selectedRow = rowOfIndex(index);
		}
		var rnc = realNumCols;
		var wrap = rnc >= 4 && swap;
		var wrapBefore = selectedCol >= 2;
		for (i in 0 ... _tiles.length) {
			var tile = _tiles[i];
			var x = xOfIndex(i);
			var y = yOfIndex(i);
			var w = tileW;
			var h = tileH;
			var w05 = (tileW + gapX) * .5;
			var h05 = (tileH + gapY) * .5;
			if (_selected != null) {
				var col = colOfIndex(i);
				var row = rowOfIndex(i);
				if (tile != _selected) {
					var dCol = MathUtil.sign(col - selectedCol);
					var dRow = MathUtil.sign(row - selectedRow);
					var offsetX = (selectedTileW - tileW) * .5 * dCol;
					var offsetY = (selectedTileH - tileH) * .5 * dRow;
					if (dRow == 0) {
						if (wrap && wrapBefore) {
							if (col == 0) {
								offsetX += w05;
								offsetY = -h05;
							} else {
								offsetX -= w05;
								offsetY = h05;
							}
						} else if (wrap && !wrapBefore) {
							if (col == rnc - 1) {
								offsetX -= w05;
								offsetY = -h05;
							} else {
								offsetX += w05;
								offsetY = h05;
							}
						}
					} else {
						offsetX = 0;
					}
					x += offsetX;
					y += offsetY;
					w = tileW;
					h = tileH;
				} else {
					if (wrap && wrapBefore) {
						x -= w05;
					} else if (wrap && !wrapBefore) {
						x += w05;
					}
					w = selectedTileW;
					h = selectedTileH;
				}
			}
			var vars = {
				x:x + tileW * .5 - w * .5,
				y:y + tileH * .5 - h * .5,
				w:w,
				h:h
			};
			if (immediate) {
				Tween.apply(tile, vars);
			} else {
				Tween.to(tile, 200, vars).setEase(Tween.easeOut).setVoidOnUpdate(tile.redraw);
			}
		}
	}

	public var realNumCols(get, never):Int;
	private function get_realNumCols():Int {
		return Math.round(Math.min(numCols, _tiles.length));
	}

	public var realNumRows(get, never):Int;
	private function get_realNumRows():Int {
		return Math.ceil(_tiles.length / numCols);
	}

	private function xOfIndex(index:Int):Float {
		return (index % numCols) * (tileW + gapX);
	}

	private function yOfIndex(index:Int):Float {
		return Math.floor(index / numCols) * (tileH + gapY);
	}

	private function indexOf(col:Int, row:Int):Int {
		return row * numCols + col;
	}

	private var _selected:T;
	public var selected(get, set):T;
	private function get_selected():T {
		return _selected;
	}
	private function set_selected(value:T):T {
		if (_selected != value) {
			if (_selected != null)
				_player.playSound(Assets.tap_on_m_sapphire_8662_hifi_wav);
			if (_selected != null)
				_selected.selected = false;
			_selected = value;
			if (_selected != null) {
				_selected.selected = true;
				addChild(_selected);
				updatePositions(false);
			}
		}
		return _selected;
	}
}
