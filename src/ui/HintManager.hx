package ui;

class HintManager {
	private var _owner:Widget;
	private var _hints:Array<Hint>;
	private var _offsetY:Float;

	public function new(owner:Widget) {
		_owner = owner;
		_hints = new Array<Hint>();
	}

	public function show(text:String) {
		var lastHint = _hints.length > 0 ? _hints[_hints.length - 1] : null;
		if (lastHint != null && lastHint.text == text) {
			lastHint.show(_owner, _owner.w / 2, lastHint.y + lastHint.h / 2);
			return;
		}
		var hint = new Hint();
		hint.text = text;
		hint.onRemove = onHintRemove;
		var y = _owner.h / 2;
		if (lastHint != null)
			y = lastHint.y + lastHint.h * 1.5 + 10;
		_hints.push(hint);
		hint.show(_owner, _owner.w / 2, y);
		update();
	}

	private function onHintRemove(hint:Hint):Void {
		_hints.remove(hint);
		update();
	}

	private function update():Void {
		var h = 0.;
		for (i in 0 ... _hints.length) {
			var hint = _hints[i];
			h += hint.h + 10;
		}
		if (h > 10)
			h -= 10;
		var y = _owner.h / 2 - h / 2;
		for (i in 0 ... _hints.length) {
			var hint = _hints[i];
			hint.targetY = y;
			y += hint.h + 10;
		}
	}

	public function doOnEnterFrame():Void {
		for (i in 0 ... _hints.length) {
			var hint = _hints[i];
			hint.y += (hint.targetY - hint.y) * .05;
		}
	}
}