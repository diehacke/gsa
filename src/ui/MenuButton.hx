package ui;

import flash.text.TextField;
import flash.display.BitmapData;
import flash.display.Bitmap;

class MenuButton extends AButton {
	private var _tf:TextField;
	private var _color:Int;

	public function new() {
		super();
	}

	override private function init():Void {
		_w = UIRects.menu_button_up.w;
		_h = UIRects.menu_button_up.h;

		_tf = new SmartFormat("Arial", 26).setBold(true).setColor(0xffffff)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		addChild(_tf);
	}

	public var text(get, set):String;
	private function get_text():String {
		return _tf.text;
	}
	private function set_text(value:String):String {
		_tf.text = value;
		return _tf.text;
	}

	private var _offset:Float;

	override public function redraw():Void {
		_tf.x = _offset + _w / 2 - _tf.width / 2 - 10;
		_tf.y = _offset + _h / 2 - _tf.height / 2;

		var g = graphics;
		g.clear();
		if (_selected) {
			if (_isDown)
				UIRects.menu_button_selected_down.draw(g, Assets.ui_png, 0, 0);
			else if (_isOver)
				UIRects.menu_button_selected_over.draw(g, Assets.ui_png, 0, 0);
			else
				UIRects.menu_button_selected.draw(g, Assets.ui_png, 0, 0);
		} else {
			if (_isDown)
				UIRects.menu_button_down.draw(g, Assets.ui_png, 0, 0);
			else if (_isOver)
				UIRects.menu_button_over.draw(g, Assets.ui_png, 0, 0);
			else
				UIRects.menu_button_up.draw(g, Assets.ui_png, 0, 0);
		}
	}

	override public function updateState():Void {
		_offset = _isDown ? 2 : 0;
		redraw();
	}
}
