package ui;
import flash.display.BitmapData;
import flash.geom.ColorTransform;
import flash.geom.Matrix;

class DigitsPanel extends Widget {
	private static var DIGIT_W:Int = 32;
	private static var DIGIT_H:Int = 51;

	private var _bd:BitmapData;

	public function new() {
		super();
		_bd = Assets.digits_png;
		_text = "";
		updateText();
		transform.colorTransform = new ColorTransform(1, .2, 0);
	}

	private var _text:String;
	public var text(get, set):String;
	private function get_text():String {
		return _text;
	}
	private function set_text(value:String):String {
		if (_text != value) {
			_text = value;
			updateText();
		}
		return _text;
	}

	private function /**/updateText():Void {
		_w = _text.length * DIGIT_W;
		_h = DIGIT_H;
		var g = graphics;
		g.clear();
		g.beginFill(0x000000, .5);
		g.drawRect(0, 0, _w, _h);
		g.endFill();
		for (i in 0 ... _text.length) {
			var index = switch (_text.charAt(i)) {
				case '0': 0;
				case '1': 1;
				case '2': 2;
				case '3': 3;
				case '4': 4;
				case '5': 5;
				case '6': 6;
				case '7': 7;
				case '8': 8;
				case '9': 9;
				case ':': 10;
				case '-': 12;
				default: 11;
			}
			g.beginBitmapFill(_bd, new Matrix(1, 0, 0, 1, (i - index) * DIGIT_W, 0), false);
			g.drawRect(i * DIGIT_W, 0, DIGIT_W, DIGIT_H);
			g.endFill();
		}
	}
}
