package ui;

import flash.display.Shape;
import game.RocketStage;
import flash.display.MovieClip;
import flash.text.TextField;
import model.StageModel;
import flash.events.MouseEvent;
import ui.Widget;
import ui.SmartFormat;
import ui.ParameterLine;
import utils.StringUtil;

class ShipView extends Widget {
	private var _tf:TextField;
	private var _parameters:Array<ParameterLine>;
	private var _mc:MovieClip;
	private var _shape:Shape;

	public function new(model:StageModel) {
		super();
		_model = model;

		doubleClickEnabled = true;
		mouseChildren = false;

		_shape = new Shape();
		_shape.alpha = .47;
		addChild(_shape);

		_mc = model.info.newMc();
		RocketStage.setFrame(_mc, false, true, true);
		addChild(_mc);

		_tf = new SmartFormat("Tahoma", 16, 0xffffff).setBold(true)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		addChild(_tf);

		_parameters = new Array<ParameterLine>();
		for (i in 0 ... 6) {
			var parameter = new ParameterLine();
			addChild(parameter);
			_parameters.push(parameter);
		}
	}
	
	private var _model:StageModel;
	public var model(get, never):StageModel;
	private function get_model():StageModel {
		return _model;
	}

	override public function redraw():Void {
		_w = 200;

		_mc.x = _w  / 2;
		_mc.y = 100;

		_tf.text = _model.name + "";
		_tf.x = 30;
		_tf.y = 4;

		var hasNozzles = _model.info.nozzles.length > 0;

		_parameters[0].title = "total m";
		_parameters[0].value = StringUtil.intX1000(_model.info.netM + _model.info.fuelM);

		_parameters[1].title = "net m";
		_parameters[1].value = StringUtil.intX1000(_model.info.netM);
		_parameters[1].visible = hasNozzles;

		_parameters[2].title = "fuel m";
		_parameters[2].value = StringUtil.intX1000(_model.info.fuelM);
		_parameters[2].visible = hasNozzles;

		_parameters[3].title = "fuel v";
		_parameters[3].value = StringUtil.fixed2(_model.info.fuelV);
		_parameters[3].visible = hasNozzles;

		_parameters[4].title = "fuel rate";
		_parameters[4].value = StringUtil.fixed2(_model.info.fuelRate);
		_parameters[4].visible = hasNozzles;

		_parameters[5].title = "max overload";
		_parameters[5].value = StringUtil.fixed2(_model.maxARatio) + " g";

		var offsetY = 148;
		for (parameter in _parameters) {
			if (parameter.visible) {
				parameter.w = _w - 25;
				parameter.x = 20;
				parameter.y = offsetY;
				parameter.redraw();
				offsetY += 20;
			}
		}
		_h = offsetY + 10;

		var g = graphics;
		g.clear();
		UIRects.ship_tile_normal.drawUnoffsettedVertical(g, Assets.ui_png, 0, 0, 40, 30, _h);

		var g = _shape.graphics;
		g.clear();
		g.beginBitmapFill(Assets.trails_png);
		g.drawRect(15, 148, _w - 15, _h - 148 - 7);
		g.endFill();
	}
}
