package ui;

import flash.display.Graphics;
import flash.text.TextField;
import ui.SmartFormat;
import utils.Tween;

class PlanetParameter extends Widget {
	public static var WIDTH:Float = 230;

	private var _left:Bool;
	private var _titleTf:TextField;
	private var _valueTf:TextField;

	public function new(left:Bool) {
		super();

		_left = left;

		_titleTf = new SmartFormat("Tahoma", 14, 0xffffff).setBold(true).newAutoSized();
		_titleTf.wordWrap = true;
		addChild(_titleTf);

		_valueTf = new SmartFormat("Tahoma", 14, 0xffffff).setBold(true).newAutoSized();
		addChild(_valueTf);

		_w = 10;
	}

	public var title(get, set):String;
	private function get_title():String {
		return _titleTf.text;
	}
	private function set_title(value:String):String {
		_titleTf.text = value;
		return _titleTf.text;
	}

	public var value(get, set):String;
	private function get_value():String {
		return _valueTf.text;
	}
	private function set_value(value:String):String {
		_valueTf.text = value;
		return _valueTf.text;
	}

	override public function redraw():Void {
		_titleTf.x = _left ? 10 : -_w + 10;
		_titleTf.width = WIDTH * .65 - 15;
		_h = _titleTf.height;

		_valueTf.x = _left ? _w * .65 + 4 : -_w * .35 - 4;

		var g = graphics;
		g.clear();
		drawBg(g, 0, 0, _left ? _w : -_w, _h);
	}

	private function drawBg(g:Graphics, x:Float, y:Float, w:Float, h:Float):Void {
		g.beginFill(0x888888);
		g.moveTo(x, y);
		g.lineTo(x + w * .95, y);
		g.lineTo(x + w, y + h * .5);
		g.lineTo(x + w * .95, y + h);
		g.lineTo(x, y + h);
		g.lineTo(x, y);
		g.endFill();
	}

	public function show(duration:Int):Void {
		Tween.to(this, duration, {w:WIDTH}).setVoidOnUpdate(redraw);
	}
}
