package ui;

import flash.display.Graphics;
import flash.display.BitmapData;
import flash.geom.Matrix;

class UIRect {
	private static inline var TOLERANCE:Float = 2;

	public var x:Float;
	public var y:Float;
	public var w:Float;
	public var h:Float;
	public var offsetX:Float;
	public var offsetY:Float;

	public var logicW:Float;
	public var logicH:Float;

	public function new(x:Float, y:Float, w:Float, h:Float, offsetX:Float, offsetY:Float) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.offsetX = offsetX;
		this.offsetY = offsetY;

		logicW = w - TOLERANCE * 2;
		logicH = h - TOLERANCE * 2;
	}

	public function draw(g:Graphics, bd:BitmapData, x:Float, y:Float):Void {
		g.beginBitmapFill(bd, new Matrix(1, 0, 0, 1, -this.x + x, -this.y + y), false);
		g.drawRect(x + offsetX, y + offsetY, w, h);
		g.endFill();
	}

	public function drawWithoutTolerance(g:Graphics, bd:BitmapData, x:Float, y:Float):Void {
		g.beginBitmapFill(bd, new Matrix(1, 0, 0, 1, -this.x + x, -this.y + y), false);
		g.drawRect(x + offsetX + TOLERANCE, y + offsetY + TOLERANCE, w - TOLERANCE * 2, h - TOLERANCE * 2);
		g.endFill();
	}

	public function drawUnoffsettedVertical(
		g:Graphics, bd:BitmapData, x:Float, y:Float, topH:Float, bottomH:Float, h:Float):Void {
		x -= TOLERANCE;
		y -= TOLERANCE;
		h += TOLERANCE * 2;
		var x0 = this.x + offsetX;
		var y0 = this.y + offsetY;
		g.beginBitmapFill(bd, new Matrix(1, 0, 0, 1, x - x0, y - y0), false);
		g.drawRect(x, y, w, topH);
		g.endFill();
		if (h > topH + bottomH) {
			var k = (h - topH - bottomH) / (this.h - topH - bottomH);
			g.beginBitmapFill(bd, new Matrix(1, 0, 0, k, x - x0, y - y0 * k + topH * (1 - k)), false);
			g.drawRect(x, y + topH, w, h - topH - bottomH);
			g.endFill();
		}
		g.beginBitmapFill(bd, new Matrix(1, 0, 0, 1, x - x0, y - y0 - this.h + h), false);
		g.drawRect(x, y + h - bottomH, w, bottomH);
		g.endFill();
	}
}
