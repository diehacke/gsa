package ui;

import model.StageModel;
import flash.text.TextField;
import ui.SmartFormat;
import game.RocketStage;
import flash.display.MovieClip;
import flash.display.BlendMode;

class StageCountedTile extends Widget {
	private var _mc:MovieClip;
	private var _count:TextField;

	public function new(model:StageModel, count:Int) {
		super();

		_mc = model.info.newMc();
		_mc.blendMode = BlendMode.LAYER;
		RocketStage.setFrame(_mc, false, false, false);
		addChild(_mc);

		_count = new SmartFormat("Tahoma", 16, UICommon.GOLD).setBold(true)
			.setFilters(UICommon.GOLD_FILTERS).newAutoSized();
		_count.text = count > 0 ? "x" + count : "";
		addChild(_count);
	}

	override public function redraw():Void {
		_w = 60;
		_h = 100;

		_count.x = Std.int(_w - _count.width);
		_count.y = Std.int(_h * .5 - _count.height * .5);

		_mc.x = 30;
		_mc.y = Std.int(_h * .5);
	}
}
