package ui;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.display.Sprite;
import flash.display.Bitmap;
import flash.ui.Keyboard;
import ui.Widget;
import utils.Tween;
import utils.KeyUtil;

class TileList<T:Tile> extends Widget {
	private var _player:Player;
	private var _content:Widget;
	private var _sb:ScrollBar;
	private var _btOwner:Sprite;
	private var _bt:Bitmap;

	public function new(player:Player) {
		super();
		_player = player;

		_tiles = new Array<T>();
		tileW = 100;
		tileH = 100;
		gapY = 10;

		_content = new Widget();
		addChild(_content);

		_sb = new ScrollBar();
		_sb.addEventListener(Event.CHANGE, onScrollBarChange);
		addChild(_sb);

		addEventListener(MouseEvent.MOUSE_WHEEL, onContentMouseWheel);

		_btOwner = new Sprite();
		UIRects.scroll_bar_v_key_owner.draw(_btOwner.graphics, Assets.ui_png, 0, 0);
		addChild(_btOwner);

		_bt = new Bitmap(Assets.key_bt_png);
		_bt.x = -_bt.width - 2;
		_bt.y = -_bt.height - 2;
		_btOwner.addChild(_bt);

		_h = 100;
	}

	private function onScrollBarChange(event:Event):Void {
		updateScroll();
	}

	private function onContentMouseWheel(event:MouseEvent):Void {
		scroll(-event.delta);
	}

	public var tileW:Int;
	public var tileH:Int;
	public var gapY:Int;

	private var _tiles:Array<T>;

	public function clear():Void {
		for (tile in _tiles) {
			removeChild(tile);
		}
		_tiles = new Array<T>();
	}

	public function add(tile:T):Void {
		_tiles.push(tile);
		_content.addChild(tile);
	}

	override public function redraw():Void {
		_w = tileW + 30;

		var g = graphics;
		g.clear();
		g.beginFill(0x000000, 0);
		g.drawRect(0, 0, _w, _h);
		g.endFill();

		_content.w = tileW;
		_content.h = _tiles.length * (tileH + gapY) - (_tiles.length > 0 ? gapY : 0);

		_sb.x = _w - 20;
		_sb.h = _h;
		_sb.contentSize = _content.h;
		_sb.redraw();

		_btOwner.x = _w;

		for (i in 0 ... _tiles.length) {
			var tile = _tiles[i];
			tile.x = 0;
			tile.y = i * (tileH + gapY);
		}
		updateScroll();
	}

	public function redrawTiles():Void {
		for (tile in _tiles) {
			tile.redraw();
		}
	}

	private function updateScroll():Void {
		_content.scrollRect = new Rectangle(0, _sb.value, _w, _h);
	}

	public function doOnKeyDown(keyCode:UInt):Void {
		if (keyCode == Keyboard.UP || keyCode == KeyUtil.K || keyCode == KeyUtil.W)
			scroll(-3);
		if (keyCode == Keyboard.DOWN || keyCode == KeyUtil.J || keyCode == KeyUtil.S)
			scroll(3);
	}

	private function scroll(delta:Int):Void {
		_sb.value += delta * 30;
		_sb.redraw();
		updateScroll();
	}

	private var _selected:T;
	public var selected(get, set):T;
	private function get_selected():T {
		return _selected;
	}
	private function set_selected(value:T):T {
		if (_selected != value) {
			if (_selected != null)
				_player.playSound(Assets.tap_on_m_sapphire_8662_hifi_wav);
			if (_selected != null)
				_selected.selected = false;
			_selected = value;
			if (_selected != null) {
				_selected.selected = true;
			}
		}
		return _selected;
	}
}
