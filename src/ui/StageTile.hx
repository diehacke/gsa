package ui;

import flash.display.Shape;
import flash.geom.ColorTransform;
import flash.display.Bitmap;
import game.RocketStage;
import flash.events.Event;
import ui.UIRects;
import flash.display.MovieClip;
import flash.display.BlendMode;
import flash.text.TextField;
import model.StageModel;
import flash.events.MouseEvent;
import utils.StringUtil;
import utils.Signal;

class StageTile extends Tile {
	public static var WIDTH:Int = 250;
	public static var HEIGHT:Int = 150;

	public var mouseDown:Signal<StageTile> = new Signal<StageTile>();
	public var doubleClick:Signal<StageTile> = new Signal<StageTile>();

	private var _shape:Shape;
	private var _title:TextField;
	private var _count:TextField;
	private var _parameters:Array<ParameterLine>;
	private var _mc:MovieClip;
	private var _lock:Bitmap;
	private var _getUsedCount:StageModel->Int;

	public function new(model:StageModel, getUsedCount:StageModel->Int) {
		super();
		_model = model;
		_getUsedCount = getUsedCount;
		_w = WIDTH;
		_h = HEIGHT;

		doubleClickEnabled = true;
		mouseChildren = false;

		_shape = new Shape();
		_shape.alpha = .47;
		addChild(_shape);

		_mc = model.info.newMc();
		_mc.blendMode = BlendMode.LAYER;
		RocketStage.setFrame(_mc, false, false, false);
		addChild(_mc);

		_title = new SmartFormat("Tahoma", 16, 0xffffff).setBold(true)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		addChild(_title);

		_count = new SmartFormat("Tahoma", 16, UICommon.GOLD).setBold(true)
			.setFilters(UICommon.GOLD_FILTERS).newAutoSized();
		addChild(_count);

		_parameters = new Array<ParameterLine>();
		for (i in 0 ... 5) {
			var parameter = new ParameterLine();
			addChild(parameter);
			_parameters.push(parameter);
		}

		_lock = new Bitmap(Assets.lock_png);
		_lock.smoothing = true;
		_lock.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 255, 255, 255, 0);
		addChild(_lock);

		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick);
	}
	
	private var _model:StageModel;
	public var model(get, never):StageModel;
	private function get_model():StageModel {
		return _model;
	}

	override public function redraw():Void {
		var locked = _model.getLocked();

		var g = graphics;
		g.clear();
		UIRects.stage_tile_normal.draw(g, Assets.ui_png, 0, 0);

		var g = _shape.graphics;
		g.clear();
		g.beginBitmapFill(Assets.trails_png);
		g.drawRect(100, 30, 140, 113);
		g.endFill();
		if (locked) {
			g.beginFill(0x000000, .5);
			g.drawRect(0, 30, _w - 10, _h - 37);
			g.endFill();
		}

		var count = _model.count - _getUsedCount(_model);

		_mc.x = 55;
		_mc.y = _h / 2 + 20;
		if (!locked) {
			_mc.filters = null;
			_mc.alpha = count > 0 ? 1 : .2;
		} else {
			_mc.filters = UICommon.GRAY_FILTERS;
			_mc.alpha = 1;
		}

		_lock.visible = locked;
		_lock.x = _w - 100;
		_lock.y = Std.int(30 + (_h - 37) * .5 - _lock.height * .5);

		_title.text = _model.name + "";
		_title.x = 10;
		_title.y = 5;

		_count.text = "x" + count;
		_count.x = _w - 20 - _count.width;
		_count.y = 5;
		_count.visible = !locked;

		var hasNozzles = _model.info.nozzles.length > 0;

		_parameters[0].title = "total m";
		_parameters[0].value = StringUtil.intX1000(_model.info.netM + _model.info.fuelM);
		_parameters[0].visible = !locked;

		_parameters[1].title = "net m";
		_parameters[1].value = StringUtil.intX1000(_model.info.netM);
		_parameters[1].visible = hasNozzles && !locked;

		_parameters[2].title = "fuel m";
		_parameters[2].value = StringUtil.intX1000(_model.info.fuelM);
		_parameters[2].visible = hasNozzles && !locked;

		_parameters[3].title = "fuel v";
		_parameters[3].value = StringUtil.fixed2(_model.info.fuelV);
		_parameters[3].visible = hasNozzles && !locked;

		_parameters[4].title = "fuel rate";
		_parameters[4].value = StringUtil.fixed2(_model.info.fuelRate);
		_parameters[4].visible = hasNozzles && !locked;

		var offsetY = 36;
		for (parameter in _parameters) {
			parameter.w = 124;
			parameter.x = 110;
			parameter.y = offsetY;
			parameter.redraw();
			offsetY += 20;
		}
	}

	private function onMouseDown(event:MouseEvent):Void {
		mouseDown.dispatch(this);
	}

	private function onDoubleClick(event:MouseEvent):Void {
		doubleClick.dispatch(this);
	}
}
