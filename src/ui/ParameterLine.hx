package ui;

import flash.text.TextField;

class ParameterLine extends Widget {
	private var _titleTf:TextField;
	private var _valueTf:TextField;

	public function new() {
		super();

		_h = 20;
		_w = 100;

		_titleTf = new SmartFormat("Tahoma", 14).setBold(true).newAutoSized();
		addChild(_titleTf);

		_valueTf = new SmartFormat("Tahoma", 14, UICommon.BLUE).setBold(true)
			.newAutoSized();
		addChild(_valueTf);
	}

	public var title(get, set):String;
	private function get_title():String {
		return _titleTf.text;
	}
	private function set_title(value:String):String {
		_titleTf.text = value;
		return value;
	}

	public var value(get, set):String;
	private function get_value():String {
		return _valueTf.text;
	}
	private function set_value(value:String):String {
		_valueTf.text = value;
		return value;
	}

	override public function redraw():Void {
		_valueTf.x = _w - _valueTf.width;
	}
}
