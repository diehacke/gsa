package ui;

import flash.text.TextField;
import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import utils.KeyUtil;

class NextButton extends AButton {
	private var _tf:TextField;
	private var _color:Int;
	private var _keyBitmap:Bitmap;

	public function new() {
		super();
	}

	override private function init():Void {
		_w = UIRects.next_button_up.w;
		_h = UIRects.next_button_up.h;

		_tf = new SmartFormat("Arial", 26).setBold(true).setColor(0xffffff)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		addChild(_tf);

		_keyBitmap = new Bitmap(Assets.key_space_png);
		_keyBitmap.x = -_keyBitmap.width;
		_keyBitmap.y = _h - _keyBitmap.height - 2;
		addChild(_keyBitmap);
	}

	public var text(get, set):String;
	private function get_text():String {
		return _tf.text;
	}
	private function set_text(value:String):String {
		_tf.text = value;
		return value;
	}

	private var _offset:Float;

	override public function redraw():Void {
		_tf.x = _offset - _w / 2 - _tf.width / 2 + 5;
		_tf.y = _offset + 30 - _tf.height / 2;

		var g = graphics;
		g.clear();
		if (_isDown)
			UIRects.next_button_down.draw(g, Assets.ui_png, 0, _h);
		else if (_isOver)
			UIRects.next_button_over.draw(g, Assets.ui_png, 0, _h);
		else
			UIRects.next_button_up.draw(g, Assets.ui_png, 0, _h);
	}

	override public function updateState():Void {
		_offset = _isDown ? 2 : 0;
		redraw();
	}

	public function doOnKeyDown(keyCode:UInt):Void {
		if (keyCode == Keyboard.SPACE ||
			keyCode == Keyboard.ENTER)
			processClick();
	}
}
