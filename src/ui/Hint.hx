package ui;
import flash.text.TextField;
import flash.text.TextFormatAlign;
import utils.Tween;
import utils.TweenKey;

class Hint extends Widget {
	private var _tf:TextField;
	private var _key:TweenKey;

	public function new() {
		super();
		_key = new TweenKey();
		var format = new SmartFormat("Tahoma", 20).setBold(true);
		format.align = TextFormatAlign.CENTER;
		_tf = format.setColor(0xffffff).newAutoSized();
		addChild(_tf);
	}

	public var text(get, set):String;
	private function get_text():String {
		return _tf.text;
	}
	private function set_text(value:String):String {
		_tf.text = value;
		return _tf.text;
	}

	public var targetY:Float;

	override public function redraw():Void {
		_w = _tf.width + 40;
		_h = _tf.height + 20;

		var g = graphics;
		g.clear();
		g.beginFill(0x808080, .8);
		g.drawRoundRect(0, 0, _w, _h, 8, 8);
		g.endFill();

		_tf.x = _w / 2 - _tf.width / 2;
		_tf.y = _h / 2 - _tf.height / 2;
	}

	private var _state:Int;

	public function show(parent:Widget, x:Float, y:Float):Void {
		parent.addChild(this);
		redraw();
		this.x = x - _w / 2;
		this.y = y - _h / 2;

		if (_state == 0) {
			alpha = 0;
			showStart();
		} if (_state == 2) {
			onShowComplete();
		} else if (_state == 3) {
			showStart();
		}
	}

	private function showStart():Void {
		_state = 1;
		var vars = {alpha:1};
		Tween.to(this, 100, vars, _key).setVoidOnComplete(onShowComplete);
	}

	private function onShowComplete():Void {
		_state = 2;
		Tween.to(this, 1000, null, _key).setVoidOnComplete(onIdleComplete);
	}

	private function onIdleComplete():Void {
		_state = 3;
		var vars = {alpha:0};
		Tween.to(this, 500, vars, _key).setVoidOnComplete(onHideComplete);
	}

	public var onRemove:Hint->Void;

	private function onHideComplete():Void {
		_state = 0;
		if (parent != null)
			parent.removeChild(this);
		if (onRemove != null)
			onRemove(this);
	}
}
