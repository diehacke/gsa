package ui;

import flash.text.TextField;
import flash.filters.BitmapFilter;
import flash.geom.ColorTransform;
import model.Mission;
import game.RocketStage;
import flash.display.Graphics;
import flash.display.MovieClip;
import flash.display.Shape;
import flash.display.Bitmap;
import flash.display.BlendMode;
import flash.events.MouseEvent;
import utils.Signal;

class MissionTile extends Tile {
	public static var WIDTH:Int = 160;
	public static var HEIGHT:Int = 140;
	public static var SELECTED_WIDTH:Int = 270;
	public static var SELECTED_HEIGHT:Int = 280;

	public var mouseDown:Signal<MissionTile> = new Signal<MissionTile>();
	public var doubleClick:Signal<MissionTile> = new Signal<MissionTile>();

	private var _allowFog:Bool;

	private var _mark:Bitmap;
	private var _markShape:Shape;
	private var _lock:Bitmap;
	private var _lockTf:TextField;
	private var _title:TextField;
	private var _isSubtitleWhite:Bool;
	private var _articlesBg:Shape;
	private var _articleWhite:SmartFormat;
	private var _articleBlack:SmartFormat;
	private var _subtitle:TextField;
	private var _mc:MovieClip;

	public function new(mission:Mission, allowFog:Bool) {
		super();
		_mission = mission;
		_allowFog = allowFog;
		_w = WIDTH;
		_h = HEIGHT;

		doubleClickEnabled = true;
		mouseChildren = false;

		var rocket = mission.newDefaultRocketModel();

		_mc = rocket.info.newMc();
		RocketStage.setFrame(_mc, false, true, true);
		addChild(_mc);

		_lock = new Bitmap(Assets.lock_png);
		_lock.smoothing = true;
		_lock.transform.colorTransform = new ColorTransform(1, 1, 1, 1, 255, 255, 255, 0);
		addChild(_lock);

		_lockTf = new SmartFormat("Tahomra", 12, 0xffffff).setBold(true).setCenter().newAutoSized();
		_lockTf.text = _mission.getLockText();
		addChild(_lockTf);

		_title = new SmartFormat("Tahoma", 16, 0xffffff).setBold(true)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		_title.text = _mission.title != null && _mission.title != "" ? _mission.title : rocket.name;
		addChild(_title);

		_articlesBg = new Shape();
		addChild(_articlesBg);

		_articleWhite = new SmartFormat("Tahoma", 14, 0xffffff).setBold(true);
		_articleBlack = new SmartFormat("Tahoma", 14, 0x000000).setBold(false);

		_subtitle = _articleBlack.newAutoSized();
		_subtitle.text = _mission.subtitle != null ? _mission.subtitle : "";
		addChild(_subtitle);

		_markShape = new Shape();
		addChild(_markShape);

		_mark = new Bitmap(Assets.mark_png);
		_mark.smoothing = true;
		addChild(_mark);

		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick);
	}
	
	private var _mission:Mission;
	public var mission(get, never):Mission;
	private function get_mission():Mission {
		return _mission;
	}

	private var _descTf:TextField;
	private var _descImage:Shape;
	private var _mcFilters:Array<BitmapFilter>;

	override public function redraw():Void {
		_mc.x = WIDTH * .5;
		_mc.y = HEIGHT * .5 + 10;
		_mc.alpha = _mission.opened ? 1. : .5;

		var mcFilters = _mission.opened ? null : UICommon.GRAY_FILTERS;
		if (_mcFilters != mcFilters) {
			_mcFilters = mcFilters;
			_mc.filters = mcFilters;
		}

		_mark.x = _w - _mark.width - 4;
		_mark.y = _h - _mark.height - 2;
		_mark.visible = _mission.completed;
		_mark.filters = UICommon.BLACK_FILTERS;

		var g = _markShape.graphics;
		g.clear();
		if (_allowFog && _mission.completed) {
			g.beginFill(0xffffff, .5);
			g.drawRect(0, 0, _w, _h);
			g.endFill();
		}

		_lock.x = _w * .5 - _lock.width * .5;
		_lock.y = _h * .5 - _lock.height * .5 + (_lockTf.text != "" ? 15 : 0);
		_lock.visible = !_mission.opened;

		_lockTf.x = _w * .5 - _lockTf.width * .5;
		_lockTf.y = 20;
		_lockTf.visible = !_mission.opened;

		_title.x = _w * .5 - _title.width * .5;
		_title.alpha = _mission.opened ? 1 : .6;

		var g = _articlesBg.graphics;
		g.clear();

		var isSubtitleWhite = _h > SELECTED_HEIGHT * .8;
		if (_isSubtitleWhite != isSubtitleWhite) {
			_isSubtitleWhite = isSubtitleWhite;
			if (_isSubtitleWhite) {
				_articleWhite.applyTo(_subtitle);
			} else {
				_articleBlack.applyTo(_subtitle);
			}
		}
		var k = (_h - HEIGHT) / (SELECTED_HEIGHT - HEIGHT);

		_subtitle.width = _w - 4;
		_subtitle.x = 10 + (_w * .5 - _subtitle.width * .5 - 10) * (1 - k);
		_subtitle.y = _h - _subtitle.height - 6;

		if (k > .01) {
			if (_subtitle.text != "") {
				drawSubtitleBg(g, 0, _subtitle.y, (WIDTH + 10) * k, _subtitle.height);
			}
		}

		if (_h > SELECTED_HEIGHT * .8) {
			var k = (_h - SELECTED_HEIGHT * .8) / (SELECTED_HEIGHT - SELECTED_HEIGHT * .8);
			if (_descTf == null) {
				_descTf = new SmartFormat("Tahoma", 14).newAutoSized();
				_descTf.wordWrap = true;
				_descTf.width = SELECTED_WIDTH - WIDTH - 10;
				_descTf.text = _mission.descText != null ? _mission.descText : "";
				addChildAt(_descTf, 0);
			}
			_descTf.x = Math.min(WIDTH + 20, _w - _descTf.width - 10);
			_descTf.y = 30;
			_descTf.alpha = k;
		} else {
			if (_descTf != null) {
				removeChild(_descTf);
				_descTf = null;
			}
		}

		if (_h > SELECTED_HEIGHT * .8) {
			var k = (_h - SELECTED_HEIGHT * .8) / (SELECTED_HEIGHT - SELECTED_HEIGHT * .8);
			if (_mission.descImage != null) {
				if (_descImage == null) {
					_descImage = new Shape();
					_mission.descImage.drawWithoutTolerance(_descImage.graphics, Assets.ui_png, 0, 0);
					addChildAt(_descImage, 0);
				}
				_descImage.x = _w - _mission.descImage.logicW;
				_descImage.y = _h - _mission.descImage.logicH;
				_descImage.alpha = k;
			}
		} else {
			if (_descImage != null) {
				removeChild(_descImage);
				_descImage = null;
			}
		}

		updateSelected();
	}

	private function drawSubtitleBg(g:Graphics, x:Float, y:Float, w:Float, h:Float):Void {
		g.beginFill(0x888888);
		g.moveTo(x, y);
		g.lineTo(x + w * .95, y);
		g.lineTo(x + w, y + h * .5);
		g.lineTo(x + w * .95, y + h);
		g.lineTo(x, y + h);
		g.lineTo(x, y);
		g.endFill();
	}

	private function onMouseDown(event:MouseEvent):Void {
		mouseDown.dispatch(this);
	}

	private function onDoubleClick(event:MouseEvent):Void {
		doubleClick.dispatch(this);
	}

	override private function updateSelected():Void {
		var g = graphics;
		g.clear();
		var headerH = 22;
		var headerOpenedColor = _mission.title != null && _mission.title != "" ? 0x888888 : 0x0080cc;
		var headerClosedColor = _mission.title != null && _mission.title != "" ? 0x505050 : 0x003050;
		var color = _mission.opened ? headerOpenedColor : 0x505050;
		g.beginFill(color);
		g.drawRect(0, 0, _w, headerH);
		g.endFill();
		var color = _mission.opened ? 0xffffff : 0x808080;
		g.beginFill(color);
		g.drawRect(0, headerH, _w, _h - headerH);
		g.endFill();

		if (_selected) {
			var offset = 5;
			var bevel = 6;
			var x0 = -offset;
			var x1 = _w + offset;
			var y0 = -offset;
			var y1 = _h + offset;
			g.lineStyle(4, 0x888888);
			g.moveTo(x0 + bevel, y0);
			g.lineTo(x1 - bevel, y0);
			g.lineTo(x1, y0 + bevel);
			g.lineTo(x1, y1 - bevel);
			g.lineTo(x1 - bevel, y1);
			g.lineTo(x0 + bevel, y1);
			g.lineTo(x0, y1 - bevel);
			g.lineTo(x0, y0 + bevel);
			g.lineTo(x0 + bevel, y0);
		}
	}
}
