package ui;

import flash.events.MouseEvent;
import flash.display.Stage;

class AButton extends Tile {
	public function new() {
		super();
		
		mouseEnabled = true;
		mouseChildren = false;
		
		_enabled = true;
		_selected = false;
		_isDown = false;
		_isOver = false;
		
		init();
		updateEnabled();
	}

	private function init():Void {
	}
	
	private var _isDown:Bool;
	private var _isOver:Bool;
	
	private var _enabled:Bool;
	public var enabled(get, never):Bool;
	private function get_enabled():Bool {
		return _enabled;
	}
	private function set_enabled(value:Bool):Bool {
		if (_enabled != value) {
			_enabled = value;
			updateEnabled();
		}
		return _enabled;
	}
	
	private function /**/updateEnabled() {
		buttonMode = _enabled;
		if (_enabled) {
			removeEventListener(MouseEvent.CLICK, onBlockMouse);
			removeEventListener(MouseEvent.MOUSE_DOWN, onBlockMouse);
			removeEventListener(MouseEvent.MOUSE_UP, onBlockMouse);
			
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
		} else {
			_isOver = false;
			_isDown = false;
			
			addEventListener(MouseEvent.CLICK, onBlockMouse, false, 100500);
			addEventListener(MouseEvent.MOUSE_DOWN, onBlockMouse, false, 100500);
			addEventListener(MouseEvent.MOUSE_UP, onBlockMouse, false, 100500);
			
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			removeEventListener(MouseEvent.ROLL_OVER, onRollOver);
			removeEventListener(MouseEvent.ROLL_OUT, onRollOut);
			
			if (_stage != null) {
				_stage.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
			}
		}
		updateState();
	}
	
	private function /**/onBlockMouse(event:MouseEvent):Void {
		event.stopImmediatePropagation();
	}

	private var _stage:Stage;
	
	private function /**/onMouseDown(event:MouseEvent):Void {
		if (!_enabled) {
			event.stopImmediatePropagation();
			return;
		}
		
		if (stage == null) {
			return;
		}
		_stage = stage;
		_stage.addEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
		_isDown = true;
		updateState();
	}
	
	private function /**/onStageMouseUp(event:MouseEvent):Void {
		_stage.removeEventListener(MouseEvent.MOUSE_UP, onStageMouseUp);
		if (!_enabled) {
			event.stopImmediatePropagation();
			return;
		}
		
		_isDown = false;
		updateState();
	}

	private function /**/onRollOver(event:MouseEvent):Void {
		_isOver = true;
		updateState();
	}
	
	private function /**/onRollOut(event:MouseEvent):Void {
		_isOver = false;
		updateState();
	}
	
	override private function /**/updateSelected():Void {
		updateState();
	}
	
	private function /**/updateState():Void {
	}

	public function processClick():Void {
		if (_enabled)
			dispatchEvent(new MouseEvent(MouseEvent.CLICK));
	}
}
