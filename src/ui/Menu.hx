package ui;

import flash.display.Shape;
import flash.display.Sprite;
import flash.display.Bitmap;
import utils.Tween;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import ui.Button;
import utils.KeyUtil;
import utils.Signal;

class Menu extends Widget {
	public var escape:Signal<Menu> = new Signal<Menu>();

	private var _player:Player;
	private var _hasEsc:Bool;
	private var _container:Sprite;
	private var _selector:Shape;
	private var _frame:Shape;

	public function new(player:Player, hasEsc:Bool) {
		super();
		_player = player;
		_hasEsc = hasEsc;

		_buttons = new Array<MenuButton>();

		_container = new Sprite();
		addChild(_container);

		_selector = new Shape();
		_selector.x = -164;
		UIRects.selector_normal.draw(_selector.graphics, Assets.ui_png, 0, 0);
		_container.addChild(_selector);

		_frame = new Shape();
		_container.addChild(_frame);

		_playOpenClose = true;
	}

	private var _playOpenClose:Bool;
	public var playOpenClose(get, set):Bool;
	private function get_playOpenClose():Bool {
		return _playOpenClose;
	}
	private function set_playOpenClose(value:Bool):Bool {
		_playOpenClose = value;
		return _playOpenClose;
	}

	private var _buttons:Array<MenuButton>;

	public function addButton(text:String):MenuButton {
		var button = new MenuButton();
		button.text = text;
		button.redraw();
		_container.addChild(button);
		_buttons.push(button);
		if (_selectedButton == null)
			setSelected(button, true);
		return button;
	}

	override public function redraw():Void {
		var h = 68;
		_w = 340;
		_h = Std.int(_buttons.length * h + 46);

		var offsetY = -_buttons.length * h - 10;
		var index = 0;
		for (button in _buttons) {
			button.x = -137;
			button.y = offsetY + h * index;
			index++;
		}

		_frame.x = -_w / 2;
		_frame.y = -_h;
		_frame.graphics.clear();
		if (_hasEsc) {
			UIRects.menu_frame_escape.drawUnoffsettedVertical(_frame.graphics, Assets.ui_png, 0, 0, 34, 24, _h);
		} else {
			UIRects.menu_frame_normal.drawUnoffsettedVertical(_frame.graphics, Assets.ui_png, 0, 0, 34, 24, _h);
		}
		_selector.y = getSelectorY();
		_container.y = _h;
	}

	private var _selectedButton:MenuButton;

	public function doOnKeyDown(keyCode:UInt):Void {
		if (!_opened)
			return;
		if (keyCode == Keyboard.UP || keyCode == KeyUtil.K)
			move(-1);
		if (keyCode == Keyboard.DOWN || keyCode == KeyUtil.J)
			move(1);
		if (keyCode == Keyboard.ESCAPE ||
			keyCode == KeyUtil.LEFT_BRACKET && KeyUtil.isDown(Keyboard.CONTROL)) {
			if (escape != null)
				escape.dispatch(this);
		}
		if (keyCode == Keyboard.ENTER || keyCode == Keyboard.SPACE) {
			if (_selectedButton != null)
				_selectedButton.processClick();
		}
	}

	private function move(offset:Int):Void {
		if (_buttons.length > 0) {
			var index = _buttons.indexOf(_selectedButton);
			index += offset;
			if (index < 0)
				index = 0;
			else if (index >= _buttons.length)
				index = _buttons.length - 1;
			setSelected(_buttons[index], false);
		}
	}

	private function getSelectorY():Float {
		return _selectedButton != null ? _selectedButton.y - 8 : 0;
	}

	public function setSelected(button:MenuButton, immediate:Bool):Void {
		if (_selectedButton != button) {
			if (_selectedButton != null)
				_selectedButton.selected = false;
			_selectedButton = button;
			if (_selectedButton != null) {
				_selectedButton.selected = true;
				if (immediate) {
					Tween.killByKey(_selector);
					_selector.y = getSelectorY();
				} else {
					Tween.to(_selector, 150, {y:getSelectorY()}).setEase(Tween.easeOut);
					_player.playSound(Assets.tap_on_m_sapphire_8662_hifi_wav);
				}
			}
		}
	}

	private var _opened:Bool;

	public function open(immediate:Bool):Void {
		if (_opened)
			return;
		_opened = true;
		if (immediate) {
			_container.y = 0;
		} else {
			Tween.killByKey(_container);
			Tween.to(_container, 150, {y:0}).setEase(Tween.easeOut);
			if (_playOpenClose)
				_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
		}
	}

	public function close():Void {
		if (!_opened)
			return;
		_opened = false;
		Tween.to(_container, 150, {y:_h}).setEase(Tween.easeOut).setVoidOnComplete(onCloseComplete);
		if (_playOpenClose)
			_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
	}

	public var onCloseComplete:Void -> Void;
}
