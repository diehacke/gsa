package ui;

import flash.text.TextField;
import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import utils.KeyUtil;

class BackButton extends AButton {
	private var _tf:TextField;
	private var _color:Int;
	private var _keyBitmap:Bitmap;

	public function new() {
		super();
	}

	override private function init():Void {
		_w = UIRects.back_button_up.w;
		_h = UIRects.back_button_up.h;

		_tf = new SmartFormat("Arial", 26).setBold(true).setColor(0xffffff)
			.setFilters(UICommon.BLACK_FILTERS).newAutoSized();
		_tf.text = "<";
		addChild(_tf);

		_keyBitmap = new Bitmap(Assets.key_esc_png);
		_keyBitmap.x = 4;
		_keyBitmap.y = _h - _keyBitmap.height - 2;
		addChild(_keyBitmap);
	}

	private var _offset:Float;

	override public function redraw():Void {
		_tf.x = _offset + _w / 2 - _tf.width / 2 - 5;
		_tf.y = _offset + 30 - _tf.height / 2;

		var g = graphics;
		g.clear();
		if (_isDown)
			UIRects.back_button_down.draw(g, Assets.ui_png, 0, _h);
		else if (_isOver)
			UIRects.back_button_over.draw(g, Assets.ui_png, 0, _h);
		else
			UIRects.back_button_up.draw(g, Assets.ui_png, 0, _h);
	}

	override public function updateState():Void {
		_offset = _isDown ? 2 : 0;
		redraw();
	}

	public function doOnKeyDown(keyCode:UInt):Void {
		if (keyCode == Keyboard.ESCAPE ||
			keyCode == KeyUtil.LEFT_BRACKET && KeyUtil.isDown(Keyboard.CONTROL))
			processClick();
	}
}
