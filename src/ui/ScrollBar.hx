package ui;

import flash.geom.Rectangle;
import flash.geom.Matrix;
import flash.events.Event;
import flash.events.MouseEvent;
import ui.Button;

class ScrollBar extends Widget {
	private var _thumb:Thumb;

	public function new() {
		super();

		_value = 0;
		_contentSize = 100;

		_thumb = new Thumb();
		_thumb.addEventListener(MouseEvent.MOUSE_DOWN, onThumbMouseDown);
		addChild(_thumb);
	}

	private function onThumbMouseDown(event:MouseEvent):Void {
		_thumb.startDrag(false, new Rectangle(0, 0, 0, _h - _thumb.h));
		stage.addEventListener(MouseEvent.MOUSE_UP, onThumbMouseUp);
		stage.addEventListener(MouseEvent.MOUSE_MOVE, onThumbMouseMove);
	}

	private function onThumbMouseUp(event:MouseEvent):Void {
		stage.removeEventListener(MouseEvent.MOUSE_UP, onThumbMouseUp);
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, onThumbMouseMove);
		_thumb.stopDrag();
	}

	private function onThumbMouseMove(event:MouseEvent):Void {
		_value = _thumb.y * (_contentSize - _h) / (_h - _thumb.h);
		dispatchEvent(new Event(Event.CHANGE));
	}

	override public function redraw():Void {
		_w = 20;

		var g = graphics;
		g.clear();
		UIRects.scroll_bar_v_top.draw(g, Assets.ui_png, _w, 0);
		UIRects.scroll_bar_v_bottom.draw(g, Assets.ui_png, _w, _h);
		var x = _w - 21;
		var y = 11;
		g.beginBitmapFill(Assets.perforated_plate_2_png, new Matrix(1, 0, 0, 1, x, y));
		g.drawRect(x, y, 14, _h - 22);
		g.endFill();

		if (_contentSize > _h) {
			_thumb.visible = true;
			_thumb.h = _h * _h / _contentSize;
			_thumb.redraw();
			if (_value < 0)
				_value = 0;
			else if (_value > _contentSize - _h)
				_value = _contentSize - _h;
			_thumb.y = _value * (_h - _thumb.h) / (_contentSize - h);
		} else {
			_thumb.visible = false;
			_value = 0;
		}
	}

	private var _value:Float;
	public var value(get, set):Float;
	private function get_value():Float {
		return _value;
	}
	private function set_value(value:Float):Float {
		_value = value;
		return _value;
	}

	private var _contentSize:Float;
	public var contentSize(get, set):Float;
	private function get_contentSize():Float {
		return _contentSize;
	}
	private function set_contentSize(value:Float):Float {
		_contentSize = value;
		return _contentSize;
	}
}
