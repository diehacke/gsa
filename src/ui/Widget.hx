package ui;

import flash.display.Sprite;

class Widget extends Sprite {
	public function new() {
		super();
		_w = 0;
		_h = 0;
	}

	private var _w:Float;
	public var w(get, set):Float;
	private function get_w():Float {
		return _w;
	}
	private function set_w(value:Float):Float {
		_w = value;
		return _w;
	}

	private var _h:Float;
	public var h(get, set):Float;
	private function get_h():Float {
		return _h;
	}
	private function set_h(value:Float):Float {
		_h = value;
		return _h;
	}

	public function redraw():Void {
	}
}
