package ui;

class Tile extends Widget {
	public function new() {
		super();
	}

	private var _selected:Bool;
	public var selected(get, set):Bool;
	private function get_selected():Bool {
		return _selected;
	}
	private function set_selected(value:Bool) {
		if (_selected != value) {
			_selected = value;
			updateSelected();
		}
		return _selected;
	}

	private function updateSelected():Void {
	}
}
