package ui;

import flash.display.MovieClip;
import flash.events.MouseEvent;
import model.StageModel;
import game.RocketStage;
import utils.Signal;

class StageView extends Widget {
	public var rollOver:Signal<StageView> = new Signal<StageView>();
	public var rollOut:Signal<StageView> = new Signal<StageView>();
	public var mouseDown:Signal<StageView> = new Signal<StageView>();

	private var _model:StageModel;
	public var model(get, never):StageModel;
	public function get_model():StageModel {
		return _model;
	}

	private var _mc:MovieClip;

	public function new(model:StageModel) {
		super();
		_model = model;

		_mc = _model.info.newMc();
		_mc.x = 0;
		_mc.y = 0;
		_mc.rotation = 0;
		_mc.gotoAndStop(1);
		addChild(_mc);

		addEventListener(MouseEvent.ROLL_OVER, onRollOver);
		addEventListener(MouseEvent.ROLL_OUT, onRollOut);
		addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
	}

	private function onRollOver(event:MouseEvent):Void {
		rollOver.dispatch(this);
	}

	private function onRollOut(event:MouseEvent):Void {
		rollOut.dispatch(this);
	}

	private function onMouseDown(event:MouseEvent):Void {
		mouseDown.dispatch(this);
	}

	override public function redraw():Void {
		_mc.scaleX = _model.calculatedMirror ? -1 : 1;
		RocketStage.setFrame(_mc, _model.linkBottom.child != null, false, false);
	}
}
