package ui;

import flash.display.Sprite;
import model.StageModelIterator;
import model.StageModel;

class RocketPartView extends Widget {
	private var _views:Map<StageModel, StageView>;
	private var _onViewCreate:StageView->Void;

	private var _model:StageModel;
	public var model(get, never):StageModel;
	private function get_model():StageModel {
		return _model;
	}

	private var _nozzlesH:Float;
	public var nozzlesH(get, never):Float;
	private function get_nozzlesH():Float {
		return _nozzlesH;
	}

	public function new(model:StageModel, onViewCreate:StageView->Void) {
		super();
		_model = model;
		_onViewCreate = onViewCreate;
	}

	public function getView(model:StageModel):StageView {
		return _views[model];
	}

	override public function redraw():Void {
		while (numChildren > 0) {
			removeChildAt(0);
		}
		_views = new Map<StageModel, StageView>();
		_nozzlesH = 0;
		var iterator = new StageModelIterator(_model);
		while (iterator.next()) {
			var model = iterator.current;
			var parent = model.parent;
			var view = new StageView(model);
			_views[model] = view;
			view.x = model.calculatedX;
			view.y = model.calculatedY;
			view.redraw();
			addChild(view);
			if (_onViewCreate != null)
				_onViewCreate(view);
			if (_nozzlesH < model.calculatedY + model.nozzlesH)
				_nozzlesH = model.calculatedY + model.nozzlesH;
		}
	}

	public function removeStage(model:StageModel):Bool {
		if (model.parent == null)
			return false;
		model.parent.child = null;
		redraw();
		return true;
	}
}
