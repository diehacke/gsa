package ui;
import flash.display.BitmapData;
import flash.geom.ColorTransform;
import flash.geom.Matrix;
import haxe.ds.StringMap;

class SymbolPanel extends Widget {
	private static var SYMBOL_W:Int = 34;
	private static var SYMBOL_H:Int = 54;
	private static var NUM_LINE_SYMBOLS:Int = 10;

	private var _bd:BitmapData;
	private var _indexByChar:StringMap<Int>;

	public function new() {
		super();
		_bd = Assets.symbols_png;
		_indexByChar = new StringMap();
		var symbols = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ -";
		for (i in 0 ... symbols.length) {
			_indexByChar.set(symbols.charAt(i), i);
		}
		_text = "";
		update();
		transform.colorTransform = new ColorTransform(1, .2, 0);
	}

	private var _text:String;
	public var text(get, set):String;
	private function get_text():String {
		return _text;
	}
	private function set_text(value:String):String {
		if (_text != value) {
			_text = value;
			update();
		}
		return _text;
	}

	private function update():Void {
		_w = _text.length * SYMBOL_W;
		_h = SYMBOL_H;
		var g = graphics;
		g.clear();
		g.beginFill(0x000000, .5);
		g.drawRect(0, 0, _w, _h);
		g.endFill();
		for (i in 0 ... _text.length) {
			var index = _indexByChar.get(_text.charAt(i));
			var x = (index % NUM_LINE_SYMBOLS) * SYMBOL_W;
			var y = Std.int(index / NUM_LINE_SYMBOLS) * SYMBOL_H;
			var matrix = new Matrix(
				1, 0, 0, 1,
				i * SYMBOL_W - x, -y
			);
			g.beginBitmapFill(_bd, matrix, false);
			g.drawRect(i * SYMBOL_W, 0, SYMBOL_W, SYMBOL_H);
			g.endFill();
		}
	}
}
