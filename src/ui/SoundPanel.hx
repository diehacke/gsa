package ui;

import flash.events.MouseEvent;

class SoundPanel extends Widget {
	private var _player:Player;
	private var _sound:SoundButton;
	private var _music:SoundButton;

	public function new(player:Player) {
		super();

		_player = player;

		_sound = new SoundButton(false);
		_sound.redraw();
		_sound.addEventListener(MouseEvent.MOUSE_DOWN, onSoundMouseDown);
		addChild(_sound);

		_music = new SoundButton(true);
		_music.redraw();
		_music.addEventListener(MouseEvent.MOUSE_DOWN, onMusicMouseDown);
		addChild(_music);

		updateState();
	}

	override public function redraw():Void {
		_music.x = 36;

		UIRects.sound_bg_normal.draw(graphics, Assets.ui_png, 0, 0);
	}

	private function onSoundMouseDown(event:MouseEvent):Void {
		_player.soundMuted = !_player.soundMuted;
		updateState();
	}

	private function onMusicMouseDown(event:MouseEvent):Void {
		_player.musicMuted = !_player.musicMuted;
		updateState();
	}

	private function updateState():Void {
		_sound.selected = !_player.soundMuted;
		_music.selected = !_player.musicMuted;
	}
}
