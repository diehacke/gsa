package ui;

import flash.text.TextField;
import flash.display.BitmapData;
import flash.display.Bitmap;

class Button extends AButton {
	private var _tf:TextField;
	private var _color:Int;

	public function new() {
		super();
	}

	override private function init():Void {
		_w = 100;
		_h = 100;

		_color = 0x0080ff;

		_format = new SmartFormat("Tahoma", 14).setBold(true).setColor(0xffffff);
		_tf = _format.newAutoSized();
		addChild(_tf);
	}

	private var _format:SmartFormat;
	public var format(get, set):SmartFormat;
	private function get_format():SmartFormat {
		return _format;
	}
	private function set_format(value:SmartFormat):SmartFormat {
		_format = value;
		_format.applyTo(_tf);
		return _format;
	}

	public var text(get, set):String;
	private function get_text():String {
		return _tf.text;
	}
	private function set_text(value:String):String {
		_tf.text = value;
		return _tf.text;
	}

	private var _keyBitmap:Bitmap;
	public var key(get, set):BitmapData;
	private function get_key():BitmapData {
		return _keyBitmap != null ? _keyBitmap.bitmapData : null;
	}
	private function set_key(value:BitmapData):BitmapData {
		if (value != null) {
			if (_keyBitmap == null) {
				_keyBitmap = new Bitmap();
				addChild(_keyBitmap);
			}
			_keyBitmap.bitmapData = value;
		} else {
			if (_keyBitmap != null)
				_keyBitmap.bitmapData = null;
		}
		return value;
	}

	private var _offset:Float;

	override public function redraw():Void {
		_tf.x = _offset + _w / 2 - _tf.width / 2;
		_tf.y = _offset + _h / 2 - _tf.height / 2;

		if (_keyBitmap != null) {
			_keyBitmap.x = Std.int(_w / 2 - _keyBitmap.width / 2 + _offset);
			_keyBitmap.y = Std.int(_h - _keyBitmap.height + 9 + _offset);
		}

		var g = graphics;
		g.clear();
		g.beginFill(_color);
		g.drawRoundRect(_offset, _offset, _w, _h, 10, 10);
		g.endFill();

		if (_selected) {
			var offset = 5;
			g.lineStyle(2, _color);
			g.drawRoundRect(-offset, -offset, _w + offset * 2, _h + offset * 2, 10, 10);
		}
	}

	override public function updateState():Void {
		_offset = _isDown ? 2 : 0;
		redraw();
	}
}
