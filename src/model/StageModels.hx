package model;

import game.StageInfo;
import game.Stages;
import model.StageModelStack;

class StageModels {
	private static var _models:Array<StageModel> = new Array<StageModel>();
	private static var _modelByName:Map<String, StageModel> = new Map<String, StageModel>();

	public static var models(get, never):Iterable<StageModel>;
	private static function get_models():Iterable<StageModel> {
		return _models;
	}

	public static function getModel(name:String):StageModel {
		return _modelByName[name];
	}

	private static function addModel(info:StageInfo, name:String):StageModel {
		var model = new StageModel(info);
		model.name = name;
		info.model = model;
		_models.push(model);
		_modelByName[name] = model;
		return model;
	}

	public static function init():Void {
		var model = addModel(Stages.shipprimary, "Translator");
		model.isShip = true;
		model.maxARatio = 5;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkTop.type = LinkType.COWL_1;

		var model = addModel(Stages.shiptelescope, "Telescope");
		model.isShip = true;
		model.maxARatio = 2.6;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.COWL_2;
		model.linkRight.type = LinkType.COWL_2;

		var model = addModel(Stages.shipexplorer, "Explorer");
		model.isShip = true;
		model.maxARatio = 3;
		model.hasAntenna = true;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.COWL_2;
		model.linkRight.type = LinkType.COWL_2;
		model.discoverRadius = 2000;
		model.photoRadius = 600;
		model.closePhotoRadius = 120;
		model.closePhotoK = 2;

		var model = addModel(Stages.shipexplorer2, "Explorer M");
		model.isShip = true;
		model.maxARatio = 3;
		model.hasAntenna = true;
		model.hasBrolly = true;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.COWL_2;
		model.linkRight.type = LinkType.COWL_2;
		model.discoverRadius = 2000;
		model.photoRadius = 600;
		model.closePhotoRadius = 120;
		model.closePhotoK = 2;

		var model = addModel(Stages.accelerationblock, "Acceleration block");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;

		var model = addModel(Stages.ionblock, "Ion engine");
		model.isIon = true;
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;

		var model = addModel(Stages.ionfblock, "Ion engine F");
		model.isIon = true;
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;

		var model = addModel(Stages.stage33, "Stage 3/3");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.stage22, "Stage 2/2");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.stage23, "Stage 2/3");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.sidestage153, "Side stage 1.5/3");
		model.isSide = true;
		model.linkType = LinkType.SIDE;
		model.linkBottom.type = LinkType.BOTTOM_1;

		var model = addModel(Stages.accelerator, "Accelerator");
		model.isSide = true;
		model.linkType = LinkType.SIDE;
		model.linkBottom.type = LinkType.BOTTOM_05;

		var model = addModel(Stages.accelerators, "Accelerator S");
		model.isSide = true;
		model.linkType = LinkType.SIDE;
		model.linkBottom.type = LinkType.BOTTOM_05;
		model.linkLeft.type = LinkType.SIDE;

		var model = addModel(Stages.stage31forced, "Stage 3/1 (forced)");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.stage21forced, "Stage 2/1 (forced)");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.stage53hard, "Stage 5/3 (hard)");
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.sidestage53hard, "Stage S 5/3 (hard)");
		model.isSide = true;
		model.linkType = LinkType.SIDE;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.SIDE;
		model.linkRight.type = LinkType.SIDE;

		var model = addModel(Stages.acceleratorforced, "Accelerator (forced)");
		model.isSide = true;
		model.linkType = LinkType.SIDE;
		model.linkBottom.type = LinkType.BOTTOM_05;

		var model = addModel(Stages.acceleratorsforced, "Accelerator S (forced)");
		model.isSide = true;
		model.linkType = LinkType.SIDE;
		model.linkBottom.type = LinkType.BOTTOM_05;
		model.linkLeft.type = LinkType.SIDE;

		var model = addModel(Stages.joint12, "Joint 1/2");
		model.isJoint = true;
		model.linkType = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.BOTTOM_1;
		model.linkRight.type = LinkType.BOTTOM_1;

		var model = addModel(Stages.joint13, "Joint 1/3");
		model.isJoint = true;
		model.linkType = LinkType.BOTTOM_1;
		model.linkBottom.type = LinkType.BOTTOM_1;
		model.linkLeft.type = LinkType.BOTTOM_1;
		model.linkRight.type = LinkType.BOTTOM_1;

		var model = addModel(Stages.cowlprimary, "Cowl");
		model.isCowl = true;
		model.linkType = LinkType.COWL_1;

		var model = addModel(Stages.cowlbig, "Cowl (big)");
		model.isCowl = true;
		model.linkType = LinkType.COWL_2;
		
		initLevels();
	}

	public static var levels:Array<StageModelStack> = new Array<StageModelStack>();

	private static function addStage(info:StageInfo, count:Int):Void {
		var index = levels.length - 1;
		levels[index] = new StageModelStack(info.model, count, levels[index]);
	}

	private static function initLevels():Void {
		/*0*/levels.push(null);
		addStage(Stages.accelerationblock, 1);//
		addStage(Stages.stage23, 1);//
		addStage(Stages.cowlprimary, 1);//
		/*1*/levels.push(null);
		addStage(Stages.accelerator, 2);//
		addStage(Stages.stage22, 1);//
		addStage(Stages.cowlbig, 2);//
		addStage(Stages.stage33, 1);//
		/*2*/levels.push(null);
		addStage(Stages.stage23, 1);//
		/*3*/levels.push(null);
		addStage(Stages.joint12, 1);//
		//------------------------------------
		/*4*/levels.push(null);
		addStage(Stages.sidestage153, 4);//
		addStage(Stages.joint13, 1);//
		addStage(Stages.stage23, 2);
		/*5*/levels.push(null);
		addStage(Stages.ionblock, 1);//
		addStage(Stages.stage31forced, 1);//
		/*6*/levels.push(null);
		addStage(Stages.acceleratorforced, 2);//
		/*7*/levels.push(null);
		addStage(Stages.accelerators, 2);//
		/*8*/levels.push(null);
		addStage(Stages.acceleratorsforced, 2);//
		/*9*/levels.push(null);
		addStage(Stages.stage21forced, 1);//
		/*10*/levels.push(null);
		addStage(Stages.stage53hard, 1);//
		addStage(Stages.stage23, 1);
		/*11*/levels.push(null);
		addStage(Stages.sidestage53hard, 2);//
		addStage(Stages.accelerators, 2);
		/*12*/levels.push(null);
		addStage(Stages.ionfblock, 1);//
		addStage(Stages.stage21forced, 1);
		/*13*/levels.push(null);
		addStage(Stages.stage31forced, 1);
		addStage(Stages.stage53hard, 1);
		/*14*/levels.push(null);
		addStage(Stages.acceleratorsforced, 2);
		addStage(Stages.sidestage53hard, 2);
		/*15*/levels.push(null);
		addStage(Stages.stage53hard, 1);
		addStage(Stages.acceleratorforced, 2);
		/*16*/levels.push(null);
		addStage(Stages.sidestage153, 2);
		addStage(Stages.sidestage53hard, 2);
		addStage(Stages.ionblock, 1);
		/*17*/levels.push(null);
		addStage(Stages.stage21forced, 1);
		addStage(Stages.stage31forced, 1);
		/*18*/levels.push(null);
		addStage(Stages.stage21forced, 1);
		addStage(Stages.acceleratorsforced, 2);
		/*19*/levels.push(null);
		addStage(Stages.stage31forced, 1);
		addStage(Stages.joint12, 1);
		/*20*/levels.push(null);
		addStage(Stages.accelerators, 2);
		addStage(Stages.joint12, 1);
		/*21*/levels.push(null);
		addStage(Stages.accelerator, 2);
		addStage(Stages.stage23, 1);
		addStage(Stages.acceleratorforced, 2);
		addStage(Stages.ionblock, 1);
		//------------------------------------
		/*22*/levels.push(null);
		addStage(Stages.joint13, 1);
		addStage(Stages.accelerationblock, 1);
		/*23*/levels.push(null);
		addStage(Stages.stage22, 1);
		addStage(Stages.sidestage53hard, 2);
		/*24*/levels.push(null);
		addStage(Stages.stage23, 1);
		addStage(Stages.stage53hard, 1);
		/*25*/levels.push(null);
		addStage(Stages.stage33, 1);
		addStage(Stages.sidestage153, 2);
		addStage(Stages.sidestage53hard, 2);
		/*26*/levels.push(null);
		addStage(Stages.accelerator, 2);
		addStage(Stages.stage53hard, 1);
		/*27*/levels.push(null);
		addStage(Stages.accelerators, 2);
		addStage(Stages.stage53hard, 1);
		/*28*/levels.push(null);
		addStage(Stages.joint13, 1);
		addStage(Stages.stage53hard, 1);
		addStage(Stages.sidestage53hard, 2);
		/*29*/levels.push(null);
		addStage(Stages.joint12, 1);
		addStage(Stages.sidestage153, 2);

		/*------------------------------------
		addStage(Stages.accelerationblock, 1);
		addStage(Stages.ionblock, 1);
		addStage(Stages.ionfblock, 1);
		addStage(Stages.stage22, 1);
		addStage(Stages.stage23, 1);
		addStage(Stages.stage33, 1);
		addStage(Stages.accelerator, 2);
		addStage(Stages.accelerators, 2);
		addStage(Stages.joint12, 1);
		addStage(Stages.joint13, 1);
		addStage(Stages.sidestage153, 2);
		addStage(Stages.stage21forced, 1);
		addStage(Stages.stage31forced, 1);
		addStage(Stages.acceleratorforced, 2);
		addStage(Stages.acceleratorsforced, 2);
		addStage(Stages.stage53hard, 1);
		addStage(Stages.sidestage53hard, 2);
		------------------------------------*/
	}
}
