package model;

import flash.geom.Point;

class LinkModel {
	public function new(stage:StageModel, x:Float, y:Float) {
		this.stage = stage;
		this.x = x;
		this.y = y;
	}

	public var stage(default, null):StageModel;
	public var x(default, null):Float;
	public var y(default, null):Float;

	public var type:LinkType = LinkType.NONE;

	private var _child:StageModel;
	public var child(get, set):StageModel;
	private function get_child():StageModel {
		return _child;
	}
	private function set_child(value:StageModel):StageModel {
		if (_child != value) {
			if (_child != null) {
				_child.parent = null;
			}
			_child = value;
			if (_child != null) {
				_child.parent = this;
			}
		}
		return _child;
	}

	public var isLeft(get, never):Bool;
	private function get_isLeft():Bool {
		return stage.linkLeft == this;
	}

	public var isRight(get, never):Bool;
	private function get_isRight():Bool {
		return stage.linkRight == this;
	}

	public var isTop(get, never):Bool;
	private function get_isTop():Bool {
		return stage.linkTop == this;
	}

	public var isBottom(get, never):Bool;
	private function get_isBottom():Bool {
		return stage.linkBottom == this;
	}

	public var calculatedMirror:Bool;
	public var calculatedX:Float;
	public var calculatedY:Float;
}
