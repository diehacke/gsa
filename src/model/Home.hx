package model;

import flash.errors.Error;
import utils.ListUtil;
import game.PlanetInfos;
import game.Planet;
import game.Rocket;
import game.WorldSystem;
import game.Stages;
import haxe.ds.IntMap;
import model.Mission;
import tutorial.Tutorial10;
import tutorial.Tutorial20;
import tutorial.Tutorial30;
import tutorial.Tutorial40;
import tutorial.Tutorial50;
import tutorial.Tutorial;
import ui.UIRects;
import utils.TabData;

typedef MissionData = {
	id:Int,
	completed:Bool,
	rocketModel:StageData
}

typedef HomeData = {
	selectedMissionId:Int,
	missionDatas:Array<MissionData>,
	planetDatas:Array<StatusData>
}

typedef StageData = {
	name:String,
	left:StageData,
	right:StageData,
	top:StageData,
	bottom:StageData
}

typedef StatusData = {
	name:String,
	discovered:Bool,
	hasPhoto:Bool,
	hasClosePhoto:Bool,
	hasLandingPhoto:Bool
}

class Home {
	public function new() {
		reset();
	}

	public function reset():Void {
		for (info in PlanetInfos.all) {
			info.status.discovered = false;
			info.status.hasPhoto = false;
			info.status.hasClosePhoto = false;
			info.status.hasLandingPhoto = false;
		}

		PlanetInfos.arial.status.discovered = true;
		PlanetInfos.arial.status.hasClosePhoto = true;
		PlanetInfos.arial.status.hasPhoto = true;
		PlanetInfos.arial.status.hasLandingPhoto = true;

		PlanetInfos.glory.status.discovered = true;
		PlanetInfos.glory.status.hasClosePhoto = true;
		PlanetInfos.glory.status.hasPhoto = true;
		PlanetInfos.glory.status.hasLandingPhoto = true;

		PlanetInfos.loma.status.discovered = true;
		PlanetInfos.loma.status.hasPhoto = true;
		PlanetInfos.loma.status.hasClosePhoto = true;

		_missions = new Array<Mission>();
		var mission10 = addMission(10)
			.setOpened(true)
			.setWorldAndRocket(newArialSystem, newPrimaryRocket)
			.setDesc("Place your first space vehicle in orbit!", UIRects.mission_preview_1)
			.setTutorial("Tutorial 1", new Tutorial10())
			.setIncreaseLevel(1);
		var mission20 = addMission(20)
			.setWorldAndRocket(newArialSystem, newTelescopeRocket)
			.setDesc("Place telescope in " + PlanetInfos.glory.name + " orbit for planets exploring",
				UIRects.mission_preview_2)
			.setTutorial("Tutorial 2", new Tutorial20())
			.setIncreaseLevel(2);
		var mission30 = addMission(30)
			.setWorldAndRocket(newArialSystem, newExplorerRocket)
			.setDesc("Fly away from " + PlanetInfos.glory.name + " to interplanetary space",
				UIRects.mission_preview_8)
			.setTutorial("Tutorial 3", new Tutorial30())
			.setIncreaseLevel(2);
		var mission40 = addMission(40).setSubtitle("Site: " + PlanetInfos.glory.name + " orbit")
			.setWorldAndRocket(newArialSystem_GloryOrbit, newExplorerMRocket_single)
			.setDesc("Test landing with brolly\n\nUse 'E' button to show/hide brolly",
				UIRects.mission_preview_4)
			.setAllowConstructor(false)
			.setTutorial("Tutorial 4", new Tutorial40())
			.setIncreaseLevel(3);
		var mission50 = addMission(50).setSubtitle("Site: " + PlanetInfos.loma.name + " orbit")
			.setWorldAndRocket(newArialSystem_LomaOrbit, newExplorerRocket_single)
			.setDesc("Ship is placed in " + PlanetInfos.loma.name + " orbit\n\nTry to land it",
				UIRects.mission_preview_3)
			.setTutorial("Tutorial 5", new Tutorial50())
			.setAllowConstructor(false)
			.setIncreaseLevel(3);
		var mission31 = addMission(31)
			.setWorldAndRocket(newArialSystem, newExplorerRocket)
			.setDesc("Research unknown planets!", UIRects.mission_preview_8);
		var mission60 = addMission(60)
			.setWorldAndRocket(newArialSystem, newExplorerMRocket)
			.setDesc("Planets with atmosphere exploring is more simple with brolly",
				UIRects.mission_preview_4);
		var mission80 = addMission(80).setSubtitle("Site: " + PlanetInfos.purisa.name + " orbit")
			.setWorldAndRocket(newArialSystem_PurisaOrbit, newExplorerMRocket_single)
			.setDesc("Mission in process\n\nTake control of it", UIRects.mission_preview_7)
			.setTutorial("Side mission", null)
			.setAllowConstructor(false);

		mission10.openOnCompleteMissions.push(mission20);
		mission10.openOnCompleteMissions.push(mission30);

		mission20.openOnCompleteMissions.push(mission40);
		mission20.openOnCompleteMissions.push(mission50);

		mission30.openOnCompleteMissions.push(mission31);
		mission30.openOnCompleteMissions.push(mission40);
		mission30.openOnCompleteMissions.push(mission50);

		mission40.openOnCompleteMissions.push(mission60);

		mission50.openOnCompleteMissions.push(mission60);

		mission80.planetForOpen = PlanetInfos.purisa;

		updateOpenedByComplete();
		updateStagesCount();

		selectedMission = _missions[0];
	}

	private function addMission(id:Int):Mission {
		var mission = new Mission(id);
		_missions.push(mission);
		return mission;
	}

	private function newArialSystem(rocket:Rocket):WorldSystem {
		var system = new ArialSystem();
		system.setLanded(system.glory, rocket);
		return system;
	}

	private function newArialSystem_GloryOrbit(rocket:Rocket):WorldSystem {
		var system = new ArialSystem();
		system.setSatellite(system.glory, rocket, 70, 0, false);
		return system;
	}

	private function newArialSystem_LomaOrbit(rocket:Rocket):WorldSystem {
		var system = new ArialSystem();
		system.setSatellite(system.loma, rocket, 30, Math.PI * 1.5, true);
		return system;
	}

	private function newArialSystem_PurisaOrbit(rocket:Rocket):WorldSystem {
		var system = new ArialSystem();
		system.setSatellite(system.purisa, rocket, 800, 0, false);
		return system;
	}

	private function newPrimaryRocket():StageModel {
		return TabData.execute('
			Stages.shipprimary.model.clone()
			|setTop Stages.cowlprimary.model.clone()
			|setBottom Stages.accelerationblock.model.clone()
				|setBottom Stages.stage23.model.clone()
		');
	}

	private function newTelescopeRocket():StageModel {
		return TabData.execute('
			Stages.shiptelescope.model.clone()
			|setLeft Stages.cowlbig.model.clone()
			|setRight Stages.cowlbig.model.clone()
			|setBottom Stages.stage22.model.clone()
				|setLeft Stages.accelerator.model.clone()
				|setRight Stages.accelerator.model.clone()
		');
	}

	private function newExplorerRocket():StageModel {
		return TabData.execute('
			Stages.shipexplorer.model.clone()
			|setLeft Stages.cowlbig.model.clone()
			|setRight Stages.cowlbig.model.clone()
			|setBottom Stages.accelerationblock.model.clone()
				|setBottom Stages.stage33.model.clone()
					|setLeft Stages.accelerator.model.clone()
					|setRight Stages.accelerator.model.clone()
		');
	}

	private function newExplorerMRocket():StageModel {
		return TabData.execute('
			Stages.shipexplorer2.model.clone()
			|setLeft Stages.cowlbig.model.clone()
			|setRight Stages.cowlbig.model.clone()
			|setBottom Stages.stage22.model.clone()
				|setBottom Stages.joint12.model.clone()
					|setLeft Stages.stage23.model.clone()
						|setLeft Stages.accelerator.model.clone()
					|setRight Stages.stage23.model.clone()
						|setLeft Stages.accelerator.model.clone()
		');
	}

	private function newExplorerRocket_single():StageModel {
		return TabData.execute('
			Stages.shipexplorer.model.clone()
		');
	}

	private function newExplorerMRocket_single():StageModel {
		return TabData.execute('
			Stages.shipexplorer2.model.clone()
		');
	}

	private var _missions:Array<Mission>;
	public var missions(get_missions, never):Array<Mission>;
	private function get_missions():Array<Mission> {
		return _missions;
	}

	public var selectedMission:Mission;

	public function save(data:HomeData):Void {
		data.selectedMissionId = selectedMission != null ? selectedMission.id : -1;
		data.missionDatas = new Array<MissionData>();
		for (mission in _missions) {
			var missionData = {
				id:mission.id,
				completed:mission.completed,
				rocketModel:serializeStage(mission.rocketModel)
			};
			data.missionDatas.push(missionData);
		}

		data.planetDatas = new Array<StatusData>();
		for (info in PlanetInfos.all) {
			if (info.status != null) {
				data.planetDatas.push({
					name:info.name,
					discovered:info.status.discovered,
					hasPhoto:info.status.hasPhoto,
					hasClosePhoto:info.status.hasClosePhoto,
					hasLandingPhoto:info.status.hasLandingPhoto
				});
			}
		}
	}

	public function load(data:HomeData):Void {
		reset();
		try {
			var missionById = new IntMap<Mission>();
			for (mission in _missions) {
				missionById.set(mission.id, mission);
			}
			if (data.missionDatas != null) {
				for (missionData in data.missionDatas) {
					var mission = missionById.get(missionData.id);
					if (mission != null) {
						mission.completed = missionData.completed;
						mission.rocketModel = unserializeStage(missionData.rocketModel);
					}
				}
			}

			selectedMission = missionById.get(data.selectedMissionId);
			if (selectedMission == null)
				selectedMission = _missions[0];

			var statusById = new Map<String, PlanetStatus>();
			for (info in PlanetInfos.all) {
				if (info.status != null) {
					statusById[info.name] = info.status;
				}
			}
			if (data.planetDatas != null) {
				for (planetData in data.planetDatas) {
					if (planetData != null) {
						var status = statusById[planetData.name];
						if (status != null) {
							if (planetData.discovered)
								status.discovered = true;
							if (planetData.hasPhoto)
								status.hasPhoto = true;
							if (planetData.hasClosePhoto)
								status.hasClosePhoto = true;
							if (planetData.hasLandingPhoto)
								status.hasLandingPhoto = true;
						}
					}
				}
			}
			
			updateOpenedByComplete();
			updateStagesCount();
		} catch (error:Error) {
			reset();
		}
	}

	private static function serializeStage(model:StageModel):StageData {
		if (model == null)
			return null;
		return {
			name:model.name,
			left:serializeStage(model.linkLeft.child),
			right:serializeStage(model.linkRight.child),
			top:serializeStage(model.linkTop.child),
			bottom:serializeStage(model.linkBottom.child)
		};
	}

	private static function unserializeStage(data:StageData):StageModel {
		if (data == null)
			return null;
		var model = StageModels.getModel(data.name).clone();
		if (model == null)
			return null;
		model.linkLeft.child = unserializeStage(data.left);
		model.linkRight.child = unserializeStage(data.right);
		model.linkTop.child = unserializeStage(data.top);
		model.linkBottom.child = unserializeStage(data.bottom);
		return model;
	}

	private function updateOpenedByComplete():Void {
		for (mission in _missions) {
			if (mission.completed) {
				for (missionI in mission.openOnCompleteMissions) {
					missionI.opened = true;
				}
			}
			if (mission.planetForOpen != null && mission.planetForOpen.status.hasClosePhoto) {
				mission.opened = true;
			}
		}
	}

	public function updateStagesCount():Void {
		for (model in StageModels.models) {
			model.count = 0;
			model.visible = false;
		}
		var level0 = 0;
		var level1 = 0;
		for (mission in missions) {
			if (mission.completed && mission.increaseLevel > level0) {
				level0 = mission.increaseLevel;
			}
		}
		for (info in PlanetInfos.all) {
			if (!info.status.discovered)
				continue;
			if (info == PlanetInfos.arial ||
				info == PlanetInfos.glory)
				continue;
			if (info.status.hasLandingPhoto)
				level1++;
			if (info == PlanetInfos.loma)
				continue;
			if (info.status.hasPhoto)
				level1++;
			if (info.status.hasClosePhoto)
				level1++;
		}
		var level = level0 + level1;
		if (level > StageModels.levels.length - 1)
			level = StageModels.levels.length - 1;
		for (levelI in 0 ... level + 1) {
			var stack = StageModels.levels[levelI];
			while (stack != null) {
				if (stack.count > 0) {
					stack.model.count += stack.count;
					stack.model.visible = true;
				}
				stack = stack.next;
			}
		}
		if (level + 1 < StageModels.levels.length) {
			var stack = StageModels.levels[level + 1];
			while (stack != null) {
				if (stack.count > 0) {
					stack.model.visible = true;
				}
				stack = stack.next;
			}
		}
	}
}
