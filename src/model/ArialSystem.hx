package model;

import flash.Vector;
import game.Ring;
import game.Planet;
import game.PlanetInfos;
import game.World;
import game.WorldSystem;
import game.Rocket;
import utils.Vertex;
import utils.MathUtil;
import utils.TabData;

class ArialSystem extends WorldSystem {
	public var helvetica:Planet;
	public var verdana:Planet;
	public var loma:Planet;
	public var tahoma:Planet;
	public var saab:Planet;
	public var purisa:Planet;
	public var dotum:Planet;
	public var kinnary:Planet;
	public var sans:Planet;

	public function new() {
		super();

		star = new Planet(PlanetInfos.arial);
		star.r.setXY(0, 0);
		star.v.setXY(0, 0);
		star.m = 4800;
		star.surface = Assets.surface_arial_png;
		addUnit(star);

		helvetica = new Planet(PlanetInfos.helvetica);
		helvetica.m = 1.8;
		helvetica.surface = Assets.surface_helvetica_png;
		setSatellite(star, helvetica, 1000, 0, false);

		verdana = new Planet(PlanetInfos.verdana);
		verdana.m = 10;
		verdana.surface = Assets.surface_verdana_png;
		setSatellite(star, verdana, 3500, 0, false);
		addRing(verdana, 100, 0, 1.7, 1.7, 1, 0, 0, true, 0xcccccc);
		addRing(verdana, 150, 0, 1.6, 1.6, 1, 0, 0, true, 0xcccccc);

		glory = new Planet(PlanetInfos.glory);
		glory.m = World.GLORY_M;
		glory.surface = Assets.surface_glory_png;
		//for (i in 0 ... 6) {
		//	glory.rings = new Ring(glory.rings, glory.m, 80 + i * 5, 5, .4, Math.PI * .3, 0xffffff, 0xcccccc);
		//}
		setSatellite(star, glory, 7500, 0, false);

		loma = new Planet(PlanetInfos.loma);
		loma.m = .3;
		loma.surface = Assets.surface_loma_png;
		setSatellite(glory, loma, 230, Math.PI * 1 / 3, false);

		tahoma = new Planet(PlanetInfos.tahoma);
		tahoma.m = 50;
		tahoma.surface = Assets.surface_tahoma_png;
		setSatellite(star, tahoma, 14000, Math.PI * 1 / 3, true);

		saab = new Planet(PlanetInfos.saab);
		saab.m = .08;
		saab.surface = Assets.surface_helvetica_png;
		setSatellite(tahoma, saab, 550, 0, false);

		//addRing(tahoma, 100, 20, .5, 1, 250, Math.PI / 3, Math.PI / 6, true, 0xccccff);
		//addRing(tahoma, 150, 5, .5, 1, 150, Math.PI / 3, Math.PI / 6, true, 0xeeddff);
		tahoma.rings = new Ring(tahoma.rings, tahoma.m, 95, 5, .4, -Math.PI * .1, 0xccccff, 0x8888cc);
		tahoma.rings = new Ring(tahoma.rings, tahoma.m, 100, 5, .4, -Math.PI * .1, 0xccccff, 0x8888cc);
		tahoma.rings = new Ring(tahoma.rings, tahoma.m, 110, 10, .4, -Math.PI * .1, 0xccccff, 0x8888cc);
		tahoma.rings = new Ring(tahoma.rings, tahoma.m, 155, 5, .4, -Math.PI * .1, 0xeeddff, 0xccddff);

		purisa = new Planet(PlanetInfos.purisa);
		purisa.m = 50;
		purisa.surface = Assets.surface_purisa_png;
		setSatellite(star, purisa, 18000, Math.PI * 2 / 3, false);

		dotum = new Planet(PlanetInfos.dotum);
		dotum.m = 5;
		dotum.surface = Assets.surface_dotum_png;
		setSatellite(purisa, dotum, 200, Math.PI * 2 / 3, false);

		kinnary = new Planet(PlanetInfos.kinnary);
		kinnary.m = .5;
		kinnary.surface = Assets.surface_kinnary_png;
		setSatellite(purisa, kinnary, 500, 0, false);

		sans = new Planet(PlanetInfos.sans);
		sans.m = 15;
		sans.surface = Assets.surface_sans_png;
		setSatellite(star, sans, 35000, Math.PI * 1 / 3, false);

		//addRing(sans, 75, 5, .5, 1, 50, Math.PI / 3, Math.PI / 6, true, 0xffccff);
		//addRing(sans, 70, 5, .5, 1, 50, -Math.PI / 6, Math.PI / 3, true, 0xffccff);
		sans.rings = new Ring(sans.rings, sans.m, 75, 2, .43, -Math.PI * .1, 0xaaccff, 0xffccff);
		sans.rings = new Ring(sans.rings, sans.m, 78, 3, .43, -Math.PI * .1, 0xaaccff, 0xffccff);
		sans.rings = new Ring(sans.rings, sans.m, 70, 2, .43, -Math.PI * .7, 0xaaccff, 0xffccff);
		sans.rings = new Ring(sans.rings, sans.m, 73, 3, .43, -Math.PI * .7, 0xaaccff, 0xffccff);
	}
}
