package model;

class StageModelStack {
	public var model(default, null):StageModel;
	public var count(default, null):Int;
	public var next(default, null):StageModelStack;

	public function new(model:StageModel, count:Int, next:StageModelStack) {
		this.model = model;
		this.count = count;
		this.next = next;
	}
}
