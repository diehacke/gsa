package model;

import flash.display.MovieClip;
import game.Rocket;
import game.WorldSystem;
import game.PlanetInfo;
import tutorial.Tutorial;
import ui.UIRect;

class Mission {
	private var _id:Int;
	public var id(get_id, never):Int;
	private function get_id():Int {
		return _id;
	}

	public function new(id:Int) {
		_id = id;
		openOnCompleteMissions = new Array<Mission>();
	}

	public var title:String;
	public var subtitle:String;
	public var opened:Bool;
	public var completed:Bool;
	public var allowConstructor:Bool = true;
	public var newWorld:Rocket->WorldSystem;
	public var newDefaultRocketModel:Void->StageModel;
	public var rocketModel:StageModel;
	public var descText:String;
	public var descImage:UIRect;
	public var tutorial:Tutorial;
	public var openOnCompleteMissions:Array<Mission>;
	public var increaseLevel:Int;
	public var planetForOpen:PlanetInfo;

	public function getLockText():String {
		if (planetForOpen != null) {
			if (planetForOpen.status.discovered)
				return "Need\n" + planetForOpen.name + " HD photo";
			return planetForOpen.name + "\nisn't discovered yet";
		}
		return "";
	}

	public function setSubtitle(value:String):Mission {
		subtitle = value;
		return this;
	}

	public function setOpened(value:Bool):Mission {
		opened = value;
		return this;
	}

	public function setWorldAndRocket(
		newWorld:Rocket->WorldSystem, newDefaultRocketModel:Void->StageModel):Mission {
		this.newWorld = newWorld;
		this.newDefaultRocketModel = newDefaultRocketModel;
		return this;
	}

	public function setAllowConstructor(value:Bool):Mission {
		allowConstructor = value;
		return this;
	}

	public function setDesc(descText:String, descImage:UIRect):Mission {
		this.descText = descText;
		this.descImage = descImage;
		return this;
	}

	public function setTutorial(title:String, tutorial:Tutorial):Mission {
		this.title = title;
		this.tutorial = tutorial;
		return this;
	}

	public function setIncreaseLevel(value:Int):Mission {
		increaseLevel = value;
		return this;
	}
}
