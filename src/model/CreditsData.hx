package model;

typedef CreditsItemData = {
	type:String,
	asset:String,
	name:String,
	changes:String,
	licenceName:String,
	sources:Array<String>,
	authorName:String,
	authorSites:Array<String>
}

class CreditsData {
	public static var data:Array<CreditsItemData> = [
		{
			type:"code",
			asset:"elm.utils.Rand",
			name:"HGE Random",
			changes:"minor changes",
			licenceName:"",
			sources:["http://gamedevblogs.ru/blog/205.html"],
			authorName:"elmortem",
			authorSites:["http://gamedevblogs.ru/profile/elmortem/created/topics/"]
		},
		{
			type:"code",
			asset:"deep.macro.AssetsMacros, deep.macro.IAssets",
			name:"Macros for assets embedding",
			changes:"subsidiary changed",
			licenceName:"",
			sources:["http://haxe.ru/makrosy-haxe-avtomaticheskoe-vstraivanie-resursov-assets-embedding",
				"https://github.com/profelis/macros_lessons/tree/master/macros_2"],
			authorName:"deep",
			authorSites:["http://haxe.ru/blogs/deep",
				"https://github.com/profelis"]
		},
		{
			type:"code",
			asset:"utils.MathUtil.invSqrt",
			name:"Fast inverse square root calculation from Quake",
			changes:"",
			licenceName:"",
			sources:["http://ncannasse.fr/blog/fast_inverse_square_root"],
			authorName:"Nicolas Cannasse",
			authorSites:["https://twitter.com/ncannasse"]
		},
		{
			type:"code",
			asset:"utils.MathUtil.atan2",
			name:"Fast atan2 calculation",
			changes:"",
			licenceName:"",
			sources:["https://gist.github.com/volkansalma/2972237"],
			authorName:"volkansalma",
			authorSites:["https://github.com/volkansalma"]
		},
		{
			type:"sound",
			asset:"tap_on_m_sapphire_8662_hifi.wav",
			name:"Sound Fx » Interfaces: Clunks: Tap on Monitor",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/Interfaces/Clunks/Tap_on_M-Sapphire-8662/index.php"],
			authorName:"SapphireFox",
			authorSites:[]
		},
		{
			type:"sound",
			asset:"clunk1_Intermed_601_hifi.wav",
			name:"Sound Fx » Interfaces: Clunks: Clunk1",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/Interfaces/Clunks/Clunk1-Intermed-601/index.php"],
			authorName:"Intermedia Design Graphics",
			authorSites:["http://www.idgraphics.com"]
		},
		{
			type:"sound",
			asset:"Blast_Of_Kevin_Ph_7939_hifi.wav",
			name:"Sound Fx » Transportation: Rockets: Blast Off",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/Transportation/Rockets/Blast_Of-Kevin_Ph-7939/index.php"],
			authorName:"Kevin Phillips",
			authorSites:[]
		},
		{
			type:"sound",
			asset:"Explosio_Adrian_G_7936_hifi.mp3",
			name:"Sound Fx » Cartoon: Explosions: Explosion",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/Cartoon/Explosions/Explosio-Adrian_G-7936/index.php"],
			authorName:"Adrian Gallant",
			authorSites:[]
		},
		{
			type:"sound",
			asset:"knock.wav",
			name:"Sound Fx » People: Movement: 4 Running Steps",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/People/Movement/4_Runnin-BlackCow-8185/index.php"],
			authorName:"BlackCow",
			authorSites:[]
		},
		{
			type:"sound",
			asset:"unlink.wav",
			name:"Sound Fx » Domestic: Doors: door closing",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/Domestic/Doors/door_clo-donnie_t-7336/index.php"],
			authorName:"donnie thompson",
			authorSites:["http://www.geocities.com/vertigowebdesign"]
		},
		{
			type:"sound",
			asset:"clap.wav",
			name:"Sound Fx » People: Handclap reverb",
			changes:"equalized",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/People/Handclap-Liquid-8848/index.php"],
			authorName:"Liquid",
			authorSites:["http://www.soundsnap.com"]
		},
		{
			type:"sound",
			asset:"spacey_alarm.wav",
			name:"Sound Fx » Electronic: Alarms: spacey alarm",
			changes:"cutted",
			licenceName:"Freeware",
			sources:["http://www.flashkit.com/soundfx/Electronic/Alarms/spacey_a-genex89-8028/index.php"],
			authorName:"genex89",
			authorSites:[]
		},
		{
			type:"sound",
			asset:"... character talkng ...",
			name:"Generated with CW Speak",
			changes:"seeded up",
			licenceName:null,
			sources:[],
			authorName:null,
			authorSites:["http://codewelt.com/proj/speak"]
		},
		{
			type:"sound",
			asset:"... music ...",
			name: "Writen with lmms default instruments",
			changes:"",
			licenceName:null,
			sources:[],
			authorName:null,
			authorSites:["https://lmms.io"]
		}
	];
}
