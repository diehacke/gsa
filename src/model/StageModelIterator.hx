package model;

import flash.Vector;

class StageModelIterator {
	private var _model:StageModel;
	private var _stack:Vector<StageModel>;

	public function new(model:StageModel) {
		_model = model;
		_stack = new Vector<StageModel>();
		reset();
	}

	public var current(default, null):StageModel;

	public function reset():Void {
		current = null;
		_stack.length = 0;
		_stack.push(_model);
	}

	public function next():Bool {
		if (_stack.length == 0) {
			current = null;
			return false;
		}
		current = _stack.pop();
		if (current.linkLeft.child != null)
			_stack.push(current.linkLeft.child);
		if (current.linkRight.child != null)
			_stack.push(current.linkRight.child);
		if (current.linkTop.child != null)
			_stack.push(current.linkTop.child);
		if (current.linkBottom.child != null)
			_stack.push(current.linkBottom.child);
		return true;
	}
}
