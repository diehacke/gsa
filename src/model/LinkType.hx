package model;

enum LinkType {
	NONE;
	BOTTOM_1;
	BOTTOM_05;
	SIDE;
	COWL_1;
	COWL_2;
}
