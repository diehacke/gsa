package model;
import game.PlanetInfos;

class PlanetStatus {
	public function new() {
	}

	public var discovered:Bool;
	public var hasPhoto:Bool;
	public var hasClosePhoto:Bool;
	public var hasLandingPhoto:Bool;
}
