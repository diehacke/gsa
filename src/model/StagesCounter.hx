package model;

class StagesCounter {
	public function new() {
	}

	private var _countOf:Map<StageModel, Int>;

	public function start():StagesCounter {
		_countOf = new Map<StageModel, Int>();
		for (model in StageModels.models) {
			_countOf[model] = model.count;
		}
		return this;
	}

	public function stop():StageModelStack {
		var stack = null;
		if (_countOf != null) {
			for (model in StageModels.models) {
				var count = model.count - _countOf[model];
				if (count > 0) {
					stack = new StageModelStack(model, count, stack);
				}
			}
		}
		start();
		return stack;
	}
}
