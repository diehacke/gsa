package model;

import game.StageInfo;
import game.RocketStage;

class StageModel {
	public function new(info:StageInfo) {
		this.info = info;

		handleX = info.handle.x;
		handleY = info.handle.y;
		maxARatio = 1;

		linkLeft = new LinkModel(this, info.linkLeft.x, info.linkLeft.y);
		linkRight = new LinkModel(this, info.linkRight.x, info.linkRight.y);
		linkTop = new LinkModel(this, info.linkTop.x, info.linkTop.y);
		linkBottom = new LinkModel(this, info.linkBottom.x, info.linkBottom.y);
		_links = [linkLeft, linkRight, linkTop, linkBottom];

		nozzlesH = 0;
		if (info.nozzles.length > 0) {
			var first = true;
			for (point in info.nozzles) {
				if (first || nozzlesH < point.y)
					nozzlesH = point.y;
				first = false;
			}
		} else {
			nozzlesH = linkBottom.y;
		}
	}

	public var info(default, null):StageInfo;
	public var name:String;

	public var parent:LinkModel;

	public var handleX(default, null):Float;
	public var handleY(default, null):Float;
	public var nozzlesH(default, null):Float;

	public var linkLeft(default, null):LinkModel;
	public var linkRight(default, null):LinkModel;
	public var linkTop(default, null):LinkModel;
	public var linkBottom(default, null):LinkModel;

	private var _links:Array<LinkModel>;
	public var links(get, never):Iterable<LinkModel>;
	private function get_links():Iterable<LinkModel> {
		return _links;
	}

	public var isJoint:Bool;
	public var isShip:Bool;
	public var isCowl:Bool;
	public var isSide:Bool;
	public var hasAntenna:Bool;
	public var hasBrolly:Bool;
	public var isIon:Bool;
	public var maxARatio:Float;
	public var linkType:LinkType = LinkType.NONE;

	public var discoverRadius:Float = 0;
	public var photoRadius:Float = 0;
	public var closePhotoRadius:Float = 0;
	public var closePhotoK:Float = 1;

	public var count:Int;
	public var visible:Bool;

	public function getLocked():Bool {
		return count <= 0;
	}

	public function clone():StageModel {
		var model = new StageModel(info);
		model.name = name;
		model.isShip = isShip;
		model.isJoint = isJoint;
		model.isCowl = isCowl;
		model.isSide = isSide;
		model.hasBrolly = hasBrolly;
		model.hasAntenna = hasAntenna;
		model.maxARatio = maxARatio;
		model.linkType = linkType;
		model.discoverRadius = discoverRadius;
		model.photoRadius = photoRadius;
		model.closePhotoRadius = closePhotoRadius;
		model.closePhotoK = closePhotoK;
		model.isIon = isIon;
		for (i in 0 ... 4) {
			var link = _links[i];
			var modelLink = model._links[i];
			modelLink.type = link.type;
			if (link.child != null)
				modelLink.child = link.child.clone();
		}
		return model;
	}

	public function setLeft(stage:StageModel):StageModel {
		linkLeft.child = stage;
		return this;
	}

	public function setRight(stage:StageModel):StageModel {
		linkRight.child = stage;
		return this;
	}

	public function setTop(stage:StageModel):StageModel {
		linkTop.child = stage;
		return this;
	}

	public function setBottom(stage:StageModel):StageModel {
		linkBottom.child = stage;
		return this;
	}

	public function newRootRocketStage():RocketStage {
		calculateTree();
		return newRocketStage();
	}

	private function newRocketStage():RocketStage {
		var stage = new RocketStage(this);
		if (linkLeft.child != null)
			linkLeft.child.newRocketStage().linkTo(stage.linkLeft);
		if (linkRight.child != null)
			linkRight.child.newRocketStage().linkTo(stage.linkRight);
		if (linkTop.child != null)
			linkTop.child.newRocketStage().linkTo(stage.linkTop);
		if (linkBottom.child != null)
			linkBottom.child.newRocketStage().linkTo(stage.linkBottom);
		return stage;
	}

	public function calculateTree():StageModel {
		var root = this;
		while (root.parent != null) {
			root = root.parent.stage;
		}
		root.privateCalculateTree(0, 0, false);
		return this;
	}

	private function privateCalculateTree(x:Float, y:Float, mirror:Bool):Void {
		calculatedX = x - handleX * (mirror ? -1 : 1);
		calculatedY = y - handleY;
		calculatedMirror = mirror;
		for (link in links) {
			link.calculatedX = calculatedX + link.x * (mirror ? -1 : 1);
			link.calculatedY = calculatedY + link.y;
			link.calculatedMirror = link.isRight != mirror;
			if (link.child != null) {
				link.child.privateCalculateTree(link.calculatedX, link.calculatedY, link.calculatedMirror);
			}
		}
	}

	public var calculatedMirror:Bool;
	public var calculatedX:Float;
	public var calculatedY:Float;

	public function getUsedCount(model:StageModel):Int {
		var count = 0;
		if (name == model.name)
			count++;
		if (linkLeft.child != null)
			count += linkLeft.child.getUsedCount(model);
		if (linkRight.child != null)
			count += linkRight.child.getUsedCount(model);
		if (linkTop.child != null)
			count += linkTop.child.getUsedCount(model);
		if (linkBottom.child != null)
			count += linkBottom.child.getUsedCount(model);
		return count;
	}

	public function equalTo(model:StageModel):Bool {
		if (model == null || name != model.name)
			return false;
		if (linkLeft.child != null || model.linkLeft.child != null) {
			if (linkLeft.child == null)
				return false;
			if (!linkLeft.child.equalTo(model.linkLeft.child))
				return false;
		}
		if (linkRight.child != null || model.linkRight.child != null) {
			if (linkRight.child == null)
				return false;
			if (!linkRight.child.equalTo(model.linkRight.child))
				return false;
		}
		if (linkTop.child != null || model.linkTop.child != null) {
			if (linkTop.child == null)
				return false;
			if (!linkTop.child.equalTo(model.linkTop.child))
				return false;
		}
		if (linkBottom.child != null || model.linkBottom.child != null) {
			if (linkBottom.child == null)
				return false;
			if (!linkBottom.child.equalTo(model.linkBottom.child))
				return false;
		}
		return true;
	}
}
