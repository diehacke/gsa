package game;

import flash.display.Sprite;
import flash.display.Graphics;
import flash.display.BitmapData;

class BigCamera {
	public function new() {
	}

	public var scale:Float = 1;
	public var x:Float = 0;
	public var y:Float = 0;
	public var halfW:Float = 0;
	public var halfH:Float = 0;
	public var bottomContainer:Sprite;
	public var container:Sprite;
	public var topContainer:Sprite;
	public var bottomGraphics:Graphics;
	public var graphics:Graphics;
	public var topGraphics:Graphics;
	public var halfSW:Float = 0;
	public var halfSH:Float = 0;
	public var bd:BitmapData;
	public var player:Player;

	public function updateHalfSize():Void {
		halfW = halfSW / scale;
		halfH = halfSH / scale;
	}

	public function updateGraphics():Void {
		bottomGraphics = bottomContainer.graphics;
		graphics = container.graphics;
		topGraphics = topContainer.graphics;
	}

	public var outSX:Float;
	public var outSY:Float;

	inline public function screenXY(x:Float, y:Float):Void {
		outSX = halfSW + (x - this.x) * scale;
		outSY = halfSH + (y - this.y) * scale;
	}

	inline public function inArea(x:Float, y:Float, radius:Float):Bool {
		var dx = x - this.x;
		var dy = y - this.y;
		return dx + radius > -halfW &&
			dx - radius < halfW &&
			dy + radius > -halfH &&
			dy - radius < halfH;
	}
}
