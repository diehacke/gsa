package game;

import flash.Vector;
import utils.RotationMatrix;
import utils.MathUtil;
import flash.display.Graphics;
import flash.display.Sprite;
import utils.Vertex;

class Unit {
	public function new() {
	}

	public var name:String;
	public var startR:Vertex = new Vertex();
	public var startV:Vertex = new Vertex();
	public var r:Vertex = new Vertex();
	public var v:Vertex = new Vertex();
	public var engineA:Vertex = new Vertex();
	public var m:Float = 1;
	public var radius:Float = 50;
	public var atmRadius:Float = 0;
	public var hasM:Bool = true;
	public var hasSize:Bool = true;
	public var path:UnitPath;
	public var asPlanet:Planet;
	public var inAtmosphere:Bool;

	public function render(camera:Camera):Void {
	}

	public function renderBig(camera:BigCamera):Void {
	}

	inline private function updateAtmA(
		atmKl:Float, atmKn:Float, direction:Vertex, massive:Vector<Unit>, dt:Float):Void {
		inAtmosphere = false;
		for (unit in massive) {
			var dx = r.x - unit.r.x;
			var dy = r.y - unit.r.y;
			if (dx * dx + dy * dy < unit.atmRadius * unit.atmRadius) {
				var maxH = unit.atmRadius - unit.radius;
				var h = MathUtil.sqrt(dx * dx + dy * dy) - unit.radius;
				if (h > 0 && h < maxH) {
					var kk = 1 - h / maxH;
					var k = .5 * kk * kk / m;
					var vx = v.x - unit.v.x;
					var vy = v.y - unit.v.y;

					var elx = direction.x;
					var ely = direction.y;
					var enx = -direction.y;
					var eny = direction.x;
					var vl = elx * vx + ely * vy;
					var vn = enx * vx + eny * vy;
					var vlx = vl * elx;
					var vly = vl * ely;
					var vnx = vn * enx;
					var vny = vn * eny;
					engineA.x -= vlx * k * atmKl;
					engineA.y -= vly * k * atmKl;
					engineA.x -= vnx * k * atmKn;
					engineA.y -= vny * k * atmKn;
					inAtmosphere = true;
					break;
				}
			}
		}
	}
}
