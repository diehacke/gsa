package game;

import flash.Vector;
import utils.Tween;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.display.Graphics;
import flash.geom.Matrix;
import flash.ui.Keyboard;
import utils.KeyUtil;
import utils.RotationMatrix;
import utils.MathUtil;
import utils.Vertex;

class Rocket extends Unit {
	inline private static var FUEL_RATE:Float = .2;
	inline private static var FUEL_FORCE:Float = 60;

	public var stages:Array<RocketStage>;
	public var ship:RocketStage;

	public function new(ship:RocketStage) {
		super();
		this.ship = ship;
		hasM = false;
		hasSize = false;

		stages = new Array<RocketStage>();
		addStages(ship);
		arrangeStages(ship);
		ship.offsetY = -getMY();
		arrangeStages(ship);
		m = getM();

		_works = new Vector<RocketStage>();
		updateWorks();
		updateFoot();
	}

	private function addStages(stage:RocketStage):Void {
		stages.push(stage);
		for (link in stage.links) {
			if (link.child != null)
				addStages(link.child);
		}
	}

	private function arrangeStages(stage:RocketStage):Void {
		for (link in stage.links) {
			if (link.child != null) {
				link.child.offsetX = stage.offsetX + link.x - link.child.handleX;
				link.child.offsetY = stage.offsetY + link.y - link.child.handleY;
				arrangeStages(link.child);
			}
		}
	}

	private var _works:Vector<RocketStage>;

	private function updateWorks():Void {
		_works.length = 0;
		findWorks(ship);
	}

	private var _cowlValue:Int;
	private var _sizeX:Float;
	private var _sizeY:Float;

	private function updateFoot():Void {
		footY = .0;
		headY = .0;
		for (stage in stages) {
			for (point in stage.nozzles) {
				var y = stage.offsetY + point.y;
				if (footY < y)
					footY = y;
			}
			var y = stage.offsetY;
			if (headY > y)
				headY = y;
			stage.updateState();
		}
		if (ship.linkTop.child != null && ship.linkTop.child.isCowl) {
			_cowlValue = 2;
		} else {
			_cowlValue = 0;
			if (ship.linkLeft.child != null && ship.linkLeft.child.isCowl)
				_cowlValue++;
			if (ship.linkRight.child != null && ship.linkRight.child.isCowl)
				_cowlValue++;
		}

		var first = true;
		var minX = 0.;
		var maxX = 0.;
		var minY = 0.;
		var maxY = 0.;
		for (stage in stages) {
			if (first || minX > stage.offsetX - stage.sizeX / 2)
				minX = stage.offsetX - stage.sizeX / 2;
			if (first || maxX < stage.offsetX + stage.sizeX / 2)
				maxX = stage.offsetX + stage.sizeX / 2;
			if (first || minY > stage.offsetY - stage.sizeY / 2)
				minY = stage.offsetY - stage.sizeY / 2;
			if (first || maxY < stage.offsetY + stage.sizeY / 2)
				maxY = stage.offsetY + stage.sizeY / 2;
			first = false;
		}
		_sizeX = maxX - minX;
		_sizeY = maxY - minY;
	}

	private function getMY():Float {
		var sumM = .0;
		var sumMxY = .0;
		for (stage in stages) {
			var m = stage.netM + stage.maxFuelM;
			sumM += m;
			sumMxY += m * stage.offsetY;
		}
		return if (sumM > .000001)
			sumMxY / sumM;
		else
			.0;
	}

	private function findWorks(stage:RocketStage):Void {
		if (stage.linkBottom.child == null) {
			if (stage.isSide) {
				var endStage = null;
				var stageI = stage;
				while (stageI != null) {
					if (!stageI.isSide || stageI.parent == null) {
						endStage = stageI;
						break;
					}
					if (stageI.linkBottom.child != null)
						break;
					stageI = stageI.parent.owner;
				}
				if (endStage != null && endStage.linkBottom.child == null) {
					_works.push(stage);
				}
			} else {
				_works.push(stage);
			}
		} else {
			findWorks(stage.linkBottom.child);
		}
		if (stage.linkLeft.child != null)
			findWorks(stage.linkLeft.child);
		if (stage.linkRight.child != null)
			findWorks(stage.linkRight.child);
		if (stage.linkTop.child != null)
			findWorks(stage.linkTop.child);
	}

	private var _alpha:Float = -Math.PI / 2;
	private var _dAlpha:Float = .0;


	public var footY:Float;
	public var headY:Float;

	public var direction:Vertex = new Vertex().setXY(1, 0);
	public var collisionV:Vertex = new Vertex().setXY(0, 0);

	public var fixed:Unit;
	public var fixedX:Float;
	public var fixedY:Float;
	public var explosionCompleted:Bool;
	public var showSending:Bool;

	private var _exploded:Bool;
	private var _explosionType:ExplosionType;
	private var _explodedX:Float;
	private var _explodedY:Float;
	private var _explodedPlanet:Unit;
	private var _explosion:MovieClip;
	private var _enginesEnabled:Bool;

	inline public function getEnginesEnabled():Bool {
		return _enginesEnabled;
	}

	inline public function getExploded():Bool {
		return _exploded;
	}

	public function updateControl(world:World):Void {
		if (_exploded)
			return;

		if (world.radioAccessible) {
			var hasFuel = false;
			for (stage in _works) {
				if (!stage.empty) {
					hasFuel = true;
					break;
				}
			}
			_enginesEnabled = hasFuel && KeyUtil.isDown(Keyboard.UP);
			if (_enginesEnabled) {
				if (world.accelerationIndex > 1)
					world.accelerationIndex = 1;
			}
		}
		_dAlpha = .0;
		if (world.radioAccessible) {
			if (KeyUtil.isDown(Keyboard.LEFT)) {
				_dAlpha = -Math.PI * world.spf * .7;
			}
			if (KeyUtil.isDown(Keyboard.RIGHT)) {
				_dAlpha = Math.PI * world.spf * .7;
			}
		}
		updateDirection();
	}

	public var outUnlinked:Bool;

	public function updateEngineA(massive:Vector<Unit>, stages:Vector<RocketStage>, dt:Float):Void {
		outUnlinked = false;
		if (!ship.deployed &&
			ship.linkLeft.child == null && ship.linkRight.child == null && ship.linkTop.child == null)
			ship.deployed = true;
			
		var needUnlink = false;
		if (_needDeploy) {
			_needDeploy = false;
			if (ship.deployed) {
				if (ship.hasBrolly)
					ship.brolly = !ship.brolly;
			} else {
				for (link in ship.links) {
					if (link.child != null && link.child.isCowl && !link.child.needCowlRemove) {
						link.child.needCowlRemove = true;
						needUnlink = true;
					}
				}
			}
		}
		if (_needUnlink) {
			_needUnlink = false;
			for (stage in this.stages) {
				if (!hasChilds(stage)) {
					stage.needUnlink = true;
					needUnlink = true;
				}
			}
			for (link in ship.links) {
				if (link.child != null && link.child.isCowl && !link.child.needCowlRemove) {
					link.child.needCowlRemove = true;
					needUnlink = true;
				}
			}
		}
		if (_enginesEnabled) {
			var aa = .0;
			var m0 = getM();
			for (stage in _works) {
				if (!stage.empty) {
					var dm = stage.fuelRate * dt * FUEL_RATE;
					var k = 1.;
					if (stage.fuelM < dm) {
						k = stage.fuelM / dm;
						dm = stage.fuelM;
						stage.empty = true;
						needUnlink = true;
					}
					stage.fuelM -= dm;
					aa += stage.fuelRate * stage.fuelV * k;
				}
			}
			var m1 = getM();
			m = m1;
			engineA.set(direction).mult(FUEL_FORCE * aa * 2 / (m0 + m1));
		} else {
			engineA.setXY(0, 0);
		}
		if (needUnlink) {
			outUnlinked = unlinkEmpty(stages);
		}
		{
			var kl = _sizeX * RocketStage.ATM_SIZE_K;
			var kn = _sizeY * RocketStage.ATM_SIZE_K;
			kl += kn * .3;
			var atmKl;
			var atmKn;
			if (_exploded) {
				atmKl = 1.;
				atmKn = 1.;
			} else if (ship.brolly) {
				atmKl = 17. + kl;
				atmKn = kn;
			} else {
				if (_cowlValue >= 2) {
					atmKl = .35 * kl;
					atmKn = .8 * kn;
				} else if (_cowlValue == 1) {
					atmKl = .7 * kl;
					atmKn = kn;
				} else {
					atmKl = kl;
					atmKn = kn;
				}
			}
			updateAtmA(atmKl, atmKn, direction, massive, dt);
		}
		var explosionA = World.GLORY_G * ship.maxARatio;
		if (!_exploded && engineA.length2() > explosionA * explosionA) {
			var delta = (engineA.length() - explosionA) / explosionA;
			if (delta > .6)
				delta = .6;
			explosionAdt += dt * delta;
			if (explosionAdt > World.EXPLOSION_ADT) {
				_exploded = true;
				_explosionType = ExplosionType.OVERLOAD;
				_explodedX = 0;
				_explodedY = 0;
				_explodedPlanet = this;
				headY = 0;
				footY = 0;
			}
		} else {
			explosionAdt = 0;
		}
	}

	public var explosionAdt:Float = .0;

	private inline function updateDirection():Void {
		direction.x = Math.cos(_alpha);
		direction.y = Math.sin(_alpha);
	}

	private function getM():Float {
		var result = .0;
		for (stage in stages) {
			result += stage.netM + stage.fuelM;
		}
		return result;
	}

	private var _needDeploy:Bool;
	private var _needUnlink:Bool;

	public function deploy():Void {
		_needDeploy = true;
	}

	public function unlink():Void {
		_needUnlink = true;
	}

	private function unlinkEmpty(stages:Vector<RocketStage>):Bool {
		_matrix.rotateYTo(direction.x, direction.y);
		var changed = false;
		for (i in 0 ... 10) {
			var needUpdateWorks = false;
			for (stage in _works) {
				if ((stage.empty || stage.needUnlink) && stage.parent != null && !hasChilds(stage) &&
					(!stage.isCowl || stage.needCowlRemove ||
					 stage.parent != null && hasOnlyCowls(stage.parent.owner))) {
					var isLeft = stage.parent != null && stage.parent == stage.parent.owner.linkLeft;
					var isRight = stage.parent != null && stage.parent == stage.parent.owner.linkRight;
					var isTop = stage.parent != null && stage.parent == stage.parent.owner.linkTop;
					stage.unlink();
					this.stages.remove(stage);

					_matrix.transform(stage.offsetX, stage.offsetY);
					stage.r.x = r.x + _matrix.outX;
					stage.r.y = r.y + _matrix.outY;
					stage.direction.x = direction.x;
					stage.direction.y = direction.y;

					stage.v.set(v);
					stage.m = stage.netM + stage.fuelM;

					if (isLeft || isRight) {
						_matrix.transform(isLeft ? -1 : 1, 0);
						var v = 5. * (stage.isCowl ? 2 : 1);
						stage.v.x += _matrix.outX * v;
						stage.v.y += _matrix.outY * v;
					} else {
						_matrix.transform(0, isTop ? -1 : 1);
						var v = 2. * (stage.isCowl ? 1.5 : 1);
						stage.v.x += _matrix.outX * v;
						stage.v.y += _matrix.outY * v;
					}

					if (stage.isJoint) {
						stage.destroy();
					} else {
						stages.push(stage);
					}
					needUpdateWorks = true;
					changed = true;
				}
			}
			if (!needUpdateWorks)
				break;
			updateWorks();
		}
		if (changed) {
			if (ship.linkLeft.child == null && ship.linkRight.child == null && ship.linkTop.child == null)
				ship.deployed = true;
			m = getM();

			var oldOffsetY = ship.offsetY;
			ship.offsetY = 0;
			arrangeStages(ship);
			ship.offsetY = -getMY();
			arrangeStages(ship);
			updateFoot();
			var delta = ship.offsetY - oldOffsetY;
			r.x += direction.x * delta;
			r.y += direction.y * delta;
			cameraOffsetX -= direction.x * delta;
			cameraOffsetY -= direction.y * delta;
			Tween.to(this, 500, {cameraOffsetX:0, cameraOffsetY:0}).setEase(Tween.easeInOut);
		}
		return changed;
	}

	private function hasChilds(stage:RocketStage):Bool {
		return stage.linkTop.child != null ||
			stage.linkBottom.child != null ||
			stage.linkLeft.child != null ||
			stage.linkRight.child != null;
	}

	private function hasOnlyCowls(stage:RocketStage):Bool {
		return (stage.linkTop.child == null || stage.linkTop.child.isCowl) &&
			(stage.linkBottom.child == null || stage.linkBottom.child.isCowl) &&
			(stage.linkLeft.child == null || stage.linkLeft.child.isCowl) &&
			(stage.linkRight.child == null || stage.linkRight.child.isCowl);
	}

	private static var _vertices:Vector<Float> = new Vector<Float>();
	private static var _indices:Vector<Int> = new Vector<Int>();
	private static var _uvtData:Vector<Float> = new Vector<Float>();
	private static var _matrix:RotationMatrix = new RotationMatrix();

	private var _showSendingIndex:Float = 0;

	override public function render(camera:Camera):Void {
		if (stages.length > 0) {
			ship.r.x = r.x;
			ship.r.y = r.y;
			ship.direction.x = direction.x;
			ship.direction.y = direction.y;
			ship.render(camera);
			if (!_exploded && showSending) {
				_showSendingIndex += .3;
				var sin = Math.sin(_showSendingIndex);
				camera.screenXY(r.x, r.y);
				var k0 = (_showSendingIndex % 10) * .1;
				var k1 = ((_showSendingIndex + 5) % 10) * .1;
				camera.topGraphics.lineStyle(0, 0xffffff, k0 < .5 ? k0 * 2. : 2. - 2. * k0);
				camera.topGraphics.drawCircle(camera.outSX, camera.outSY, 10 + k0 * 10);
				camera.topGraphics.lineStyle(0, 0xffffff, k1 < .5 ? k1 * 2. : 2. - 2. * k1);
				camera.topGraphics.drawCircle(camera.outSX, camera.outSY, 10 + k1 * 10);
			}
		}
	}

	public var cameraOffsetX:Float = .0;
	public var cameraOffsetY:Float = .0;

	private var _flameIndex:Int;

	override public function renderBig(camera:BigCamera):Void {
		_matrix.rotateYTo(direction.x, direction.y);
		for (stage in stages) {
			_matrix.transform(stage.offsetX, stage.offsetY);
			stage.r.x = r.x + _matrix.outX;
			stage.r.y = r.y + _matrix.outY;
			stage.direction.x = direction.x;
			stage.direction.y = direction.y;
			stage.renderBig(camera);
		}
		if (_enginesEnabled) {
			_vertices.length = 0;
			_indices.length = 0;
			_uvtData.length = 0;
			var verticesI = 0;
			var indicesI = 0;
			var indicesII = 0;
			var uvtI = 0;
			var kw = 1 / Planet.planetsWidth;
			var kh = 1 / Planet.planetsHeight;
			for (stage in _works) {
				if (!stage.hasFuel())
					continue;
				for (point in stage.nozzles) {
					_matrix.transform(point.x, point.y);

					var x = stage.r.x + _matrix.outX;
					var y = stage.r.y + _matrix.outY;
					camera.screenXY(x, y);
					x = camera.outSX;
					y = camera.outSY;
					var sizeX = stage.nozzlesWidth * camera.scale * .7;
					var sizeY = stage.nozzlesWidth * camera.scale * (stage.isIon ? 3 : 2);

					_matrix.transform(-sizeX, 0);
					_vertices[verticesI++] = x + _matrix.outX;
					_vertices[verticesI++] = y + _matrix.outY;
					_matrix.transform(sizeX, 0);
					_vertices[verticesI++] = x + _matrix.outX;
					_vertices[verticesI++] = y + _matrix.outY;
					_matrix.transform(sizeX, sizeY);
					_vertices[verticesI++] = x + _matrix.outX;
					_vertices[verticesI++] = y + _matrix.outY;
					_matrix.transform(-sizeX, sizeY);
					_vertices[verticesI++] = x + _matrix.outX;
					_vertices[verticesI++] = y + _matrix.outY;

					_indices[indicesI++] = indicesII + 0;
					_indices[indicesI++] = indicesII + 1;
					_indices[indicesI++] = indicesII + 2;
					_indices[indicesI++] = indicesII + 2;
					_indices[indicesI++] = indicesII + 3;
					_indices[indicesI++] = indicesII + 0;
					indicesII += 4;

					var ux;
					var uy;
					var u0;
					var v0;
					var u1;
					var v1;
					if (stage.isIon) {
						ux = PlanetInfos.ionFlameTextureX + PlanetInfos.ionFlameTextureWidth * _flameIndex;
						uy = PlanetInfos.ionFlameTextureY;
						u0 = ux * kw;
						v0 = uy * kh;
						u1 = (ux + PlanetInfos.ionFlameTextureWidth) * kw;
						v1 = (uy + PlanetInfos.ionFlameTextureHeight) * kh;
					} else {
						ux = PlanetInfos.flameTextureX + PlanetInfos.flameTextureWidth * _flameIndex;
						uy = PlanetInfos.flameTextureY;
						u0 = ux * kw;
						v0 = uy * kh;
						u1 = (ux + PlanetInfos.flameTextureWidth) * kw;
						v1 = (uy + PlanetInfos.flameTextureHeight) * kh;
					}

					_uvtData[uvtI++] = u0;
					_uvtData[uvtI++] = v0;
					_uvtData[uvtI++] = 1;

					_uvtData[uvtI++] = u1;
					_uvtData[uvtI++] = v0;
					_uvtData[uvtI++] = 1;

					_uvtData[uvtI++] = u1;
					_uvtData[uvtI++] = v1;
					_uvtData[uvtI++] = 1;

					_uvtData[uvtI++] = u0;
					_uvtData[uvtI++] = v1;
					_uvtData[uvtI++] = 1;
				}
			}
			camera.graphics.lineStyle();
			camera.graphics.beginBitmapFill(Planet.planets);
			camera.graphics.drawTriangles(_vertices, _indices, _uvtData);
			camera.graphics.endFill();
			_flameIndex++;
			if (_flameIndex >= PlanetInfos.flameFrames)
				_flameIndex = 0;
		}

		var offsetY = 10.;
		var offsetX = 10.;
		var height = 6.;
		for (stage in _works) {
			if (stage.hasEngine) {
				var k = stage.fuelM / stage.maxFuelM;
				var g = camera.topContainer.graphics;
				var width = Std.int(stage.maxFuelM * 1600);

				g.lineStyle(0, World.LINE_COLOR);
				g.beginFill(0x000000, .5);
				g.drawRect(offsetX - 2, offsetY - 2, width + 3, height + 3);
				g.endFill();

				g.lineStyle();
				g.beginFill(World.LINE_COLOR);
				g.drawRect(offsetX + (1 - k) * width, offsetY, k * width, height);
				g.endFill();

				offsetY += height + 10;
			}
		}

		if (_exploded && _explosion == null) {
			switch (_explosionType) {
				case ExplosionType.SURFACE:
					_explosion = Assets.newMCMushroomExplosion();
					_explosion.rotation = MathUtil.getRotation(_explodedX, _explodedY);
				case ExplosionType.OVERLOAD:
					_explosion = Assets.newMCExplosionWithRest();
					_explosion.rotation = MathUtil.getRotation(direction.x, direction.y);
				case ExplosionType.RING:
					_explosion = Assets.newMCExplosion();
					_explosion.rotation = MathUtil.getRotation(direction.x, direction.y);
			}
			camera.container.addChild(_explosion);
			if (camera.player != null)
				camera.player.playSound(Assets.Explosio_Adrian_G_7936_hifi_mp3);
			for (stage in stages) {
				stage.destroy();
			}
			_enginesEnabled = false;
		}
		if (_exploded) {
			camera.screenXY(_explodedPlanet.r.x + _explodedX, _explodedPlanet.r.y + _explodedY);
			_explosion.x = camera.outSX;
			_explosion.y = camera.outSY;
			if (_explosion.currentFrame > 50) {
				_explosion.gotoAndStop(50);
				explosionCompleted = true;
			}
		}

		if (!_exploded && showSending) {
			var sin = Math.sin(_showSendingIndex);
			camera.screenXY(ship.r.x, ship.r.y);
			var k0 = (_showSendingIndex % 10) * .1;
			var k1 = ((_showSendingIndex + 5) % 10) * .1;
			camera.topGraphics.lineStyle(2, 0xffffff, k0 < .5 ? k0 * 2. : 2. - 2. * k0);
			camera.topGraphics.drawCircle(camera.outSX, camera.outSY, 20 + k0 * 20);
			camera.topGraphics.lineStyle(2, 0xffffff, k1 < .5 ? k1 * 2. : 2. - 2. * k1);
			camera.topGraphics.drawCircle(camera.outSX, camera.outSY, 20 + k1 * 20);
		}
	}

	private static var _footR:Vertex = new Vertex().setXY(0, 0);
	private static var _vv:Vertex = new Vertex();
	private static var _rr:Vertex = new Vertex();

	private var _collisionTimer:Int;
	private var _lastCollision:Int;

	public var outLandingPlanet:Unit;
	public var outCollisionDetected:Bool;

	public function updateCollisions(all:Vector<Unit>, dt:Float, g:Graphics):Void {
		outLandingPlanet = null;
		outCollisionDetected = false;
		collisionV.x = 0;
		collisionV.y = 0;
		var collisionX = .0;
		var collisionY = .0;
		var hasCollision = false;
		var collisionPlanet = null;
		for (unit in all) {
			var planet = unit.asPlanet;
			if (planet != null &&
				planet.rings != null &&
				planet.r.distance2(r) < 9 * planet.radius * planet.radius) {
				var ring = planet.rings;
				while (ring != null) {
					var rx0 = r.x - planet.r.x;
					var ry0 = r.y - planet.r.y;
					var rx1 = rx0 + (v.x - planet.v.x) * dt;
					var ry1 = ry0 + (v.y - planet.v.y) * dt;
					var x0 = ring.rcos * (ring.radius - ring.width);
					var y0 = ring.rsin * (ring.radius - ring.width);
					var x1 = ring.rcos * ring.radius;
					var y1 = ring.rsin * ring.radius;
					if (MathUtil.segmentsCrossing(x0, y0, x1, y1, rx0, ry0, rx1, ry1) ||
						MathUtil.segmentsCrossing(-x0, -y0, -x1, -y1, rx0, ry0, rx1, ry1)) {
						hasCollision = true;
						collisionX = MathUtil.outX;
						collisionY = MathUtil.outY;
						collisionPlanet = unit;
						_exploded = true;
						_explosionType = ExplosionType.RING;
						_explodedX = collisionX;
						_explodedY = collisionY;
						_explodedPlanet = unit;
						fixed = unit;
						fixedX = collisionX;
						fixedY = collisionY;
						break;
					}
					ring = ring.next;
				}
			}
		}
		if (!hasCollision)
		for (i in 0 ... 2) {
			if (i == 0) {
				_footR.x = r.x - direction.x * footY;
				_footR.y = r.y - direction.y * footY;
			} else {
				_footR.x = r.x - direction.x * headY;
				_footR.y = r.y - direction.y * headY;
			}
			for (planet in all) {
				if (planet == this || !planet.hasSize)
					continue;

				var rr = planet.radius + 10;
				var xx = r.x - planet.r.x;
				var yy = r.y - planet.r.y;
				if (xx * xx + yy * yy > rr * rr)
					continue;

				_rr.set(_footR).minus(planet.r);
				_vv.set(v).minus(planet.v).mult(dt);
				var vx = v.x - planet.v.x;
				var vy = v.y - planet.v.y;
				var criterion = planet.radius + _vv.length();
				if (_rr.length2() < criterion * criterion) {
					processCollision(_footR, _rr, _vv, planet, dt, g);
					if (_outHasCollision) {
						hasCollision = true;
						collisionX = _outCollisionX;
						collisionY = _outCollisionY;
						collisionPlanet = _outPlanet;
						if (!_exploded && vx * vx + vy * vy > World.EXPLOSION_V * World.EXPLOSION_V) {
							_exploded = true;
							_explosionType = ExplosionType.SURFACE;
							_explodedX = _outCollisionX;
							_explodedY = _outCollisionY;
							_explodedPlanet = _outPlanet;
							var vx0 = v.x;
							var vy0 = v.y;
							v.x = planet.v.x;
							v.y = planet.v.y;
							collisionV.x = vx0 - v.x;
							collisionV.y = vy0 - v.y;
						}
					}
					break;
				}
			}
		}
		if (hasCollision && !_exploded) {
			var angle = MathUtil.getAngle(collisionX, collisionY, direction.x, direction.y);
			var sizeRatio = 30 / (RocketStage.SCALE * (footY * 2));
			var separator = Math.PI * .5 * sizeRatio;
			var kk = 10. / sizeRatio;
			if (angle > separator) {
				angle = MathUtil.getAngle(-collisionY, collisionX, direction.x, direction.y);
			} else if (angle < -separator) {
				angle = MathUtil.getAngle(collisionY, -collisionX, direction.x, direction.y);
			} else {
				kk = 10.;
			}
			var mRatio = .0001 * (collisionX * collisionX + collisionY * collisionY) / (collisionPlanet.m * m);
			var k = angle > 0 ? 1. : -1.;
			angle *= k;
			var dAngle = kk * dt / (sizeRatio * mRatio + angle);
			if (dAngle > angle)
				dAngle = angle;
			dAngle = _dAlpha - k * dAngle;
			_alpha += dAngle;
			updateDirection();

			var x0 = direction.x * footY;
			var y0 = direction.y * footY;
			var cos = Math.cos(dAngle);
			var sin = Math.sin(dAngle);
			var x1 = x0 * cos - y0 * sin;
			var y1 = x0 * sin + y0 * cos;
			r.x += x1 - x0;
			r.y += y1 - y0;

			if (_collisionTimer == 0) {
				outCollisionDetected = true;
			}
			if (_collisionTimer == 10) {
				outLandingPlanet = collisionPlanet;
			}
			_collisionTimer++;
			_lastCollision = 3;
		} else {
			_alpha += _dAlpha;
			if (_lastCollision <= 0)
				_collisionTimer = 0;
			_lastCollision--;
		}
		_dAlpha = 0;
	}

	private static var _outHasCollision:Bool;
	private static var _outPlanet:Unit;
	private static var _outCollisionX:Float;
	private static var _outCollisionY:Float;

	private function processCollision(
		footR:Vertex, rr:Vertex, vv:Vertex, planet:Unit, dt:Float, g:Graphics):Bool {
		_outHasCollision = false;
		if (rr.x * rr.x + rr.y * rr.y > planet.radius * planet.radius + .001) {
			var vvlength = Math.sqrt(vv.x * vv.x + vv.y * vv.y);
			var vx0 = v.x;
			var vy0 = v.y;
			var lx = vv.x / vvlength;
			var ly = vv.y / vvlength;
			var h = rr.x * ly - rr.y * lx;
			if (MathUtil.abs(h) < planet.radius) {
				var a = Math.sqrt(planet.radius * planet.radius - h * h);
				var nx = -ly;
				var ny = lx;
				var ox = -h * nx - a * lx;
				var oy = -h * ny - a * ly;
				if (g != null) {
					g.lineStyle(0, 0xff00ff);
					g.drawCircle(planet.r.x + ox, planet.r.y + oy, 2);
				}
				var vx = ox - rr.x;
				var vy = oy - rr.y;
				if (vx * vx + vy * vy < vv.x * vv.x + vv.y * vv.y) {
					vx += (vx - vv.x) / dt;
					vy += (vy - vv.y) / dt;

					v.x = planet.v.x;
					v.y = planet.v.y;
					collisionV.x = vx0 - v.x;
					collisionV.y = vy0 - v.y;
					if (g != null) {
						g.lineStyle(0, 0x0000ff);
						g.drawCircle(r.x + v.x, r.y + v.y, 2);
					}
					_outHasCollision = true;
					_outPlanet = planet;
					_outCollisionX = ox;
					_outCollisionY = oy;
				}
			}
		} else {
			var xx = rr.x + vv.x;
			var yy = rr.y + vv.y;
			var vx = v.x;
			var vy = v.y;
			var length2 = xx * xx + yy * yy;
			if (length2 < planet.radius * planet.radius) {
				var length = Math.sqrt(length2);
				var delta = planet.radius - length;
				var dx = xx / length;
				var dy = yy / length;
				vx += delta * dx / dt;
				vy += delta * dy / dt;
				var nx = dy;
				var ny = -dx;
				var vv = vv.x * nx + vv.y * ny;
				vx -= nx * vv / dt;
				vy -= ny * vv / dt;

				v.x = planet.v.x;
				v.y = planet.v.y;
				collisionV.x = vx - v.x;
				collisionV.y = vy - v.y;

				_outHasCollision = true;
				_outPlanet = planet;
				_outCollisionX = dx * planet.radius;
				_outCollisionY = dy * planet.radius;
			}
		}
		return _outHasCollision;
	}
}
