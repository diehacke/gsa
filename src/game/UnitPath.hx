package game;

import flash.Vector;
import utils.Vertex;

class UnitPath {
	public var _items:Vector<Float>;
	public var _offset:Int;
	public var _mask:Int;

	public var length:Int;

	public var lastR:Vertex = new Vertex();
	public var lastV:Vertex = new Vertex();

	public function new(power:Int) {
		var size = 1 << power;
		_mask = size - 1;
		_items = new Vector<Float>(size << 1, true);
	}

	inline public function pushR(r:Vertex):Void {
		var i = ((_offset + length) & _mask) << 1;
		_items[i] = r.x;
		_items[i + 1] = r.y;
		length++;
	}

	inline public function shift(count:Int):Void {
		_offset = (_offset + count) & _mask;
		length -= count;
	}

	inline public function getR(index:Int, outR:Vertex):Void {
		var i = ((_offset + index) & _mask) << 1;
		outR.x = _items[i];
		outR.y = _items[i + 1];
	}

	inline public function getRf(index:Float, outR:Vertex):Void {
		var indexI = Std.int(index);
		var indexF = index - indexI;
		var i = ((_offset + indexI) & _mask) << 1;
		var i1 = ((_offset + indexI + 1) & _mask) << 1;
		outR.x = _items[i] * (1 - indexF) + _items[i1] * indexF;
		outR.y = _items[i + 1] * (1 - indexF) + _items[i1 + 1] * indexF;
	}
}
