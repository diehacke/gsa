package game;

import utils.Vertex;
import flash.display.Graphics;
import flash.Vector;

class Asteroid extends Unit {
	public function new() {
		super();
		hasM = false;
	}

	public var direction:Vertex = new Vertex();
	public var color:Int;

	private static var _vertices:Vector<Float> = new Vector<Float>();
	private static var _indices:Vector<Int> = new Vector<Int>();
	private static var _uvtData:Vector<Float> = new Vector<Float>();

	private static var _commands:Vector<Int> = {
		var commands = new Vector<Int>(2, true);
		commands[0] = 1;
		commands[1] = 2;
		commands;
	}
	private static var _coords = new Vector<Float>(4, true);

	override public function render(camera:Camera):Void {
		if (camera.inArea(r.x, r.y, radius) && radius * camera.scale > .1) {
			var g = camera.bottomGraphics;
			camera.screenXY(r.x, r.y);
			g.lineStyle();
			g.beginFill(color);
			g.drawCircle(camera.outSX, camera.outSY, camera.scale * radius);
			g.endFill();
		}
	}

	override public function renderBig(camera:BigCamera):Void {
		if (camera.inArea(r.x, r.y, radius * 1.5))
		{
			camera.screenXY(r.x, r.y);

			var info = PlanetInfos.asteroid;
			var k = camera.scale;
			var x = camera.outSX;
			var y = camera.outSY;

			_vertices.length = 0;
			_indices.length = 0;
			_uvtData.length = 0;

			var size = (radius / info.radius) * info.size * k;
			_vertices[0] = x - size;
			_vertices[1] = y - size;
			_vertices[2] = x + size;
			_vertices[3] = y - size;
			_vertices[4] = x + size;
			_vertices[5] = y + size;
			_vertices[6] = x - size;
			_vertices[7] = y + size;

			_indices[0] = 0;
			_indices[1] = 1;
			_indices[2] = 2;
			_indices[3] = 2;
			_indices[4] = 3;
			_indices[5] = 0;

			var u0 = (info.centerX - info.size) / Planet.planetsWidth;
			var v0 = (info.centerY - info.size) / Planet.planetsHeight;
			var u1 = (info.centerX + info.size) / Planet.planetsWidth;
			var v1 = (info.centerY + info.size) / Planet.planetsHeight;

			_uvtData[0] = u0;
			_uvtData[1] = v0;
			_uvtData[2] = 1;

			_uvtData[3] = u1;
			_uvtData[4] = v0;
			_uvtData[5] = 1;

			_uvtData[6] = u1;
			_uvtData[7] = v1;
			_uvtData[8] = 1;

			_uvtData[9] = u0;
			_uvtData[10] = v1;
			_uvtData[11] = 1;

			var g = camera.bottomGraphics;
			g.lineStyle();
			g.beginBitmapFill(Planet.planets, null, false, true);
			g.drawTriangles(_vertices, _indices, _uvtData);
			g.endFill();
		}
	}
}
