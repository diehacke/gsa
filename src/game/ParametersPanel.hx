package game;

import flash.display.Shape;
import utils.MathUtil;
import ui.UIRects;
import utils.StringUtil;
import flash.text.TextField;
import ui.SmartFormat;
import ui.Widget;
import utils.RotationMatrix;
import utils.StringUtil;

class ParametersPanel extends Widget {
	public static var OFFSET_Y:Float = 60;

	private var _blueFormat:SmartFormat;
	private var _greenFormat:SmartFormat;
	private var _velocityGreen:Bool;

	private var _velocityTf:TextField;
	private var _distanceTf:TextField;
	private var _overloadTf:TextField;
	private var _nameTf:TextField;
	private var _shape:Shape;
	private var _noSignalTf:TextField;

	public function new() {
		super();

		_blueFormat = new SmartFormat("Tahoma", 14).setBold(true).setRight().setColor(World.LINE_COLOR);
		_greenFormat = _blueFormat.clone().setColor(0x00ff00);

		_nameTf = _blueFormat.clone().setCenter().newFixed();
		_nameTf.height = 30;
		addChild(_nameTf);

		_shape = new Shape();
		addChild(_shape);

		_velocityTf = _blueFormat.newFixed();
		_velocityTf.width = 75;
		_velocityTf.height = 19;
		addChild(_velocityTf);

		_distanceTf = _blueFormat.newFixed();
		_distanceTf.width = 100;
		_distanceTf.height = 19;
		addChild(_distanceTf);

		_overloadTf = new SmartFormat("Tahoma", 24).setBold(true).setColor(0xff0000).newAutoSized();
		_overloadTf.visible = false;
		addChild(_overloadTf);

		_noSignalTf = new SmartFormat("Tahoma", 14).setBold(true).setColor(World.LINE_COLOR).newAutoSized();
		_noSignalTf.text = "No signal";
		addChild(_noSignalTf);

		updateNoSignal();
	}

	override public function redraw():Void {
		var h = 65.;

		_nameTf.width = _w;
		_nameTf.x = Std.int(_w * .5 - _nameTf.width * .5);

		_velocityTf.x = 7;
		_velocityTf.y = 36;

		_distanceTf.x = 94;
		_distanceTf.y = 36;

		var g = graphics;
		g.clear();
		UIRects.parameters_panel_normal.drawWithoutTolerance(g, Assets.ui_png, 0, 0);
		g.lineStyle(0, 0x808080);
		g.moveTo(0, 60);
		g.lineTo(0, _h);

		_noSignalTf.x = _w * .5 - _noSignalTf.width * .5;
		_noSignalTf.y = 60 + (_h - 60) * .5 - _noSignalTf.height * .5;
	}

	private var _noSignal:Bool = false;

	private function updateNoSignal():Void {
		_noSignalTf.visible = _noSignal;
		if (_noSignal) {
			_shape.graphics.clear();
		}
		_nameTf.visible = !_noSignal;
		_velocityTf.visible = !_noSignal;
		_distanceTf.visible = !_noSignal;
	}

	public function setNoSignal(value:Bool):Void {
		if (_noSignal != value) {
			_noSignal = value;
			updateNoSignal();
		}
	}

	private static var _matrix:RotationMatrix = new RotationMatrix();

	public function update(world:World, bigCamera:BigCamera):Void {
		var g = _shape.graphics;
		g.clear();
		if (_noSignal) {
			g.beginFill(0x000000, .5);
			g.drawRect(_noSignalTf.x, _noSignalTf.y, _noSignalTf.width, _noSignalTf.height);
			g.endFill();
			return;
		}

		_nameTf.text = world.getTargetName();

		var v = world.getRocketV();
		var vLength = v.length();
		_velocityTf.text = vLength < 1000 ? StringUtil.fixed2(vLength * .1) : Math.round(vLength * .1) + "";

		var canExplode = vLength >= World.EXPLOSION_V;
		setVelicityGreen(!canExplode);

		var distance = world.getDistance();
		_distanceTf.text = distance < 10000 ? StringUtil.fixed2(distance) : Math.round(distance) + "";

		var r = world.getRocketR();
		var rLength = r.length();
		if (vLength > .1) {
			if (!canExplode) {
				g.lineStyle();
				g.beginFill(0x008000);
				var ww = 54;
				var ratio = vLength / World.EXPLOSION_V;
				g.drawRect(
					_velocityTf.x + _velocityTf.width - ww + ww * ratio,
					_velocityTf.y,
					ww * (1 - ratio),
					_velocityTf.height);
				g.endFill();
			}
			drawArrow(10, 43, v.x / vLength, v.y / vLength, canExplode ? World.LINE_COLOR : 0x00ff00);
		}
		var ex = -r.x / rLength;
		var ey = -r.y / rLength;
		if (distance > .005)
			drawArrow(104, 43, ex, ey, World.LINE_COLOR);

		var minDistance = .2;
		var maxDistance = 20.;
		if (distance > minDistance && distance < maxDistance) {
			g.lineStyle(0, canExplode ? World.LINE_COLOR : 0x00ff00);
			var size = _w - 10;
			var x0 = _w / 2;
			var y0 = _h - size / 2 - 5;
			g.moveTo(x0 - size / 2 * ex, y0 - size / 2 * ey);
			g.lineTo(x0 + size / 2 * ex, y0 + size / 2 * ey);
			var distancePart = size / 2 - size * distance / maxDistance;
			g.moveTo(x0 + distancePart * ex + 5 * ey, y0 + distancePart * ey - 5 * ex);
			g.lineTo(x0 + distancePart * ex - 5 * ey, y0 + distancePart * ey + 5 * ex);
			g.moveTo(x0 + size / 2 * ex + 5 * ey, y0 + size / 2 * ey - 5 * ex);
			g.lineTo(x0 + size / 2 * ex - 5 * ey, y0 + size / 2 * ey + 5 * ex);
		}

		var rocket = world.getRocket();
		var maxA = rocket.ship.maxARatio * World.GLORY_G;
		var k = .6;
		if (rocket.engineA.length2() > (maxA * k) * (maxA * k)) {
			var a = rocket.engineA.length();
			var ax = rocket.engineA.x / a;
			var ay = rocket.engineA.y / a;
			_matrix.rotateXTo(ax, ay);
			bigCamera.screenXY(rocket.ship.r.x, rocket.ship.r.y);
			var ratio = (a / maxA - k) / k;
			if (ratio > 1)
				ratio = 1;
			var x0 = bigCamera.outSX;
			var y0 = bigCamera.outSY;
			var ww = 20;
			var hh = 15;
			var ww2 = ww * 2 * ratio - ww;
			var alpha = .5 + .5 * rocket.explosionAdt / World.EXPLOSION_ADT;
			if (alpha > 1)
				alpha = 1;

			var color = 0xff0000;

			_overloadTf.visible = true;
			_overloadTf.text = StringUtil.fixed2(a / World.GLORY_G) + " g";
			_overloadTf.x = x0 - _overloadTf.width * .5;
			_overloadTf.y = y0 - 60;

			g.lineStyle();
			g.beginFill(0x000000, alpha);

			_matrix.transform(-ww - 2, -hh - 2);
			g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww + 2, -hh - 2);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww + 12, 0);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww + 2, hh + 2);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-ww - 2, hh + 2);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-ww - 2, -hh - 2);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);

			_matrix.transform(-ww, -hh);
			g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww2, -hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww2 + 10, 0);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww2, hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-ww, hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-ww, -hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);

			g.endFill();

			g.lineStyle();
			g.beginFill(color, alpha);
			_matrix.transform(-ww, -hh);
			g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww2, -hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww2 + 10, 0);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(ww2, hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-ww, hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-ww, -hh);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			g.endFill();
		} else {
			_overloadTf.visible = false;
		}

		drawTutorialOrientation(world, bigCamera);
	}

	private function drawArrow(x0:Float, y0:Float, lx:Float, ly:Float, color:Int):Void {
		var g = _shape.graphics;
		g.lineStyle(2, color);
		_matrix.rotateYTo(lx, ly);
		_matrix.transform(-5, -5);
		g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
		_matrix.transform(0, -10);
		g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
		_matrix.transform(0, 10);
		g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
		_matrix.transform(0, -10);
		g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
		_matrix.transform(5, -5);
		g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
	}

	private function setVelicityGreen(value:Bool):Void {
		if (_velocityGreen != value) {
			_velocityGreen = value;
			if (_velocityGreen) {
				_greenFormat.applyTo(_velocityTf);
			} else {
				_blueFormat.applyTo(_velocityTf);
			}
		}
	}

	public var tutorialOrientationX:Float;
	public var tutorialOrientationY:Float;

	private function drawTutorialOrientation(world:World, bigCamera:BigCamera):Void {
		if (!Math.isNaN(tutorialOrientationX) && !Math.isNaN(tutorialOrientationY)) {
			var rocket = world.getRocket();
			bigCamera.screenXY(rocket.r.x, rocket.r.y);
			var x0 = bigCamera.outSX;
			var y0 = bigCamera.outSY + OFFSET_Y;

			var g = _shape.graphics;
			g.lineStyle(2, World.ROCKET_PATH_COLOR);
			var l = MathUtil.sqrt(
				tutorialOrientationX * tutorialOrientationX + tutorialOrientationY * tutorialOrientationY
			);
			_matrix.rotateYTo(tutorialOrientationX / l, tutorialOrientationY / l);

			_matrix.transform(-20, 30);
			g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-20, -30);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(-25, -20);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);

			_matrix.transform(20, 30);
			g.moveTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(20, -30);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
			_matrix.transform(25, -20);
			g.lineTo(x0 + _matrix.outX, y0 + _matrix.outY);
		}
	}
}
