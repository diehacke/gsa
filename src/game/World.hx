package game;

import flash.display.Graphics;
import utils.Signal;
import flash.Lib;
import flash.errors.Error;
import utils.Tween;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.Vector;
import flash.ui.Keyboard;
import flash.utils.ByteArray;
import utils.ListUtil;
import utils.Vertex;
import utils.KeyUtil;
import utils.MathUtil;
import model.PlanetStatus;

class World {
	public static var G:Float = 2000 * 125;
	public static var EXPLOSION_V:Float = 36.;
	public static var EXPLOSION_ADT:Float = 0.003;
	public static var LINE_COLOR:Int = 0xffffff;
	public static var PATH_COLOR:Int = 0xbbbbbb;
	public static var ROCKET_PATH_COLOR:Int = 0x00ffff;
	public static var CLOSE_RADIO_DISTANCE:Float = 100;

	private static var TIME_RATIO:Float = .1;
	private static var PATH_LENGTH_POWER:Int = 15;
	private static var PATH_LENGTH:Int = 1 << PATH_LENGTH_POWER;
	private static var PHOTO_SENDING_TIME:Float = 10.;

	public static var GLORY_M:Float = 15.;
	public static var GLORY_G:Float = {
		var distance = PlanetInfos.glory.radius;
		G * GLORY_M / (distance * distance);
	};
	public static var GLORY_RARIDUS:Float = PlanetInfos.glory.radius;

	public static function getAcceleration(m:Float, r:Float):Float {
		return G * m / (r * r);
	}

	public static function getFirstVelocity(m:Float, r:Float):Float {
		return Math.sqrt(G * m / r);
	}

	public static function getSecondVelocity(m:Float, r:Float):Float {
		return Math.sqrt(2 * G * m / r);
	}

	public var discovered:Signal<Planet> = new Signal<Planet>();
	public var photo:Signal<Planet> = new Signal<Planet>();
	public var closePhotoStart:Signal<Planet> = new Signal<Planet>();
	public var closePhoto:Signal<Planet> = new Signal<Planet>();
	public var landingPhoto:Signal<Planet> = new Signal<Planet>();

	public var spf:Float;

	private var _camera:Camera;
	private var _bigCamera:BigCamera;
	private var _massive:Vector<Unit>;
	private var _massless:Vector<Unit>;
	private var _stages:Vector<RocketStage>;
	private var _all:Vector<Unit>;
	private var _targetUnit:Unit;
	private var _star:Planet;
	private var _glory:Planet;
	private var _rocket:Rocket;

	inline public function getMassive():Vector<Unit> {
		return _massive;
	}

	inline public function getRocket():Rocket {
		return _rocket;
	}

	inline public function getTargetUnit():Unit {
		return _targetUnit;
	}

	inline public function getStar():Planet {
		return _star;
	}

	inline public function getGlory():Planet {
		return _glory;
	}

	private static var _rocketV:Vertex = new Vertex();
	private static var _rocketR:Vertex = new Vertex();

	public function getRocketV():Vertex {
		return _rocketV.set(_rocket.v).minus(_targetUnit.v);
	}

	public function getRocketR():Vertex {
		return _rocketR.set(_rocket.r).minus(_targetUnit.r);
	}

	public function deployRocket():Void {
		if (radioAccessible)
			_rocket.deploy();
	}

	public function unlinkRocketStages():Void {
		if (radioAccessible)
			_rocket.unlink();
	}

	private var _cycleTime:Float;

	private function initCycleTime(system:WorldSystem):Void {
		var distance = system.glory.r.distance(system.star.r);
		var w = system.glory.v.length() / distance;
		_cycleTime = Math.PI * 2 / w;
	}

	private var _time:Float;

	public function getTimeInCycles():Float {
		return _time / _cycleTime;
	}

	public function getDistance():Float {
		var dx = _rocket.r.x - _rocket.direction.x * _rocket.footY - _targetUnit.r.x;
		var dy = _rocket.r.y - _rocket.direction.y * _rocket.footY - _targetUnit.r.y;
		return Math.sqrt(dx * dx + dy * dy) - _targetUnit.radius;
	}

	public function getTargetName():String {
		return _targetUnit.name;
	}

	public function getTargetStatus():PlanetStatus {
		return _targetUnit.asPlanet != null ? _targetUnit.asPlanet.status : null;
	}

	public function new(fps:Float, camera:Camera, bigCamera:BigCamera, system:WorldSystem) {
		spf = 1 / fps;
		_camera = camera;
		_bigCamera = bigCamera;
		_massive = new Vector<Unit>();
		_massless = new Vector<Unit>();
		_stages = new Vector<RocketStage>();
		_all = new Vector<Unit>();

		initCycleTime(system);

		for (unit in system.units) {
			if (unit.hasM) {
				unit.path = new UnitPath(PATH_LENGTH_POWER);
				unit.path.lastR.set(unit.r);
				unit.path.lastV.set(unit.v);
				_massive.push(unit);
			} else {
				_massless.push(unit);
			}
			unit.startR.set(unit.r);
			unit.startV.set(unit.v);
			_all.push(unit);
		}
		_star = system.star;
		_glory = system.glory;
		_rocket = system.rocket;
		_time = 0;
	}

	private function updateCamera():Void {
		_camera.sunX = _star.r.x;
		_camera.sunY = _star.r.y;
		_camera.accelerationIndex = accelerationIndex;
		_camera.x = _targetUnit.r.x + _cameraOffsetX;
		_camera.y = _targetUnit.r.y + _cameraOffsetY;
		var scale = {
			var distanceX = MathUtil.abs(_camera.x - _rocket.r.x);
			var distanceY = MathUtil.abs(_camera.y - _rocket.r.y);
			var dx = _camera.x - _rocket.r.x;
			var dy = _camera.y - _rocket.r.y;
			var distance = MathUtil.sqrt(dx * dx + dy * dy);
			Math.min(1, (Math.min(_camera.halfSW, _camera.halfSH) * 0.95) / distance);
		}
		var someDown = false;
		var k = 1.5;
		if (KeyUtil.isDown(KeyUtil.W) || KeyUtil.isDown(KeyUtil.Wru)) {
			if (_maxScale > _camera.scale)
				_maxScale = _camera.scale;
			_maxScale -= _maxScale * spf * k;
			someDown = true;
		}
		if (KeyUtil.isDown(KeyUtil.S) || KeyUtil.isDown(KeyUtil.Sru)) {
			if (_maxScale > _camera.scale)
				_maxScale = _camera.scale;
			_maxScale += _maxScale * spf * k;
			someDown = true;
		}
		if (_maxScale > 1)
			_maxScale = 1;
		else if (_maxScale < 1 / 128)
			_maxScale = 1 / 128;
		if (!someDown) {
			_camera.setScaleAndRelax(scale, _maxScale);
		} else {
			_camera.setScale(Math.min(scale, _maxScale));
		}
		
		_bigCamera.x = _rocket.r.x + _rocket.cameraOffsetX;
		_bigCamera.y = _rocket.r.y + _rocket.cameraOffsetY;
	}

	private var _accelerationIndex:Int;
	public var accelerationIndex(get, set):Int;
	public function get_accelerationIndex():Int {
		return _accelerationIndex;
	}
	public function set_accelerationIndex(value:Int):Int {
		_accelerationIndex = value;
		return _accelerationIndex;
	}

	inline private function updateUnitV(unit:Unit, ax:Float, ay:Float, dt:Float):Void {
		for (unitJ in _massive) {
			if (unitJ != unit) {
				var dx = unitJ.r.x - unit.r.x;
				var dy = unitJ.r.y - unit.r.y;
				var distance2 = dx * dx + dy * dy;
				var a = G * unitJ.m * MathUtil.invSqrt(distance2) / distance2;
				ax += a * dx;
				ay += a * dy;
			}
		}
		unit.v.x += ax * dt;
		unit.v.y += ay * dt;
	}

	private var _maxScale:Float = 1;
	private var _pathIndex:Int = 0;
	private var _pathIndexF:Float = 0.;

	private function fillPaths():Void {
		var dt = TIME_RATIO * spf;
		for (unitI in _massive) {
			unitI.r.set(unitI.path.lastR);
			unitI.v.set(unitI.path.lastV);
		}
		for (i in 0 ... 200)
		{
			if (_pathIndex + 1 >= PATH_LENGTH)
				break;
			_pathIndex++;
			for (unitI in _massive) {
				updateUnitV(unitI, 0, 0, dt);
			}
			for (unitI in _massive) {
				unitI.r.x += unitI.v.x * dt;
				unitI.r.y += unitI.v.y * dt;
				unitI.path.pushR(unitI.r);
				unitI.path.lastR.set(unitI.r);
				unitI.path.lastV.set(unitI.v);
			}
		}
	}

	public var radioAccessible:Bool;

	public var tutorialOrbitVisible:Bool;
	public var tutorialOrbitR0:Float;
	public var tutorialOrbitR1:Float;
	
	public function update():Void {
		radioAccessible = _glory == null ||
			_glory.r.distance2(_rocket.r) < CLOSE_RADIO_DISTANCE * CLOSE_RADIO_DISTANCE ||
			_rocket.ship.hasAntenna && _rocket.ship.deployed && !_rocket.explosionCompleted;

		_rocket.updateControl(this);
		if (_accelerationIndex < 0)
			_accelerationIndex = 0;
		else if (_accelerationIndex > 4)
			_accelerationIndex = 4;
		var timeIterations = 1;
		var timeStep = 1.;
		switch (_accelerationIndex) {
			case 0:
				timeIterations = 1;
				timeStep = .2;
			case 1:
				timeIterations = 1;
				timeStep = 1.;
			case 2:
				timeIterations = 5;
				timeStep = 1.;
			case 3:
				timeIterations = 10;
				timeStep = 4.;
			case 4:
				timeIterations = 50;
				timeStep = 4.;
		}
		fillPaths();

		for (unitI in _massive) {
			var ring = unitI.asPlanet != null ? unitI.asPlanet.rings : null;
			while (ring != null) {
				ring.update(TIME_RATIO * spf * timeStep * timeIterations);
				ring = ring.next;
			}
		}
		for (unitI in _all) {
			unitI.v.set(unitI.startV);
			unitI.r.set(unitI.startR);
		}
		for (i in 0 ... timeIterations) {
			calculateStepForAll(timeStep);
		}
		for (unitI in _all) {
			unitI.startV.set(unitI.v);
			unitI.startR.set(unitI.r);
		}

		setTarget(ListUtil.select(_massive, true, {
			var r = value.r.distance(_rocket.r);
			value.m / (r * r * r);
		}));
		updateAcceleration(timeIterations * timeStep * TIME_RATIO * spf * timeStep);
		updateCamera();

		_time += TIME_RATIO * spf * timeStep * timeIterations;
	}

	private var _accelerationPlanet:Unit;

	private function updateAcceleration(dt:Float):Void {
		var planet = null;
		var k = 1.;
		var rx0 = _rocket.r.x;
		var ry0 = _rocket.r.y;
		var rx = rx0 + _rocket.v.x * dt * k;
		var ry = ry0 + _rocket.v.y * dt * k;
		for (unit in _massive) {
			var radius = unit.radius + ListUtil.max(unit.atmRadius, 15);
			var dx = unit.r.x + unit.v.x * dt * k - rx;
			var dy = unit.r.y + unit.v.y * dt * k - ry;
			if (dx * dx + dy * dy < radius * radius) {
				planet = unit;
			} else {
				var dx = unit.r.x - rx0;
				var dy = unit.r.y - ry0;
				if (dx * dx + dy * dy < radius * radius) {
					planet = unit;
				}
			}
		}
		if (planet != null && planet != _accelerationPlanet) {
			_accelerationIndex = 0;
		}
		if (_accelerationPlanet != null && planet == null) {
			if (_targetUnit != _accelerationPlanet)
				_accelerationPlanet = null;
		} else {
			_accelerationPlanet = planet;
		}
		if (radioAccessible) {
			var ship = _rocket.ship;
			if (ship.deployed && _time > .001) {
				for (unit in _massive) {
					var planet = unit.asPlanet;
					if (planet != null) {
						var distance2 = ship.r.distance2(planet.r) * GLORY_RARIDUS / planet.radius;
						if (!planet.status.discovered) {
							var minR = planet.radius + ship.discoverRadius;
							if (distance2 < minR * minR) {
								planet.status.discovered = true;
								discovered.dispatch(planet);
							}
						} else if (!planet.status.hasPhoto) {
							var minR = planet.radius + ship.photoRadius;
							if (distance2 < minR * minR) {
								planet.status.hasPhoto = true;
								photo.dispatch(planet);
							}
						} else if (!planet.status.hasClosePhoto) {
							var minR = planet.radius * ship.closePhotoK + ship.closePhotoRadius;
							if (distance2 < minR * minR) {
								var task = _photoTasks;
								while (task != null && task.planet != planet) {
									task = task.next;
								}
								if (task == null) {
									task = new PhotoTask();
									task.planet = planet;
									task.timer = PHOTO_SENDING_TIME * TIME_RATIO;
									task.next = _photoTasks;
									_photoTasks = task;
									closePhotoStart.dispatch(planet);
								}
							}
						}
					}
				}
			}
			var task = _photoTasks;
			if (task != null && !_rocket.getExploded()) {
				task.timer -= dt;
				if (task.timer <= .0) {
					_photoTasks = _photoTasks.next;
					task.planet.status.hasClosePhoto = true;
					closePhoto.dispatch(task.planet);
				}
			}
			_rocket.showSending = _photoTasks != null;
		}
	}

	private var _photoTasks:PhotoTask;

	private static var _r0:Vertex = new Vertex();
	private static var _r1:Vertex = new Vertex();
	
	private var _stagesToAdd:Vector<RocketStage> = new Vector<RocketStage>();

	private function calculateStepForAll(timeStep:Float):Void {
		if (_pathIndex < 0)
			throw new Error("Path is over");
		var dt = TIME_RATIO * spf * timeStep;

		_stagesToAdd.length = 0;
		_rocket.updateEngineA(_massive, _stagesToAdd, dt);
		if (_rocket.outUnlinked) {
			_bigCamera.player.playSound(Assets.unlink_wav);
		}
		for (stage in _stages) {
			stage.updateEngineA(_massive, dt);
		}

		for (stage in _stagesToAdd)
		{
			_massless.push(stage);
			_stages.push(stage);
			stage.startR.set(stage.r);
			stage.startV.set(stage.v);
			_all.push(stage);
		}

		for (unitI in _massive) {
			unitI.path.getRf(_pathIndexF, _r0);
			unitI.path.getRf(_pathIndexF + timeStep, _r1);
			unitI.v.x = (_r1.x - _r0.x) / dt;
			unitI.v.y = (_r1.y - _r0.y) / dt;
		}
		for (unitI in _massless) {
			updateUnitV(unitI, unitI.engineA.x, unitI.engineA.y, dt);
		}

		_rocket.updateCollisions(_all, dt, null);
		for (stage in _stages) {
			stage.updateCollisions(_all, dt, null);
		}

		for (unitI in _massive) {
			unitI.path.getRf(_pathIndexF + timeStep, unitI.r);
		}
		_rocket.v.plus(_rocket.collisionV);
		for (unitI in _massless) {
			if (unitI != _rocket) {
				unitI.r.x += unitI.v.x * dt;
				unitI.r.y += unitI.v.y * dt;
			}
		}
		if (_rocket.fixed == null) {
			_rocket.r.x += _rocket.v.x * dt;
			_rocket.r.y += _rocket.v.y * dt;
		} else {
			_rocket.v.x = _rocket.fixed.v.x;
			_rocket.v.y = _rocket.fixed.v.y;
			_rocket.r.x = _rocket.fixed.r.x + _rocket.fixedX;
			_rocket.r.y = _rocket.fixed.r.y + _rocket.fixedY;
		}
		_rocket.v.minus(_rocket.collisionV);

		_pathIndexF += timeStep;
		var pathIndexI = Std.int(_pathIndexF);
		if (pathIndexI > 0) {
			_pathIndexF -= pathIndexI;
			_pathIndex -= pathIndexI;
			for (unitI in _massive) {
				unitI.path.shift(pathIndexI);
			}
		}

		if (_rocket.outLandingPlanet != null) {
			var planet = _rocket.outLandingPlanet.asPlanet;
			if (planet != null && !planet.status.hasLandingPhoto) {
				planet.status.hasLandingPhoto = true;
				landingPhoto.dispatch(planet);
			}
		}
		if (_rocket.outCollisionDetected && _time > .001) {
			_bigCamera.player.playSound(Assets.knock_wav);
		}
	}

	private static var _targetR:Vertex = new Vertex();
	private static var _unitR:Vertex = new Vertex();

	private static var _commands:Vector<Int> = new Vector<Int>();
	private static var _coords = new Vector<Float>();

	private var _orbitMinR2:Float;
	inline public function getOrbitMinR2():Float {
		return _orbitMinR2;
	}

	private var _orbitMaxR2:Float;
	inline public function getOrbitMaxR2():Float {
		return _orbitMaxR2;
	}

	private var _orbitMaxRx:Float;
	inline public function getOrbitMaxRx():Float {
		return _orbitMaxRx;
	}

	private var _orbitMaxRy:Float;
	inline public function getOrbitMaxRy():Float {
		return _orbitMaxRy;
	}

	private var _orbitMinRx:Float;
	inline public function getOrbitMinRx():Float {
		return _orbitMinRx;
	}

	private var _orbitMinRy:Float;
	inline public function getOrbitMinRy():Float {
		return _orbitMinRy;
	}

	private var _orbitCollision:Unit;
	inline public function getOrbitCollision():Unit {
		return _orbitCollision;
	}

	public var showMaxPoint:Bool;
	public var showMinPoint:Bool;

	public function render() {
		if (_targetUnit == null)
			return;

		for (unit in _all) {
			unit.render(_camera);
			unit.renderBig(_bigCamera);
		}

		var commandI;
		var coordI;
		var g;

		commandI = 0;
		coordI = 0;
		_commands.length = 0;
		_coords.length = 0;

		g = _camera.bottomGraphics;
		if (_glory != null && !(_rocket.ship.hasAntenna && _rocket.ship.deployed)) {
			g.lineStyle();
			g.beginFill(radioAccessible ? 0xffffff : 0xff0000, .1);
			_camera.screenXY(_glory.r.x, _glory.r.y);
			g.drawCircle(_camera.outSX, _camera.outSY, CLOSE_RADIO_DISTANCE * _camera.scale);
			g.endFill();
		}
		if (_glory != null && tutorialOrbitVisible) {
			_camera.screenXY(_glory.r.x, _glory.r.y);
			g.beginFill(ROCKET_PATH_COLOR, .2);
			g.drawCircle(_camera.outSX, _camera.outSY, tutorialOrbitR0);
			g.drawCircle(_camera.outSX, _camera.outSY, tutorialOrbitR1);
			g.endFill();
		}
		var step = 128;
		var dt = TIME_RATIO * spf * step;
		for (unit in _massive) {
			if (unit != _targetUnit && (unit.asPlanet == null || unit.asPlanet.status.discovered)) {
				_camera.screenXY(unit.r.x, unit.r.y);
				var lastX = _camera.outSX;
				var lastY = _camera.outSY;
				var i = step;

				_targetUnit.path.getR(i, _targetR);
				unit.path.getR(i, _unitR);
				startAngle(_targetR, _unitR);

				var inArea = _camera.outSX > .0 &&
					_camera.outSX < _camera.halfSW * 2 &&
					_camera.outSY > .0 &&
					_camera.outSY < _camera.halfSH * 2;
				if (inArea) {
					lastX = _camera.outSX;
					lastY = _camera.outSY;
					_commands[commandI++] = 1;
					_coords[coordI++] = lastX;
					_coords[coordI++] = lastY;
				}
				var lastInArea = inArea;
				while (i < _pathIndex) {
					_targetUnit.path.getR(i, _targetR);
					unit.path.getR(i, _unitR);
					_camera.screenXY(
						_targetUnit.r.x + _unitR.x - _targetR.x,
						_targetUnit.r.y + _unitR.y - _targetR.y);
					inArea = _camera.outSX > .0 &&
						_camera.outSX < _camera.halfSW * 2 &&
						_camera.outSY > .0 &&
						_camera.outSY < _camera.halfSH * 2;
					if (inArea != lastInArea) {
						if (inArea) {
							lastX = _camera.outSX;
							lastY = _camera.outSY;
							_commands[commandI++] = 1;
							_coords[coordI++] = lastX;
							_coords[coordI++] = lastY;
						}
						lastInArea = inArea;
					}
					if (inArea) {
						var dx = _camera.outSX - lastX;
						var dy = _camera.outSY - lastY;
						if (dx * dx + dy * dy > 3 * 3) {
							_commands[commandI++] = 2;
							_coords[coordI++] = _camera.outSX;
							_coords[coordI++] = _camera.outSY;
							lastX = _camera.outSX;
							lastY = _camera.outSY;
						}
					}
					if (MathUtil.abs(getAngle(_targetR, _unitR)) > Math.PI * 2)
						break;
					i += step;
				}
			}
		}
		g.lineStyle(0, PATH_COLOR);
		g.drawPath(_commands, _coords);

		commandI = 0;
		coordI = 0;
		_commands.length = 0;
		_coords.length = 0;

		g = _camera.topGraphics;
		g.lineStyle(0, ROCKET_PATH_COLOR);
		var x0 = .0;
		var y0 = .0;
		var x1 = .0;
		var y1 = .0;
		var distance = -1.;
		var first = true;
		var lastPathSX = 0.;
		var lastPathSY = 0.;
		{
			_camera.screenXY(_rocket.r.x, _rocket.r.y);
			lastPathSX = _camera.outSX;
			lastPathSY = _camera.outSY;
			_commands[commandI++] = 1;
			_coords[coordI++] = lastPathSX;
			_coords[coordI++] = lastPathSY;
		}
		{
			var dt = TIME_RATIO * spf * (1 - _pathIndexF);
			updateUnitV(_rocket, 0, 0, dt);
			_rocket.r.x += _rocket.v.x * dt;
			_rocket.r.y += _rocket.v.y * dt;
		}
		var orbitMinFirst = true;
		_orbitMinR2 = 0;
		_orbitMaxR2 = 0;
		_orbitMaxRx = 0;
		_orbitMaxRy = 0;
		_orbitCollision = null;
		if (_rocket.fixed == null) {
			var i = 1;
			var step = 2;
			var boundary = Std.int(PATH_LENGTH / 20);
			_targetUnit.path.getR(i, _targetR);
			startAngle(_targetR, _rocket.r);
			var collisionUnit = null;
			while (i < PATH_LENGTH) {
				if (i + step - 1 >= _pathIndex)
					break;
				_targetUnit.path.getR(i, _targetR);

				var dt = TIME_RATIO * spf * step;
				updateUnitV(_rocket, 0, 0, dt);
				_rocket.r.x += _rocket.v.x * dt;
				_rocket.r.y += _rocket.v.y * dt;
				for (unitI in _massive) {
					unitI.path.getR(i + step - 1, unitI.r);
				}

				if (MathUtil.abs(getAngle(_targetUnit.r, _rocket.r)) > Math.PI * 2)
					break;

				if (collisionUnit != null)
					break;
				for (unit in _massive) {
					var dx = unit.r.x - _rocket.r.x;
					var dy = unit.r.y - _rocket.r.y;
					if (dx * dx + dy * dy < unit.radius * unit.radius) {
						collisionUnit = unit;
						break;
					}
				}
				if (collisionUnit != null) {
					if (collisionUnit != _targetUnit) {
						_camera.screenXY(
							_targetUnit.startR.x + collisionUnit.r.x - _targetUnit.r.x,
							_targetUnit.startR.y + collisionUnit.r.y - _targetUnit.r.y);
						g.drawCircle(_camera.outSX, _camera.outSY,
							collisionUnit.radius * _camera.scale);
					}
					_orbitCollision = collisionUnit;
					break;
				}
				{
					var dx = _rocket.r.x - _targetUnit.r.x;
					var dy = _rocket.r.y - _targetUnit.r.y;
					var rr2 = dx * dx + dy * dy;
					if (_orbitMaxR2 < rr2) {
						_orbitMaxR2 = rr2;
						_orbitMaxRx = dx;
						_orbitMaxRy = dy;
					}
					if (orbitMinFirst || _orbitMinR2 > rr2) {
						orbitMinFirst = false;
						_orbitMinR2 = rr2;
						_orbitMinRx = dx;
						_orbitMinRy = dy;
					}
					_camera.screenXY(_targetUnit.startR.x + dx, _targetUnit.startR.y + dy);
					var dx = _camera.outSX - lastPathSX;
					var dy = _camera.outSY - lastPathSY;
					if (dx * dx + dy * dy > 2 * 2) {
						lastPathSX = _camera.outSX;
						lastPathSY = _camera.outSY;
						_commands[commandI++] = 2;
						_coords[coordI++] = lastPathSX;
						_coords[coordI++] = lastPathSY;
					}
				}
				var unit = ListUtil.select(
					_massive,
					value != _rocket,
					{
						var dx = value.r.x - _rocket.r.x;
						var dy = value.r.y - _rocket.r.y;
						-(dx * dx + dy * dy);
					}
				);
				if (unit != null && unit != _targetUnit) {
					var dx = unit.r.x - _rocket.r.x;
					var dy = unit.r.y - _rocket.r.y;
					var distanceI = dx * dx + dy * dy;
					if (distance < 0) {
						distance = distanceI;
					} else {
						if (!first && distanceI < distance)
							first = true;
						if (distanceI < distance) {
							distance = distanceI;
						} else if (first && distanceI > distance) {
							first = false;
							if (i > step * 2 && unit.asPlanet != null && unit.asPlanet.status.discovered) {
								_camera.screenXY(
									_targetUnit.startR.x + unit.r.x - _targetUnit.r.x,
									_targetUnit.startR.y + unit.r.y - _targetUnit.r.y);
								g.moveTo(lastPathSX, lastPathSY);
								g.lineTo(_camera.outSX, _camera.outSY);
								g.drawCircle(_camera.outSX, _camera.outSY, unit.radius * _camera.scale);
							}
						}
					}
				}
				i += step;
				if (i > boundary)
					step = 16;
			}
			g.drawPath(_commands, _coords);
		}
		for (unitI in _massive) {
			unitI.v.set(unitI.startV);
			unitI.r.set(unitI.startR);
		}
		_rocket.v.set(_rocket.startV);
		_rocket.r.set(_rocket.startR);

		if (showMaxPoint) {
			_camera.screenXY(_targetUnit.r.x + _orbitMaxRx, _targetUnit.r.y + _orbitMaxRy);
			g.lineStyle(0, 0x00ffff);
			g.drawCircle(_camera.outSX, _camera.outSY, 2);
		}
		if (showMinPoint) {
			_camera.screenXY(_targetUnit.r.x + _orbitMinRx, _targetUnit.r.y + _orbitMinRy);
			g.lineStyle(0, 0x00ffff);
			g.drawCircle(_camera.outSX, _camera.outSY, 2);
		}
	}

	private var _angleX:Float;
	private var _angleY:Float;
	private var _angle:Float;

	inline private function startAngle(targetR:Vertex, unitR:Vertex):Void {
		_angleX = unitR.x - targetR.x;
		_angleY = unitR.y - targetR.y;
		_angle = 0.;
	}

	inline private function getAngle(targetR:Vertex, unitR:Vertex):Float {
		var angleX = unitR.x - targetR.x;
		var angleY = unitR.y - targetR.y;
		var k = MathUtil.invSqrt(angleX * angleX + angleY * angleY);
		var ex = k * angleX;
		var ey = k * angleY;
		var dx = angleX - _angleX;
		var dy = angleY - _angleY;
		_angle += (ex * dy - ey * dx) * k;
		_angleX = angleX;
		_angleY = angleY;
		return _angle;
	}

	private var _cameraOffsetX:Float = 0;
	private var _cameraOffsetY:Float = 0;

	private function setTarget(unit:Unit):Void {
		if (_targetUnit != unit) {
			_targetUnit = unit;
			_cameraOffsetX = _camera.x - _targetUnit.r.x;
			_cameraOffsetY = _camera.y - _targetUnit.r.y;
			Tween.to(this, 500, {_cameraOffsetX:0, _cameraOffsetY:0}).setEase(Tween.easeInOut);
		}
	}
}
