package game;

import flash.geom.Point;

class Link {
	public function new(owner:RocketStage, x:Float, y:Float) {
		this.owner = owner;
		this.x = x;
		this.y = y;
	}

	public var owner:RocketStage;
	public var x:Float;
	public var y:Float;
	public var child:RocketStage;
}
