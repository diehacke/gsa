package game;

import flash.Vector;
import utils.MathUtil;
import flash.display.Sprite;
import elm.utils.Rand;
import flash.geom.Matrix;
import flash.display.Shape;
import flash.display.BitmapData;
import game.PlanetInfos;
import utils.ListUtil;
import utils.RotationMatrix;
import utils.Vertex;

class Ring {
	private static var N:Int = 400;
	private static var COS:Vector<Float> = {
		var values = new Vector<Float>(N + 1, true);
		for (i in 0 ... N + 1) {
			values[i] = Math.cos(i * Math.PI * 2 / N);
		}
		values;
	}
	private static var SIN:Vector<Float> = {
		var values = new Vector<Float>(N + 1, true);
		for (i in 0 ... N + 1) {
			values[i] = Math.sin(i * Math.PI * 2 / N);
		}
		values;
	}

	private static var _shape:Shape = new Shape();
	private static var _matrix:Matrix = new Matrix();
	private static var _sqrt2_2:Float = Math.sqrt(2) / 2;

	public var radius:Float;
	public var rcos:Float;
	public var rsin:Float;
	public var width:Float;
	public var next:Ring;

	private var _color0:UInt;
	private var _color1:UInt;
	private var _scale:Float;
	private var _bd:BitmapData;
	private var _bdHalfSize:Int;
	private var _r0:Float;
	private var _r1:Float;
	private var _w:Float;

	public function new(
		next:Ring, m:Float, radius:Float, width:Float, scale:Float, angle:Float,
		color0:UInt, color1:UInt) {
		this.next = next;
		this.radius = radius;
		this.width = width;
		_scale = scale;
		rcos = Math.cos(angle);
		rsin = Math.sin(angle);
		_color0 = color0;
		_color1 = color1;
		_r0 = radius - width;
		_r1 = radius / Math.cos(Math.PI / 8);
		_bdHalfSize = Math.ceil(radius) + 2;
		var bdSize = _bdHalfSize << 1;
		_bd = new BitmapData(bdSize, bdSize, true, 0x000000);
		_w = World.getFirstVelocity(m, radius) / radius;
		var g = _shape.graphics;
		var x = _bdHalfSize;
		var y = _bdHalfSize;
		g.beginFill(color0, .2);
		g.drawCircle(x, y, radius);
		g.drawCircle(x, y, radius - width);
		g.endFill();
		g.beginFill(color1, .2);
		g.drawCircle(x, y, radius - width * .4);
		g.drawCircle(x, y, radius - width * .6);
		g.endFill();
		g.lineStyle(1, color1, .2);
		for (i in 0 ... 200) {
			var r = radius - width * Math.random();
			var angle = Math.random() * Math.PI * 2;
			var dAngle = Math.random() * 2 / r;
			g.moveTo(x + r * Math.cos(angle), y + r * Math.sin(angle));
			for (i in 0 ... 8) {
				angle += dAngle * i;
				g.lineTo(x + r * Math.cos(angle), y + r * Math.sin(angle));
			}
		}
		_bd.draw(_shape);
		g.clear();
	}

	private var _angle:Float = 0;

	inline public function update(dt:Float):Void {
		_angle += _w * dt;
		_angle = _angle % (Math.PI * 2);
	}

	public function render(r:Vertex, camera:Camera):Void {
		var sin = Math.sin(-_angle);
		var cos = Math.cos(-_angle);

		camera.screenXY(r.x, r.y);
		_a = rcos;
		_b = -rsin * _scale;
		_c = rsin;
		_d = rcos * _scale;
		_tx = camera.outSX;
		_ty = camera.outSY;

		for (i in 0 ... 2) {
			var x0;
			var y0;
			var x1;
			var y1;
			var x2;
			var y2;
			var x3;
			var y3;
			var x4;
			var y4;

			if (i == 0) {
				x0 = -1;
				y0 = .0;
				x1 = -_sqrt2_2;
				y1 = -_sqrt2_2;
				x2 = .0;
				y2 = -1;
				x3 = _sqrt2_2;
				y3 = -_sqrt2_2;
				x4 = 1;
				y4 = .0;
			} else {
				x0 = -1;
				y0 = .0;
				x1 = -_sqrt2_2;
				y1 = _sqrt2_2;
				x2 = .0;
				y2 = 1;
				x3 = _sqrt2_2;
				y3 = _sqrt2_2;
				x4 = 1;
				y4 = .0;
			}

			/*
			scale, 0, 0, scale, -_bdHalfSize * scale, -_bdHalfSize * scale
			cos, -sin, sin, cos, 0, 0
			1, 0, 0, _scale, 0, 0
			rcos, rsin, -rsin, rcos, 0, 0
			1, 0, 0, 1, _tx, _ty
			*/
			var scale = camera.scale;
			var tt = -_bdHalfSize * scale;
			_matrix.a = scale * (rsin * _scale * sin + rcos * cos);
			_matrix.b = scale * (rsin * cos - rcos * _scale * sin);
			_matrix.c = scale * (rcos * sin - rsin * _scale * cos);
			_matrix.d = scale * (rsin * sin + rcos * _scale * cos);
			_matrix.tx = tt * (rsin * _scale * sin + rcos * cos + rcos * sin - rsin * _scale * cos) + _tx;
			_matrix.ty = tt * (rsin * cos - rcos * _scale * sin + rsin * sin + rcos * _scale * cos) + _ty;

			var g = i == 0 ? camera.bottomGraphics : camera.topGraphics;
			g.beginBitmapFill(_bd, _matrix, false, true);

			transform(scale * _r1 * x0, scale * _r1 * y0);
			g.moveTo(_outX, _outY);
			transform(scale * _r1 * x1, scale * _r1 * y1);
			g.lineTo(_outX, _outY);
			transform(scale * _r1 * x2, scale * _r1 * y2);
			g.lineTo(_outX, _outY);
			transform(scale * _r1 * x3, scale * _r1 * y3);
			g.lineTo(_outX, _outY);
			transform(scale * _r1 * x4, scale * _r1 * y4);
			g.lineTo(_outX, _outY);

			transform(scale * _r0 * x4, scale * _r0 * y4);
			g.lineTo(_outX, _outY);
			transform(scale * _r0 * x3, scale * _r0 * y3);
			g.lineTo(_outX, _outY);
			transform(scale * _r0 * x2, scale * _r0 * y2);
			g.lineTo(_outX, _outY);
			transform(scale * _r0 * x1, scale * _r0 * y1);
			g.lineTo(_outX, _outY);
			transform(scale * _r0 * x0, scale * _r0 * y0);
			g.lineTo(_outX, _outY);

			g.endFill();

			g.drawCircle(_tx, _ty, radius * scale);
		}
	}

	public function renderBig(r:Vertex, camera:BigCamera):Void {
		if (!camera.inArea(r.x, r.y, radius))
			return;

		camera.screenXY(r.x, r.y);
		/*
		a:matrix([cos, -sin, 0], [sin, cos, 0], [0, 0, 1]);
		b:matrix([1, 0, 0], [0, _scale, 0], [0, 0, 1]);
		c:matrix([rcos, rsin, 0], [-rsin, rcos, 0], [0, 0, 1]);
		d:matrix([1, 0, 0], [0, 1, 0], [_tx, _ty, 1]);
		*/

		var sin = Math.sin(-_angle);
		var cos = Math.cos(-_angle);

		_a = rsin * _scale * sin + rcos * cos;
		_b = rcos * sin - rsin * _scale * cos;
		_c = rsin * cos - rcos * _scale * sin;
		_d = rsin * sin + rcos * _scale * cos;
		_tx = camera.outSX;
		_ty = camera.outSY;

		var g = MathUtil.getAngle(rcos, rsin, camera.x - r.x, camera.y - r.y) > 0 ?
			camera.topGraphics :
			camera.bottomGraphics;
		var m = {
			var w = radius * Math.PI * 2 / N;
			var h = width;
			Math.round(ListUtil.max(1, h / w));
		};
		var sw = camera.halfSW * 2;
		var sh = camera.halfSH * 2;
		var scale = camera.scale;
		for (ii in 0 ... N >> 4) {
			var i = ii << 4;
			var cos0 = COS[i];
			var sin0 = SIN[i];
			var cos1 = COS[i + 16];
			var sin1 = SIN[i + 16];
			var r0 = _r0 * scale;
			var r1 = _r1 * scale;
			transform(cos0 * r0, sin0 * r0);
			var x0 = _outX;
			var y0 = _outY;
			transform(cos0 * r1, sin0 * r1);
			var x1 = _outX;
			var y1 = _outY;
			transform(cos1 * r0, sin1 * r0);
			var x2 = _outX;
			var y2 = _outY;
			transform(cos1 * r1, sin1 * r1);
			var x3 = _outX;
			var y3 = _outY;

			var minX = ListUtil.min(x0, x1, x2, x3);
			var minY = ListUtil.min(y0, y1, y2, y3);
			var maxX = ListUtil.max(x0, x1, x2, x3);
			var maxY = ListUtil.max(y0, y1, y2, y3);

			if (maxX > 0 && minX < sw && maxY > 0 && minY < sh)
			for (i in ii << 4 ... (ii + 1) << 4) {
				var cos0 = COS[i];
				var sin0 = SIN[i];
				var cos1 = COS[i + 1];
				var sin1 = SIN[i + 1];

				var r0 = _r0 * scale;
				var r1 = _r1 * scale;
				transform(cos0 * r0, sin0 * r0);
				var x0 = _outX;
				var y0 = _outY;
				transform(cos0 * r1, sin0 * r1);
				var x1 = _outX;
				var y1 = _outY;
				transform(cos1 * r0, sin1 * r0);
				var x2 = _outX;
				var y2 = _outY;
				transform(cos1 * r1, sin1 * r1);
				var x3 = _outX;
				var y3 = _outY;

				var minX = ListUtil.min(x0, x1, x2, x3);
				var minY = ListUtil.min(y0, y1, y2, y3);
				var maxX = ListUtil.max(x0, x1, x2, x3);
				var maxY = ListUtil.max(y0, y1, y2, y3);

				if (maxX > 0 && minX < sw && maxY > 0 && minY < sh)
				{
					for (j in 0 ... m) {
						var r0 = _r0 * scale + j / m * width * scale;
						var r1 = r0 + 1 / m * width * scale;
						transform(cos0 * r0, sin0 * r0);
						var x0 = _outX;
						var y0 = _outY;
						transform(cos0 * r1, sin0 * r1);
						var x1 = _outX;
						var y1 = _outY;
						transform(cos1 * r0, sin1 * r0);
						var x2 = _outX;
						var y2 = _outY;
						transform(cos1 * r1, sin1 * r1);
						var x3 = _outX;
						var y3 = _outY;

						var minX = ListUtil.min(x0, x1, x2, x3);
						var minY = ListUtil.min(y0, y1, y2, y3);
						var maxX = ListUtil.max(x0, x1, x2, x3);
						var maxY = ListUtil.max(y0, y1, y2, y3);

						if (maxX > 0 && minX < sw && maxY > 0 && minY < sh)
						{
							Rand.instance.seed = (i + 1) % N * 100 + j * 10;
							var right = Rand.instance.getFloat(0, 1) < .5;
							var rightY0 = Rand.instance.getFloat(.1, .9);
							var rightY1 = rightY0 + Rand.instance.getFloat(.01, .3);

							Rand.instance.seed = i * 100 + j * 10;
							var left = Rand.instance.getFloat(0, 1) < .5;
							var leftY0 = Rand.instance.getFloat(.1, .9);
							var leftY1 = leftY0 + Rand.instance.getFloat(.01, .3);

							var k = Rand.instance.getFloat(.01, .1);
							var nx = (x1 - x0) * k;
							var ny = (y1 - y0) * k;

							var k0 = Rand.instance.getFloat(.1, .9);
							var k1 = Rand.instance.getFloat(.3, .7);
							var cx = MathUtil.lerp(MathUtil.lerp(x0, x1, k0), MathUtil.lerp(x2, x3, k0), k1);
							var cy = MathUtil.lerp(MathUtil.lerp(y0, y1, k0), MathUtil.lerp(y2, y3, k0), k1);
							g.beginFill(_color1);
							g.moveTo(MathUtil.lerp(x0, x1, k0), MathUtil.lerp(y0, y1, k0));
							g.lineTo(cx - nx, cy - ny);
							g.lineTo(MathUtil.lerp(x2, x3, k0), MathUtil.lerp(y2, y3, k0));
							g.lineTo(cx + nx, cy + ny);
							g.endFill();

							if (right) {
								var k = (rightY0 + rightY1) * .5;
								g.beginFill(_color0);
								g.moveTo(MathUtil.lerp(x2, x3, rightY0), MathUtil.lerp(y2, y3, rightY0));
								g.lineTo(MathUtil.lerp(x2, x3, rightY1), MathUtil.lerp(y2, y3, rightY1));
								g.lineTo(MathUtil.lerp(x0, x1, k), MathUtil.lerp(y0, y1, k));
								g.endFill();
							}
							if (left) {
								var k = (leftY0 + leftY1) * .5;
								g.beginFill(_color0);
								g.moveTo(MathUtil.lerp(x0, x1, leftY0), MathUtil.lerp(y0, y1, leftY0));
								g.lineTo(MathUtil.lerp(x0, x1, leftY1), MathUtil.lerp(y0, y1, leftY1));
								g.lineTo(MathUtil.lerp(x2, x3, k), MathUtil.lerp(y2, y3, k));
								g.endFill();
							}
						}
					}
				}
			}
		}
	}

	private var _a:Float;
	private var _b:Float;
	private var _c:Float;
	private var _d:Float;
	private var _tx:Float;
	private var _ty:Float;
	private var _outX:Float;
	private var _outY:Float;

	inline private function transform(x:Float, y:Float):Void {
		_outX = _a * x + _b * y + _tx;
		_outY = _c * x + _d * y + _ty;
	}
}
