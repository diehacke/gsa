package game;

import flash.display.Bitmap;
import flash.text.TextField;
import ui.UIRects;
import ui.Widget;
import ui.SmartFormat;
import utils.StringUtil;

class TimePanel extends Widget {
	private var _timeTf:TextField;

	public function new() {
		super();

		_timeTf = new SmartFormat("Tahoma", 14).setBold(true).setColor(World.LINE_COLOR).setCenter().newFixed();
		_timeTf.height = 30;
		_timeTf.width = 100;
		addChild(_timeTf);

		redrawBack();
	}

	override public function redraw():Void {
		_w = UIRects.time_panel_0.logicW - 5;
		_h = UIRects.time_panel_0.logicH;
		_timeTf.x = Std.int((_w - 5) * .5 - _timeTf.width * .5);
	}

	private var _accelerationIndex:Int;

	public function setParameters(time:Float, accelerationIndex:Int):Void {
		_timeTf.text = StringUtil.time(time);
		if (_accelerationIndex != accelerationIndex) {
			_accelerationIndex = accelerationIndex;
			redrawBack();
		}
	}

	private function redrawBack():Void {
		var rect = switch (_accelerationIndex) {
			case 1: UIRects.time_panel_1;
			case 2: UIRects.time_panel_2;
			case 3: UIRects.time_panel_3;
			case 4: UIRects.time_panel_4;
			default: UIRects.time_panel_0;
		};
		var g = graphics;
		g.clear();
		rect.drawWithoutTolerance(g, Assets.ui_png, 0, 0);
	}
}
