package game;

import flash.display.Graphics;
import flash.display.Sprite;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.Vector;
import model.PlanetStatus;
import utils.ListUtil;
import utils.MathUtil;
import utils.RotationMatrix;
import utils.Vertex;

class Planet extends Unit {
	public static var planets:BitmapData = Assets.planets_png;
	public static var planetsWidth:Float = planets.width;
	public static var planetsHeight:Float = planets.height;
	public static var planetsLow:BitmapData = {
		var bd = new BitmapData(planets.width >> 3, planets.height >> 3, true, 0x000000);
		bd.draw(new Bitmap(planets), new Matrix(1 / 8, 0, 0, 1 / 8));
		bd;
	}

	private static var N:Int = 100;
	private static var COS:Vector<Float> = {
		var values = new Vector<Float>(N + 1, true);
		for (i in 0 ... N + 1) {
			values[i] = Math.cos(i * Math.PI * 2 / N);
		}
		values;
	}
	private static var SIN:Vector<Float> = {
		var values = new Vector<Float>(N + 1, true);
		for (i in 0 ... N + 1) {
			values[i] = Math.sin(i * Math.PI * 2 / N);
		}
		values;
	}

	private var _info:PlanetInfo;

	public function new(info:PlanetInfo) {
		super();
		_info = info;
		name = _info.name;
		radius = _info.radius;
		atmRadius = _info.atmH > .001 ? _info.radius + _info.atmH : .0;
		surface = Assets.surface_loma_png;
		status = _info.status;
		asPlanet = this;
	}

	public var surface:BitmapData;
	public var status:PlanetStatus;
	public var rings:Ring;

	private static var _vertices:Vector<Float> = new Vector<Float>();
	private static var _indices:Vector<Int> = new Vector<Int>();
	private static var _uvtData:Vector<Float> = new Vector<Float>();
	private static var _matrix:RotationMatrix = new RotationMatrix();

	override public function render(camera:Camera):Void {
		if (!status.discovered)
			return;
		if (status.hasClosePhoto) {
			var ring = rings;
			while (ring != null) {
				ring.render(r, camera);
				ring = ring.next;
			}
		} else if (status.hasPhoto) {
			drawLowRings(camera);
		}
		if (camera.inArea(r.x, r.y, radius))
		{
			camera.screenXY(r.x, r.y);
			var x = camera.outSX;
			var y = camera.outSY;
			if (camera.scale * radius > 1.5) {
				var texture = null;
				if (status.hasClosePhoto) {
					texture = planets;
				} else if (status.hasPhoto) {
					texture = planetsLow;
				} else {
					var g = camera.topGraphics;
					g.beginFill(0xcccccc);
					g.drawCircle(x, y, radius * camera.scale);
					g.endFill();
					return;
				}

				var size = camera.scale * _info.size;

				_vertices.length = 0;
				_indices.length = 0;
				_uvtData.length = 0;

				_vertices[0] = x - size;
				_vertices[1] = y - size;
				_vertices[2] = x + size;
				_vertices[3] = y - size;
				_vertices[4] = x + size;
				_vertices[5] = y + size;
				_vertices[6] = x - size;
				_vertices[7] = y + size;

				_indices[0] = 0;
				_indices[1] = 1;
				_indices[2] = 2;
				_indices[3] = 2;
				_indices[4] = 3;
				_indices[5] = 0;

				var u0 = (_info.centerX - _info.size) / planetsWidth;
				var v0 = (_info.centerY - _info.size) / planetsHeight;
				var u1 = (_info.centerX + _info.size) / planetsWidth;
				var v1 = (_info.centerY + _info.size) / planetsHeight;

				_uvtData[0] = u0;
				_uvtData[1] = v0;
				_uvtData[2] = 1;

				_uvtData[3] = u1;
				_uvtData[4] = v0;
				_uvtData[5] = 1;

				_uvtData[6] = u1;
				_uvtData[7] = v1;
				_uvtData[8] = 1;

				_uvtData[9] = u0;
				_uvtData[10] = v1;
				_uvtData[11] = 1;

				var g = camera.graphics;
				g.lineStyle();
				g.beginBitmapFill(texture, null, false, true);
				g.drawTriangles(_vertices, _indices, _uvtData);
				g.endFill();

				if (_info.hasDark) {
					var dx = camera.sunX - r.x;
					var dy = camera.sunY - r.y;
					var length = MathUtil.sqrt(dx * dx + dy * dy);
					var cos = dx / length;
					var sin = dy / length;
					_matrix.rotateXTo(dx / length, dy / length);

					_vertices.length = 0;
					_indices.length = 0;
					_uvtData.length = 0;

					var part = .25;

					_matrix.transform(-size, -size);
					_vertices[0] = x + _matrix.outX;
					_vertices[1] = y + _matrix.outY;
					_matrix.transform(size * part, -size);
					_vertices[2] = x + _matrix.outX;
					_vertices[3] = y + _matrix.outY;
					_matrix.transform(size * part, size);
					_vertices[4] = x + _matrix.outX;
					_vertices[5] = y + _matrix.outY;
					_matrix.transform(-size, size);
					_vertices[6] = x + _matrix.outX;
					_vertices[7] = y + _matrix.outY;

					_indices[0] = 0;
					_indices[1] = 1;
					_indices[2] = 2;
					_indices[3] = 2;
					_indices[4] = 3;
					_indices[5] = 0;

					var u0 = (_info.centerX + _info.size * 2 - _info.size) / planetsWidth;
					var v0 = (_info.centerY - _info.size) / planetsHeight;
					var u1 = (_info.centerX + _info.size * 2 + _info.size * part) / planetsWidth;
					var v1 = (_info.centerY + _info.size) / planetsHeight;

					_uvtData[0] = u0;
					_uvtData[1] = v0;
					_uvtData[2] = 1;

					_uvtData[3] = u1;
					_uvtData[4] = v0;
					_uvtData[5] = 1;

					_uvtData[6] = u1;
					_uvtData[7] = v1;
					_uvtData[8] = 1;

					_uvtData[9] = u0;
					_uvtData[10] = v1;
					_uvtData[11] = 1;

					var g = camera.graphics;
					g.lineStyle();
					g.beginBitmapFill(texture, null, false, true);
					g.drawTriangles(_vertices, _indices, _uvtData);
					g.endFill();
				}
			} else {
				var g = camera.topGraphics;
				g.lineStyle(0, World.LINE_COLOR);
				g.drawCircle(x, y, Math.max(2, radius * camera.scale * 2));
			}
		}
	}

	override public function renderBig(camera:BigCamera):Void {
		var ring = rings;
		while (ring != null) {
			ring.renderBig(r, camera);
			ring = ring.next;
		}
		if (camera.inArea(r.x, r.y, radius + 20)) {
			camera.screenXY(r.x, r.y);
			var g = camera.graphics;

			_vertices.length = 0;
			_indices.length = 0;
			_uvtData.length = 0;
			var index = 0;
			var verticesI = 0;
			var indicesI = 0;
			var uvtI = 0;
			var size = _info.atmH;
			var scale = camera.scale;

			var sRadius0 = (radius - 1) * scale;
			var sRadius1 = (radius + size) * scale;
			var nh = 6;
			var size1 = size * 1 / 5;
			if (size < .1) {
				nh = 2;
				size1 = 1;
				sRadius1 = (radius + 1) * scale;
			}
			for (i in 0 ... N) {
				var cos0 = COS[i];
				var sin0 = SIN[i];
				var cos1 = COS[i + 1];
				var sin1 = SIN[i + 1];

				var x0 = camera.outSX + cos0 * sRadius0;
				var y0 = camera.outSY + sin0 * sRadius0;
				var x1 = camera.outSX + cos0 * sRadius1;
				var y1 = camera.outSY + sin0 * sRadius1;
				var x2 = camera.outSX + cos1 * sRadius0;
				var y2 = camera.outSY + sin1 * sRadius0;
				var x3 = camera.outSX + cos1 * sRadius1;
				var y3 = camera.outSY + sin1 * sRadius1;

				var minX = ListUtil.min(x0, x1, x2, x3);
				var minY = ListUtil.min(y0, y1, y2, y3);
				var maxX = ListUtil.max(x0, x1, x2, x3);
				var maxY = ListUtil.max(y0, y1, y2, y3);

				if (maxX > 0 && minX < camera.halfSW * 2 && maxY > 0 && minY < camera.halfSH * 2)
				{
					for (j in 0 ... nh) {
						var sRadius0 = .0;
						var sRadius1 = .0;
						var v0 = .0;
						var v1 = .0;
						switch (j) {
							case 0:
								sRadius0 = (radius - 1) * scale;
								sRadius1 = (radius) * scale;
								v0 = 1;
								v1 = .75;
							case 1:
								sRadius0 = (radius) * scale;
								sRadius1 = (radius + size1) * scale;
								v0 = .75;
								v1 = .5;
							case 2:
								sRadius0 = (radius + size * 1 / 5) * scale;
								sRadius1 = (radius + size * 2 / 5) * scale;
								v0 = .5;
								v1 = .25;
							case 3:
								sRadius0 = (radius + size * 2 / 5) * scale;
								sRadius1 = (radius + size * 3 / 5) * scale;
								v0 = .5;
								v1 = .25;
							case 4:
								sRadius0 = (radius + size * 3 / 5) * scale;
								sRadius1 = (radius + size * 4 / 5) * scale;
								v0 = .5;
								v1 = .25;
							case 5:
								sRadius0 = (radius + size * 4 / 5) * scale;
								sRadius1 = (radius + size * 5 / 5) * scale;
								v0 = .25;
								v1 = 0;
						}
						var x0 = camera.outSX + cos0 * sRadius0;
						var y0 = camera.outSY + sin0 * sRadius0;
						var x1 = camera.outSX + cos0 * sRadius1;
						var y1 = camera.outSY + sin0 * sRadius1;
						var x2 = camera.outSX + cos1 * sRadius0;
						var y2 = camera.outSY + sin1 * sRadius0;
						var x3 = camera.outSX + cos1 * sRadius1;
						var y3 = camera.outSY + sin1 * sRadius1;

						var minX = ListUtil.min(x0, x1, x2, x3);
						var minY = ListUtil.min(y0, y1, y2, y3);
						var maxX = ListUtil.max(x0, x1, x2, x3);
						var maxY = ListUtil.max(y0, y1, y2, y3);

						if (maxX > 0 && minX < camera.halfSW * 2 &&
							maxY > 0 && minY < camera.halfSH * 2)
						{
							_vertices[verticesI++] = x0;
							_vertices[verticesI++] = y0;
							_vertices[verticesI++] = x1;
							_vertices[verticesI++] = y1;
							_vertices[verticesI++] = x2;
							_vertices[verticesI++] = y2;
							_vertices[verticesI++] = x3;
							_vertices[verticesI++] = y3;

							_indices[indicesI++] = index + 0;
							_indices[indicesI++] = index + 1;
							_indices[indicesI++] = index + 2;
							_indices[indicesI++] = index + 1;
							_indices[indicesI++] = index + 3;
							_indices[indicesI++] = index + 2;
							index += 4;

							_uvtData[uvtI++] = 0;
							_uvtData[uvtI++] = v0;
							_uvtData[uvtI++] = 1;

							_uvtData[uvtI++] = 0;
							_uvtData[uvtI++] = v1;
							_uvtData[uvtI++] = 1;

							_uvtData[uvtI++] = 1;
							_uvtData[uvtI++] = v0;
							_uvtData[uvtI++] = 1;

							_uvtData[uvtI++] = 1;
							_uvtData[uvtI++] = v1;
							_uvtData[uvtI++] = 1;
						}
					}
				}
			}

			g.lineStyle();
			g.beginBitmapFill(surface, null, false, true);

			g.drawTriangles(_vertices, _indices, _uvtData);
			g.endFill();
		}
	}

	private var _lowRingsBdTop:BitmapData;
	private var _lowRingsBdBottom:BitmapData;
	private var _lowRingsSize:Float;

	private static var _ringsCamera:Camera;
	private static var _ringsR:Vertex = new Vertex();
	private static var _ringsMatrix:Matrix = new Matrix();
	inline private static var RINGS_K:Int = 8;

	private function drawLowRings(camera:Camera):Void {
		if (rings != null) {
			if (_ringsCamera == null) {
				_ringsCamera = new Camera();
				_ringsCamera.scale = 1 / RINGS_K;
				_ringsCamera.x = 0;
				_ringsCamera.y = 0;
				_ringsCamera.bottomContainer = new Sprite();
				_ringsCamera.container = new Sprite();
				_ringsCamera.topContainer = new Sprite();
				_ringsCamera.updateGraphics();
			}
			if (_lowRingsBdTop == null) {
				_ringsCamera.topGraphics.clear();
				_ringsCamera.bottomGraphics.clear();
				_ringsR.setXY(0, 0);
				var ring = rings;
				var maxRadius = .0;
				while (ring != null) {
					ring.render(_ringsR, _ringsCamera);
					if (maxRadius < ring.radius)
						maxRadius = ring.radius;
					ring = ring.next;
				}
				_lowRingsSize = Math.ceil(maxRadius * 2.1 / RINGS_K) * RINGS_K;

				var size = Math.round(_lowRingsSize * _ringsCamera.scale);
				_ringsMatrix.a = 1;
				_ringsMatrix.b = 0;
				_ringsMatrix.c = 0;
				_ringsMatrix.d = 1;
				_ringsMatrix.tx = size * .5;
				_ringsMatrix.ty = size * .5;
				_lowRingsBdTop = new BitmapData(size, size, true, 0x000000);
				_lowRingsBdBottom = new BitmapData(size, size, true, 0x000000);
				_lowRingsBdTop.draw(_ringsCamera.topContainer, _ringsMatrix);
				_lowRingsBdBottom.draw(_ringsCamera.bottomContainer, _ringsMatrix);
			}

			camera.screenXY(r.x, r.y);
			var x = camera.outSX;
			var y = camera.outSY;

			var scale = camera.scale / _ringsCamera.scale;
			var size = _lowRingsSize * camera.scale;
			_ringsMatrix.a = scale;
			_ringsMatrix.b = 0;
			_ringsMatrix.c = 0;
			_ringsMatrix.d = scale;
			_ringsMatrix.tx = x - _lowRingsSize * scale * .5 + size * .5;
			_ringsMatrix.ty = y - _lowRingsSize * scale * .5 + size * .5;

			camera.topGraphics.beginBitmapFill(_lowRingsBdTop, _ringsMatrix, true, true);
			camera.topGraphics.drawRect(x - size * .5, y - size * .5, size, size);
			camera.topGraphics.endFill();

			camera.bottomGraphics.beginBitmapFill(_lowRingsBdBottom, _ringsMatrix, true, true);
			camera.bottomGraphics.drawRect(x - size * .5, y - size * .5, size, size);
			camera.bottomGraphics.endFill();
		}
	}

	public function drawSimple(g:Graphics, x:Float, y:Float):Void {
		var size = _info.size;

		_vertices.length = 0;
		_indices.length = 0;
		_uvtData.length = 0;

		var i;

		i = 0;
		_vertices[i++] = x - size;
		_vertices[i++] = y - size;
		_vertices[i++] = x + size;
		_vertices[i++] = y - size;
		_vertices[i++] = x + size;
		_vertices[i++] = y + size;
		_vertices[i++] = x - size;
		_vertices[i++] = y + size;

		i = 0;
		_indices[i++] = 0;
		_indices[i++] = 1;
		_indices[i++] = 2;
		_indices[i++] = 2;
		_indices[i++] = 3;
		_indices[i++] = 0;

		var u0 = (_info.centerX - _info.size) / planetsWidth;
		var v0 = (_info.centerY - _info.size) / planetsHeight;
		var u1 = (_info.centerX + _info.size) / planetsWidth;
		var v1 = (_info.centerY + _info.size) / planetsHeight;

		i = 0;

		_uvtData[i++] = u0;
		_uvtData[i++] = v0;
		_uvtData[i++] = 1;

		_uvtData[i++] = u1;
		_uvtData[i++] = v0;
		_uvtData[i++] = 1;

		_uvtData[i++] = u1;
		_uvtData[i++] = v1;
		_uvtData[i++] = 1;

		_uvtData[i++] = u0;
		_uvtData[i++] = v1;
		_uvtData[i++] = 1;

		g.lineStyle();
		g.beginBitmapFill(planets, null, false, false);
		g.drawTriangles(_vertices, _indices, _uvtData);
		g.endFill();
	}
}
