package game;

import screens.Screen;
import game.Rocket;
import model.StageModels;
import utils.MathUtil;
import utils.Vertex;
import utils.RotationMatrix;
import flash.Vector;
import flash.display.Graphics;

class RoundIntersectionTest extends Screen {
	public function new() {
		super();
	}

	override public function doOn():Void {
		var g = graphics;
		g.lineStyle(0, 0x00ff00);
		var all = new Vector<Unit>();
		var target = new Unit();
		all.push(target);
		var rocket = new Rocket(Stages.shipexplorer.model.newRootRocketStage());
		all.push(rocket);
		target.r.x = 250;
		target.r.y = 250;
		target.v.x = 0;
		target.v.y = 0;
		target.radius = 100;
		g.drawCircle(target.r.x, target.r.y, target.radius);
		rocket.r.setXY(180, 250);
		rocket.v.setXY(10, -180);
		var angle = Math.PI * .2;
		var direction = new Vertex().setXY(Math.cos(angle), Math.sin(angle));
		var radiusX = 20;
		var radiusY = 30;

		g.moveTo(rocket.r.x, rocket.r.y);
		g.lineTo(rocket.r.x + rocket.v.x, rocket.r.y + rocket.v.y);

		var _matrix = new RotationMatrix();
		_matrix.rotateYTo(direction.x, direction.y);
		_matrix.transform(-radiusX, -radiusY);
		g.moveTo(rocket.r.x + _matrix.outX, rocket.r.y + _matrix.outY);
		_matrix.transform(radiusX, -radiusY);
		g.lineTo(rocket.r.x + _matrix.outX, rocket.r.y + _matrix.outY);
		_matrix.transform(radiusX, radiusY);
		g.lineTo(rocket.r.x + _matrix.outX, rocket.r.y + _matrix.outY);
		_matrix.transform(-radiusX, radiusY);
		g.lineTo(rocket.r.x + _matrix.outX, rocket.r.y + _matrix.outY);
		_matrix.transform(-radiusX, -radiusY);
		g.lineTo(rocket.r.x + _matrix.outX, rocket.r.y + _matrix.outY);

		rocket.updateCollisions(all, 1, g);
	}
}
