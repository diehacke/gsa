package game;
import model.PlanetStatus;

class PlanetInfo {
	public var name:String;
	public var centerX:Float;
	public var centerY:Float;
	public var size:Float;
	public var radius:Float;
	public var atmH:Float;
	public var hasDark:Bool;
	public var status:PlanetStatus;

	public function new(name:String) {
		this.name = name;
		status = new PlanetStatus();
	}

	public function setSprite(centerX:Float, centerY:Float, size:Float):PlanetInfo {
		this.centerX = centerX;
		this.centerY = centerY;
		this.size = size;
		return this;
	}

	public function setRadius(radius:Float):PlanetInfo {
		this.radius = radius;
		return this;
	}

	public function setAtmH(atmH:Float):PlanetInfo {
		this.atmH = atmH;
		return this;
	}

	public function setHasDark(hasDark:Bool):PlanetInfo {
		this.hasDark = hasDark;
		return this;
	}
}
