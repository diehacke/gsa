package game;

import flash.Vector;
import utils.Vertex;
import utils.MathUtil;
import haxe.ds.ObjectMap;

class WorldSystem {
	public function new() {
	}

	public var star:Planet;
	public var glory:Planet;
	public var rocket:Rocket;

	private var _units:Array<Unit> = new Array<Unit>();
	private var _unitSet:Map<Unit, Bool> = new Map<Unit, Bool>();

	public var units(get, never):Iterable<Unit>;
	private function get_units():Iterable<Unit> {
		return _units;
	}

	public function addUnit(unit:Unit):Void {
		if (!_unitSet.exists(unit)) {
			_units.push(unit);
			_unitSet.set(unit, true);
		}
	}

	public function removeUnit(unit:Unit):Void {
		if (_unitSet.exists(unit)) {
			_unitSet.remove(unit);
			_units.remove(unit);
		}
	}

	public function addRing(planet:Unit, r:Float, width:Float, minRadius:Float, maxRadius:Float, count:Int,
		alpha:Float, betta:Float, direct:Bool, color:Int):Void {
		for (i in 0 ... count) {
			var unit = new Asteroid();
			var angle = 2 * Math.PI * (i + Math.random()) / count;
			setSatellite(planet, unit, r + width / 2 - width * Math.random() , angle, direct);
			unit.radius = minRadius + (maxRadius - minRadius) * Math.random();
			unit.hasM = false;
			unit.color = color;
		}
	}

	public function setSatellite(planet:Unit, satellite:Unit, r:Float, angle:Float, direct:Bool):Void {
		satellite.r.x = r * Math.cos(angle);
		satellite.r.y = r * Math.sin(angle);
		
		var v = MathUtil.sqrt(World.G * planet.m / r);
		satellite.v.x = (direct ? -1 : 1) * v * Math.sin(angle);
		satellite.v.y = (direct ? 1 : -1) * v * Math.cos(angle);

		satellite.r.plus(planet.r);
		satellite.v.plus(planet.v);

		addUnit(satellite);
	}

	public function setSatelliteByPeriod(planet:Unit, satellite:Unit, period:Float, angle:Float, direct:Bool):Void {
		var r = Math.pow(period * period * World.G * planet.m / (4 * Math.PI * Math.PI), 1 / 3);
		satellite.r.x = r * Math.cos(angle);
		satellite.r.y = r * Math.sin(angle);
		
		var v = MathUtil.sqrt(World.G * planet.m / r);
		satellite.v.x = (direct ? -1 : 1) * v * Math.sin(angle);
		satellite.v.y = (direct ? 1 : -1) * v * Math.cos(angle);

		satellite.r.plus(planet.r);
		satellite.v.plus(planet.v);

		addUnit(satellite);
	}

	public function setLanded(planet:Unit, unit:Unit):Void {
		unit.v.set(planet.v);
		unit.r.setXY(planet.r.x, planet.r.y - planet.radius + .01);
		addUnit(unit);
	}
}
