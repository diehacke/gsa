package game;

import windows.Window;

enum WindowVariant {
	WINDOW(window:Window);
	ACTION(action:Void->Void);
}
