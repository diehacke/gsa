package game;

import flash.Vector;
import model.StageModel;
import game.Link;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.display.Graphics;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.ui.Keyboard;
import utils.KeyUtil;
import utils.RotationMatrix;
import utils.MathUtil;
import utils.Vertex;

class RocketStage extends Unit {
	inline public static var SCALE:Float = 400;
	inline public static var ATM_SIZE_K:Float = SCALE * .8 / 30;

	private var _mc:MovieClip;
	private var _bigMc:MovieClip;
	private var _isShip:Bool;

	public function new(model:StageModel) {
		super();
		hasM = false;
		hasSize = false;

		var info = model.info;
		var mirror = model.calculatedMirror;

		hasEngine = info.nozzles.length > 0;
		isIon = model.isIon;
		empty = !hasEngine;
		netM = info.netM;
		fuelM = info.fuelM;
		maxFuelM = info.fuelM;
		fuelRate = info.fuelRate;
		fuelV = info.fuelV;
		handleX = info.handle.x / SCALE;
		handleY = info.handle.y / SCALE;
		sizeX = info.sizeX / SCALE;
		sizeY = info.sizeY / SCALE;
		isJoint = model.isJoint;
		isCowl = model.isCowl;
		isSide = model.isSide;
		hasAntenna = model.hasAntenna;
		hasBrolly = model.hasBrolly;
		maxARatio = model.maxARatio;

		discoverRadius = model.discoverRadius;
		photoRadius = model.photoRadius;
		closePhotoRadius = model.closePhotoRadius;
		closePhotoK = model.closePhotoK;

		_isShip = model.isShip;
		if (_isShip) {
			_mc = info.newMc();
			setFrame(_mc, false, false, false);
			_mc.scaleX = _mc.scaleY = .4;
		}

		_bigMc = info.newMc();
		_bigMc.gotoAndStop(1);

		if (mirror) {
			if (_mc != null)
				_mc.scaleX *= -1;
			_bigMc.scaleX *= -1;
			handleX *= -1;
		}

		linkLeft = newLink(this, info.linkLeft, mirror);
		linkRight = newLink(this, info.linkRight, mirror);
		linkTop = newLink(this, info.linkTop, mirror);
		linkBottom = newLink(this, info.linkBottom, mirror);

		links = new Vector<Link>(4, true);
		links[0] = linkLeft;
		links[1] = linkRight;
		links[2] = linkTop;
		links[3] = linkBottom;

		nozzles = new Vector<Point>();
		for (point in info.nozzles) {
			nozzles.push(new Point(point.x / SCALE, point.y / SCALE));
		}
		nozzlesWidth = info.nozzlesWidth / SCALE;

		direction = new Vertex().setXY(1, 0);
		offsetX = 0;
		offsetY = 0;

		_deployed = false;
	}

	private function newLink(stage:RocketStage, point:Point, mirror:Bool):Link {
		var link = new Link(stage, point.x / SCALE, point.y / SCALE);
		if (mirror)
			link.x *= -1;
		return link;
	}

	inline public function hasFuel():Bool {
		return fuelM > .00001;
	}

	public function linkTo(link:Link):Void {
		unlink();
		parent = link;
		parent.child = this;
	}

	public function unlink():Void {
		if (parent != null) {
			parent.child = null;
			parent = null;
		}
	}

	inline public static function setFrame(mc:MovieClip, linked:Bool, deployed:Bool, brolly:Bool):Void {
		var numFrames = mc.totalFrames;
		if (deployed && numFrames >= 4) {
			if (brolly && numFrames >= 6) {
				if (linked) {
					mc.gotoAndStop(5);
				} else {
					mc.gotoAndStop(6);
				}
			} else {
				if (linked) {
					mc.gotoAndStop(3);
				} else {
					mc.gotoAndStop(4);
				}
			}
		} else {
			if (linked) {
				mc.gotoAndStop(1);
			} else {
				mc.gotoAndStop(2);
			}
		}
	}

	public function updateState():Void {
		if (_mc != null) {
			setFrame(_mc, false, _deployed, _brolly);
		}
		if (_bigMc != null)
			setFrame(_bigMc, linkBottom.child != null, _deployed, _brolly);
	}

	public var nozzles:Vector<Point>;
	public var nozzlesWidth:Float;

	public var linkLeft:Link;
	public var linkRight:Link;
	public var linkTop:Link;
	public var linkBottom:Link;
	public var links:Vector<Link>;

	public var parent:Link;
	public var handleX:Float;
	public var handleY:Float;

	public var offsetX:Float;
	public var offsetY:Float;
	public var direction:Vertex;

	public var hasEngine:Bool;
	public var isIon:Bool;
	public var empty:Bool;
	public var netM:Float;
	public var fuelM:Float;
	public var maxFuelM:Float;
	public var fuelRate:Float;
	public var fuelV:Float;
	
	public var isJoint:Bool;
	public var isCowl:Bool;
	public var isSide:Bool;
	public var hasAntenna:Bool;
	public var hasBrolly:Bool;
	public var needCowlRemove:Bool;
	public var needUnlink:Bool;
	public var maxARatio:Float;
	public var sizeX:Float;
	public var sizeY:Float;

	private var _deployed:Bool;
	public var deployed(get, set):Bool;
	private function get_deployed():Bool {
		return _deployed;
	}
	private function set_deployed(value:Bool):Bool {
		if (_deployed != value) {
			_deployed = value;
			updateState();
		}
		return _deployed;
	}

	private var _brolly:Bool;
	public var brolly(get, set):Bool;
	private function get_brolly():Bool {
		return _brolly;
	}
	private function set_brolly(value:Bool):Bool {
		if (_brolly != value) {
			_brolly = value;
			updateState();
		}
		return _brolly;
	}

	override public function render(camera:Camera):Void {
		if (_mc != null) {
			camera.screenXY(r.x, r.y);
			var x = camera.outSX;
			var y = camera.outSY;

			if (_mc.parent != camera.container)
				camera.container.addChild(_mc);

			_mc.rotation = MathUtil.getRotation(direction.x, direction.y);
			_mc.x = x;
			_mc.y = y;
		}
	}

	private var _exploded:Bool;
	private var _explodedX:Float;
	private var _explodedY:Float;
	private var _explodedPlanet:Unit;
	private var _explosion:MovieClip;

	override public function renderBig(camera:BigCamera):Void {
		if (_bigMc != null) {
			camera.screenXY(r.x, r.y);
			var x = camera.outSX;
			var y = camera.outSY;

			if (_bigMc.parent != camera.container)
				camera.container.addChild(_bigMc);

			_bigMc.rotation = MathUtil.getRotation(direction.x, direction.y);
			_bigMc.x = x;
			_bigMc.y = y;
		}
		if (_exploded && _explosion == null) {
			_explosion = Assets.newMCMushroomExplosion();
			camera.container.addChild(_explosion);
			_explosion.rotation = MathUtil.getRotation(_explodedX, _explodedY);
			if (camera.player != null) {
				camera.screenXY(r.x, r.y);
				var tolerance = 200;
				if (camera.outSX > -tolerance && camera.outSX < camera.halfSW * 2 + tolerance &&
					camera.outSY > -tolerance && camera.outSY < camera.halfSH * 2 + tolerance)
					camera.player.playSound(Assets.Explosio_Adrian_G_7936_hifi_mp3);
			}
			destroy();
		}
		if (_exploded) {
			camera.screenXY(_explodedPlanet.r.x + _explodedX, _explodedPlanet.r.y + _explodedY);
			_explosion.x = camera.outSX;
			_explosion.y = camera.outSY;
			if (_explosion.currentFrame > 30) {
				_explosion.gotoAndStop(30);
				_explosion.parent.addChildAt(_explosion, 0);
			}
		}
	}

	public function updateEngineA(massive:Vector<Unit>, dt:Float):Void {
		engineA.setXY(0, 0);

		var kl = sizeX * RocketStage.ATM_SIZE_K;
		var kr = sizeY * RocketStage.ATM_SIZE_K;
		updateAtmA(kl, kr, direction, massive, dt);
	}

	public var collisionV:Vertex = new Vertex().setXY(0, 0);

	private static var _vv:Vertex = new Vertex();
	private static var _rr:Vertex = new Vertex();

	public function updateCollisions(all:Vector<Unit>, dt:Float, g:Graphics):Void {
		collisionV.x = 0;
		collisionV.y = 0;
		for (planet in all) {
			if (planet == this || !planet.hasSize)
				continue;
			var rr = planet.radius + 10;
			var xx = r.x - planet.r.x;
			var yy = r.y - planet.r.y;
			if (xx * xx + yy * yy > rr * rr)
				continue;
			_rr.set(r).minus(planet.r);
			_vv.set(v).minus(planet.v).mult(dt);
			var vx = v.x - planet.v.x;
			var vy = v.y - planet.v.y;
			var criterion = planet.radius + _vv.length();
			if (_rr.length2() < criterion * criterion) {
				processCollision(_rr, _vv, planet, dt, g);
				if (_outHasCollision) {
					var explosionV = 20.;
					if (!_exploded && vx * vx + vy * vy > explosionV * explosionV) {
						_exploded = true;
						_explodedX = _outCollisionX;
						_explodedY = _outCollisionY;
						_explodedPlanet = _outPlanet;
					}
				}
				break;
			}
		}
	}

	private static var _outHasCollision:Bool;
	private static var _outPlanet:Unit;
	private static var _outCollisionX:Float;
	private static var _outCollisionY:Float;

	private function processCollision(rr:Vertex, vv:Vertex, planet:Unit, dt:Float, g:Graphics):Void {
		_outHasCollision = false;
		var xx = rr.x + vv.x;
		var yy = rr.y + vv.y;
		var vx = v.x;
		var vy = v.y;
		var length2 = xx * xx + yy * yy;
		if (length2 < planet.radius * planet.radius) {
			var length = Math.sqrt(length2);
			var delta = planet.radius - length;
			var dx = xx / length;
			var dy = yy / length;
			vx += delta * dx / dt;
			vy += delta * dy / dt;
			var nx = dy;
			var ny = -dx;
			var vv = vv.x * nx + vv.y * ny;
			vx -= nx * vv / dt;
			vy -= ny * vv / dt;

			v.x = planet.v.x;
			v.y = planet.v.y;
			collisionV.x = vx - v.x;
			collisionV.y = vy - v.y;

			_outHasCollision = true;
			_outPlanet = planet;
			_outCollisionX = dx * planet.radius;
			_outCollisionY = dy * planet.radius;
		}
	}

	public function destroy():Void {
		if (_mc != null && _mc.parent != null)
			_mc.parent.removeChild(_mc);
		_mc = null;
		if (_bigMc != null && _bigMc.parent != null)
			_bigMc.parent.removeChild(_bigMc);
		_bigMc = null;
	}

	public var discoverRadius:Float;
	public var discoverTime:Float;

	public var photoRadius:Float;
	public var photoTime:Float;

	public var closePhotoRadius:Float;
	public var closePhotoK:Float;
	public var closePhotoTime:Float;
}
