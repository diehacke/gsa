package game;

import flash.display.Sprite;
import utils.Tween;
import flash.display.BitmapData;
import flash.display.Graphics;

class Camera {
	public function new() {
	}

	public var scale:Float = 1;
	public var x:Float = 0;
	public var y:Float = 0;
	public var halfW:Float = 0;
	public var halfH:Float = 0;
	public var bottomContainer:Sprite;
	public var container:Sprite;
	public var topContainer:Sprite;
	public var bottomGraphics:Graphics;
	public var graphics:Graphics;
	public var topGraphics:Graphics;
	public var halfSW:Float = 0;
	public var halfSH:Float = 0;
	public var sunX:Float = 0;
	public var sunY:Float = 0;
	public var accelerationIndex:Float = 1;

	public function updateHalfSize():Void {
		halfW = halfSW / scale;
		halfH = halfSH / scale;
	}

	public function updateGraphics():Void {
		bottomGraphics = bottomContainer.graphics;
		graphics = container.graphics;
		topGraphics = topContainer.graphics;
	}

	public var outSX:Float;
	public var outSY:Float;

	inline public function screenXY(x:Float, y:Float):Void {
		outSX = halfSW + (x - this.x) * scale;
		outSY = halfSH + (y - this.y) * scale;
	}

	inline public function inArea(x:Float, y:Float, radius:Float):Bool {
		var dx = x - this.x;
		var dy = y - this.y;
		return dx + radius > -halfW &&
			dx - radius < halfW &&
			dy + radius > -halfH &&
			dy - radius < halfH;
	}

	private var _scaleIndex:Int = 1;
	private var _settedScale:Float;

	public function setScaleAndRelax(value:Float, maxScale:Float):Void {
		_settedScale = value;
		onTweenUpdate();
		var scaleIndex = getScaleIndex(value);
		if (scaleIndex == 0) {
			_scaleIndex = 0;
			scale = value;
			Tween.killByKey(this);
		} else if (_scaleIndex != scaleIndex) {
			_scaleIndex = scaleIndex;
			var targetScale = Math.min(maxScale, 1 / _scaleIndex);
			Tween.to(this, 300, {scale:targetScale}).setEase(Tween.easeInOut).setVoidOnUpdate(onTweenUpdate);
		}
	}

	public function setScale(value:Float):Void {
		_settedScale = value;
		onTweenUpdate();
		scale = value;
		_scaleIndex = -1;
		Tween.killByKey(this);
	}

	private function getScaleIndex(value:Float):Int {
		var scaleIndex = 1;
		if (value > .99) {
			scaleIndex = 1;
		} else if (value > 1 / 2) {
			scaleIndex = 2;
		} else if (value > 1 / 4) {
			scaleIndex = 4;
		} else if (value > 1 / 8) {
			scaleIndex = 8;
		} else if (value > 1 / 16) {
			scaleIndex = 16;
		} else if (value > 1 / 32) {
			scaleIndex = 32;
		} else if (value > 1 / 64) {
			scaleIndex = 64;
		} else if (value > 1 / 128) {
			scaleIndex = 128;
		} else {
			scaleIndex = 0;
		}
		return scaleIndex;
	}

	private function onTweenUpdate():Void {
		if (scale > _settedScale)
			scale = _settedScale;
	}
}
