﻿package game;

import screens.Screen;
import utils.ListUtil;
import model.StageModelStack;
import ui.Widget;
import utils.Tween;
import windows.Window;
import windows.MissionWindow;
import windows.PlanetWindow;
import elm.utils.Rand;
import flash.geom.Matrix;
import model.StageModel;
import model.StagesCounter;
import screens.MissionsScreen;
import screens.PlanetsScreen;
import flash.events.MouseEvent;
import flash.geom.ColorTransform;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.display.Sprite;
import flash.text.TextField;
import model.Mission;
import ui.Menu;
import ui.SmartFormat;
import ui.HintManager;
import ui.DigitsPanel;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.ui.Keyboard;
import tutorial.Tutorial;
import utils.KeyUtil;
import utils.StringUtil;

class Game extends Screen {
	private var _mission:Mission;
	private var _rocket:StageModel;
	private var _startTutorial:Tutorial;

	public function new(mission:Mission, rocket:StageModel, frameRate:Float, startTutorial:Tutorial) {
		super();
		_mission = mission;
		_rocket = rocket;
		_startTutorial = startTutorial;

		_camera = new Camera();
		_camera.scale = 1;
		_camera.x = 0;
		_camera.y = 0;

		_bigCamera = new BigCamera();
		_bigCamera.scale = RocketStage.SCALE;
		_bigCamera.x = 0;
		_bigCamera.y = 0;

		var rocket = new Rocket(_rocket.newRootRocketStage());
		var worldSystem = _mission.newWorld(rocket);
		worldSystem.rocket = rocket;
		_world = new World(frameRate, _camera, _bigCamera, worldSystem);
	}

	private var _container:Widget;
	private var _bg:Bitmap;
	private var _keyBitmap:Bitmap;
	private var _world:World;
	private var _camera:Camera;
	private var _bigCamera:BigCamera;
	private var _bigCameraBitmap:Bitmap;
	private var _parametersPanel:ParametersPanel;
	private var _timePanel:TimePanel;
	private var _characterPanel:CharacterPanel;
	private var _hintManager:HintManager;
	private var _stagesCounter:StagesCounter;
	private var _lastChangedPlanet:Planet;
	
	override public function doOn():Void {
		_container = new Widget();
		addChild(_container);

		_bg = new Bitmap(Assets.stars_jpg);
		_container.addChild(_bg);

		_camera.bottomContainer = new Sprite();
		_camera.container = new Sprite();
		_camera.topContainer = new Sprite();
		_camera.updateGraphics();
		_container.addChild(_camera.bottomContainer);
		_container.addChild(_camera.container);
		_container.addChild(_camera.topContainer);

		_bigCamera.bottomContainer = new Sprite();
		_bigCamera.container = new Sprite();
		_bigCamera.topContainer = new Sprite();
		_bigCamera.bd = new BitmapData(200, 540, false);
		_bigCameraBitmap = new Bitmap(_bigCamera.bd);
		_bigCamera.halfSW = _bigCamera.bd.width / 2;
		_bigCamera.halfSH = _bigCamera.bd.height / 2;
		_bigCamera.updateHalfSize();
		_bigCamera.updateGraphics();
		_bigCamera.player = _player;
		_container.addChild(_bigCameraBitmap);

		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
		_player.stopMusic();

		_parametersPanel = new ParametersPanel();
		_parametersPanel.w = _bigCameraBitmap.width;
		_parametersPanel.h = ParametersPanel.OFFSET_Y + _bigCameraBitmap.height;
		_parametersPanel.redraw();
		_container.addChild(_parametersPanel);

		_timePanel = new TimePanel();
		_timePanel.redraw();
		_container.addChild(_timePanel);

		_keyBitmap = new Bitmap();
		_container.addChild(_keyBitmap);

		_hintManager = new HintManager(_container);

		if (_startTutorial != null) {
			_characterPanel = new CharacterPanel(_player);
			_characterPanel.redraw();
			_container.addChild(_characterPanel);

			setTutorialState(_startTutorial);
		}

		_windows = new Array<WindowVariant>();

		_stagesCounter = new StagesCounter().start();

		_world.discovered.add(onPlanetDiscovered);
		_world.photo.add(onPlanetPhoto);
		_world.closePhotoStart.add(onPlanetClosePhotoStart);
		_world.closePhoto.add(onPlanetClosePhoto);
		_world.landingPhoto.add(onLandingPhoto);

		_player.setMusic(Music.ORBIT);

		if (_tutorial == null) {
			showStartHints();
		}
	}

	override public function doOff():Void {
		_world.discovered.remove(onPlanetDiscovered);
		_world.photo.remove(onPlanetPhoto);
		_world.closePhotoStart.remove(onPlanetClosePhotoStart);
		_world.closePhoto.remove(onPlanetClosePhoto);
		_world.landingPhoto.remove(onLandingPhoto);

		removeChild(_container);

		_player.engines(false);
		_player.stopMusic();
	}

	override public function doOnResize():Void {
		updateKeyBitmap();
		_container.w = _w;
		_container.h = _h;
		_bigCameraBitmap.x = Std.int(_w - _bigCameraBitmap.width);
		_bigCameraBitmap.y = ParametersPanel.OFFSET_Y;
		_parametersPanel.x = _w - _parametersPanel.w;
		_timePanel.x = Std.int(_w * .5 - _timePanel.width * .5);
		if (_characterPanel != null) {
			_characterPanel.x = Std.int(_w - _parametersPanel.w - _characterPanel.w - 20);
			_characterPanel.y = 0;
		}
	}

	private function updateKeyBitmap():Void {
		if (_menuOpened) {
			_keyBitmap.visible = false;
		} else {
			_keyBitmap.visible = true;
			if (_openedWindow != null) {
				_keyBitmap.bitmapData = _openedWindow.keyBd;
			} else if (_world.radioAccessible) {
				_keyBitmap.bitmapData = Assets.key_game_png;
			} else {
				_keyBitmap.bitmapData = Assets.key_game_no_signal_png;
			}
			_keyBitmap.x = Std.int(_w / 2 - _keyBitmap.width / 2);
			_keyBitmap.y = Std.int(_h - _keyBitmap.height - 2);
		}
		if (_characterPanel != null)
			_characterPanel.showText = !_menuOpened && _openedWindow == null;
	}

	override public function doOnKeyDown(keyCode:UInt):Void {
		if (_menuOpened && _menu != null) {
			_menu.doOnKeyDown(keyCode);
			return;
		}
		if (_openedWindow != null) {
			_openedWindow.doOnKeyDown(keyCode);
			return;
		}
		if (keyCode == Keyboard.ESCAPE ||
			keyCode == Keyboard.SPACE ||
			keyCode == Keyboard.ENTER ||
			keyCode == KeyUtil.LEFT_BRACKET && KeyUtil.isDown(Keyboard.CONTROL))
			showMenu();
		if (keyCode == KeyUtil.A || keyCode == KeyUtil.Aru) {
			_world.accelerationIndex = 0;
		}
		if (keyCode == KeyUtil.D || keyCode == KeyUtil.Dru) {
			if (_world.accelerationIndex < 4)
				_world.accelerationIndex++;
		}
		if (keyCode == KeyUtil.E || keyCode == KeyUtil.Eru) {
			_world.deployRocket();
		}
		if (keyCode == KeyUtil.R || keyCode == KeyUtil.Rru) {
			showMenu(true);
		}
		if (keyCode == Keyboard.DOWN) {
			_world.unlinkRocketStages();
		}
	}
	
	private var _menu:Menu;
	private var _menuOpened:Bool;

	private function /**/showMenu(selectRestart:Bool = false):Void {
		if (!_menuOpened) {
			_menuOpened = true;
			if (_menu == null) {
				_menu = new Menu(_player, true);
				_menu.escape.addVoid(onMenuEscape);
				var about = _menu.addButton("About mission");
				var planets = _menu.addButton("Known planets");
				var restart = _menu.addButton("Restart mission");
				var exit = _menu.addButton("Exit mission");
				var close = _menu.addButton("Close menu");
				about.addEventListener(MouseEvent.CLICK, onAboutMissionClick);
				planets.addEventListener(MouseEvent.CLICK, onPlanetsClick);
				restart.addEventListener(MouseEvent.CLICK, onRestartMissionClick);
				exit.addEventListener(MouseEvent.CLICK, onExitMissionClick);
				close.addEventListener(MouseEvent.CLICK, onCloseMenuClick);
				if (selectRestart) {
					_menu.setSelected(restart, true);
				} else if (_tutorial != null) {
					_menu.setSelected(_tutorial.getCompleted() ? exit : restart, true);
				} else {
					var rocket = _world.getRocket();
					var needRestart = rocket.getExploded() ||
						!rocket.ship.hasFuel() &&
						rocket.ship.linkBottom.child == null &&
						rocket.ship.linkLeft.child == null &&
						rocket.ship.linkRight.child == null &&
						rocket.ship.linkTop.child == null;
					_menu.setSelected(needRestart ? restart : exit, true);
				}
				_menu.onCloseComplete = onCloseMenuComplete;
				_menu.x = _w / 2;
				_menu.y = _h;
				_menu.redraw();
				_container.addChild(_menu);
			}
			_menu.open(false);
			updateKeyBitmap();
			_player.setMusic(Music.MENU);
		}
	}

	private function /**/closeMenu():Void {
		if (_menuOpened) {
			_menuOpened = false;
			if (_menu != null) {
				_menu.close();
			}
			updateKeyBitmap();
			_player.setMusic(Music.ORBIT);
		}
	}

	private var _windows:Array<WindowVariant>;
	private var _openedWindow:Window;

	private function /**/openWindow(window:Window):Void {
		_windows.push(WindowVariant.WINDOW(window));
		if (_openedWindow == null)
			openNextWindow();
	}

	private function /**/openAction(action:Void->Void):Void {
		_windows.push(WindowVariant.ACTION(action));
	}

	private function /**/openNextWindow():Void {
		var windowVariant = _windows.shift();
		if (windowVariant == null) {
			_openedWindow = null;
			return;
		}
		switch (windowVariant) {
			case WindowVariant.WINDOW(window):
				_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
				_openedWindow = window;
				updateKeyBitmap();

				window.redraw();
				window.x = Std.int(_w * .5 - window.w * .5);
				window.y = Std.int(_h * .5 - window.h * .5);
				window.closeClick.add(onWindowCloseClick);
				var vars = {
					w:window.fullW,
					h:window.fullH,
					x:Std.int(_w * .5 - window.fullW * .5),
					y:Std.int(_h * .5 - window.fullH * .5),
				};
				Tween.to(window, 200, vars).setEase(Tween.easeOut)
					.setVoidOnUpdate(window.redraw);
				_container.addChild(window);
			case WindowVariant.ACTION(action):
				if (action != null)
					action();
				openNextWindow();
		}
	}

	private function /**/onWindowCloseClick(window:Window):Void {
		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
		_openedWindow = null;
		window.closeClick.remove(onWindowCloseClick);
		var vars = {
			w:.0,
			h:.0,
			x:Std.int(_w * .5),
			y:Std.int(_h * .5),
		};
		Tween.to(window, 200, vars).setEase(Tween.easeIn)
			.setVoidOnUpdate(window.redraw).setOnComplete(onWindowCloseComplete);
		updateKeyBitmap();
		openNextWindow();
	}

	private function onWindowCloseComplete(tween:Tween<Window>):Void {
		_container.removeChild(tween.target);
	}

	private function /**/onMenuEscape():Void {
		closeMenu();
		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
	}

	private function /**/onCloseMenuComplete():Void {
		_container.removeChild(_menu);
		_menu = null;
	}

	private function /**/onExitMissionClick(event:MouseEvent):Void {
		closeMenu();
		openMissionsScreen();
	}

	private function /**/onRestartMissionClick(event:MouseEvent):Void {
		closeMenu();
		_main.setScreen(new Game(_mission, _mission.rocketModel != null ? _mission.rocketModel : _mission.newDefaultRocketModel(), stage.frameRate,
			_mission.tutorial));
	}

	private function /**/onPlanetsClick(event:MouseEvent):Void {
		closeMenu();
		_main.setScreen(new PlanetsScreen(_world.getTargetStatus(), this));
	}

	private function /**/onAboutMissionClick(event:MouseEvent):Void {
		closeMenu();
		openWindow(new MissionWindow(_mission, "About mission", null));
	}

	private function /**/onCloseMenuClick(event:MouseEvent):Void {
		closeMenu();
		_player.playSound(Assets.clunk1_Intermed_601_hifi_wav);
	}

	private var _prevAccessibleFirst:Bool = true;
	private var _prevAccessible:Bool = true;
	private var _starsJPG:BitmapData = Assets.stars_jpg;
	private var _noiseIndex:Int;
	private var _noiseMatrix:Matrix = new Matrix();

	override public function doOnEnterFrame():Void {
		_hintManager.doOnEnterFrame();
		startHintsEnterFrame();
		if (_menuOpened || _openedWindow != null)
			return;

		_camera.halfSW = _w * .5 - _bigCamera.halfSW;
		_camera.halfSH = _h * .5;
		_camera.updateHalfSize();
		_camera.bottomGraphics.clear();
		_camera.graphics.clear();
		_camera.topGraphics.clear();

		_bigCamera.bottomGraphics.clear();
		_bigCamera.graphics.clear();
		_bigCamera.topGraphics.clear();
		if (_world.radioAccessible) {
			_bigCamera.bd.copyPixels(_starsJPG, _bigCamera.bd.rect, new Point(0, 0));
		} else {
			if (_noiseIndex == 0) {
				_noiseIndex = 1;
				_noiseMatrix.a = 1;
				_noiseMatrix.tx = Math.random() * 300;
				_noiseMatrix.ty = Math.random() * 300;
				_noiseMatrix.d = 1;
			} else {
				_noiseIndex = 0;
			}
			var bd = Assets.noise_png;
			var w = _bigCamera.bd.width;
			var h = _bigCamera.bd.height;
			_bigCamera.bottomGraphics.beginBitmapFill(bd, _noiseMatrix, true, false);
			_bigCamera.bottomGraphics.drawRect(0, 0, w, h);
			_bigCamera.bottomGraphics.endFill();
			_bigCamera.bd.draw(_bigCamera.bottomContainer);
		}

		_world.update();
		_world.render();

		if (_prevAccessible != _world.radioAccessible || _prevAccessibleFirst) {
			_prevAccessible = _world.radioAccessible;
			_prevAccessibleFirst = false;

			updateKeyBitmap();
			_parametersPanel.setNoSignal(!_world.radioAccessible);
		}
		if (_world.radioAccessible) {
			_bigCamera.bd.draw(_bigCamera.bottomContainer);
			_bigCamera.bd.draw(_bigCamera.container);
			_bigCamera.bd.draw(_bigCamera.topContainer);
		}

		_parametersPanel.update(_world, _bigCamera);
		_timePanel.setParameters(_world.getTimeInCycles(), _world.accelerationIndex);
		if (_characterPanel != null) {
			_characterPanel.doOnEnterFrame();
		}
		if (_tutorial != null) {
			_tutorial.doBeforeEnterFrame();
			_tutorial.doOnEnterFrame();
			_tutorial.render(_camera);
		} else {
			var farDistance = 50000.;
			if (_world.getTargetUnit() == _world.getStar() &&
				_world.getRocketR().length2() > farDistance * farDistance) {
				if (_soFarTimer == 0) {
					_soFarTimer = 500;
					showHint("It's too far to find something");
				}
				_soFarTimer--;
			} else {
				_soFarTimer = 0;
			}
		}
		_player.engines(_world.getRocket().getEnginesEnabled());
	}

	private var _soFarTimer:Int;

	public function setTutorialOrientation(dx:Float, dy:Float):Void {
		_parametersPanel.tutorialOrientationX = dx;
		_parametersPanel.tutorialOrientationY = dy;
	}

	private var _tutorial:Tutorial;

	public function setTutorialState(tutorial:Tutorial):Void {
		if (_tutorial != null) {
			_tutorial.doOff();
		}
		_tutorial = tutorial;
		if (_tutorial != null) {
			_tutorial.init(this, _world, _characterPanel, _mission);
			_tutorial.doOn();
		}
	}

	private function onPlanetDiscovered(planet:Planet):Void {
		openWindow(new PlanetWindow(planet, "Discovered", null));
		_lastChangedPlanet = planet;
		_main.save();
	}

	private function onPlanetPhoto(planet:Planet):Void {
		_main.home.updateStagesCount();
		openWindow(new PlanetWindow(planet, "Photo obtained", _stagesCounter.stop()));
		_lastChangedPlanet = planet;
		_main.save();
	}

	private function onPlanetClosePhotoStart(planet:Planet):Void {
		showHint("Sending HD photo of " + planet.name);
	}

	private function onPlanetClosePhoto(planet:Planet):Void {
		_main.home.updateStagesCount();
		openWindow(new PlanetWindow(planet, "HD photo obtained", _stagesCounter.stop()));
		_lastChangedPlanet = planet;
		for (mission in _main.home.missions) {
			if (!mission.opened &&
				mission.planetForOpen != null &&
				mission.planetForOpen.status == planet.status) {
				mission.opened = true;
				openWindow(new MissionWindow(mission, "Mission unlocked", null));
			}
		}
		_main.save();
	}

	private function onLandingPhoto(planet:Planet):Void {
		_main.home.updateStagesCount();
		openWindow(new PlanetWindow(planet, "First landing!", _stagesCounter.stop()));
		_lastChangedPlanet = planet;
		_main.save();
	}

	public function processMissionComplete():Void {
		if (_mission.completed)
			return;
		var openedMissions = new Array<Mission>();
		_mission.completed = true;
		for (mission in _mission.openOnCompleteMissions) {
			if (!mission.opened) {
				mission.opened = true;
				openedMissions.push(mission);
			}
		}
		_main.home.updateStagesCount();
		var index = 0;
		var missionToSelected = ListUtil.select(_main.home.missions, value.opened,
			100 * (value.title != null && value.title != "" ? 1 : 0) +
			1000 * (!value.completed ? 1 : 0) +
			index--
		);
		if (missionToSelected != null)
			_main.home.selectedMission = missionToSelected;
		_main.save();
		openWindow(new MissionWindow(_mission, "Mission completed", _stagesCounter.stop()));
		for (mission in openedMissions) {
			openWindow(new MissionWindow(mission, "Mission unlocked", null));
		}
		openAction(openMissionsScreen);
	}

	private function openMissionsScreen():Void {
		if (_lastChangedPlanet != null) {
			_main.setScreen(new PlanetsScreen(_lastChangedPlanet.status, null));
		} else {
			_main.setScreen(new MissionsScreen());
		}
	}

	public function showHint(text:String):Void {
		_hintManager.show(text);
	}

	public function showTimer(text:String):Void {
		var panel = new DigitsPanel();
		panel.text = text;
		panel.x = _w * .5;
		panel.y = _h * .5;
		panel.scaleX = 0;
		_container.addChild(panel);
		var scale = 3;
		var vars = {
			x:_w * .5 - panel.w * .5 * scale,
			y:_h * .5 - panel.h * .5 * scale,
			scaleX:scale,
			scaleY:scale,
			alpha:0
		};
		Tween.to(panel, 300, vars).setOnComplete(onTimerTweenComplete);

		_player.playSound(Assets.spacey_alarm_wav);
	}

	private function onTimerTweenComplete(tween:Tween<DigitsPanel>):Void {
		if (tween.target.parent != null)
			tween.target.parent.removeChild(tween.target);
	}

	private var _startHints:Array<String>;
	private var _startHintsFrame:Int;
	
	private function startHintsEnterFrame():Void {
		if (_startHints != null) {
			if (_startHintsFrame-- == 0) {
				_startHintsFrame = 60;
				if (_startHints.length > 0) {
					var text = _startHints.shift();
					if (text != null && text != "")
						showHint(text);
				} else {
					_startHints = null;
				}
			}
		}
	}

	private function showStartHints():Void {
		var total = 0;
		var discovered = 0;
		var hasPhoto = 0;
		var hasClosePhoto = 0;
		var hasLandingPhoto = 0;
		for (info in PlanetInfos.all) {
			if (info == PlanetInfos.arial ||
				info == PlanetInfos.glory ||
				info == PlanetInfos.loma ||
				info == PlanetInfos.asteroid)
				continue;
			total++;
			if (info.status.discovered)
				discovered++;
			if (info.status.hasPhoto)
				hasPhoto++;
			if (info.status.hasClosePhoto)
				hasClosePhoto++;
			if (info.status.hasLandingPhoto)
				hasLandingPhoto++;
		}
		var hasUnexplored = false;
		var hasUnlanded = false;
		if (discovered < total) {
			showHintDelayed((total - discovered) + " planets isn't discovered");
			hasUnexplored = true;
		}
		if (hasPhoto < discovered) {
			showHintDelayed((discovered - hasPhoto) + " known planets isn't explored");
			hasUnexplored = true;
		}
		if (hasClosePhoto < hasPhoto) {
			showHintDelayed((hasPhoto - hasClosePhoto) + " explored planets hasn't HD photo");
			hasUnexplored = true;
		}
		if (hasLandingPhoto < hasPhoto) {
			showHintDelayed("We never landed on " + (hasPhoto - hasLandingPhoto) + " explored planets");
			hasUnlanded = true;
		}
		if (hasUnexplored) {
			showHintDelayed(null);
			showHintDelayed("Explore it all!");
		} else if (hasUnlanded) {
			showHintDelayed(null);
			showHintDelayed("Visit it!");
		} else {
			showHintDelayed("All planets are explored");
			showHintDelayed(null);
			showHintDelayed("Just travel!");
		}
	}

	private function showHintDelayed(text:String):Void {
		if (_startHints == null)
			_startHints = new Array<String>();
		_startHints.push(text);
	}

}
