// <<Generated>>
package game;

import flash.geom.Point;
class Stages {
	public static var shipexplorer:StageInfo = {
		var info = new StageInfo(Assets.newMCShipExplorer);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 27));
		info.nozzlesWidth = 10;
		info.handle = new Point(0, 0);
		info.linkLeft = new Point(0, 18);
		info.linkRight = new Point(0, 18);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 28);
		info.netM = .015;
		info.fuelM = .010;
		info.fuelRate = .2;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 50;
		info;
	};

	public static var stage33:StageInfo = {
		var info = new StageInfo(Assets.newMCStage33);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(-8, 33));
		info.nozzles.push(new Point(0, 33));
		info.nozzles.push(new Point(8, 33));
		info.nozzlesWidth = 14;
		info.handle = new Point(0, -35);
		info.linkLeft = new Point(-17, -18);
		info.linkRight = new Point(17, -18);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 38);
		info.netM = .012;
		info.fuelM = .100;
		info.fuelRate = 3;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 66;
		info;
	};

	public static var stage22:StageInfo = {
		var info = new StageInfo(Assets.newMCStage22);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(-8, 23));
		info.nozzles.push(new Point(8, 23));
		info.nozzlesWidth = 14;
		info.handle = new Point(0, -28);
		info.linkLeft = new Point(-17, -28);
		info.linkRight = new Point(17, -28);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 28);
		info.netM = .006;
		info.fuelM = .072;
		info.fuelRate = 2;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 46;
		info;
	};

	public static var acceleratorforced:StageInfo = {
		var info = new StageInfo(Assets.newMCAcceleratorForced);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 23));
		info.nozzlesWidth = 18;
		info.handle = new Point(7, -29);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 17);
		info.netM = .008;
		info.fuelM = .028;
		info.fuelRate = 3.5;
		info.fuelV = .9;
		info.sizeX = 12;
		info.sizeY = 30;
		info;
	};

	public static var joint12:StageInfo = {
		var info = new StageInfo(Assets.newMCJoint12);
		info.nozzles = new Array<Point>();
		info.nozzlesWidth = 10;
		info.handle = new Point(0, -8);
		info.linkLeft = new Point(-16, 8);
		info.linkRight = new Point(16, 8);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 0);
		info.netM = .002;
		info.fuelM = 0;
		info.fuelRate = 1;
		info.fuelV = 1;
		info.sizeX = 60;
		info.sizeY = 8;
		info;
	};

	public static var joint13:StageInfo = {
		var info = new StageInfo(Assets.newMCJoint13);
		info.nozzles = new Array<Point>();
		info.nozzlesWidth = 10;
		info.handle = new Point(0, -8);
		info.linkLeft = new Point(-32, 8);
		info.linkRight = new Point(32, 8);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 8);
		info.netM = .003;
		info.fuelM = 0;
		info.fuelRate = 1;
		info.fuelV = 1;
		info.sizeX = 90;
		info.sizeY = 8;
		info;
	};

	public static var stage31forced:StageInfo = {
		var info = new StageInfo(Assets.newMCStage31Forced);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 35));
		info.nozzlesWidth = 26;
		info.handle = new Point(0, -35);
		info.linkLeft = new Point(-17, -18);
		info.linkRight = new Point(17, -18);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 37);
		info.netM = .014;
		info.fuelM = .098;
		info.fuelRate = 6;
		info.fuelV = .9;
		info.sizeX = 30;
		info.sizeY = 66;
		info;
	};

	public static var accelerator:StageInfo = {
		var info = new StageInfo(Assets.newMCAccelerator);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 22));
		info.nozzlesWidth = 14;
		info.handle = new Point(7, -29);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 17);
		info.netM = .005;
		info.fuelM = .030;
		info.fuelRate = 1.5;
		info.fuelV = 1;
		info.sizeX = 12;
		info.sizeY = 30;
		info;
	};

	public static var shiptelescope:StageInfo = {
		var info = new StageInfo(Assets.newMCShipTelescope);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 34));
		info.nozzlesWidth = 10;
		info.handle = new Point(0, 0);
		info.linkLeft = new Point(0, 26);
		info.linkRight = new Point(0, 26);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 36);
		info.netM = .018;
		info.fuelM = .007;
		info.fuelRate = .2;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 60;
		info;
	};

	public static var cowlbig:StageInfo = {
		var info = new StageInfo(Assets.newMCCowlBig);
		info.nozzles = new Array<Point>();
		info.nozzlesWidth = 10;
		info.handle = new Point(8, 23);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 0);
		info.netM = .001;
		info.fuelM = 0;
		info.fuelRate = 1;
		info.fuelV = 1;
		info.sizeX = 14;
		info.sizeY = 44;
		info;
	};

	public static var shipprimary:StageInfo = {
		var info = new StageInfo(Assets.newMCShipPrimary);
		info.nozzles = new Array<Point>();
		info.nozzlesWidth = 10;
		info.handle = new Point(0, 0);
		info.linkLeft = new Point(0, 7);
		info.linkRight = new Point(0, 7);
		info.linkTop = new Point(0, 7);
		info.linkBottom = new Point(0, 7);
		info.netM = .008;
		info.fuelM = 0;
		info.fuelRate = 1;
		info.fuelV = 1;
		info.sizeX = 10;
		info.sizeY = 12;
		info;
	};

	public static var cowlprimary:StageInfo = {
		var info = new StageInfo(Assets.newMCCowlPrimary);
		info.nozzles = new Array<Point>();
		info.nozzlesWidth = 10;
		info.handle = new Point(0, 9);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 0);
		info.netM = .001;
		info.fuelM = 0;
		info.fuelRate = 1;
		info.fuelV = 1;
		info.sizeX = 14;
		info.sizeY = 14;
		info;
	};

	public static var stage23:StageInfo = {
		var info = new StageInfo(Assets.newMCStage23);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(-8, 23));
		info.nozzles.push(new Point(0, 23));
		info.nozzles.push(new Point(8, 23));
		info.nozzlesWidth = 14;
		info.handle = new Point(0, -28);
		info.linkLeft = new Point(-17, -28);
		info.linkRight = new Point(17, -28);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 28);
		info.netM = .010;
		info.fuelM = .070;
		info.fuelRate = 3;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 46;
		info;
	};

	public static var accelerationblock:StageInfo = {
		var info = new StageInfo(Assets.newMCAccelerationBlock);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 16));
		info.nozzlesWidth = 12;
		info.handle = new Point(0, -10);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 18);
		info.netM = .004;
		info.fuelM = .008;
		info.fuelRate = .8;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 18;
		info;
	};

	public static var stage21forced:StageInfo = {
		var info = new StageInfo(Assets.newMCStage21Forced);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 26));
		info.nozzlesWidth = 26;
		info.handle = new Point(0, -28);
		info.linkLeft = new Point(-17, -28);
		info.linkRight = new Point(17, -28);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 28);
		info.netM = .013;
		info.fuelM = .067;
		info.fuelRate = 6;
		info.fuelV = .9;
		info.sizeX = 30;
		info.sizeY = 50;
		info;
	};

	public static var shipexplorer2:StageInfo = {
		var info = new StageInfo(Assets.newMCShipExplorer2);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 27));
		info.nozzlesWidth = 10;
		info.handle = new Point(0, 0);
		info.linkLeft = new Point(0, 18);
		info.linkRight = new Point(0, 18);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 28);
		info.netM = .017;
		info.fuelM = .011;
		info.fuelRate = .2;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 50;
		info;
	};

	public static var sidestage153:StageInfo = {
		var info = new StageInfo(Assets.newMCSideStage153);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(-8, 23));
		info.nozzles.push(new Point(0, 23));
		info.nozzles.push(new Point(8, 23));
		info.nozzlesWidth = 14;
		info.handle = new Point(15, -28);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 28);
		info.netM = .009;
		info.fuelM = .045;
		info.fuelRate = 3;
		info.fuelV = 1;
		info.sizeX = 30;
		info.sizeY = 46;
		info;
	};

	public static var accelerators:StageInfo = {
		var info = new StageInfo(Assets.newMCAcceleratorS);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 31));
		info.nozzlesWidth = 14;
		info.handle = new Point(7, -21);
		info.linkLeft = new Point(-8, -21);
		info.linkRight = new Point(0, -21);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 25);
		info.netM = .007;
		info.fuelM = .035;
		info.fuelRate = 1.5;
		info.fuelV = 1;
		info.sizeX = 12;
		info.sizeY = 48;
		info;
	};

	public static var acceleratorsforced:StageInfo = {
		var info = new StageInfo(Assets.newMCAcceleratorSForced);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 32));
		info.nozzlesWidth = 18;
		info.handle = new Point(7, -21);
		info.linkLeft = new Point(-8, -21);
		info.linkRight = new Point(0, -21);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 25);
		info.netM = .009;
		info.fuelM = .032;
		info.fuelRate = 3.5;
		info.fuelV = .9;
		info.sizeX = 12;
		info.sizeY = 48;
		info;
	};

	public static var stage53hard:StageInfo = {
		var info = new StageInfo(Assets.newMCStage53Hard);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(-8, 35));
		info.nozzles.push(new Point(0, 35));
		info.nozzles.push(new Point(8, 35));
		info.nozzlesWidth = 14;
		info.handle = new Point(0, -35);
		info.linkLeft = new Point(-22, -18);
		info.linkRight = new Point(22, -18);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 37);
		info.netM = .020;
		info.fuelM = .114;
		info.fuelRate = 10;
		info.fuelV = .9;
		info.sizeX = 42;
		info.sizeY = 66;
		info;
	};

	public static var sidestage53hard:StageInfo = {
		var info = new StageInfo(Assets.newMCSideStage53Hard);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(-8, 35));
		info.nozzles.push(new Point(0, 35));
		info.nozzles.push(new Point(8, 35));
		info.nozzlesWidth = 14;
		info.handle = new Point(21, -18);
		info.linkLeft = new Point(-22, -18);
		info.linkRight = new Point(0, -18);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 37);
		info.netM = .020;
		info.fuelM = .110;
		info.fuelRate = 10;
		info.fuelV = .9;
		info.sizeX = 42;
		info.sizeY = 66;
		info;
	};

	public static var ionblock:StageInfo = {
		var info = new StageInfo(Assets.newMCIonBlock);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 14));
		info.nozzlesWidth = 11;
		info.handle = new Point(0, -10);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 18);
		info.netM = .004;
		info.fuelM = .008;
		info.fuelRate = .02;
		info.fuelV = 10;
		info.sizeX = 30;
		info.sizeY = 18;
		info;
	};

	public static var ionfblock:StageInfo = {
		var info = new StageInfo(Assets.newMCIonFBlock);
		info.nozzles = new Array<Point>();
		info.nozzles.push(new Point(0, 19));
		info.nozzlesWidth = 11;
		info.handle = new Point(0, -18);
		info.linkLeft = new Point(0, 0);
		info.linkRight = new Point(0, 0);
		info.linkTop = new Point(0, 0);
		info.linkBottom = new Point(0, 24);
		info.netM = .005;
		info.fuelM = .016;
		info.fuelRate = .02;
		info.fuelV = 10;
		info.sizeX = 30;
		info.sizeY = 32;
		info;
	};
}