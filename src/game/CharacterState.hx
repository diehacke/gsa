package game;

enum CharacterState {
	SILENCE;
	TALK;
	HORROR;
	FACEPALM;
}
