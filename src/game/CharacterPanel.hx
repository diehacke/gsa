package game;

import flash.display.Sprite;
import flash.display.Shape;
import flash.media.Sound;
import ui.SmartFormat;
import flash.text.TextField;
import ui.UIRect;
import ui.UIRects;
import ui.Widget;

class CharacterPanel extends Widget {
	private static var NORMAL:UIRect = UIRects.character_1;
	private static var NORMAL_TALK_0:UIRect = UIRects.character_2;
	private static var NORMAL_TALK_1:UIRect = UIRects.character_3;
	private static var FACEPALM:UIRect = UIRects.character_4;
	private static var HORROR_0:UIRect = UIRects.character_5;
	private static var HORROR_1:UIRect = UIRects.character_6;

	private var _player:Player;
	private var _format:SmartFormat;
	private var _shape:Shape;
	private var _tf:TextField;
	private var _cloud:Sprite;
	private var _frame:Int;
	private var _characterStateChanged:Bool;

	public function new(player:Player) {
		super();

		_player = player;
		_showText = true;
		_format = new SmartFormat("Tahoma", 14, 0xffffff).setBold(true);

		_shape = new Shape();
		addChild(_shape);

		_characterStateChanged = true;
		_characterState = CharacterState.SILENCE;
		redrawCharacter(NORMAL);
	}

	override public function redraw():Void {
		_w = 64;
		_h = 64;
	}

	private var _characterState:CharacterState;
	public var characterState(get, never):CharacterState;
	private function get_characterState():CharacterState {
		return _characterState;
	}

	private function setCharacterState(value:CharacterState, talkTime:Int):CharacterState {
		_talkTime = talkTime;
		if (_characterState != value) {
			_characterState = value;
			_characterStateChanged = true;
		}
		return _characterState;
	}

	public function silence(text:String):Void {
		setCharacterState(CharacterState.SILENCE, 0);
		setText(text);
	}

	private var _initializedText:String;
	private var _initializedTime:Int;

	public function talk(text:String, time:Int, sound:Sound = null):Void {
		if (_initializedText != text || _initializedTime != time || _characterState != CharacterState.TALK) {
			_initializedText = text;
			_initializedTime = time;
			setCharacterState(CharacterState.TALK, time);
			setText(text);
			if (sound != null)
				_player.playSoundSolely(sound, .8);
		}
	}

	public function horror(text:String):Void {
		if (_characterState != CharacterState.HORROR) {
			setCharacterState(CharacterState.HORROR, 15);
			_player.playSoundSolely(Assets.aah_wav, 1);
		}
		setText(text);
	}

	public function facepalm(text:String):Void {
		if (_characterState != CharacterState.FACEPALM) {
			setCharacterState(CharacterState.FACEPALM, 15);
		}
		setText(text);
	}

	private var _talkTime:Int;

	private var _text:String;
	public var text(get, never):String;
	private function get_text():String {
		return _text;
	}

	private function setText(value:String):String {
		_text = value;
		updateText();
		return _text;
	}

	private var _showText:Bool;
	public var showText(get, set):Bool;
	private function get_showText():Bool {
		return _showText;
	}
	private function set_showText(value:Bool):Bool {
		_showText = value;
		updateText();
		return _showText;
	}

	private var _realText:String;

	private function updateText():Void {
		var text = _showText ? _text : null;
		if (_realText != text) {
			_realText = text;
			if (_realText != null && _realText != "") {
				if (_cloud == null) {
					_cloud = new Sprite();
					addChild(_cloud);
				}
				if (_tf == null) {
					_tf = _format.newAutoSized();
					_tf.x = 2;
					_tf.y = 2;
					_cloud.addChild(_tf);
				}
				_tf.text = text;
				_cloud.x = 30 - _tf.width;
				_cloud.y = 80;

				var w = _tf.width + 4;
				var h = _tf.height + 4;

				var g = _cloud.graphics;
				g.clear();
				g.beginFill(0x8080dd, .9);
				g.drawRect(0, 0, w, h);
				g.moveTo(w, -10);
				g.lineTo(w, 0);
				g.lineTo(w - 10, 0);
				g.endFill();
			} else {
				if (_cloud != null) {
					removeChild(_cloud);
					_cloud = null;
				}
				if (_tf != null) {
					_tf = null;
				}
			}
		}
	}

	private function redrawCharacter(rect:UIRect):Void {
		var g = _shape.graphics;
		g.clear();
		rect.draw(g, Assets.ui_png, 32, _h);
	}

	public function doOnEnterFrame():Void {
		if (_characterStateChanged)
			_frame = 0;
		switch (_characterState) {
			case CharacterState.SILENCE:
				if (_characterStateChanged)
					redrawCharacter(NORMAL);
			case CharacterState.TALK:
				if (_talkTime > 0) {
					_talkTime--;
					if (_frame == 0) {
						redrawCharacter(NORMAL);
					} else if (_frame == 4) {
						redrawCharacter(NORMAL_TALK_0);
					} else if (_frame == 8) {
						redrawCharacter(NORMAL_TALK_1);
					} else if (_frame == 12) {
						redrawCharacter(NORMAL_TALK_0);
					}
					_frame = (_frame + 1) % 16;
				} else {
					redrawCharacter(NORMAL);
				}
			case CharacterState.FACEPALM:
				if (_characterStateChanged)
					redrawCharacter(NORMAL);
				if (_talkTime > 0) {
					_talkTime--;
					if (_talkTime == 1) {
						_player.playSoundSolely(Assets.clap_wav, 1);
					}
					if (_talkTime == 0) {
						redrawCharacter(FACEPALM);
					}
				}
			case CharacterState.HORROR:
				if (_characterStateChanged)
					redrawCharacter(HORROR_0);
				if (_talkTime > 0) {
					_talkTime--;
					if (_talkTime == 0) {
						redrawCharacter(HORROR_0);
					} else if (_frame == 0) {
						redrawCharacter(HORROR_0);
					} else if (_frame == 4) {
						redrawCharacter(HORROR_1);
					}
					_frame = (_frame + 1) % 8;
				}
		}
		_characterStateChanged = false;
	}
}
