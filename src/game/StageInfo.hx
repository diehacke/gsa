package game;

import flash.display.MovieClip;
import model.StageModel;
import flash.geom.Point;

class StageInfo {
	public var newMc:Void->MovieClip;

	public function new(newMc:Void->MovieClip) {
		this.newMc = newMc;
	}

	public var nozzles:Array<Point>;
	public var nozzlesWidth:Float;

	public var handle:Point;

	public var linkLeft:Point;
	public var linkRight:Point;
	public var linkTop:Point;
	public var linkBottom:Point;

	public var netM:Float;
	public var fuelM:Float;
	public var fuelRate:Float;
	public var fuelV:Float;

	public var sizeX:Float;
	public var sizeY:Float;

	public var model:StageModel;
}
