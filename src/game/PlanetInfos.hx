// <<Generated>>
package game;

class PlanetInfos {
	private static var _all:Array<PlanetInfo> = new Array<PlanetInfo>();
	public static var all(get, never):Iterable<PlanetInfo>;
	private static function get_all():Iterable<PlanetInfo> {
		return _all;
	}

	private static function add(name:String):PlanetInfo {
		var info = new PlanetInfo(name);
		_all.push(info);
		return info;
	}

	public static var glory:PlanetInfo = add("Glory")
		.setSprite(64, 64, 62)
		.setRadius(50)
		.setAtmH(6)
		.setHasDark(true);

	public static var loma:PlanetInfo = add("Loma")
		.setSprite(442, 26, 18)
		.setRadius(17)
		.setAtmH(0)
		.setHasDark(true);

	public static var kinnary:PlanetInfo = add("Kinnary")
		.setSprite(515, 20, 13)
		.setRadius(12)
		.setAtmH(0)
		.setHasDark(true);

	public static var arial:PlanetInfo = add("Arial")
		.setSprite(794, 229, 228)
		.setRadius(195)
		.setAtmH(28)
		.setHasDark(false);

	public static var verdana:PlanetInfo = add("Verdana")
		.setSprite(65, 192, 64)
		.setRadius(51)
		.setAtmH(10)
		.setHasDark(true);

	public static var asteroid:PlanetInfo = add("Asteroid")
		.setSprite(260, 56, 54)
		.setRadius(52)
		.setAtmH(0)
		.setHasDark(false);

	public static var tahoma:PlanetInfo = add("Tahoma")
		.setSprite(314, 253, 84)
		.setRadius(80)
		.setAtmH(0)
		.setHasDark(true);

	public static var helvetica:PlanetInfo = add("Helvetica")
		.setSprite(349, 33, 24)
		.setRadius(23)
		.setAtmH(0)
		.setHasDark(true);

	public static var sans:PlanetInfo = add("Sans")
		.setSprite(63, 321, 60)
		.setRadius(43)
		.setAtmH(6)
		.setHasDark(true);

	public static var flameTextureX:Float = 326;
	public static var flameTextureY:Float = 72;
	public static var flameTextureWidth:Float = 25;
	public static var flameTextureHeight:Float = 39;
	public static var flameFrames:Int = 9;

	public static var saab:PlanetInfo = add("Saab")
		.setSprite(518, 53, 9)
		.setRadius(8)
		.setAtmH(0)
		.setHasDark(true);

	public static var purisa:PlanetInfo = add("Purisa")
		.setSprite(324, 436, 94)
		.setRadius(86)
		.setAtmH(0)
		.setHasDark(true);

	public static var dotum:PlanetInfo = add("Dotum")
		.setSprite(63, 444, 60)
		.setRadius(43)
		.setAtmH(6)
		.setHasDark(true);

	public static var ionFlameTextureX:Float = 20;
	public static var ionFlameTextureY:Float = 600;
	public static var ionFlameTextureWidth:Float = 25;
	public static var ionFlameTextureHeight:Float = 39;
	public static var ionFlameFrames:Int = 9;
}