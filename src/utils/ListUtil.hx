package utils;

import haxe.macro.Expr;

class ListUtil {
	macro public static function select(list:Expr, filter:Expr, criterion:Expr):Expr {
		return macro {
			var maxCriterion = 0.;
			var selected = null;
			var first = true;
			for (value in ${list}) {
				if (${filter}) {
					var criterionI = ${criterion};
					if (first || maxCriterion < criterionI) {
						first = false;
						maxCriterion = criterionI;
						selected = value;
					}
				}
			}
			selected;
		};
	}

	macro public static function min(values:Array<ExprOf<Float>>):ExprOf<Float> {
		var exprs = new Array<Expr>();
		exprs.push(macro var result:Float = ${values[0]});
		for (i in 1 ... values.length) {
			exprs.push(macro if (result > ${values[i]})
				result = ${values[i]});
		}
		exprs.push(macro result);
		return macro $b{exprs};
	}

	macro public static function minInt(values:Array<ExprOf<Int>>):ExprOf<Int> {
		var exprs = new Array<Expr>();
		exprs.push(macro var result:Int = ${values[0]});
		for (i in 1 ... values.length) {
			exprs.push(macro if (result > ${values[i]})
				result = ${values[i]});
		}
		exprs.push(macro result);
		return macro $b{exprs};
	}

	macro public static function max(values:Array<ExprOf<Float>>):ExprOf<Float> {
		var exprs = new Array<Expr>();
		exprs.push(macro var result:Float = ${values[0]});
		for (i in 1 ... values.length) {
			exprs.push(macro if (result < ${values[i]})
				result = ${values[i]});
		}
		exprs.push(macro result);
		return macro $b{exprs};
	}

	macro public static function maxInt(values:Array<ExprOf<Int>>):ExprOf<Int> {
		var exprs = new Array<Expr>();
		exprs.push(macro var result:Int = ${values[0]});
		for (i in 1 ... values.length) {
			exprs.push(macro if (result < ${values[i]})
				result = ${values[i]});
		}
		exprs.push(macro result);
		return macro $b{exprs};
	}
}
