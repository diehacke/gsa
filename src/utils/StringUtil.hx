package utils;

class StringUtil {
	public static function /**/stringOfSeconds(seconds:Int):String {
		var hours = Std.int(seconds / 3600);
		return (hours > 0 ? hours + "h " : "") + string2Of(Std.int(seconds / 60)) + ":" + string2Of(seconds % 60);
	}

	public static inline function /**/string2Of(value:Int):String {
		var result = value + "";
		return result.length == 1 ? "0" + result : result;
	}

	public static inline function /**/fixed2(value:Float):String {
		var text = Math.round(value * 100) / 100 + "";
		var index = text.indexOf(".");
		if (index == -1)
			text += ".00";
		else if (index == text.length - 2)
			text += "0";
		return text;
	}

	public static inline function /**/time(value:Float):String {
		var text = Math.round(value * 10000) / 10000 + "";
		var length = text.length;
		var index = text.indexOf(".");
		if (index == -1)
			text += ":0000";
		else
			text = text.substr(0, index) + " : " + text.substr(index + 1);
		if (index == length - 4)
			text += "0";
		if (index == length - 3)
			text += "00";
		else if (index == length - 2)
			text += "000";
		return text;
	}

	public static inline function /**/intX1000(value:Float):String {
		return Math.round(value * 1000) + "";
	}
}
