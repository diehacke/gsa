package utils;

import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.display.Stage;
import flash.Lib;
import haxe.ds.IntMap;

class KeyUtil {
	private static inline var /**/INT_MAX_VALUE:Int = 0x7fffffff;

	public static inline var /**/N0:UInt = 48;
	public static inline var /**/N1:UInt = 49;
	public static inline var /**/N2:UInt = 50;
	public static inline var /**/N3:UInt = 51;
	public static inline var /**/N4:UInt = 52;
	public static inline var /**/N5:UInt = 53;
	public static inline var /**/N6:UInt = 54;
	public static inline var /**/N7:UInt = 55;
	public static inline var /**/N8:UInt = 56;
	public static inline var /**/N9:UInt = 57;
	public static inline var /**/LEFT_BRACKET:UInt = 219;
	public static inline var /**/H:UInt = 72;
	public static inline var /**/Hru:UInt = 1088;
	public static inline var /**/J:UInt = 74;
	public static inline var /**/Jru:UInt = 1086;
	public static inline var /**/K:UInt = 75;
	public static inline var /**/Kru:UInt = 1083;
	public static inline var /**/L:UInt = 76;
	public static inline var /**/Lru:UInt = 1076;
	public static inline var /**/W:UInt = 87;
	public static inline var /**/Wru:UInt = 1094;
	public static inline var /**/A:UInt = 65;
	public static inline var /**/Aru:UInt = 1092;
	public static inline var /**/S:UInt = 83;
	public static inline var /**/Sru:UInt = 1099;
	public static inline var /**/D:UInt = 68;
	public static inline var /**/Dru:UInt = 1074;
	public static inline var /**/Q:UInt = 81;
	public static inline var /**/Qru:UInt = 1081;
	public static inline var /**/E:UInt = 69;
	public static inline var /**/Eru:UInt = 1091;
	public static inline var /**/T:UInt = 84;
	public static inline var /**/Tru:UInt = 1077;
	public static inline var /**/U:UInt = 85;
	public static inline var /**/Uru:UInt = 1075;
	public static inline var /**/C:UInt = 67;
	public static inline var /**/Cru:UInt = 1089;
	public static inline var /**/I:UInt = 73;
	public static inline var /**/Iru:UInt = 1096;
	public static inline var /**/M:UInt = 77;
	public static inline var /**/Mru:UInt = 1100;
	public static inline var /**/R:UInt = 82;
	public static inline var /**/Rru:UInt = 1082;
	public static inline var /**/Y:UInt = 89;
	public static inline var /**/Yru:UInt = 1085;
	
	private static var _hash:IntMap<Bool>;
	private static var _stage:Stage;
	
	public static function init():Void {
		_hash = new IntMap();
		_stage = Lib.current.stage;
		_stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		_stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown, false, INT_MAX_VALUE);
		_stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp, false, INT_MAX_VALUE);
	}
	
	public static function /**/isDown(keyCode:Int):Bool {
		return _hash.exists(keyCode);
	}
	
	private static function /**/onKeyDown(event:KeyboardEvent):Void {
		_hash.set(event.keyCode, true);
	}
	
	private static function /**/onKeyUp(event:KeyboardEvent):Void {
		_hash.remove(event.keyCode);
	}

	private static function /**/onEnterFrame(event:Event):Void {
		if (_stage.focus != null && _stage.focus.stage == null)
			_stage.focus = null;
	}
}
