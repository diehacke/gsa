﻿package utils;

class Vertex {
	public var x:Float = 0;
	public var y:Float = 0;
	
	public function new() {
	}

	public inline function set(v:Vertex):Vertex {
		x = v.x;
		y = v.y;
		return this;
	}

	public inline function /**/setXY(x:Float, y:Float):Vertex {
		this.x = x;
		this.y = y;
		return this;
	}

	public inline function /**/plus(v:Vertex):Vertex {
		x += v.x;
		y += v.y;
		return this;
	}

	public inline function /**/minus(v:Vertex):Vertex {
		x -= v.x;
		y -= v.y;
		return this;
	}

	public inline function /**/mult(s:Float):Vertex {
		x *= s;
		y *= s;
		return this;
	}

	public inline function /**/normalize():Vertex {
		var k = MathUtil.invSqrt(x * x + y * y);
		x *= k;
		y *= k;
		return this;
	}

	public inline function length():Float {
		return MathUtil.sqrt(x * x + y * y);
	}

	public inline function length2():Float {
		return x * x + y * y;
	}
	
	public inline function distance(v:Vertex):Float {
		var dx = v.x - x;
		var dy = v.y - y;
		return MathUtil.sqrt(dx * dx + dy * dy);
	}

	public function distance2(v:Vertex):Float {
		var dx = v.x - x;
		var dy = v.y - y;
		return dx * dx + dy * dy;
	}

	public function toString():String {
		return "(" + x + ", " + y + ")";
	}
}
