package utils;

import flash.Vector;

class BoolGrid {
	private var _array:Vector<Bool>;
	private var _rowLength:Int;

	public function new(minI:Int, minJ:Int, maxI:Int, maxJ:Int) {
		_minI = minI;
		_minJ = minJ;
		_maxI = maxI;
		_maxJ = maxJ;
		_rowLength = _maxJ - _minJ + 1;
		_array = new Vector<Bool>((_maxI - _minI + 1) * (_maxJ - _minJ + 1), true);
	}

	private var _minI:Int;
	public var minI(get, never):Int;
	public function get_minI() {
		return _minI;
	}

	private var _minJ:Int;
	public var minJ(get, never):Int;
	public function get_minJ() {
		return _minJ;
	}

	private var _maxI:Int;
	public var maxI(get, never):Int;
	public function get_maxI() {
		return _maxI;
	}

	private var _maxJ:Int;
	public var maxJ(get, never):Int;
	public function get_maxJ() {
		return _maxJ;
	}

	public function get(i:Int, j:Int):Bool {
		return j >= _minJ && j <= _maxJ && i >= _minI && i <= _maxI ?
			_array[(i - minI) * _rowLength + j - _minJ] : false;
	}

	public function set(i:Int, j:Int, value:Bool):Void {
		if (j >= _minJ && j <= _maxJ && i >= _minI && i <= _maxI)
			_array[(i - minI) * _rowLength + j - _minJ] = value;
	}
}
