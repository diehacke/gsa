package ;

import utils.StringUtil;
import haxe.macro.Expr;
import haxe.macro.Expr;
import haxe.ds.StringMap;
import elm.utils.Rand;

class StartHelper {
	macro public static function check(allowedDomains:Array<String>, mainExpr:Expr):Expr {
		var mainExprs;
		switch (mainExpr.expr) {
			case EBlock(exprs):
				mainExprs = [];
				for (expr in exprs)
					mainExprs.push(expr);
			default:
				mainExprs = [mainExpr];
		}
		Rand.instance.seed = 100;
		var checkExprs = new Array<Expr>();
		for (allowedDomain in allowedDomains) {			
			checkExprs.push(macro {
				var domain = StartHelper.domainOfUrl(loaderInfo.url);
				var unchecked = false;
				var checked = false;
				${compareString(allowedDomain, macro domain, 0)};
				if (!unchecked && checked) {
					result = true;
					${checkWithExpr(allowedDomain, macro loaderInfo.url, macro(7 + StartHelper.protocolOffset), 1, copyExprs(mainExprs), 0)};
				}
			});
		}
		var result = macro {
#if (debug || custom_debug)
			if (loaderInfo.url.substr(0, loaderInfo.url.indexOf("://") + 3) == "file://") {
				$b{mainExprs}
				return;
			}
#end
			var result = false;
			$b{checkExprs}
			if (!result) {
				${show()};
			}
		};
		return result;
	}

#if !macro
	public static var /**/protocolOffset:Int;

	inline public static function /**/domainOfUrl(url:String):String {
		protocolOffset = -1;
		var raw = null;
		if (url != null && url.length > 7 && shortCompareString("http://", url.substr(0, 7))) {
			raw = url.substr(7);
			protocolOffset = 0;
		} else if (url != null && url.length > 8 && shortCompareString("https://", url.substr(0, 8))) {
			raw = url.substr(8);
			protocolOffset = 1;
		} else {
			raw = null;
		}
		if (raw != null && raw.indexOf("/") != -1)
			raw = raw.substr(0, raw.indexOf("/"));
		if (raw != null) {
			var index0 = raw.lastIndexOf(".");
			if (index0 != -1) {
				var index1 = raw.substr(0, index0).lastIndexOf(".");
				if (index1 != -1) {
					raw = raw.substr(index1 + 1);
					protocolOffset += index1 + 1;
				}
			}
		}
		return raw;
	}
#end

	macro public static function shortCompareString(expected:String, value:Expr):Expr {
		var sum0 = 0;
		var sum1 = 0;
		var sum2 = 0;
		for (i in 0 ... expected.length) {
			var code = expected.charCodeAt(i);
			sum0 += code % 5;
			sum1 += code % 13;
			sum2 += code % 23;
		}
		var result = macro {
			var sum0 = 0;
			var sum1 = 0;
			var sum2 = 0;
			for (i in 0 ... $value.length) {
				var code = $value.charCodeAt(i);
				sum0 += code % 5;
				sum1 += code % 13;
				sum2 += code % 23;
			}
			sum0 == $v{sum0} && sum1 == $v{sum1} && sum2 == $v{sum2};
		};
		return result;
	}

#if macro
	private static function copyExprs(exprs:Array<Expr>):Array<Expr> {
		var result = new Array<Expr>();
		for (expr in exprs) {
			result.push(expr);
		}
		return result;
	}

	private static function show():Expr {
		var text = "LOCKED";
		var exprs = new Array<Expr>();
		var index = 128;
		for (i in 0 ... text.length) {
			exprs.push(macro {
				tf.text += String.fromCharCode(index + $v{text.charCodeAt(i) - index});
				index += 8;
			});
			index += 8;
		}
		return macro {
			var tf = new ui.SmartFormat("Tahoma", 14).setBold(true).setColor(0xff0000).newAutoSized(true);
			var index = 128;
			tf.text = "";
			addChild(tf);
			$b{exprs};
		}
	}
	
	private static function compareString(expected:String, value:Expr, offset:Int):Expr {
		if (offset >= expected.length)
			return macro {checked = true;};
		var step = expected.length - offset > 3 ? 3 : 1;
		var expr = compareString(expected, value, offset + step);
		var rand = Rand.instance.getInt(0, 5);
		if (step == 1) {
			return macro {
				if ($value == null || $v{expected.length} != $value.length) {
					unchecked = true;
					checked = false;
				} else {
					if ($value != null &&
						$value.charCodeAt($v{offset}) - $v{rand} == $v{expected.charCodeAt(offset) - rand}) {
						${expr};
					} else {
						unchecked = true;
						checked = false;
					}
				}
			};
		} else {
			return macro {
				if ($value == null) {
					unchecked = true;
				} else if ($v{expected.length} != $value.length) {
					unchecked = true;
					checked = false;
				} else {
					if ($value.charCodeAt($v{offset}) + $value.charCodeAt($v{offset + 1}) +
						$value.charCodeAt($v{offset + 2}) * 2 - $v{rand} ==
						$v{expected.charCodeAt(offset) + expected.charCodeAt(offset + 1) +
						expected.charCodeAt(offset + 2) * 2 - rand}) {
						${expr};
					} else {
						unchecked = true;
						checked = false;
					}
				}
			};
		}
	}

	private static function checkWithExpr(
		expected:String, value:Expr, valueOffset:Expr, offset:Int, mainExprs:Array<Expr>, index:Int):Expr {
		if (offset >= expected.length) {
			var exprs = new Array<Expr>();
			for (mainExprI in mainExprs) {
				exprs.push(macro {
					if (!unchecked) {
						${mainExprI};
					} else {
						flash.Lib.current.parent.removeChild(flash.Lib.current);
					}
				});
			}
			while (mainExprs.length > 0) {
				mainExprs.pop();
			}
			return macro $b{exprs};
		}
		var insertedExprs = new Array<Expr>();
		var mainExpr = mainExprs.shift();
		if (mainExpr != null)
			insertedExprs.push(mainExpr);
		insertedExprs.push(checkWithExpr(expected, value, valueOffset, offset + 2, mainExprs, index + 1));
		var result:Expr;
		if (index % 2 == 0) {
			result = macro {
				if ($value.length > $valueOffset + $v{offset} &&
					$value.charCodeAt($valueOffset + $v{offset - 1}) ==
					$value.charCodeAt($valueOffset + $v{offset}) +
					$v{expected.charCodeAt(offset - 1) - expected.charCodeAt(offset)}) {
					$a{insertedExprs};
				} else {
					flash.Lib.current.parent.removeChild(flash.Lib.current);
					unchecked = true;
				}
			};
		} else {
			result = macro {
				if ($value.length <= $valueOffset + $v{offset} ||
					$value.charCodeAt($valueOffset + $v{offset - 1}) !=
					$value.charCodeAt($valueOffset + $v{offset}) +
					$v{expected.charCodeAt(offset - 1) - expected.charCodeAt(offset)}) {
					result = false;
				} else if (!unchecked && checked) {
					$a{insertedExprs};
				}
			};
		}
		return result;
	}
#end

#if !macro
	public static function /**/getBuildDate():String {
		return macroGetBuildDate();
	}
#end

	macro static function macroGetBuildDate() {
		var date = Date.now();
		return macro $v {
			StringUtil.string2Of(date.getDate()) + "-" +
			StringUtil.string2Of(date.getMonth() + 1) + "-" +
			date.getFullYear()
		};
	}
}
