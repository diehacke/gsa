package tutorial;

import game.CharacterState;
import game.Planet;
import game.PlanetInfo;
import game.World;
import utils.ListUtil;
import utils.MathUtil;
import utils.TweenKey;

class Tutorial20Base extends Tutorial {
	public function new() {
		super();
	}

	override public function doOn():Void {
		_state = UNKNOWN;
		_completeTimer = 0;
	}

	override public function doOff():Void {
		_panel.silence("");
	}

	private var UNKNOWN = new TweenKey();
	private var ON_LAND = new TweenKey();
	private var CLIMB_AND_ROTATION = new TweenKey();
	private var DRIFT_AND_RACING = new TweenKey();
	private var ORBIT_REACHED_0 = new TweenKey();
	private var ORBIT_REACHED_1 = new TweenKey();
	private var NEED_DEPLOY = new TweenKey();
	private var BEFORE_CRASH = new TweenKey();
	private var CRASH = new TweenKey();
	private var FAIL = new TweenKey();

	private var _state:TweenKey;
	private var _completeTimer:Int;

	private function discoverPlanet(info:PlanetInfo):Void {
		var planet = null;
		for (unit in _world.getMassive()) {
			if (unit.asPlanet != null && unit.asPlanet.status == info.status) {
				planet = unit.asPlanet;
			}
		}
		if (planet != null && !planet.status.discovered) {
			planet.status.discovered = true;
			_world.discovered.dispatch(planet);
		}
	}
}
