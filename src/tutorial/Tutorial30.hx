package tutorial;

import game.World;
import game.PlanetInfos;
import utils.ListUtil;
import utils.MathUtil;

class Tutorial30 extends Tutorial30Base {
	public function new() {
		super();
	}

	override public function doOnEnterFrame():Void {
		var state = UNKNOWN;
		var substate = 0;
		var hasFuel = _rocket.ship.hasFuel();
		var hasStages = this.hasStages();
		_game.setTutorialOrientation(Math.NaN, Math.NaN);
		while (true) {
			if (_rocket.getExploded()) {
				state = CRASH;
				if (state != _state) {
					_panel.facepalm("Press SPACE");
				}
				break;
			}
			if (_completed) {
				if (_state != TO_FAR && _r > 50000 || _state == TO_FAR && _r > 45000) {
					state = TO_FAR;
					if (state != _state) {
						_panel.talk("It's too far to find something", 60,
							Assets.talk_its_too_far_to_find_something_mp3);
					}
					break;
				}
				state = COMPLETE;
				if (state != _state) {
					_panel.silence("Mission complete");
				}
				break;
			}
			if (_target == _world.getGlory() && _r > _target.atmRadius) {
				var secondV = World.getSecondVelocity(_target.m, _r);
				if (_v > secondV + (_state != PRECOMPLETE ? secondV * .05 : .0)) {
					state = PRECOMPLETE;
					if (state != _state) {
						_panel.talk("Second velocity reached!", 60, Assets.talk_second_velocity_reached_mp3);
						_speedUpTimer = 70;
						substate = 0;
					}
					if (_speedUpTimer-- < 0) {
						substate = 1;
						if (_world.accelerationIndex >= 3)
							substate = 2;
					}
					if (substate != _substate) {
						if (substate == 1) {
							_panel.talk("Press D to speed up the time", 60,
								Assets.talk_press_d_to_speed_up_the_time_mp3);
						} else if (substate == 2) {
							_panel.talk("Wait", 40, Assets.talk_wait_mp3);
						}
					}
					break;
				}
			}
			if (_target == _world.getStar()) {
				state = COMPLETE;
				if (state != _state) {
					_panel.talk("We went out of planet gravity range!", 60,
						Assets.talk_we_went_out_of_the_planets_gravity_mp3);
					_completeTimer = 100;
				}
				if (_completeTimer-- == 0) {
					_game.processMissionComplete();
					_completed = true;
					_panel.silence("Mission completed\n(Press SPACE)");
				}
				break;
			}
			if (_world.getOrbitCollision() != null && _r < _target.radius + 4 &&
				_vx * _rx + _vy * _ry < 0 && _v > World.EXPLOSION_V * .9) {
				state = BEFORE_CRASH;
				if (state != _state) {
					_panel.horror("Apparat will crash!");
				}
				break;
			}
			var minRadius = _state == FAIL ? _target.atmRadius * 3.5 : _target.atmRadius * 3;
			if (!hasFuel && !hasStages &&
				_target == _world.getGlory() && _maxR < minRadius) {
				state = FAIL;
				if (state != _state) {
					_panel.facepalm("It seems we can't leave " + PlanetInfos.glory.name);
				}
				break;
			}
			if (!hasFuel && !hasStages) {
				if (_world.accelerationIndex < 3) {
					state = MAY_BE_PRECOMPLETE_0;
					if (state != _state) {
						_panel.talk("Press D to speed up the time", 60,
							Assets.talk_press_d_to_speed_up_the_time_mp3);
					}
				} else {
					state = MAY_BE_PRECOMPLETE_1;
					if (state != _state) {
						_panel.talk("Wait", 40, Assets.talk_wait_mp3);
					}
				}
				break;
			}
			var atmH = _target.atmRadius - _target.radius;
			if (_minR < (_state == ORBITING ? _target.atmRadius : _target.atmRadius - .5 * atmH) &&
				_maxR < _target.atmRadius * 3) {
				state = ORBITING;
				if (state != _state) {
					_panel.talk("Place vehicle in orbit", 60, Assets.talk_place_vehicle_in_orbit_mp3);
				}
				break;
			}
			if (state == RACING && _minR > _target.radius + atmH * .01 ||
				_maxR > _target.atmRadius * 3 ||
				state != RACING && _minR > _target.atmRadius) {
				state = RACING;
				if (state != _state) {
					_panel.talk("Full speed ahead!", 40, Assets.talk_full_speed_ahead_mp3);
				}
				if (_rx * _vy - _ry * _vx > 0)
					_game.setTutorialOrientation(-_ry, _rx);
				else
					_game.setTutorialOrientation(_ry, -_rx);
				break;
			}
			break;
		}
		if (state == UNKNOWN) {
			if (state != _state) {
				_panel.silence(null);
			}
		}
		_state = state;
		_substate = substate;
	}
}
