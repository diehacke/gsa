package tutorial;

import game.World;
import utils.ListUtil;
import utils.MathUtil;

class Tutorial10 extends Tutorial10Base {
	public function new() {
		super();
	}

	override public function doOnEnterFrame():Void {
		var state = UNKNOWN;
		var substate = 0;
		_game.setTutorialOrientation(Math.NaN, Math.NaN);
		_world.showMaxPoint = false;
		while (true) {
			if (_rocket.getExploded()) {
				state = CRASH;
				if (state != _state) {
					_panel.facepalm("Press SPACE");
				}
				break;
			}
			var orbitReached = _state == ORBIT_REACHED ?
				_minR > _target.atmRadius - .15 && _world.getOrbitCollision() == null :
				_minR > _target.atmRadius - .1 && _world.getOrbitCollision() == null;
			if (orbitReached) {
				state = ORBIT_REACHED;
				if (state != _state) {
					_panel.talk("Orbit reached!", 40, Assets.talk_orbit_reached_mp3);
					_completeTimer = 100;
				}
				if (hasStages()) {
					substate = 1;
					if (_substate != substate) {
						_needUnlinkTimer = 100;
					}
					if (_needUnlinkTimer-- == 0) {
						_panel.talk("Press DOWN\nto unlink stages", 80,
							Assets.talk_press_down_to_unlink_stages_mp3);
					}
					break;
				}
				substate = 0;
				if (substate != _substate) {
					_panel.talk("Vehicle is placed\nin orbit now", 80,
						Assets.talk_vehicle_is_placed_in_orbit_now_mp3);
				}
				if (_completeTimer-- == 0) {
					_game.processMissionComplete();
					_completed = true;
					_panel.silence("Mission completed\n(Press SPACE)");
				}
				break;
			}
			if (!_rocket.ship.hasFuel() && !hasStages()) {
				state = FAIL;
				if (state != _state) {
					_panel.facepalm("We can't change anything");
				}
				break;
			}
			if (_world.getOrbitCollision() != null && _r < _target.radius + 4 &&
				_vx * _rx + _vy * _ry < 0 && _v > World.EXPLOSION_V * .9) {
				state = BEFORE_CRASH;
				if (state != _state) {
					_panel.horror("Apparat will crash!");
				}
				break;
			}
			if (_r - _rocket.footY < _target.radius + .1 && _v < .1) {
				state = ON_LAND;
				if (state != _state) {
					_panel.talk("Press UP", 40, Assets.talk_press_up_mp3);
				}
				break;
			}
			var maxR = _target.atmRadius + 8;
			if (_maxR < (_state == CLIMB_AND_ROTATION ? maxR + 1 : maxR)) {
				state = CLIMB_AND_ROTATION;
				if (state != _state) {
					substate = 0;
					_panel.talk("Use LEFT/RIGHT\nto tilt rocket", 60,
						Assets.talk_use_left_right_to_tilt_rocket_mp3);
				}
				if (!_rocket.getEnginesEnabled()) {
					substate = 1;
					if (substate != _substate) {
						_panel.talk("Don't switch off engines", 60, Assets.talk_dont_switch_off_engines_mp3);
					}
				}
				var angle = MathUtil.getAngle(1, 0, _rx, _ry);
				angle -= Math.PI * .5 * ListUtil.min(1, (_r - _target.radius) / 5);
				_game.setTutorialOrientation(Math.cos(angle), Math.sin(angle));
				break;
			}
			if ((Math.isNaN(_maxX) || Math.isNaN(_maxY)) &&
				_maxR > maxR && _r > _target.atmRadius && !_rocket.getEnginesEnabled()) {
				_maxX = _world.getOrbitMaxRx();
				_maxY = _world.getOrbitMaxRy();
			}
			var delta = Math.PI * .015;
			if (!Math.isNaN(_maxX) && !Math.isNaN(_maxY)) {
				_world.showMaxPoint = true;
				var angle = MathUtil.getAngle(_maxX, _maxY, _rx, _ry);
				if (_racingCounter == 0) {
					if (angle < delta * 3) {
						_racingCounter++;
						_game.showTimer("3");
					}
				} else if (_racingCounter == 1) {
					if (angle < delta * 2) {
						_racingCounter++;
						_game.showTimer("2");
					}
				} else if (_racingCounter == 2) {
					if (angle < delta) {
						_racingCounter++;
						_game.showTimer("1");
					}
				} else if (_racingCounter == 3) {
					if (angle < delta * .1) {
						_racingCounter++;
						_game.showTimer("0");
					}
				}
				if (angle < 0) {
					state = RACING;
					if (_state != state) {
						_panel.talk("Full speed ahead (press UP)!", 40, Assets.talk_full_speed_ahead_mp3);
					}
					_game.setTutorialOrientation(_ry, -_rx);
					_world.showMaxPoint = false;
					break;
				}
			}
			if (_rocket.getEnginesEnabled()) {
				state = DRIFT_0;
				if (state != _state) {
					_panel.talk("Switch off engines!", 40, Assets.talk_switch_off_engines_mp3);
				}
				break;
			}
			if (!Math.isNaN(_maxX) && !Math.isNaN(_maxY)) {
				var directionX = _maxY;
				var directionY = -_maxX;
				var angle = MathUtil.getAngle(_rocket.direction.x, _rocket.direction.y, directionX, directionY);
				if (MathUtil.abs(angle) > Math.PI * .05) {
					state = DRIFT_1;
					if (state != _state) {
						_panel.talk("Orient the rocket", 60, Assets.talk_orient_the_rocket_mp3);
					}
					_game.setTutorialOrientation(directionX, directionY);
					break;
				}
			}
			state = DRIFT_2;
			if (state != _state) {
				_panel.talk("Wait\n(You may use D & A\nto skip time)", 30, Assets.talk_wait_mp3);
			}
			break;
		}
		if (state == UNKNOWN) {
			if (state != _state) {
				_panel.silence(null);
			}
		}
		_state = state;
		_substate = substate;
	}
}
