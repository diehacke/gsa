package tutorial;

import game.CharacterState;
import game.World;
import utils.ListUtil;
import utils.MathUtil;
import utils.TweenKey;

class Tutorial50Base extends Tutorial {
	public function new() {
		super();
	}

	override public function doOn():Void {
		_state = UNKNOWN;
		_wasOnLand = false;
		_wasLanding1 = false;
		_completeTimer = 0;
		_instructionsTimer = 0;
	}

	override public function doOff():Void {
		_panel.silence("");
	}

	private var UNKNOWN = new TweenKey();
	private var BEFORE_CRASH = new TweenKey();
	private var CRASH = new TweenKey();
	private var FAIL = new TweenKey();

	private var GET_OUT_FROM_ORBIT = new TweenKey();
	private var LANDING_0 = new TweenKey();
	private var LANDING_1 = new TweenKey();
	private var LANDING_2 = new TweenKey();
	private var ON_LAND = new TweenKey();
	private var COMPLETE = new TweenKey();

	private var _state:TweenKey;

	private var _wasOnLand:Bool;
	private var _wasLanding1:Bool;
	private var _completeTimer:Int;
	private var _instructionsTimer:Int;
}
