package tutorial;

import game.CharacterState;
import game.World;
import utils.ListUtil;
import utils.MathUtil;
import utils.TweenKey;

class Tutorial30Base extends Tutorial {
	public function new() {
		super();
	}

	override public function doOn():Void {
		_state = NONE;
		_completeTimer = 0;
		_instructionsTimer = 0;
		_speedUpTimer = 0;
		_substate = 0;
	}

	override public function doOff():Void {
		_panel.silence("");
	}

	private var NONE = new TweenKey();
	private var UNKNOWN = new TweenKey();
	private var BEFORE_CRASH = new TweenKey();
	private var CRASH = new TweenKey();
	private var FAIL = new TweenKey();

	private var ORBITING = new TweenKey();
	private var RACING = new TweenKey();
	private var PRECOMPLETE = new TweenKey();
	private var MAY_BE_PRECOMPLETE_0 = new TweenKey();
	private var MAY_BE_PRECOMPLETE_1 = new TweenKey();
	private var COMPLETE = new TweenKey();
	private var TO_FAR = new TweenKey();

	private var _state:TweenKey;
	private var _substate:Int;

	private var _completeTimer:Int;
	private var _instructionsTimer:Int;
	private var _speedUpTimer:Int;
}
