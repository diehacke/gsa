package tutorial;

import game.CharacterState;
import utils.RotationMatrix;
import game.Camera;
import game.PlanetInfos;
import game.World;
import utils.ListUtil;
import utils.MathUtil;
import utils.Vertex;

class Tutorial20 extends Tutorial20Base {
	public function new() {
		super();
	}

	private static var H0:Float = 20;
	private static var H1:Float = 30;

	override public function doOnEnterFrame():Void {
		var state = UNKNOWN;
		var r0 = _target.radius + H0;
		var r1 = _target.radius + H1;
		_game.setTutorialOrientation(Math.NaN, Math.NaN);
		_world.tutorialOrbitVisible = false;
		while (true) {
			if (_rocket.getExploded()) {
				state = CRASH;
				if (state != _state) {
					_panel.facepalm("We can't change anything");
				}
				break;
			}
			if (_completed) {
				break;
			}
			if (_r > _target.radius + (_target.atmRadius - _target.radius) * .6 && !_rocket.ship.deployed) {
				state = NEED_DEPLOY;
				if (state != _state) {
					_panel.talk("Unlink cowl by E!", 40, Assets.talk_unlink_cowl_by_e_mp3);
				}
				setClimbAngle();
				break;
			}
			if ((_state == ORBIT_REACHED_1 && _minR > r0 - 1 && _maxR < r1 + 1 ||
				_state != ORBIT_REACHED_1 && _minR > r0 && _maxR < r1) &&
				_world.getOrbitCollision() == null && _rocket.ship.deployed) {
				state = ORBIT_REACHED_1;
				if (state != _state) {
					_panel.talk("Orbit is correct now", 40, Assets.talk_orbit_is_corrent_now_mp3);
					_completeTimer = 100;
				}
				if (_completeTimer-- == 0) {
					discoverPlanet(PlanetInfos.verdana);
					discoverPlanet(PlanetInfos.tahoma);
					discoverPlanet(PlanetInfos.purisa);
					_game.processMissionComplete();
					_completed = true;
					_panel.silence("Mission completed\n(Press SPACE)");
				}
				_world.tutorialOrbitVisible = false;
				break;
			}
			if (!_rocket.ship.hasFuel() && !hasStages()) {
				state = FAIL;
				if (state != _state) {
					_panel.facepalm("We can't change anything");
				}
				break;
			}
			var minRadius = _state == ORBIT_REACHED_0 ?
				(_target.radius + _target.atmRadius) * .5 :
				_target.atmRadius;
			if (_minR > minRadius && _world.getOrbitCollision() == null) {
				state = ORBIT_REACHED_0;
				if (_state != state) {
					_panel.talk("Need to correct orbit", 40, Assets.talk_need_to_correct_orbit_mp3);
				}
				_world.tutorialOrbitVisible = true;
				_world.tutorialOrbitR0 = r0;
				_world.tutorialOrbitR1 = r1;
				break;
			}
			if (_world.getOrbitCollision() != null && _r < _target.radius + 4 &&
				_vx * _rx + _vy * _ry < 0 && _v > World.EXPLOSION_V * .9) {
				state = BEFORE_CRASH;
				if (state != _state) {
					_panel.horror("Apparat will crash!");
				}
				break;
			}
			if (_r < _target.radius + .1 && _v < .1) {
				state = ON_LAND;
				break;
			}
			if (_maxR < _target.atmRadius + 3) {
				state = CLIMB_AND_ROTATION;
				setClimbAngle();
				break;
			}
			state = DRIFT_AND_RACING;
			break;
		}
		if (state != _state && state == ON_LAND || state == CLIMB_AND_ROTATION || state == DRIFT_AND_RACING) {
			if (_rocket.ship.deployed)
				_panel.talk("Continue orbital injection", 60, Assets.talk_continue_orbital_injection_mp3);
			else
				_panel.talk("Place vehicle in orbit", 60,
					Assets.talk_place_vehicle_in_orbit_mp3);
		}
		_state = state;
	}

	private function setClimbAngle():Void {
		var angle = MathUtil.getAngle(1, 0, _rx, _ry);
		angle -= Math.PI * .5 * ListUtil.min(1, (_r - _target.radius) / 6);
		_game.setTutorialOrientation(Math.cos(angle), Math.sin(angle));
		_world.tutorialOrbitVisible = false;
	}

	private static var _direction:Vertex = new Vertex();
	private static var _matrix:RotationMatrix = new RotationMatrix();

	override public function render(camera:Camera):Void {
		if (_completed)
			return;
		if (_rocket.getEnginesEnabled())
			return;
		var r0 = _target.radius + H0;
		var r1 = _target.radius + H1;
		if (MathUtil.abs(_maxR - _minR) > 3) {
			for (i in 0 ... 2) {
				var rx = i == 0 ? _world.getOrbitMinRx() : _world.getOrbitMaxRx();
				var ry = i == 0 ? _world.getOrbitMinRy() : _world.getOrbitMaxRy();
				var needDraw = false;
				if ((i == 0 ? _maxR : _minR) < r0 + .5) {
					_direction.setXY(ry, -rx);
					needDraw = true;
				} else if ((i == 0 ? _maxR : _minR) > r1 - .5) {
					_direction.setXY(-ry, rx);
					needDraw = true;
				}
				if (needDraw) {
					_direction.normalize();
					if (rx * rx + ry * ry > _target.atmRadius * _target.atmRadius)
						drawDirection(camera, _target.r.x + rx, _target.r.y + ry, _direction, true);
				}
			}
		} else {
			if (_rx * _rx + _ry * _ry > _target.atmRadius * _target.atmRadius) {
				var needDraw = false;
				if ((_minR + _maxR) * .5 < r0 + .1) {
					_direction.setXY(_ry, -_rx);
					needDraw = true;
				} else if ((_maxR + _minR) * .5 > r1 - .1) {
					_direction.setXY(-_ry, _rx);
					needDraw = true;
				}
				if (needDraw) {
					_direction.normalize();
					drawDirection(camera, _target.r.x + _rx, _target.r.y + _ry, _direction, false);
				}
			}
		}
	}

	private function drawDirection(camera:Camera, x:Float, y:Float, direction:Vertex, drawCircle:Bool):Void {
		var g = camera.graphics;
		g.lineStyle(0, 0x00ffff);
		camera.screenXY(x, y);
		if (drawCircle)
			g.drawCircle(camera.outSX, camera.outSY, 5);
		g.moveTo(camera.outSX, camera.outSY);
		var size = 50;
		_matrix.rotateXTo(direction.x, direction.y);
		_matrix.transform(size, 0);
		g.lineTo(camera.outSX + _matrix.outX, camera.outSY + _matrix.outY);
		_matrix.transform(size - 8, -4);
		g.lineTo(camera.outSX + _matrix.outX, camera.outSY + _matrix.outY);
		_matrix.transform(size, 0);
		g.moveTo(camera.outSX + _matrix.outX, camera.outSY + _matrix.outY);
		_matrix.transform(size - 8, 4);
		g.lineTo(camera.outSX + _matrix.outX, camera.outSY + _matrix.outY);
	}
}
