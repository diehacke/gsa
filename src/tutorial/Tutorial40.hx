package tutorial;

import game.World;
import game.PlanetInfos;
import utils.ListUtil;
import utils.MathUtil;

class Tutorial40 extends Tutorial40Base {
	public function new() {
		super();
	}

	override public function doOnEnterFrame():Void {
		var state = UNKNOWN;
		var substate = 0;
		_game.setTutorialOrientation(Math.NaN, Math.NaN);
		while (true) {
			if (_rocket.getExploded()) {
				state = CRASH;
				if (state != _state) {
					_panel.facepalm("Press SPACE");
				}
				break;
			}
			if (!_mission.completed && _target == _world.getStar()) {
				state = COMPLETE;
				if (state != _state) {
					_panel.facepalm("We went out of " + PlanetInfos.glory.name + " range!");
				}
				break;
			}
			if (_world.getOrbitCollision() != null && _r < _target.radius + 4 &&
				_vx * _rx + _vy * _ry < 0 && _v > World.EXPLOSION_V * .9 &&
				!_rocket.ship.brolly) {
				state = BEFORE_CRASH;
				if (state != _state) {
					_panel.horror("Apparat will crash!");
				}
				break;
			}
			var atmH = _target.atmRadius - _target.radius;
			if (!_mission.completed && !_rocket.ship.hasFuel() && !hasStages() &&
				_minR > _target.atmRadius + (_state == FAIL ? -atmH * .1 : atmH * .1) &&
				_world.getOrbitCollision() == null) {
				state = FAIL;
				if (state != _state) {
					_panel.facepalm("We can't change anything");
				}
				break;
			}
			if (!_rocket.ship.brolly) {
				state = NEED_DEPLOY;
				if (state != _state) {
					_panel.talk("Press E to deploy the brolly", 60,
						Assets.talk_press_e_to_deploy_the_brolly_mp3);
				}
				break;
			}
			if (_r - _rocket.footY < _target.radius + .1 && _v < .1) {
				state = ON_LAND;
				if (state != _state) {
					_completeTimer = 100;
					_panel.talk("Landing successfully!", 60, Assets.talk_landing_successfuly_mp3);
				}
				if (_completeTimer-- == 0) {
					_game.processMissionComplete();
					_completed = false;
					_panel.silence("Mission completed\n(Press SPACE)");
				}
				break;
			}
			if (_minR > _target.atmRadius + (_state == GET_OUT_FROM_ORBIT ? -atmH * .3 : -atmH * .1)) {
				state = GET_OUT_FROM_ORBIT;
				if (state != _state) {
					_panel.talk("Rotate the ship & run engines", 60,
						Assets.talk_rotate_the_ship_and_run_engines_mp3);
				}
				if (_rx * _vy - _ry * _vx > 0)
					_game.setTutorialOrientation(_ry, -_rx);
				else
					_game.setTutorialOrientation(-_ry, _rx);
				break;
			}
			var minR = _target.atmRadius +
				(_state == LANDING_0 || _state == LANDING_1 ? -atmH * .1 : -atmH * .3);
			if (_minR < minR) {
				if (_rocket.getEnginesEnabled()) {
					state = LANDING_0;
					if (state != _state) {
						_panel.talk("Switch off engines!", 60, Assets.talk_switch_off_engines_mp3);
					}
					break;
				}
				state = LANDING_1;
				if (state != _state) {
					_panel.talk("Change the angle to prevent overloading", 60,
						Assets.talk_change_the_angle_to_prevent_overloading_mp3);
				}
				_game.setTutorialOrientation(_rx, _ry);
				break;
			}
			break;
		}
		if (state == UNKNOWN) {
			if (state != _state) {
				_panel.silence(null);
			}
		}
		_state = state;
	}
}
