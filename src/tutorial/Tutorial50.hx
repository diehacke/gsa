package tutorial;

import game.World;
import game.PlanetInfos;
import utils.ListUtil;
import utils.MathUtil;

class Tutorial50 extends Tutorial50Base {
	public function new() {
		super();
	}

	override public function doOnEnterFrame():Void {
		var state = UNKNOWN;
		var substate = 0;
		_game.setTutorialOrientation(Math.NaN, Math.NaN);
		while (true) {
			if (_rocket.getExploded()) {
				state = CRASH;
				if (state != _state) {
					_panel.facepalm("Press SPACE");
				}
				break;
			}
			if (!_mission.completed && _target == _world.getStar()) {
				state = COMPLETE;
				if (state != _state) {
					_panel.facepalm("We went out of " + PlanetInfos.glory.name + " range!");
				}
				break;
			}
			if (_world.getOrbitCollision() != null && _r < _target.radius + 4 &&
				_vx * _rx + _vy * _ry < 0 && _v > World.EXPLOSION_V * (_state == BEFORE_CRASH ? 1 : 1.05) &&
				!_rocket.ship.hasFuel()) {
				state = BEFORE_CRASH;
				if (state != _state) {
					_panel.horror("Apparat will crash!");
				}
				break;
			}
			if (!_wasOnLand && !_mission.completed && !_rocket.ship.hasFuel() && !hasStages() &&
				_minR > _target.radius + (_state == FAIL ? 1 : 1.5) &&
				_world.getOrbitCollision() == null) {
				state = FAIL;
				if (state != _state) {
					_panel.facepalm("We can't change anything");
				}
				break;
			}
			if (_r - _rocket.footY < _target.radius + .1 && _v < .1) {
				state = ON_LAND;
				if (state != _state) {
					_wasOnLand = true;
					_completeTimer = 100;
					_panel.talk("Landing successfully!", 60, Assets.talk_landing_successfuly_mp3);
				}
				if (_completeTimer-- == 0) {
					_game.processMissionComplete();
					_completed = false;
					_panel.silence("Mission completed\n(Press SPACE)");
				}
				break;
			}
			if (!_wasOnLand && _world.getOrbitCollision() == null) {
				state = GET_OUT_FROM_ORBIT;
				if (state != _state) {
					_panel.talk("Rotate the ship & run engines", 60,
						Assets.talk_rotate_the_ship_and_run_engines_mp3);
				}
				_game.setTutorialOrientation(-_vx, -_vy);
				break;
			}
			if (!_wasOnLand && _world.getOrbitCollision() != null) {
				_game.setTutorialOrientation(-_vx, -_vy);
				var maxV = _state == LANDING_0 ? World.EXPLOSION_V * .9 : World.EXPLOSION_V * .7;
				if (_r > _target.radius + 6 || _v < maxV || _vx * _rx + _vy * _ry > 0) {
					if (_rocket.getEnginesEnabled()) {
						state = LANDING_0;
						if (state != _state) {
							_panel.talk("Switch off engines!", 40, Assets.talk_switch_off_engines_mp3);
						}
						break;
					}
					state = LANDING_1;
					if (state != _state) {
						_panel.talk("Wait" + (_wasLanding1 ? "" : "\n(you can use D & A keys)"), 30,
							Assets.talk_wait_mp3);
					}
					_wasLanding1 = true;
					break;
				}
				state = LANDING_2;
				if (state != _state) {
					_panel.talk("Run engines", 60, Assets.talk_run_engines_mp3);
				}
				break;
			}
			break;
		}
		if (state == UNKNOWN) {
			if (state != _state) {
				_panel.silence(null);
			}
		}
		_state = state;
	}
}
