package tutorial;

import game.Game;
import game.Camera;
import model.Mission;
import utils.MathUtil;
import game.Unit;
import game.Rocket;
import game.World;
import game.CharacterPanel;

class Tutorial {
	public function new() {
	}

	private var _game:Game;
	private var _world:World;
	private var _rocket:Rocket;
	private var _panel:CharacterPanel;
	private var _mission:Mission;

	public function init(game:Game, world:World, panel:CharacterPanel, mission:Mission):Void {
		_game = game;
		_world = world;
		_rocket = _world.getRocket();
		_panel = panel;
		_mission = mission;
		_completed = false;
	}

	private var _completed:Bool;
	inline public function getCompleted():Bool {
		return _completed;
	}

	public function doOn():Void {
	}

	public function doOff():Void {
	}

	private var _target:Unit;
	private var _vx:Float;
	private var _vy:Float;
	private var _v:Float;
	private var _rx:Float;
	private var _ry:Float;
	private var _r:Float;
	private var _minR:Float;
	private var _maxR:Float;

	public function doBeforeEnterFrame():Void {
		_target = _world.getTargetUnit();
		_vx = _rocket.v.x - _target.v.x;
		_vy = _rocket.v.y - _target.v.y;
		_v = MathUtil.sqrt(_vx * _vx + _vy * _vy);
		_rx = _rocket.r.x - _target.r.x;
		_ry = _rocket.r.y - _target.r.y;
		_r = Math.sqrt(_rocket.r.distance2(_target.r));
		_minR = MathUtil.sqrt(_world.getOrbitMinR2());
		_maxR = MathUtil.sqrt(_world.getOrbitMaxR2());
	}

	public function doOnEnterFrame():Void {
	}

	inline private function hasStages():Bool {
		var hasStages = false;
		for (link in _rocket.ship.links) {
			if (link.child != null) {
				hasStages = true;
				break;
			}
		}
		return hasStages;
	}

	public function render(camera:Camera):Void {
	}
}
