package tutorial;

import game.CharacterState;
import utils.RotationMatrix;
import game.World;
import utils.ListUtil;
import utils.MathUtil;
import utils.TweenKey;

class Tutorial10Base extends Tutorial {
	public function new() {
		super();
	}

	override public function doOn():Void {
		_state = UNKNOWN;
		_substate = 0;
		_needUnlinkTimer = 0;
		_completeTimer = 0;
		_racingCounter = 0;
		_maxX = Math.NaN;
		_maxY = Math.NaN;

		var angle = Math.PI * .1;
	}

	override public function doOff():Void {
		_panel.silence("");
	}

	private var UNKNOWN = new TweenKey();
	private var ON_LAND = new TweenKey();
	private var CLIMB_AND_ROTATION = new TweenKey();
	private var DRIFT_0 = new TweenKey();
	private var DRIFT_1 = new TweenKey();
	private var DRIFT_2 = new TweenKey();
	private var RACING = new TweenKey();
	private var ORBIT_REACHED = new TweenKey();
	private var BEFORE_CRASH = new TweenKey();
	private var CRASH = new TweenKey();
	private var FAIL = new TweenKey();

	private var _state:TweenKey;
	private var _substate:Int;

	private var _needUnlinkTimer:Int;
	private var _completeTimer:Int;

	private var _maxX:Float;
	private var _maxY:Float;
	private var _racingCounter:Int;
}
