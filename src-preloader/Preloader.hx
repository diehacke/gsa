package ;

import flash.Lib;
import flash.display.Sprite;
import flash.events.Event;
import flash.display.BitmapData;
import flash.geom.ColorTransform;
import flash.geom.Matrix;
import flash.display.StageScaleMode;
import flash.display.StageAlign;

class Preloader extends Sprite {
	private var _digitsPanel:PreloaderDigitsPanel;

    public function new() {
        super();

		_angle0 = 0;
		_angle1 = 0;

		addEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
		addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
    }

	private function onAddedToStage(event:Event):Void {
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;

		_digitsPanel = new PreloaderDigitsPanel();
		_digitsPanel.value = 0;
		addChild(_digitsPanel);

        addEventListener(Event.ENTER_FRAME, onLoadProgress);
		stage.addEventListener(Event.RESIZE, onStageResize);
		resize();
	}

	private function onRemovedFromStage(event:Event):Void {
        removeEventListener(Event.ENTER_FRAME, onLoadProgress);
		stage.removeEventListener(Event.RESIZE, onStageResize);
	}

	private function onStageResize(event:Event = null):Void {
		resize();
	}

	private function resize():Void {
		_digitsPanel.x = stage.stageWidth / 2 - _digitsPanel.w / 2;
		_digitsPanel.y = stage.stageHeight / 2 - _digitsPanel.h / 2;

		var g = graphics;
		g.clear();
		g.beginFill(0x000000);
		g.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
		g.endFill();
	}

	private var _angle0:Float;
	private var _angle1:Float;

    private function onLoadProgress(event:Event):Void {
        var bytesLoaded = Lib.current.stage.loaderInfo.bytesLoaded;
        var bytesTotal = Lib.current.stage.loaderInfo.bytesTotal;
        var loaded = bytesLoaded >= bytesTotal;
        var percents = loaded ? 100 : Math.round((bytesLoaded / bytesTotal) * 100);
        _digitsPanel.value = percents;

		var g = graphics;
		g.clear();
		resize();
        if (loaded) {
			graphics.clear();
            removeEventListener(Event.ENTER_FRAME, onLoadProgress);
            Lib.current.removeChild(this);
            var cls = Type.resolveClass("Main");
            var app = Type.createInstance(cls, []);
            Lib.current.addChild(app);
        }

		_x0 = Lib.current.stage.stageWidth * .5;
		_y0 = Lib.current.stage.stageHeight * .5;

		_angle0 += .01 * 2;
		_angle1 += .05 * 2;

		g.beginFill(0x001020);
		g.drawCircle(getX(_angle0 - .1), getY(_angle0 - .1), 10);
		g.endFill();
		g.beginFill(0x002030);
		g.drawCircle(getX(_angle0 - .075), getY(_angle0 - .075), 10);
		g.endFill();

		g.beginFill(0x001020);
		g.drawCircle(getXX(_angle0 - .1, _angle1 - .2), getYY(_angle0 - .1, _angle1 - .2), 5);
		g.endFill();
		g.beginFill(0x002030);
		g.drawCircle(getXX(_angle0 - .075, _angle1 - .15), getYY(_angle0 - .075, _angle1 - .15), 5);
		g.endFill();

		g.beginFill(0x003060);
		g.drawCircle(getX(_angle0 - .05), getY(_angle0 - .05), 10);
		g.endFill();
		g.beginFill(0x004575);
		g.drawCircle(getX(_angle0 - .025), getY(_angle0 - .025), 10);
		g.endFill();

		g.beginFill(0x003060);
		g.drawCircle(getXX(_angle0 - .05, _angle1 - .1), getYY(_angle0 - .05, _angle1 - .1), 5);
		g.endFill();
		g.beginFill(0x004575);
		g.drawCircle(getXX(_angle0 - .025, _angle1 - .05), getYY(_angle0 - .025, _angle1 - .05), 5);
		g.endFill();

		g.lineStyle(0, 0x005080);
		g.drawCircle(_x0, _y0, 100);
		g.lineStyle();

		g.beginFill(0x0088ff);
		g.drawCircle(getX(_angle0), getY(_angle0), 10);
		g.endFill();
		
		g.beginFill(0x0088ff);
		g.drawCircle(getXX(_angle0, _angle1), getYY(_angle0, _angle1), 5);
		g.endFill();
    }

	private var _x0:Float;
	private var _y0:Float;

	private function getX(angle0:Float):Float {
		return _x0 + 100 * Math.cos(angle0);
	}

	private function getY(angle0:Float):Float {
		return _y0 + 100 * Math.sin(angle0);
	}

	private function getXX(angle0:Float, angle1:Float):Float {
		return getX(angle0) + 20 * Math.cos(angle1);
	}

	private function getYY(angle0:Float, angle1:Float):Float {
		return getY(angle0) + 20 * Math.sin(angle1);
	}

    public static function main() {
        Lib.current.addChild(new Preloader());
    }
}

@:bitmap('assets/digits.png')
class PreloaderDigits extends BitmapData {}

class PreloaderDigitsPanel extends Sprite {
	private static var DIGIT_W:Int = 32;
	private static var DIGIT_H:Int = 51;

	private var _w:Float;
	public var w(get, never):Float;
	private function get_w():Float {
		return _w;
	}

	private var _h:Float;
	public var h(get, never):Float;
	private function get_h():Float {
		return _h;
	}

	private var _bd:BitmapData;

	public function new() {
		super();
		_bd = new PreloaderDigits(0, 0);
		update();
		transform.colorTransform = new ColorTransform(0, .5, 1);
	}

	private var _value:Int;
	public var value(get, set):Int;
	private function get_value():Int {
		return _value;
	}
	private function set_value(value:Int):Int {
		if (_value != value) {
			_value = value;
			update();
		}
		return _value;
	}

	private function update():Void {
		var text = "   " + _value;
		text = text.substring(text.length - 3);
		_w = text.length * DIGIT_W;
		_h = DIGIT_H;
		var g = graphics;
		g.clear();
		g.beginFill(0x000000, .5);
		g.drawRect(0, 0, _w, _h);
		g.endFill();
		for (i in 0 ... text.length) {
			var char = text.charAt(i);
			var index = if (char == "0") 0;
			else if (char == "1") 1;
			else if (char == "2") 2;
			else if (char == "3") 3;
			else if (char == "4") 4;
			else if (char == "5") 5;
			else if (char == "6") 6;
			else if (char == "7") 7;
			else if (char == "8") 8;
			else if (char == "9") 9;
			else 11;
			g.beginBitmapFill(_bd, new Matrix(1, 0, 0, 1, (i - index) * DIGIT_W, 0), false);
			g.drawRect(i * DIGIT_W, 0, DIGIT_W, DIGIT_H);
			g.endFill();
		}
	}
}
