package;

import sys.FileSystem;
import sys.io.File;

class TestSuiteGenerator {
	private static var _testSrcPath:String = "test";
	private static var _testSuiteName:String = "HTestSuite";
	
	public static function main():Void {
		var classes = new Array<String>();
		getFilteredClassesInDirectory(classes, "");
		var content = generateTestSuiteClassFromClasses(classes);
		var testSuite = _testSrcPath + "/" + _testSuiteName + ".hx";
		File.saveContent(testSuite, content);
	}
	
	private static function getFilteredClassesInDirectory(classes:Array<String>, path:String):Void {
		var realPath = _testSrcPath + "/" + path;
		var files = FileSystem.readDirectory(realPath);
		for(file in files) {
			if (FileSystem.isDirectory(realPath + "/" + file))
				getFilteredClassesInDirectory(classes, path != "" ? path + "/" + file : file);
			if (!~/.*Test\.hx$/.match(file))
				continue;
			var clasz = path + "/" + file.substr(0, -3);
			clasz = clasz.split("/").join(".");
			if(clasz == "TestMain")
				continue;
			if (clasz.charAt(0) == '.')
				clasz = clasz.substring(1);
			classes.push(clasz);
		}	
	}
	
	private static function generateTestSuiteClassFromClasses(classes:Array<String>):String {
		var tests:String = "";
		for(clasz in classes) {
			tests += "\t\tadd(" + clasz + ");\n";
		}
		var template =
			"// << Autogenerated >>\n" +
			"package;\n" +
			"\n" +
			"class " + _testSuiteName + " extends massive.munit.TestSuite {\n" +
			"\tpublic function new() {\n" +
			"\t\tsuper();\n" +
			tests +
			"\t}\n" +
			"}";
		return template;
	}
}
