var gsa = "file:///Z|/home/user/gsa/";
var doc = function() {
	fl.openDocument(gsa + "assets-raw/ui.fla");
	var uiDoc = fl.getDocumentDOM();

	fl.createDocument();
	var newDoc = fl.getDocumentDOM();

	for (var i in uiDoc.library.items) {
		var item = uiDoc.library.items[i];
		uiDoc.library.addItemToDocument({x:0, y:0}, item.name);
		uiDoc.clipCut();
		newDoc.clipPaste();
		newDoc.clipCut();
	}

	newDoc.width = 1024;
	newDoc.height = 1024;
	newDoc.backgroundColor = "#000000";
	return newDoc;
}();
var tolerance = 2;
var rects = [];
var timeline = doc.timelines[doc.currentTimeline];
timeline.currentLayer = 0;
timeline.currentFrame = 0;
var layer = timeline.layers[timeline.currentLayer];
var frame = layer.frames[timeline.currentFrame];
for (var i in doc.library.items) {
	var item = doc.library.items[i];
	var childTimeline = item.timeline;
	if (childTimeline) {
		for (var j = 0; j < childTimeline.frameCount; j++) {
			var frameJ = childTimeline.layers[0].frames[j];
			if (frameJ.name) {
				doc.library.addItemToDocument({x:0, y:0}, item.name);
				var element = frame.elements[rects.length];
				var m = element.matrix;
				m.tx = 0;
				m.ty = 0;
				element.matrix = m;
				var rect = {};
				rect.name = item.name + "_" + frameJ.name;
				rect.firstFrame = j;
				rects.push(rect);
				element.symbolType = "graphic";
			}
		}
	}
}
for (var i in rects) {
	var rect = rects[i];
	var element = frame.elements[i];
	element.firstFrame = rect.firstFrame;
	doc.selectNone();
	doc.selection = [element];
	var selection = doc.getSelectionRect();
	rect.index = i;
	rect.w = Math.round(selection.right - selection.left + tolerance * 2);
	rect.h = Math.round(selection.bottom - selection.top + tolerance * 2);
	rect.offsetX = Math.round(selection.left) - tolerance;
	rect.offsetY= Math.round(selection.top) - tolerance;
}
rects.sort(function(a, b) {
	return a.h - b.h;
});
var rects0 = [];
var rects1 = [];
var i1 = Math.round(rects.length * .8);
for (var i = 0; i < rects.length; i++) {
	var rect = rects[i];
	if (i < i1) {
		rects0.push(rect);
	} else {
		rects1.push(rect);
	}
}
rects0.sort(function(a, b) {
	var delta = Math.round((a.h - b.h) / 10);
	if (delta != 0)
		return delta;
	return a.w - b.w;
});
rects1.sort(function(a, b) {
	var delta = Math.round((a.w - b.w) / 10);
	if (delta != 0)
		return delta;
	return a.h - b.h;
});
var offsetX = 0;
var offsetY = 0;
var lineH = 0;
var lineW = 0;
for (var i in rects0) {
	var rect = rects0[i];
	var element = frame.elements[rect.index];
	if (offsetX + element.width > doc.width - 2) {
		offsetX = 0;
		offsetY += lineH + 2;
		lineH = 0;
	}
	rect.x = Math.round(offsetX - rect.offsetX);
	rect.y = Math.round(offsetY - rect.offsetY);
	var m = element.matrix;
	m.tx = rect.x;
	m.ty = rect.y;
	element.matrix = m;
	offsetX += rect.w + 2;
	if (lineH < rect.h)
		lineH = rect.h;
}
var y0 = offsetY + lineH;
offsetX = 0;
offsetY = y0;
for (var i in rects1) {
	var rect = rects1[i];
	var element = frame.elements[rect.index];
	if (offsetY + element.height > doc.height - 2) {
		offsetY = y0;
		offsetX += lineW + 2;
		lineW = 0;
	}
	rect.x = Math.round(offsetX - rect.offsetX);
	rect.y = Math.round(offsetY - rect.offsetY);
	var m = element.matrix;
	m.tx = rect.x;
	m.ty = rect.y;
	element.matrix = m;
	offsetY += rect.h + 2;
	if (lineW < rect.w)
		lineW = rect.w;
}
doc.exportPNG(gsa + "assets/ui.png", true, true);

var text = "";
text += "// <<Generated>>\n"
text += "package ui;\n"
text += "\n";
text += "class UIRects {\n";
for (var i in rects) {
	var rect = rects[i];
	text += "\tpublic static var " + rect.name + ":UIRect = new UIRect(" +
		rect.x + ", " + rect.y + ", " + rect.w + ", " + rect.h + ", " + rect.offsetX + ", " + rect.offsetY +
		");\n";
}
text += "}";
FLfile.write(gsa + "src/ui/UIRects.hx", text);
