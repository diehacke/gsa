var gsa = "file:///Z|/home/user/gsa/";

fl.openDocument(gsa + "assets/stages.fla");
var doc = fl.getDocumentDOM();
doc.exportSWF(gsa + "assets/stages.swf", true);

var text = "";
text += "// <<Generated>>\n"
text += "package game;\n"
text += "\n";
text += "import flash.geom.Point;"
text += "\n";
text += "class Stages {";

var timeline = doc.timelines[doc.currentTimeline];
for (var ii = 0; ii < timeline.layerCount; ii++) {
	timeline.currentLayer = ii;
	timeline.currentFrame = 0;
	var layer = timeline.layers[timeline.currentLayer];
	var frame = layer.frames[timeline.currentFrame];
	for (var i in frame.elements) {
		var element = frame.elements[i];
		if (element.elementType == "instance" &&
			element.libraryItem &&
			element.libraryItem.itemType == "component") {

			var params = {};
			for (var p in element.parameters) {
				var param = element.parameters[p];
				params[param.name] = param.value;
			}

			var name = element.name.toLowerCase().substr(2);
			text += "\n";
			text += "	public static var " + name + ":StageInfo = {\n";
			text += "		var info = new StageInfo(Assets.new" + element.name + ");\n";
			text += "		info.nozzles = new Array<Point>();\n";
			if (params["nozzlesXs"].length > 0) {
				for (var i = 0; i < params["nozzlesXs"].length; i++) {
					text += "		info.nozzles.push(new Point(" +
						params["nozzlesXs"][i].value + ", " + params["nozzlesY"] + "));\n";
				}
			}
			text += "		info.nozzlesWidth = " + params["nozzlesWidth"] + ";\n";
			text += "		info.handle = new Point(" + params["handleX"] + ", " + params["handleY"] + ");\n";
			text += "		info.linkLeft = new Point(" + params["linkLeft"] + ", " + params["linkY"] + ");\n";
			text += "		info.linkRight = new Point(" + params["linkRight"] + ", " + params["linkY"] + ");\n";
			text += "		info.linkTop = new Point(" + 0 + ", " + params["linkTop"] + ");\n";
			text += "		info.linkBottom = new Point(" + 0 + ", " + params["linkBottom"] + ");\n";
			text += "		info.netM = " + params["netM"] + ";\n";
			text += "		info.fuelM = " + params["fuelM"] + ";\n";
			text += "		info.fuelRate = " + params["fuelRate"] + ";\n";
			text += "		info.fuelV = " + params["fuelV"] + ";\n";
			text += "		info.sizeX = " + params["sizeX"] + ";\n";
			text += "		info.sizeY = " + params["sizeY"] + ";\n";
			text += "		info;\n";
			text += "	};\n";
		}
	}
}
text += "}";
FLfile.write(gsa + "src/game/Stages.hx", text);
